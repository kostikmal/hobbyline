<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
$arReturn = [
    'cntBasketItems' => 0,
    'sum' => 0
];
if (CModule::IncludeModule("sale")) {
    $cntBasketItems = CSaleBasket::GetList(
        array(),
        array(
            "FUSER_ID" => CSaleBasket::GetBasketUserID(),
            "LID" => SITE_ID,
            "ORDER_ID" => "NULL"
        ),
        array()
    );
    $arReturn['cntBasketItems'] = $cntBasketItems;

    $dbBasketItems = CSaleBasket::GetList(
        ["ID" => "ASC"],
        [
            "FUSER_ID" => CSaleBasket::GetBasketUserID(),
            "LID" => SITE_ID,
            "ORDER_ID" => "NULL"
        ],
        false,
        false,
       ["ID", "QUANTITY", "PRICE"]
    );
    while ($arItems = $dbBasketItems->Fetch()) {
        $arReturn['sum'] += $arItems['QUANTITY'] * intval($arItems['PRICE']);
    }
    $arReturn['sum'] = number_format( $arReturn['sum'], 0, ',', ' ' );
}
else {
    $arReturn = false;
}

echo json_encode($arReturn);

?>
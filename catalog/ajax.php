<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if ( CModule::IncludeModule("catalog") ) {
    
    global $USER;

    $arUserPriceGroups = [1, 4];

    if ( $USER->IsAuthorized() ) {
        if ( in_array(WHOSALE_USER_GROUP, $USER->GetUserGroupArray()) ) {
            $arUserPriceGroups = [1,3,4];
        }
    }

    function getProductPriceForUser( $arUserPriceGroups, $nProductID) {
        $nReturn = 0;
        $allProductPrices = \Bitrix\Catalog\PriceTable::getList([
            "select" => ["*"],
            "filter" => [
                "=PRODUCT_ID" => $nProductID,
                //"=CATALOG_GROUP_ID" => 1,
            ],
            "order" => ["CATALOG_GROUP_ID" => "ASC"]
        ])->fetchAll();

        foreach ( $allProductPrices as $arProductPrice ) {
            if ( in_array($arProductPrice['CATALOG_GROUP_ID'], $arUserPriceGroups) ) {
                $nReturn = $arProductPrice['ID'];
            }
        }

        return $nReturn;
    }

    $fl = false;
    if ( (!empty($_POST["OFFERS"]) || !empty($_POST["PRODUCT"])) && $_POST["action"] == "ADD2BASKET" ) {
        /*foreach($_POST["OFFERS"] as $id => $arProp){
            foreach($arProp as $size => $qnt) {
                $id = intval($id);
                $qnt = intval($qnt);
                $arProductParams = array();
                if ($qnt > 0) {
                    $arProductParams[] = array("NAME" => $_POST["SKU_PROP_NAME"], "CODE" => $_POST["SKU_PROP_CODE"], "VALUE" => $size);
                    if (isset($_POST["ARTNUMBER"]) && !empty($_POST["ARTNUMBER"])) {
                        $arProductParams[] = array("NAME" => "Артикул", "CODE" => "ARTNUMBER", "VALUE" => $_POST["ARTNUMBER"]);
                    }
                    if (isset($_POST["COLOR"]) && !empty($_POST["COLOR"])) {
                        $arProductParams[] = array("NAME" => "Цвет", "CODE" => "COLOR", "VALUE" => $_POST["COLOR"]);
                    }
                    if (isset($_POST["COLLECTION"]) && !empty($_POST["COLLECTION"])) {
                        $arProductParams[] = array("NAME" => "Коллекция", "CODE" => "COLLECTION", "VALUE" => $_POST["COLLECTION"]);
                    }
                    if (isset($_POST["SOSTAV"]) && !empty($_POST["SOSTAV"])) {
                        $arProductParams[] = array("NAME" => "Состав", "CODE" => "sostav", "VALUE" => $_POST["SOSTAV"]);
                    }
                    Add2BasketByProductID(
                        $id,
                        $qnt,
                        array(),
                        $arProductParams
                    );
                    if (!$fl) {
                        $fl = true;
                    }
                }
            }
        }*/

        foreach($_POST["OFFERS"] as $id => $arProp){

            $nProductPiceID = getProductPriceForUser($arUserPriceGroups, intval($id));

            //file_put_contents( $_SERVER['DOCUMENT_ROOT'].'/xxx_ajax_basket.txt', print_r($nProductPiceID, true) );

            foreach($arProp as $sColor => $arSizes) {
                foreach ( $arSizes as $sSize=>$qnt ) {
                    $id = intval($id);
                    $qnt = intval($qnt);
                    $arProductParams = array();
                    if ($qnt > 0) {
                        if ( $sColor != 'none' ) {
                            $arProductParams[] = array("NAME" => "Цвет", "CODE" => "COLOR", "VALUE" => $sColor);
                        }
                        if ( $sSize != 'none' ) {
                            $arProductParams[] = array("NAME" => "Размер", "CODE" => "SIZE", "VALUE" => $sSize);
                        }
                        if (isset($_POST["ARTNUMBER"]) && !empty($_POST["ARTNUMBER"])) {
                            $arProductParams[] = array("NAME" => "Артикул", "CODE" => "ARTNUMBER", "VALUE" => $_POST["ARTNUMBER"]);
                        }
                        /*if (isset($_POST["COLOR"]) && !empty($_POST["COLOR"])) {
                            $arProductParams[] = array("NAME" => "Цвет", "CODE" => "COLOR", "VALUE" => $_POST["COLOR"]);
                        }
                        if (isset($_POST["COLLECTION"]) && !empty($_POST["COLLECTION"])) {
                            $arProductParams[] = array("NAME" => "Коллекция", "CODE" => "COLLECTION", "VALUE" => $_POST["COLLECTION"]);
                        }
                        if (isset($_POST["SOSTAV"]) && !empty($_POST["SOSTAV"])) {
                            $arProductParams[] = array("NAME" => "Состав", "CODE" => "sostav", "VALUE" => $_POST["SOSTAV"]);
                        }*/
                        Add2Basket(
                            $nProductPiceID, // $id,
                            $qnt,
                            array(),
                            $arProductParams
                        );
                        if (!$fl) {
                            $fl = true;
                        }
                    }
                }
            }
        }

        foreach($_POST["PRODUCT"] as $id => $qnt){
                $nProductPiceID = getProductPriceForUser($arUserPriceGroups, intval($id));
                $id = intval($id);
                $qnt = intval($qnt);
                $arProductParams = array();
                if ($qnt > 0) {
                    $arProductParams[] = array("NAME" => $_POST["SKU_PROP_NAME"], "CODE" => $_POST["SKU_PROP_CODE"], "VALUE" => $_POST["SKU_PROP_VALUE"]);
                    if (isset($_POST["ARTNUMBER"]) && !empty($_POST["ARTNUMBER"])) {
                        $arProductParams[] = array("NAME" => "Артикул", "CODE" => "ARTNUMBER", "VALUE" => $_POST["ARTNUMBER"]);
                    }
                    if (isset($_POST["COLOR"]) && !empty($_POST["COLOR"])) {
                        $arProductParams[] = array("NAME" => "Цвет", "CODE" => "COLOR", "VALUE" => $_POST["COLOR"]);
                    }
                    if (isset($_POST["COLLECTION"]) && !empty($_POST["COLLECTION"])) {
                        $arProductParams[] = array("NAME" => "Коллекция", "CODE" => "COLLECTION", "VALUE" => $_POST["COLLECTION"]);
                    }
                    if (isset($_POST["SOSTAV"]) && !empty($_POST["SOSTAV"])) {
                        $arProductParams[] = array("NAME" => "Состав", "CODE" => "sostav", "VALUE" => $_POST["SOSTAV"]);
                    }
                    Add2Basket(
                        $nProductPiceID, // $id,
                        $qnt,
                        array()
                        //$arProductParams
                    );
                    if (!$fl) {
                        $fl = true;
                    }
                }
        }

    }

    if ( ($_POST["action"] == "ADD2FAVORITE" || $_POST["action"] == "DEL2FAVORITE") && intval($_POST['PRODUCT']) > 0 && CModule::IncludeModule("iblock")) {

        $nUserID = $USER->GetID();

        $res = CIBlockElement::GetList(
            ["SORT"=>"ASC"],
            ['IBLOCK_ID'=>FAVORITE_PRODUCTS, 'ACTIVE'=>'Y', 'PROPERTY_USER'=>$nUserID],
            false,
            false,
            ['ID', 'PROPERTY_PRODUCTS']
        );

        if ( $_POST["action"] == "ADD2FAVORITE" ) {
            if ( $arRes = $res->GetNext() ) {
                $arRes['PROPERTY_PRODUCTS_VALUE'][] = $_POST['PRODUCT'];
                CIBlockElement::SetPropertyValuesEx( $arRes['ID'], FAVORITE_PRODUCTS, ['PRODUCTS'=>$arRes['PROPERTY_PRODUCTS_VALUE']] );
            }
            else {
                $el = new CIBlockElement;

                $PROP = [
                    'USER' => $nUserID,
                    'PRODUCTS' => [
                        $_POST['PRODUCT'],
                    ],
                ];

                $arLoadProductArray = Array(
                    "MODIFIED_BY"    => $nUserID,
                    "IBLOCK_ID"      => FAVORITE_PRODUCTS,
                    "PROPERTY_VALUES"=> $PROP,
                    "NAME"           => $USER->GetFullName().' - '.$nUserID,
                    "ACTIVE"         => "Y",
                );

                if ( $PRODUCT_ID = $el->Add($arLoadProductArray) ) {
                    $fl = true;
                }
                else {
                    $fl = false;
                    //echo "Error: ".$el->LAST_ERROR;
                }
            }
        }
        elseif ( $_POST["action"] == "DEL2FAVORITE" ) {
            if ( $arRes = $res->GetNext() ) {
                $key = array_search( $_POST['PRODUCT'], $arRes['PROPERTY_PRODUCTS_VALUE'] );
                if ( $key !== false ) {
                    unset( $arRes['PROPERTY_PRODUCTS_VALUE'][$key] );

                    if ( count($arRes['PROPERTY_PRODUCTS_VALUE']) == 0 ) {
                        $arRes['PROPERTY_PRODUCTS_VALUE'] = false;
                    }

                    $fl = true;
                }
                CIBlockElement::SetPropertyValuesEx( $arRes['ID'], FAVORITE_PRODUCTS, ['PRODUCTS'=>$arRes['PROPERTY_PRODUCTS_VALUE']] );
            }
        }
    }
    echo $fl;
}
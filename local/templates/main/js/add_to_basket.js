function updateTopBasket() {

    BX.ajax({
        url: "/catalog/ajax_top_basket.php",
        //data: "action=ADD2BASKET&",
        method: "GET",
        dataType: 'json',
        processData: false,
        start: true,
        onsuccess: function (fl) {
            if(fl){
                var objReturn = jQuery.parseJSON(fl);

                $('.basket-count__counter').html( objReturn.cntBasketItems );
                $('.icon-nav__sum_sum').html( objReturn.sum );

                //console.log( 'cntBasketItems = ' + objReturn.cntBasketItems );
                //console.log( 'sum = ' + objReturn.sum );
            }
        }
    });

}

$(document).ready(function () {
    updateTopBasket();
});

$(document).on('click', 'a.to_add_product_btn', function (e) {
    //var id = $(this).attr('id');
    //var added_txt = $(this).attr('data-added-text');
    e.preventDefault();

    var formData = $("#product-order-form").serialize();
    //console.log(formData);
    BX.ajax({
        url: "/catalog/ajax.php",
        data: "action=ADD2BASKET&" + formData,
        method: "POST",
        dataType: 'json',
        processData: false,
        start: true,
        onsuccess: function (fl) {
            if(fl){
                updateTopBasket();
                //CheckInBasktet();
                //bx_basketFKauiI.refreshCart({});
                //ym(YMNUMBER, 'reachGoal', 'inBasketClick');
                //$("#"+id).addClass('in-basket').text(added_txt);

            }
        }
    });

});

$(document).on('click', '.send-product-order', function (e) {
        //$('.send-product-order').click(function (e) {
        //alert("hi");
        //var btId = $(this).attr('id');
        var id = $(this).attr('data-id');
        //var compId = $(this).attr('data-comp-id');
        var added_txt = $(this).attr('data-added-text');
        e.preventDefault();
        if($("#product-order-form-"+id).length>0) {
            var formData = $("#product-order-form-"+id).serialize();
            //console.log(id);
            //console.log(formData);
            BX.ajax({
                url: "/catalog/ajax.php",
                data: "action=ADD2BASKET&" + formData,
                method: "POST",
                dataType: 'json',
                processData: false,
                start: true,
                onsuccess: function (fl) {
                    if(fl){
                        updateTopBasket();
                        //CheckInBasktet();
                        //bx_basketFKauiI.refreshCart({});
                        //$("#"+btId).addClass('in-basket').text(added_txt);

                    }
                }
            });
        }
});

$(document).on('click', '.lk-date__basket a', function (e) {

    var id = $(this).attr('data-product');
    var order_id = $(this).attr('data-order');
    e.preventDefault();
    if($("#order_form_"+order_id+'_'+id).length>0) {
        var formData = $("#order_form_"+order_id+'_'+id).serialize();
        BX.ajax({
            url: "/catalog/ajax.php",
            data: "action=ADD2BASKET&" + formData,
            method: "POST",
            dataType: 'json',
            processData: false,
            start: true,
            onsuccess: function (fl) {
                if(fl){
                    updateTopBasket();
                }
            }
        });
    }
});

/*$(document).on('click', '.hl-circle-btn.btn-click', function () {
    if ( !$(this).hasClass('btn-click-active') )  {
        var product_id = $(this).attr('data-product');

        BX.ajax({
            url: "/catalog/ajax.php",
            data: "action=ADD2FAVORITE&PRODUCT=" + product_id,
            method: "POST",
            dataType: 'json',
            processData: false,
            start: true,
            onsuccess: function (fl) {
                if(fl){
                    console.log('Товар добавлен в избранное');
                }
            }
        });
    }
});*/
$(document).ready(function () {
  
    window.addEventListener('scroll', function () {
        //Появление и скрытие кнопки "На верх"
        if (pageYOffset >= 500) {
            toTop.style.cssText = 'opacity: 1';
        } else {
            toTop.style.cssText = 'opacity: 0';
        }

        //Появление и скрытие фиксированного хедера
        if (document.documentElement.clientWidth >= 1024) {
            if (pageYOffset >= 200) {
                fixedHeader.style.cssText = 'display: block';
            } else {
                fixedHeader.style.cssText = 'display: none';
            }
        }
    });


    //Наложение маски на инпут для подписки в футере
    if ( document.getElementById('email') !== null ) {
        let dynamicMask = IMask(
            document.getElementById('email'), {
                mask: [{
                    mask: '+{7}(000)000-00-00'
                },
                    {
                        mask: /^\S*@?\S*$/
                    }
                ]
            }
        );
    }


// Скрываем форму "Благодарим за подписку" через 2сек.
    /*btnEmail.onclick = function () {
        setTimeout(function () {
            UIkit.modal(thxEmail).hide()
        }, 2000);
    }*/

//Слайдер НАША ИСТОРИЯ
    let sliderBtn = document.querySelectorAll('.btn-slider__btn-item');
    let sliderContent = document.querySelectorAll('.btn-slider__content-item');

    for (var i = 0; i < sliderBtn.length; i++) {
        sliderBtn[i].setAttribute('id', `b${i}`);
        sliderContent[i].setAttribute('id', `c${i}`);
    }

    $('.btn-slider__btn-item').click(function (e) {
        let id = e.target.id.slice(1);
        $('.btn-slider__content-item').removeClass('btn-slider__content-active');
        $('.btn-slider__btn-item').removeClass('btn-slider__btn-active');
        $(`#c${id}`).addClass('btn-slider__content-active');
        $(`#b${id}`).addClass('btn-slider__btn-active');
    });

//Слайдер НАША ИСТОРИЯ -- мобильные кнопки перелистывания
    $('.btn-slider__mobile-btn').click(function (e) {
        let showingContentId = Number($('.btn-slider__content-active').attr('id').slice(1));
        if (e.target.className.indexOf('btn-slider__btn-next') > 0) {
            if (showingContentId >= sliderBtn.length - 1) {
                showingContentId = 0;
            } else {
                showingContentId++;
            }
        } else if (e.target.className.indexOf('btn-slider__btn-prev')) {
            if (showingContentId <= 0) {
                showingContentId = sliderBtn.length - 1;
            } else {
                showingContentId--;
            }
        }

        $('.btn-slider__content-item').removeClass('btn-slider__content-active');
        $('.btn-slider__btn-item').removeClass('btn-slider__btn-active');

        $(`#c${showingContentId}`).addClass('btn-slider__content-active');
        $(`#b${showingContentId}`).addClass('btn-slider__btn-active');
        console.log(showingContentId);

    });

//Галерея
    $(function () {
        // $(".slider-vertical__item").each(function(i) {
        //   $("<div><h3>" + ++i + "</h3></div>").appendTo(".slider-vertical-nav");
        // });
        $(".slider-vertical-wrap").slick({
            slidesToShow: 1,
            asNavFor: ".slider-vertical-nav",
            dots: false,
            arrows: false,
            vertical: true,
            verticalSwiping: true
        });
        $(".slider-vertical-nav").slick({
            vertical: true,
            verticalSwiping: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            asNavFor: ".slider-vertical-wrap",
            nextArrow: ".slider-vertical__next",
            prevArrow: ".slider-vertical__prev",
            focusOnSelect: true
        });
    });

//Галерея в каталоге
// $(function() {
//   $(".slider-vertical-wrap-cat").slick({
//     slidesToShow: 1,
//     asNavFor: ".slider-vertical-nav-cat",
//     dots: false,
//     arrows: false,
//     vertical: true,
//     verticalSwiping: true
//   });
//   $(".slider-vertical-nav-cat").slick({
//     vertical: true,
//     verticalSwiping: true,
//     slidesToShow: 3,
//     slidesToScroll: 1,
//     asNavFor: ".slider-vertical-wrap-cat",
//     nextArrow: ".slider-vertical__next-cat",
//     prevArrow: ".slider-vertical__prev-cat",
//     focusOnSelect: true
//   });
// });

//Card slider
    $(function () {
        $(".slider-vertical-wrap-card").slick({
            slidesToShow: 1,
            asNavFor: ".slider-vertical-nav-card",
            dots: false,
            arrows: false,
            vertical: false,//true,
            verticalSwiping: false,//true
        });
        $(".slider-vertical-nav-card").slick({
            vertical: true,
            verticalSwiping: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            asNavFor: ".slider-vertical-wrap-card",
            nextArrow: ".slider-vertical__next-card",
            prevArrow: ".slider-vertical__prev-card",
            focusOnSelect: true
        });
    });
//end Card slider

    let flag = false;
    $('.hb-burger').click(function () {
        flag = !flag;
        if (flag) {
            $('.hb-burger img').attr('src', 'img/ico/burger-dis.svg');
            $('.hb-burger source').attr('srcset', 'img/ico/burger-dis.svg');
        } else {
            $('.hb-burger img').attr('src', 'img/ico/burger.svg');
            $('.hb-burger source').attr('srcset', 'img/ico/burger.svg');
        }
    });

//-------------------
    $('img.img-svg').each(function () {
        var $img = $(this);
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');
        $.get(imgURL, function (data) {
            var $svg = $(data).find('svg');
            if (typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass + ' replaced-svg');
            }
            $svg = $svg.removeAttr('xmlns:a');
            if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
                $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
            }
            $img.replaceWith($svg);
        }, 'xml');
    });

    $('.select__item').change(function () {
        let curentCity = $('.select__item').val();
        $('.selected-city').removeClass('selected-active');
        $(`#${curentCity}`).addClass('selected-active');
    });

//Форма входа или регистрации, Инпут пароля с глазом
    $('body').on('click', '.auth-form__password-control', function () {
        if ($('#password-input').attr('type') == 'password') {
            $('#password-input').attr('type', 'text');
        } else {
            $('#password-input').attr('type', 'password');
        }
        return false;
    });
//end

//Переключатель в регистрации между опт и розничным
    $('.reg__top-item').click(function () {
        $('.reg__top-item').removeClass('reg__top-active');
        $(this).addClass('reg__top-active');

        if (this.id === 'label-1') {
            $('.reg__pad').hide();
            $('#cont-1').show();
        } else if (this.id === 'label-2') {
            $('.reg__pad').hide();
            $('#cont-2').show();
        }
    });
//end

//Шаги в регистрации

    $('.reg__next').click(function (e) {
        let id = $('.reg__steps-active').prop('id');


        if (id === 'step1') {
            $('#step1 .auth-form__text input').filter(function () {
                if ($(this).hasClass('hl-require')) {
                    return $(this).parent().toggleClass('empty', !this.value)
                }
            });

            if ($('#step1 .auth-form__text').hasClass('empty')) {
                UIkit.notification({
                    message: "<span uk-icon='icon: warning'></span> Заполните пустые поля",
                    status: 'danger',
                    pos: 'center',
                    timeout: 2000
                });
                return
            }
            $('#cont-1 .reg__steps-active').removeClass('reg__steps-active');
            $('#step2').addClass('reg__steps-active');
        } else if (id === 'step2') {
            $('#step2 .auth-form__text input').filter(function () {
                if ($(this).hasClass('hl-require')) {
                    return $(this).parent().toggleClass('empty', !this.value)
                }
            });

            if ($('#step2 .auth-form__text').hasClass('empty')) {
                UIkit.notification({
                    message: "<span uk-icon='icon: warning'></span> Заполните пустые поля",
                    status: 'danger',
                    pos: 'center',
                    timeout: 2000
                });
                return
            }
            $('#cont-1 .reg__steps-active').removeClass('reg__steps-active');
            $('#step3').addClass('reg__steps-active')
        }
    });

    $('#reg-opt').click(function () {
        $('#step3 .auth-form__text input').filter(function () {
            if ($(this).hasClass('hl-require')) {
                return $(this).parent().toggleClass('empty', !this.value)
            }
        });

        if ($('#step3 .auth-form__text').hasClass('empty')) {
            UIkit.notification({
                message: "<span uk-icon='icon: warning'></span> Заполните пустые поля",
                status: 'danger',
                pos: 'center',
                timeout: 2000
            });
            return
        }
    });

    $('#reg-roz').click(function () {
        $('#roz-step2 .auth-form__text input').filter(function () {
            if ($(this).hasClass('hl-require')) {
                return $(this).parent().toggleClass('empty', !this.value)
            }
        });

        if ($('#roz-step1 .auth-form__text').hasClass('empty')) {
            UIkit.notification({
                message: "<span uk-icon='icon: warning'></span> Заполните пустые поля",
                status: 'danger',
                pos: 'center',
                timeout: 2000
            });
            return
        }
    });

    $('.reg__next-roz').click(function (e) {

        $('#roz-step1 .auth-form__text input').filter(function () {
            if ($(this).hasClass('hl-require')) {
                return $(this).parent().toggleClass('empty', !this.value)
            }
        });

        if ($('#roz-step1 .auth-form__text').hasClass('empty')) {
            UIkit.notification({
                message: "<span uk-icon='icon: warning'></span> Заполните пустые поля",
                status: 'danger',
                pos: 'center',
                timeout: 2000
            });
            return
        }

        $('#cont-2 .reg__steps-active').removeClass('reg__steps-active');
        $('#roz-step2').addClass('reg__steps-active');
    });

//Конец шаги регистрации

//Загрузка лого в лк
    $('.lk-sidebar__logo').click(function () {
        $('#logo-load').click();
    });


    $('.lk-profile__input').focus(function (e) {
        $(this).closest('.lk-profile__val').css('background', 'url("../img/ico/red-data-focus.svg")left center no-repeat');
    });

    $('.lk-profile__input').blur(function (e) {

        $(this).closest('.lk-profile__val').css('background', 'url("../img/ico/red-data.svg")left center no-repeat');
    });

//История заказов свитчер
    let switcher = document.querySelectorAll('.lk-switcher__item');
    let swContent = document.querySelectorAll('.lk-sw-content__item');
    for (var i = 0; i < switcher.length; i++) {
        $(switcher[i]).attr('id', `sw${i}`);
        $(swContent[i]).attr('id', `cw${i}`);
    }

    $('.lk-switcher__item').click(function () {
        let id = this.id.slice(2);
        $('.lk-sw-content__item').removeClass('lk-sw-content__active');
        $('.lk-switcher__item').removeClass('lk-switcher__item-active');
        $(`#cw${id}`).addClass('lk-sw-content__active');
        $(`#sw${id}`).addClass('lk-switcher__item-active');
    });


//Цена ползунок
    $(".polzunok-5").slider({
        min: 99,
        max: 9099,
        values: [99, 5000],
        range: true,
        animate: "fast",
        slide: function (event, ui) {
            $(".polzunok-input-5-left").val(ui.values[0]);
            $(".polzunok-input-5-right").val(ui.values[1]);
        }
    });
    $(".polzunok-input-5-left").val($(".polzunok-5").slider("values", 0));
    $(".polzunok-input-5-right").val($(".polzunok-5").slider("values", 1));
    $(document).focusout(function () {
        if ( $(".polzunok-input-5-left").length ) {
            var input_left = $(".polzunok-input-5-left").val().replace(/[^0-9]/g, ''),
                opt_left = $(".polzunok-5").slider("option", "min"),
                where_right = $(".polzunok-5").slider("values", 1),
                input_right = $(".polzunok-input-5-right").val().replace(/[^0-9]/g, ''),
                opt_right = $(".polzunok-5").slider("option", "max"),
                where_left = $(".polzunok-5").slider("values", 0);
            if (input_left > where_right) {
                input_left = where_right;
            }
            if (input_left < opt_left) {
                input_left = opt_left;
            }
            if (input_left == "") {
                input_left = 0;
            }
            if (input_right < where_left) {
                input_right = where_left;
            }
            if (input_right > opt_right) {
                input_right = opt_right;
            }
            if (input_right == "") {
                input_right = 0;
            }
            $(".polzunok-input-5-left").val(input_left);
            $(".polzunok-input-5-right").val(input_right);
            $(".polzunok-5").slider("values", [input_left, input_right]);
        }
    });

//Цена ползунок конец
    $('.hl-items-count__link').click(function () {
        $('.hl-items-count__link').removeClass('hl-items-count__link-active');
        $(this).addClass('hl-items-count__link-active');
    });


    $('.btn-click').click(function () {
        if ($(this).hasClass('btn-click-active')) {
            $(this).removeClass('btn-click-active');
            var product_id = $(this).attr('data-product');

            BX.ajax({
                url: "/catalog/ajax.php",
                data: "action=DEL2FAVORITE&PRODUCT=" + product_id,
                method: "POST",
                dataType: 'json',
                processData: false,
                start: true,
                onsuccess: function (fl) {
                    if(fl){
                        console.log('Товар удален из избранного');
                    }
                }
            });
        }
        else {
            $(this).addClass('btn-click-active');
            var product_id = $(this).attr('data-product');

            BX.ajax({
                url: "/catalog/ajax.php",
                data: "action=ADD2FAVORITE&PRODUCT=" + product_id,
                method: "POST",
                dataType: 'json',
                processData: false,
                start: true,
                onsuccess: function (fl) {
                    if(fl){
                        console.log('Товар добавлен в избранное');
                    }
                }
            });
        }

    });

    $('.izbr-drop__link').click(function (e) {
        e.preventDefault();
        var product_id = $(this).attr('data-product');

        if ( parseInt(product_id) > 0 ) {
            BX.ajax({
                url: "/catalog/ajax.php",
                data: "action=DEL2FAVORITE&PRODUCT=" + product_id,
                method: "POST",
                dataType: 'json',
                processData: false,
                start: true,
                onsuccess: function (fl) {
                    if(fl){
                        location.reload();
                    }
                }
            });
        }
    });

    /*Инициализация рейтинга со звездами*/
    let initRating = 4;
    $('.rating-count').text(initRating);
    $(".my-rating-6").starRating({
        totalStars: 5,
        emptyColor: '#CCCCCC',
        hoverColor: '#E3158E',
        activeColor: '#E3158E',
        readOnly: true,
        ratedColor: '#E3158E',
        starShape: 'rounded',
        initialRating: initRating,
        starSize: 13,
        strokeWidth: 0,
        useGradient: false,
        minRating: 1,
        callback: function (currentRating, $el) {
            $('.rating-count').text(currentRating);
        }
    });

    $('.rating-count').text(initRating);
    $(".my-rating-7").starRating({
        totalStars: 5,
        emptyColor: '#CCCCCC',
        hoverColor: '#E3158E',
        activeColor: '#E3158E',
        ratedColor: '#E3158E',
        starShape: 'rounded',
        initialRating: initRating,
        starSize: 20,
        strokeWidth: 0,
        useGradient: false,
        minRating: 1,
        callback: function (currentRating, $el) {
            $('.rating-count').text(currentRating);
        }
    });

    $('.rating-count').text(initRating);
    $(".my-rating-8").starRating({
        totalStars: 5,
        emptyColor: '#CCCCCC',
        hoverColor: '#F1CE3E',
        activeColor: '#F1CE3E',
        ratedColor: '#F1CE3E',
        starShape: 'rounded',
        readOnly: true,
        initialRating: initRating,
        starSize: 18,
        strokeWidth: 0,
        useGradient: false,
        minRating: 1,
        callback: function (currentRating, $el) {
            $('.rating-count').text(currentRating);
        }
    });
    /*конец звездочкам*/

    $('.step__plus').click(function () {
        var max_val = $(this).siblings('.step__input').attr('max');
        let inputVal = $(this).siblings('.step__input').val();
        if ($(this).hasClass('half-step')) {
            inputVal = Number(inputVal) + 0.5;
            $(this).siblings('.step__input').val(inputVal);
            return;
        }
        //inputVal++;

        if ( parseInt(inputVal) < parseInt(max_val) ) {
            inputVal++;
        }
        else {
            inputVal = max_val;
        }
        $(this).siblings('.step__input').val(inputVal);
    });

    $('.step__minus').click(function () {
        var min_val = $(this).siblings('.step__input').attr('min');
        let inputVal = $(this).siblings('.step__input').val();

        if ($(this).hasClass('half-step') && inputVal != 0) {
            inputVal = Number(inputVal) - 0.5;
            $(this).siblings('.step__input').val(inputVal);
            return;
        }

        if (inputVal > 0) {
            inputVal--;
            $(this).siblings('.step__input').val(inputVal);
        }
        else if ( parseInt(inputVal) < parseInt(min_val) ) {
            $(this).siblings('.step__input').val(min_val);
        }
    });

    $('.step__input').change(function () {
        var max_val = $(this).attr('max');
        var min_val = $(this).attr('min');
        var inputVal = $(this).val();

        if ( parseInt(inputVal) > parseInt(max_val) ) {
            inputVal = max_val;
            $(this).val(inputVal);
        }

        if ( parseInt(inputVal) < parseInt(min_val) ) {
            inputVal = min_val;
            $(this).val(inputVal);
        }
    });

//Отзывы Лайк Дизлайк

    $('.all-rev__like').click(function () {
        $(this).addClass('all-rev__like-active');
        $(this).siblings().removeClass('all-rev__like-active');
    });

//======

//Кнопка фильтра
    $('.hl-filter-btn-mob').click(function () {
        $('.hl-filter').toggle(200);
    });
//======

//Оформление заказа переключатель между вкладками

    $('.order-nav__select').click(function () {
        let curentId = $(this).attr('id');
        let numberId = curentId.slice(11);

        $('.order-nav__select').removeClass('order-nav__select-active');
        $('.order-content').removeClass('order-content__active');
        $(`#${curentId}`).addClass('order-nav__select-active');
        $(`#orderContainer${numberId}`).addClass('order-content__active');

    })

//======

// Клик по компаниями, установка галочки на выбранную компанию

    $('.order__companyes-item').click(function () {
        $('.order__companyes-item').removeClass('order__companyes-checked');
        $(this).addClass('order__companyes-checked');
    });

//======
});

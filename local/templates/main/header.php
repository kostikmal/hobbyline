<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
//IncludeTemplateLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/".SITE_TEMPLATE_ID."/header.php");
$APPLICATION->SetTitle("");
CJSCore::Init(array("fx"));

//\Bitrix\Main\UI\Extension::load("ui.bootstrap4");

use Bitrix\Main\Page\Asset;
$aCurPageURL = $APPLICATION->GetCurPage(false);

?><!DOCTYPE html>
<html xml:lang="<?=LANGUAGE_ID?>" lang="<?=LANGUAGE_ID?>">
<head>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(88989644, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/88989644" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <?
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/uikit/swiper/swiper-bundle.min.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/uikit/css/uikit.min.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/style.min.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/additional.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/search.css");

    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/uikit/swiper/swiper-bundle.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/uikit/js/uikit.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/uikit/js/uikit-icons.min.js");

    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/anime.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/slick.min.js");
    //Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/script.min.js");
    if ( strpos($aCurPageURL, '/vacancies/') !== false ) {
        //Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/select.min.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/select.js");
    }
    elseif ( strpos($aCurPageURL, '/catalog/products/') !== false ) {
        //Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/select.min.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/select_catalog.js");
    }

    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery-ui.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/touch-punch.min.js");
    //Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/stars-rating.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/stars-rating.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/flow.min.js");

    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/script.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/add_to_basket.js");

    Asset::getInstance()->addJs("https://unpkg.com/imask");
    ?>

    <title><?$APPLICATION->ShowTitle();?></title>
    <? $APPLICATION->ShowHead(); ?>
</head>
<body>

<div id="panel"><? $APPLICATION->ShowPanel(); ?></div>

<div class="preloader">
    <div class="loader"></div>
</div>

<header class="header main-content" id="top">

    <div class="header-top">

        <button class="hb-burger" type="button" uk-toggle="target: #offcanvas-overlay">
            <picture>
                <source srcset="<?= SITE_TEMPLATE_PATH ?>/img/ico/burger.svg" type="image/webp">
                <img src="<?= SITE_TEMPLATE_PATH ?>/img/ico/burger.svg" alt="">
            </picture>
        </button>

        <a href="/" class="logo">
            <picture>
                <source srcset="<?= SITE_TEMPLATE_PATH ?>/img/ico/logo.svg" type="image/webp">
                <img src="<?= SITE_TEMPLATE_PATH ?>/img/ico/logo.svg" alt="">
            </picture>
        </a>

        <?
        $APPLICATION->IncludeComponent(
            "bitrix:search.title",
            "header_search",
            array(
                "NUM_CATEGORIES" => "1",
                "TOP_COUNT" => "5",
                "CHECK_DATES" => "N",
                "SHOW_OTHERS" => "N",
                "PAGE" => SITE_DIR . "catalog/",
                "CATEGORY_0_TITLE" => GetMessage("SEARCH_GOODS"),
                "CATEGORY_0" => array(
                    0 => "iblock_catalog",
                ),
                "CATEGORY_0_iblock_catalog" => array(
                    0 => "all",
                ),
                "CATEGORY_OTHERS_TITLE" => GetMessage("SEARCH_OTHER"),
                "SHOW_INPUT" => "Y",
                "INPUT_ID" => "title-search-input",
                "CONTAINER_ID" => "search",
                "PRICE_CODE" => array(
                    0 => "BASE",
                ),
                "SHOW_PREVIEW" => "Y",
                "PREVIEW_WIDTH" => "75",
                "PREVIEW_HEIGHT" => "75",
                "CONVERT_CURRENCY" => "Y",
                "COMPONENT_TEMPLATE" => "HL_bootstrap_v5",
                "ORDER" => "date",
                "USE_LANGUAGE_GUESS" => "Y",
                "TEMPLATE_THEME" => "blue",
                "PRICE_VAT_INCLUDE" => "Y",
                "PREVIEW_TRUNCATE_LEN" => "",
                "CURRENCY_ID" => "RUB"
            ),
            false
        );
        ?>

        <div class="header-contacts">
            <?
            $APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "AREA_FILE_SHOW" => "file",
                    "PATH" => SITE_DIR . "include/header_phone_mail.php",
                    "AREA_FILE_RECURSIVE" => "N",
                    "EDIT_MODE" => "html",
                ),
                false,
                Array('HIDE_ICONS' => 'Y')
            );
            ?>
        </div>

        <div class="header__icon-nav icon-nav">
            <a href="#modal-call" class="icon-nav__item" uk-toggle>
                <picture>
                    <source srcset="<?=SITE_TEMPLATE_PATH?>/img/ico/call.svg" type="image/webp">
                    <img src="<?=SITE_TEMPLATE_PATH?>/img/ico/call.svg" alt="">
                </picture>
                <div uk-dropdown="pos: bottom-justify" class="icon-nav__dropdown uk-text-center dropdown">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => SITE_DIR . "include/header_feedback_title.php",
                            "AREA_FILE_RECURSIVE" => "N",
                            "EDIT_MODE" => "html",
                        ),
                        false,
                        Array('HIDE_ICONS' => 'Y')
                    ); ?>
                </div>
            </a>
            <a href="/personal/favorites/" class="icon-nav__item">
                <picture>
                    <source srcset="<?=SITE_TEMPLATE_PATH?>/img/ico/like.svg" type="image/webp">
                    <img src="<?=SITE_TEMPLATE_PATH?>/img/ico/like.svg" alt="">
                </picture>
                <div uk-dropdown="pos: bottom-justify" class="icon-nav__dropdown uk-text-center dropdown">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => SITE_DIR . "include/header_favorites_title.php",
                            "AREA_FILE_RECURSIVE" => "N",
                            "EDIT_MODE" => "html",
                        ),
                        false,
                        Array('HIDE_ICONS' => 'Y')
                    ); ?>
                </div>
            </a>
            <a href="/personal/" class="icon-nav__item">
                <picture>
                    <source srcset="<?=SITE_TEMPLATE_PATH?>/img/ico/profile.svg" type="image/webp">
                    <img src="<?=SITE_TEMPLATE_PATH?>/img/ico/profile.svg" alt="">
                </picture>
                <div uk-dropdown="pos: bottom-right" class="icon-nav__dropdown uk-text-center dropdown">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => SITE_DIR . "include/header_personal_area_title.php",
                            "AREA_FILE_RECURSIVE" => "N",
                            "EDIT_MODE" => "html",
                        ),
                        false,
                        Array('HIDE_ICONS' => 'Y')
                    ); ?>
                    <!-- <div class="icon-nav__dropdown-wrap">
                    <a href="#">Войти</a> или <a href="#">зарегистрироваться</a>
                  </div> -->
                </div>
            </a>
            <a href="/personal/cart/" class="icon-nav__item basket-count">
                <picture>
                    <source srcset="<?=SITE_TEMPLATE_PATH?>/img/ico/basket.svg" type="image/webp">
                    <img src="<?=SITE_TEMPLATE_PATH?>/img/ico/basket.svg" alt="">
                </picture>
                <div uk-dropdown="pos: bottom-justify" class="icon-nav__dropdown uk-text-center dropdown">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => SITE_DIR . "include/header_basket_title.php",
                            "AREA_FILE_RECURSIVE" => "N",
                            "EDIT_MODE" => "html",
                        ),
                        false,
                        Array('HIDE_ICONS' => 'Y')
                    ); ?>
                </div>
                <span class="basket-count__counter">
                    <?/*
                    if (!CModule::IncludeModule("sale")) return;
                    $cntBasketItems = CSaleBasket::GetList(
                        array(),
                        array(
                            "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                            "LID" => "s1",
                            "ORDER_ID" => "NULL"
                        ),
                        array()
                    );
                    echo $cntBasketItems;
                    */?> 0
                </span>
            </a>

            <span class="icon-nav__sum">
                <?
                /*if (!CModule::IncludeModule("sale")) return;

                if (CModule::IncludeModule("sale")) {

                    $arBasketItems = array();

                    $dbBasketItems = CSaleBasket::GetList(
                        array(
                            "NAME" => "ASC",
                            "ID" => "ASC"
                        ),
                        array(
                            "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                            "LID" => SITE_ID,
                            "ORDER_ID" => "NULL"
                        ),
                        false,
                        false,
                        array("ID", "QUANTITY", "PRICE")
                    );
                    while ($arItems = $dbBasketItems->Fetch()) {
                        if (strlen($arItems["CALLBACK_FUNC"]) > 0) {
                            CSaleBasket::UpdatePrice($arItems["ID"],
                                $arItems["QUANTITY"]);
                            $arItems = CSaleBasket::GetByID($arItems["ID"]);
                        }

                        $arBasketItems[] = $arItems;
                    }

                    $summ = 0;

                    for ($i = 0; $i <= $arResult["NUM_PRODUCTS"]; $i++) {

                        $summ = $summ + $arBasketItems[$i]["PRICE"] * $arBasketItems[$i]["QUANTITY"];

                    }

                }
                echo $summ;*/
                ?>
                <span class="icon-nav__sum_sum">0</span> руб.
            </span>

        </div>

        <a href="#" class="mob-search">
            <picture>
                <source srcset="<?=SITE_TEMPLATE_PATH?>/img/ico/search-mob.svg" type="image/webp">
                <img src="<?=SITE_TEMPLATE_PATH?>/img/ico/search-mob.svg" alt="">
            </picture>
        </a>

    </div>

    <div class="header-bottom">
        <?
        $APPLICATION->IncludeComponent(
            "bitrix:menu",
            "main_top_horizontal_multilevel",
            array(
                "ROOT_MENU_TYPE" => "top",
                "MENU_CACHE_TYPE" => "Y",
                "MENU_CACHE_TIME" => "86400",
                "MENU_CACHE_USE_GROUPS" => "N",
                "MENU_THEME" => "site",
                "CACHE_SELECTED_ITEMS" => "N",
                "MENU_CACHE_GET_VARS" => array(),
                "MAX_LEVEL" => "2",
                "CHILD_MENU_TYPE" => "bottom",
                "USE_EXT" => "Y",
                "DELAY" => "N",
                "ALLOW_MULTI_SELECT" => "N",
                "COMPONENT_TEMPLATE" => "main_top_horizontal_multilevel"
            ),
            false
        );
        ?>
    </div>

</header>

<!-- Sticky header -->
<div class="header__fixed" id="fixedHeader" uk-scrollspy="cls: uk-animation-slide-top; target: div; delay: 100; repeat: true">
    <div class="sticky">

        <a href="/">
            <picture>
                <source srcset="<?=SITE_TEMPLATE_PATH?>/img/ico/logo-fixed-header.svg" type="image/webp">
                <img src="<?=SITE_TEMPLATE_PATH?>/img/ico/logo-fixed-header.svg" alt="">
            </picture>
        </a>

        <?
        $APPLICATION->IncludeComponent(
            "bitrix:menu",
            "fixed_header_menu",
            array(
                "ROOT_MENU_TYPE" => "top",
                "MENU_CACHE_TYPE" => "Y",
                "MENU_CACHE_TIME" => "86400",
                "MENU_CACHE_USE_GROUPS" => "N",
                "MENU_THEME" => "site",
                "CACHE_SELECTED_ITEMS" => "N",
                "MENU_CACHE_GET_VARS" => array(),
                "MAX_LEVEL" => "2",
                "CHILD_MENU_TYPE" => "bottom",
                "USE_EXT" => "Y",
                "DELAY" => "N",
                "ALLOW_MULTI_SELECT" => "N",
                "COMPONENT_TEMPLATE" => "fixed_header_menu"
            ),
            false
        );
        ?>

        <div class="header__icon-nav icon-nav icon-nav__fixed">
            <a href="#modal-call" class="icon-nav__item" uk-toggle>
                <picture>
                    <source srcset="<?=SITE_TEMPLATE_PATH?>/img/ico/call.svg" type="image/webp">
                    <img src="<?=SITE_TEMPLATE_PATH?>/img/ico/call.svg" alt="">
                </picture>
                <div uk-dropdown="pos: bottom-justify" class="icon-nav__dropdown uk-text-center dropdown">
                    <?
                    $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => SITE_DIR . "include/header_feedback_title.php",
                            "AREA_FILE_RECURSIVE" => "N",
                            "EDIT_MODE" => "html",
                        ),
                        false,
                        Array('HIDE_ICONS' => 'Y')
                    );
                    ?>
                </div>
            </a>
            <a href="/personal/favorites/" class="icon-nav__item">
                <picture>
                    <source srcset="<?=SITE_TEMPLATE_PATH?>/img/ico/like.svg" type="image/webp">
                    <img src="<?=SITE_TEMPLATE_PATH?>/img/ico/like.svg" alt="">
                </picture>
                <div uk-dropdown="pos: bottom-justify" class="icon-nav__dropdown uk-text-center dropdown">
                    <?
                    $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => SITE_DIR . "include/header_favorites_title.php",
                            "AREA_FILE_RECURSIVE" => "N",
                            "EDIT_MODE" => "html",
                        ),
                        false,
                        Array('HIDE_ICONS' => 'Y')
                    );
                    ?>
                </div>
            </a>
            <a href="/personal/" class="icon-nav__item">
                <picture>
                    <source srcset="<?=SITE_TEMPLATE_PATH?>/img/ico/profile.svg" type="image/webp">
                    <img src="<?=SITE_TEMPLATE_PATH?>/img/ico/profile.svg" alt="">
                </picture>
                <div uk-dropdown="pos: bottom-right" class="icon-nav__dropdown uk-text-center dropdown">
                    <?
                    $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => SITE_DIR . "include/header_personal_area_title.php",
                            "AREA_FILE_RECURSIVE" => "N",
                            "EDIT_MODE" => "html",
                        ),
                        false,
                        Array('HIDE_ICONS' => 'Y')
                    );
                    ?>
                </div>
            </a>
            <a href="/personal/cart/" class="icon-nav__item basket-count">
                <picture>
                    <source srcset="<?=SITE_TEMPLATE_PATH?>/img/ico/basket.svg" type="image/webp">
                    <img src="<?=SITE_TEMPLATE_PATH?>/img/ico/basket.svg" alt="">
                </picture>
                <div uk-dropdown="pos: bottom-justify" class="icon-nav__dropdown uk-text-center dropdown">
                    <?
                    $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => SITE_DIR . "include/header_basket_title.php",
                            "AREA_FILE_RECURSIVE" => "N",
                            "EDIT_MODE" => "html",
                        ),
                        false,
                        Array('HIDE_ICONS' => 'Y')
                    );
                    ?>
                </div>
                <span class="basket-count__counter">
                    <?
                    //echo $cntBasketItems;
                    ?>0
                </span>
            </a>
            <span class="icon-nav__sum">
                <?
                //echo $summ;
                ?>
                <span class="icon-nav__sum_sum">0</span> руб.
            </span>
        </div>

    </div>
</div>
<!--end Sticky header -->

<!-- Bottom mob menu -->
<?/*<section class="mb-menu">
    <a href="#" class="mb-menu__item mb-menu__active">
        <picture>
            <source srcset="<?=SITE_TEMPLATE_PATH?>/img/ico/m-main.svg" type="image/webp">
            <img class="img-svg" src="<?=SITE_TEMPLATE_PATH?>/img/ico/m-main.svg" alt="">
        </picture>
        <span>
            Главная
        </span>
    </a>
    <a href="#" class="mb-menu__item">
        <picture>
            <source srcset="<?=SITE_TEMPLATE_PATH?>/img/ico/m-catalog.svg" type="image/webp">
            <img class="img-svg" src="<?=SITE_TEMPLATE_PATH?>/img/ico/m-catalog.svg" alt="">
        </picture>
        <span>
            Каталог
        </span>
    </a>
    <a href="basket/" class="mb-menu__item basket-count">
        <picture>
            <source srcset="<?=SITE_TEMPLATE_PATH?>/img/ico/basket.svg" type="image/webp">
            <img class="img-svg" src="<?=SITE_TEMPLATE_PATH?>/img/ico/basket.svg" alt="">
        </picture>
        <span>
            Корзина
        </span>
        <span class="basket-count__counter">
            0
        </span>
    </a>
    <a href="#" class="mb-menu__item">
        <picture>
            <source srcset="<?=SITE_TEMPLATE_PATH?>/img/ico/like.svg" type="image/webp">
            <img class="img-svg" src="<?=SITE_TEMPLATE_PATH?>/img/ico/like.svg" alt="">
        </picture>
        <span>
            Избранное
        </span>
    </a>
    <a href="#" class="mb-menu__item">
        <picture>
            <source srcset="<?=SITE_TEMPLATE_PATH?>/img/ico/profile.svg" type="image/webp">
            <img class="img-svg" src="<?=SITE_TEMPLATE_PATH?>/img/ico/profile.svg" alt="">
        </picture>
        <span>
            Войти
        </span>
    </a>
</section>*/?>

<?
$APPLICATION->IncludeComponent(
    "bitrix:menu",
    "top_second",
    array(
        "ROOT_MENU_TYPE" => "top_second",
        "MENU_CACHE_TYPE" => "Y",
        "MENU_CACHE_TIME" => "86400",
        "MENU_CACHE_USE_GROUPS" => "N",
        "MENU_THEME" => "site",
        "CACHE_SELECTED_ITEMS" => "N",
        "MENU_CACHE_GET_VARS" => array(),
        "MAX_LEVEL" => "1",
        "CHILD_MENU_TYPE" => "",
        "USE_EXT" => "Y",
        "DELAY" => "N",
        "ALLOW_MULTI_SELECT" => "N",
        "COMPONENT_TEMPLATE" => "top_second"
    ),
    false
);
?>

<!-- end Bottom mob menu -->

<!-- Offcanvas menu -->
<div id="offcanvas-overlay" uk-offcanvas="overlay: true">

    <div class="uk-offcanvas-bar uk-background-default left-menu">

        <?/*<nav class="nav-left">
            <ul class="uk-nav-default uk-nav-parent-icon" uk-nav="multiple: true">
                <li class="uk-parent nav-left__li-parent">
                    <a href="#">О компании</a>
                    <ul class="uk-nav-sub">
                        <li><a href="about-us.html">О нас</a></li>
                        <li><a href="production.html">Производство</a></li>
                        <li><a href="team.html">Команда</a></li>
                        <li><a href="vacancies.html">Вакансии</a></li>
                    </ul>
                </li>
                <li class="uk-parent nav-left__li-parent">
                    <a href="catalog.html">Каталог</a>
                    <ul class="uk-nav-sub">
                        <li><a href="#">Женские носки</a></li>
                        <li><a href="#">Мужские носки</a></li>
                        <li><a href="#">Детские носки</a></li>
                        <li><a href="#">Варежки и перчатки</a></li>
                    </ul>
                </li>
                <li class="nav-left__li-parent"><a href="lookbook.html">LookBook</a></li>
                <li class="uk-parent nav-left__li-parent">
                    <a href="#">Сотрудничество</a>
                    <ul class="uk-nav-sub">
                        <li><a href="distributor.html">Для дистрибьюторов</a></li>
                        <li><a href="opt.html">Для оптовых компаний</a></li>
                        <li><a href="retail.html">Для розницы</a></li>
                        <li><a href="corporate-clients.html">Для корпоративных клиентов</a></li>
                        <li><a href="franchising.html">Франчайзинг (в 2021)</a></li>
                    </ul>
                </li>
                <li class="nav-left__li-parent"><a href="magazine.html">Журнал</a></li>
                <li class="uk-parent nav-left__li-parent">
                    <a href="#">Сервис</a>
                    <ul class="uk-nav-sub">
                        <li><a href="delivery.html">Доставка и оплата</a></li>
                        <li><a href="return.html">Условия возврата</a></li>
                    </ul>
                </li>
                <li class="nav-left__li-parent"><a href="buy.html">Где купить</a></li>
                <li class="nav-left__li-parent"><a href="contacts.html">Контакты</a></li>
            </ul>
        </nav>*/?>

        <?
        $APPLICATION->IncludeComponent(
            "bitrix:menu",
            "offcanvas_overlay_menu",
            array(
                "ROOT_MENU_TYPE" => "top",
                "MENU_CACHE_TYPE" => "Y",
                "MENU_CACHE_TIME" => "86400",
                "MENU_CACHE_USE_GROUPS" => "N",
                "MENU_THEME" => "site",
                "CACHE_SELECTED_ITEMS" => "N",
                "MENU_CACHE_GET_VARS" => array(),
                "MAX_LEVEL" => "2",
                "CHILD_MENU_TYPE" => "bottom",
                "USE_EXT" => "Y",
                "DELAY" => "N",
                "ALLOW_MULTI_SELECT" => "N",
                "COMPONENT_TEMPLATE" => "offcanvas_overlay_menu"
            ),
            false
        );
        ?>

        <a href="#modal-call" class="left-menu__red-link">
            Заказать звонок
        </a>

        <div class="header-contacts left-menu__contacts">
            <?
            $APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "AREA_FILE_SHOW" => "file",
                    "PATH" => SITE_DIR . "include/header_phone_mail.php",
                    "AREA_FILE_RECURSIVE" => "N",
                    "EDIT_MODE" => "html",
                ),
                false,
                Array('HIDE_ICONS' => 'Y')
            );
            ?>
        </div>

        <div class="left-menu__socials socials">
            <a href="#" class="socials__item">
                <picture>
                    <source srcset="<?=SITE_TEMPLATE_PATH?>/img/ico/inst.svg" type="image/webp">
                    <img src="<?=SITE_TEMPLATE_PATH?>/img/ico/inst.svg" alt="">
                </picture>
            </a>
            <a href="#" class="socials__item">
                <picture>
                    <source srcset="<?=SITE_TEMPLATE_PATH?>/img/ico/vk.svg" type="image/webp">
                    <img src="<?=SITE_TEMPLATE_PATH?>/img/ico/vk.svg" alt="">
                </picture>
            </a>
            <a href="#" class="socials__item">
                <picture>
                    <source srcset="<?=SITE_TEMPLATE_PATH?>/img/ico/fb.svg" type="image/webp">
                    <img src="<?=SITE_TEMPLATE_PATH?>/img/ico/fb.svg" alt="">
                </picture>
            </a>
        </div>

    </div>

</div>
<!--end Offcanvas menu -->

<main>
    <?
    if ( $aCurPageURL != '/' ) {
        $APPLICATION->IncludeComponent("bitrix:breadcrumb", "hobbyline", Array(
                "START_FROM" => "0",
                "PATH" => "",
                "SITE_ID" => "s1"
            )
        );
    }
    ?>
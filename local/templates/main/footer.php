<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
</main>

<a class="to-top" href="#" id="toTop" uk-scroll>
    <div class="to-top__img">
        <picture>
            <source srcset="<?= SITE_TEMPLATE_PATH ?>/img/ico/arrow-top.svg" type="image/webp">
            <img src="<?= SITE_TEMPLATE_PATH ?>/img/ico/arrow-top.svg" alt="">
        </picture>
        <span>
            <?
            $APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "AREA_FILE_SHOW" => "file",
                    "PATH" => SITE_DIR . "include/footer_scroll_up_title.php",
                    "AREA_FILE_RECURSIVE" => "N",
                    "EDIT_MODE" => "html",
                ),
                false,
                Array('HIDE_ICONS' => 'Y')
            );
            ?>
        </span>
    </div>
</a>

<footer class="footer">
    <div class="main-content">

        <div class="footer-top">
            <?
            global $USER;
            if ( $USER->IsAuthorized() ) {
                $APPLICATION->IncludeComponent(
                    "bitrix:sender.subscribe",
                    "footer_subscribe",
                    //"template_2022_06_28",
                    array(
                        "SET_TITLE" => "N",
                        "COMPONENT_TEMPLATE" => ".default",
                        "USE_PERSONALIZATION" => "N",
                        "CONFIRMATION" => "N",
                        "HIDE_MAILINGS" => "Y",
                        "SHOW_HIDDEN" => "N",
                        "USER_CONSENT" => "Y",
                        "USER_CONSENT_ID" => "3",
                        "USER_CONSENT_IS_CHECKED" => "Y",
                        "USER_CONSENT_IS_LOADED" => "N",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_JUMP" => "Y",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "3600"
                    ),
                    false
                );
            }
            else {
                ?><div class="bx-subscribe" id="sender-subscribe"></div><?
            }
            ?>

            <a href="/" class="footer__logo">
                <picture>
                    <source srcset="<?= SITE_TEMPLATE_PATH ?>/img/ico/footer-logo.svg" type="image/webp">
                    <img src="<?= SITE_TEMPLATE_PATH ?>/img/ico/footer-logo.svg" alt="">
                </picture>
            </a>

            <?
            $APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "footer_menu",
                    array(
                        "ROOT_MENU_TYPE" => "bottom",
                        "MAX_LEVEL" => "1",
                        "MENU_CACHE_TYPE" => "A",
                        "CACHE_SELECTED_ITEMS" => "N",
                        "MENU_CACHE_TIME" => "86400",
                        "MENU_CACHE_USE_GROUPS" => "N",
                        "MENU_CACHE_GET_VARS" => array(),
                        "COMPONENT_TEMPLATE" => "footer_menu",
                        "MENU_THEME" => "site",
                        "CHILD_MENU_TYPE" => "left",
                        "USE_EXT" => "N",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N"
                    ),
                    false
            );
            ?>
        </div>

        <div class="footer-bottom">
            <div class="copy-desktop">
                <?
                $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "PATH" => SITE_DIR . "include/footer_all_rights_reserved.php",
                        "AREA_FILE_RECURSIVE" => "N",
                        "EDIT_MODE" => "html",
                    ),
                    false,
                    Array('HIDE_ICONS' => 'Y')
                );
                ?>
            </div>

            <?
            $APPLICATION->IncludeComponent("bitrix:news.list","socials",Array(
                    "DISPLAY_DATE" => "N",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "content",
                    "IBLOCK_ID" => 11,
                    "NEWS_COUNT" => 100,
                    "SORT_BY1" => "SORT",
                    "SORT_ORDER1" => "ASC",
                    "SORT_BY2" => "ID",
                    "SORT_ORDER2" => "ASC",
                    "FILTER_NAME" => "",
                    "FIELD_CODE" => Array("ID", "NAME", "PREVIEW_TEXT"),
                    "PROPERTY_CODE" => Array('FILES'),
                    "CHECK_DATES" => "N",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "ACTIVE_DATE_FORMAT" => "",
                    "SET_TITLE" => "N",
                    "SET_BROWSER_TITLE" => "N",
                    "SET_META_KEYWORDS" => "N",
                    "SET_META_DESCRIPTION" => "N",
                    "SET_LAST_MODIFIED" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "INCLUDE_SUBSECTIONS" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "86400",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "",
                    "PAGER_DESC_NUMBERING" => "Y",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "PAGER_BASE_LINK_ENABLE" => "Y",
                    "SET_STATUS_404" => "N",
                    "SHOW_404" => "N",
                    "MESSAGE_404" => "",
                    "PAGER_BASE_LINK" => "",
                    "PAGER_PARAMS_NAME" => "arrPager",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_ADDITIONAL" => ""
                )
            );
            ?>

            <div class="copy-mob">
                <?
                $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "PATH" => SITE_DIR . "include/footer_all_rights_reserved.php",
                        "AREA_FILE_RECURSIVE" => "N",
                        "EDIT_MODE" => "html",
                    ),
                    false,
                    Array('HIDE_ICONS' => 'Y')
                );
                ?>
            </div>

            <div>
                <?
                $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "PATH" => SITE_DIR . "include/footer_developer.php",
                        "AREA_FILE_RECURSIVE" => "N",
                        "EDIT_MODE" => "html",
                    ),
                    false,
                    Array('HIDE_ICONS' => 'Y')
                );
                ?>
            </div>
        </div>

    </div>
</footer>

<!-- Modals -->
<div id="thxEmail" uk-modal>
    <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical modal">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <h2 class="uk-modal-title uk-text-center">
            <?
            $APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "AREA_FILE_SHOW" => "file",
                    "PATH" => SITE_DIR . "include/footer_thanks_subscribe.php",
                    "AREA_FILE_RECURSIVE" => "N",
                    "EDIT_MODE" => "html",
                ),
                false,
                Array('HIDE_ICONS' => 'Y')
            );
            ?>
        </h2>
    </div>
</div>

<div id="modal-call" uk-modal><?

    $APPLICATION->IncludeComponent(
            "glab:feedback",
            "main_feedback",
            Array(
            "USE_CAPTCHA" => "N",
            "OK_TEXT" => "Спасибо, мы свяжемся с вами в ближайшее время.",
            //"EMAIL_TO" => "kostikmal@mail.ru",
            "EMAIL_TO" => "info@hobbyline.ru",
            "REQUIRED_FIELDS" => Array("NAME","PHONE", "CITY"),
            "EVENT_MESSAGE_ID" => Array(),
            "EVENT_NAME" => "MAIN_FORM",
        )
    );

?></div>

</body>

</html>
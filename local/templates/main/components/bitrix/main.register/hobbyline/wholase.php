<div class="reg__steps reg__steps-active" id="step1">

    <h2 class="auth-form__label"><?=GetMessage("AUTH_REGISTER")?> <span>1 <?=GetMessage("AUTH_STEP")?>/3</span></h2>

    <div class="auth-form__text">
        <input type="text" id="opt-lastname" class="hl-require" placeholder="<?=GetMessage("REGISTER_FIELD_LAST_NAME")?>*" name="REGISTER[LAST_NAME]" maxlength="255" value="<?=$arResult["VALUES"]["LAST_NAME"]?>" required>
    </div>
    <div class="auth-form__text">
        <input type="text" id="opt-name" class="hl-require" placeholder="<?=GetMessage("REGISTER_FIELD_NAME")?>*" name="REGISTER[NAME]" maxlength="255" value="<?=$arResult["VALUES"]["NAME"]?>" required>
    </div>
    <div class="auth-form__text">
        <input type="text" id="opt-patronymic"  placeholder="<?=GetMessage("REGISTER_FIELD_SECOND_NAME")?>" name="REGISTER[SECOND_NAME]" maxlength="255" value="<?=$arResult["VALUES"]["SECOND_NAME"]?>" >
    </div>
    <div class="link-half lookbook__link auth-form__login reg__next">
        <?=GetMessage("NEXT_BTTN")?>
        <picture>
            <source srcset="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" type="image/webp">
            <img src="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" class="btn-arrow-cat" alt="">
        </picture>
    </div>

</div>

<div class="reg__steps uk-animation-slide-right-medium" id="step2">

    <h2 class="auth-form__label"><?=GetMessage("AUTH_REGISTER")?> <span>2 <?=GetMessage("AUTH_STEP")?>/3</span></h2>

    <div class="auth-form__text">
        <input type="text" id="opt-org" class="hl-require" placeholder="<?=GetMessage("REGISTER_FIELD_WORK_COMPANY")?>*" name="REGISTER[WORK_COMPANY]" maxlength="255" value="<?=$arResult["VALUES"]["WORK_COMPANY"]?>" required>
    </div>
    <div class="auth-form__text">
        <input type="text" id="opt-city" class="hl-require" placeholder="<?=GetMessage("REGISTER_FIELD_PERSONAL_CITY")?>*" name="REGISTER[PERSONAL_CITY]" maxlength="255" value="<?=$arResult["VALUES"]["PERSONAL_CITY"]?>" required>
    </div>

    <div class="auth-form__text">

        <?
        foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField) {
            if( $arUserField["USER_TYPE"]["USER_TYPE_ID"] == 'string' && $arUserField['FIELD_NAME'] == 'UF_INN' ) {
                $sTemplateName = 'hobbyline_string';
            }
            else {
                $sTemplateName = $arUserField["USER_TYPE"]["USER_TYPE_ID"];
            }
            $APPLICATION->IncludeComponent(
                "bitrix:system.field.edit",
                //$arUserField["USER_TYPE"]["USER_TYPE_ID"],
                $sTemplateName,
                array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField, "form_name" => "regform"), null, array("HIDE_ICONS" => "Y")
            );
        }
        ?>

    </div>

    <div class="auth-form__text">
        <input type="text" id="opt-adres" placeholder="<?=GetMessage("REGISTER_FIELD_PERSONAL_STREET")?>" name="REGISTER[PERSONAL_STREET]" maxlength="255" value="<?=$arResult["VALUES"]["PERSONAL_STREET"]?>">
    </div>

    <div class="link-half lookbook__link auth-form__login reg__next">
        <?=GetMessage("NEXT_BTTN")?>
        <picture>
            <source srcset="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" type="image/webp">
            <img src="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" class="btn-arrow-cat" alt="">
        </picture>
    </div>

</div>

<div class="reg__steps uk-animation-slide-right-medium" id="step3">

    <h2 class="auth-form__label"><?=GetMessage("AUTH_REGISTER")?> <span>3 <?=GetMessage("AUTH_STEP")?>/3</span></h2>

    <div class="auth-form__text">
        <input type="text" id="opt-phone" class="hl-require" placeholder="<?=GetMessage("REGISTER_FIELD_PERSONAL_PHONE")?>*" name="REGISTER[PERSONAL_PHONE]" maxlength="255" value="<?=$arResult["VALUES"]["PERSONAL_PHONE"]?>" required>
    </div>
    <div class="auth-form__text">
        <input type="text" class="user_email hl-require" id="opt-email-input" placeholder="<?=GetMessage("REGISTER_FIELD_EMAIL")?>*" name="REGISTER[EMAIL]" maxlength="255" value="<?=$arResult["VALUES"]["EMAIL"]?>" required>
    </div>
    <div class="auth-form__text">
        <input type="password" id="opt-pass" class="hl-require" placeholder="<?=GetMessage("REGISTER_FIELD_PASSWORD")?>*" name="REGISTER[PASSWORD]" maxlength="255" value="<?=$arResult["VALUES"]["PASSWORD"]?>" required>
    </div>
    <div class="auth-form__text">
        <input type="password" id="opt-pass-rep" class="hl-require" placeholder="<?=GetMessage("REGISTER_FIELD_CONFIRM_PASSWORD")?>*" name="REGISTER[CONFIRM_PASSWORD]" maxlength="255" value="<?=$arResult["VALUES"]["CONFIRM_PASSWORD"]?>" required>
    </div>
    <div class="remember">
        <div>
            <input type="checkbox" class="custom-checkbox hl-require" name="REGISTER_PERS_DATA" id="pers-data" value="" required>
            <label for="pers-data" class="footer__label remember__label reg__chechbox">
                Даю согласие на <a href="#">обработку данных</a></label>
        </div>
    </div>
    <a href="#" class="link-half lookbook__link auth-form__login reg__reg-btn">
        <?=GetMessage("AUTH_REGISTER")?>
        <picture>
            <source srcset="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" type="image/webp">
            <img src="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" class="btn-arrow-cat" alt="">
        </picture>
    </a>

    <div class="send_registr_bttn" style="display: none;">
        <?/*<input type="submit" class="btn btn-primary btn_register" name="Register" value="<?=GetMessage("AUTH_REGISTER")?>" />*/?>
        <input type="submit" class="btn_register" name="register_submit_button" value="<?= GetMessage("AUTH_REGISTER") ?>"/>
        <input type="text" class="user_login" name="REGISTER[LOGIN]" maxlength="255" value="<?=$arResult["VALUES"]["LOGIN"]?>" />
    </div>

</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<div class="reg__steps reg__steps-active" id="roz-step1">

    <h2 class="auth-form__label"><?=GetMessage("AUTH_REGISTER")?> <span>1 <?=GetMessage("AUTH_STEP")?>/2</span></h2>

    <div class="auth-form__text">
        <input type="text" id="roz-lastname" class="hl-require" placeholder="<?=GetMessage("REGISTER_FIELD_LAST_NAME")?>*" name="REGISTER[LAST_NAME]" maxlength="255" value="<?=$arResult["USER_LAST_NAME"]?>" required>
    </div>
    <div class="auth-form__text">
        <input type="text" id="roz-name" class="hl-require" placeholder="<?=GetMessage("REGISTER_FIELD_NAME")?>*" name="REGISTER[NAME]" maxlength="255" value="<?=$arResult["USER_NAME"]?>" required>
    </div>
    <div class="auth-form__text">
        <input type="text" id="roz-patronymic" placeholder="<?=GetMessage("REGISTER_FIELD_SECOND_NAME")?>" name="REGISTER[SECOND_NAME]" maxlength="255" value="<?=$arResult["USER_SECOND_NAME"]?>">
    </div>
    <div class="auth-form__text">
        <input type="text" id="roz-phone" class="hl-require" placeholder="<?=GetMessage("REGISTER_FIELD_PERSONAL_PHONE")?>*" name="REGISTER[PERSONAL_PHONE]" maxlength="255" value="<?=$arResult["USER_PERSONAL_PHONE"]?>" required>
    </div>
    <div class="link-half lookbook__link auth-form__login reg__next-roz">
        <?=GetMessage("NEXT_BTTN")?>
        <picture>
            <source srcset="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" type="image/webp">
            <img src="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" class="btn-arrow-cat" alt="">
        </picture>
    </div>

</div>

<div class="reg__steps uk-animation-slide-right-medium" id="roz-step2">

    <h2 class="auth-form__label"><?=GetMessage("AUTH_REGISTER")?> <span>2 <?=GetMessage("AUTH_STEP")?>/2</span></h2>

    <div class="auth-form__text">
        <input type="text" id="roz-email-input" class="hl-require user_email" placeholder="<?=GetMessage("REGISTER_FIELD_EMAIL")?>*" name="REGISTER[EMAIL]" maxlength="255" value="<?=$arResult["USER_EMAIL"]?>" required>
    </div>

    <div class="auth-form__text">
        <input type="password" id="roz-pass" class="hl-require" placeholder="<?=GetMessage("REGISTER_FIELD_PASSWORD")?>*" name="REGISTER[PASSWORD]" maxlength="255" value="<?=$arResult["USER_PASSWORD"]?>" required>
    </div>
    <div class="auth-form__text">
        <input type="password" id="roz-pass-rep" class="hl-require" placeholder="<?=GetMessage("REGISTER_FIELD_CONFIRM_PASSWORD")?>*" name="REGISTER[CONFIRM_PASSWORD]" maxlength="255" value="<?=$arResult["USER_CONFIRM_PASSWORD"]?>" required>
    </div>

    <div class="remember">
        <div>
            <input type="checkbox" class="custom-checkbox" name="REGISTER_PESR_DATA" id="pers-dara-roz" value="" required>
            <label for="pers-dara-roz" class="footer__label remember__label reg__chechbox">
                Даю согласие на <a href="#">обработку данных</a>
            </label>
        </div>
    </div>

    <a href="#" class="link-half lookbook__link auth-form__login reg__reg-btn" id="reg-roz">
        <?=GetMessage("AUTH_REGISTER")?>
        <picture>
            <source srcset="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" type="image/webp">
            <img src="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" class="btn-arrow-cat" alt="">
        </picture>
    </a>

    <div class="send_registr_bttn" style="display: none;">
        <?/*<input type="submit" class="btn btn-primary btn_register" name="Register" value="<?=GetMessage("AUTH_REGISTER")?>" />*/?>
        <input type="submit" class="btn_register" name="register_submit_button" value="<?= GetMessage("AUTH_REGISTER") ?>"/>
        <input type="text" class="user_login" name="REGISTER[LOGIN]" maxlength="255" value="<?=$arResult["VALUES"]["LOGIN"]?>" />
    </div>

</div>
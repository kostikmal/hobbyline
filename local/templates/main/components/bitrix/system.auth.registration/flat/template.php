<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if($arResult["SHOW_SMS_FIELD"] == true)
{
	CJSCore::Init('phone_auth');
}

//one css for all system.auth.* forms
$APPLICATION->SetAdditionalCSS("/bitrix/css/main/system.auth/flat/style.css");
?>
<div class="main-content auth-wrap">
    <div class="auth-form reg">

        <div class="reg__top-nav">
            <div class="reg__top-item reg__top-active" id="label-1">Оптовый клиент</div>
            <div class="reg__top-item" id="label-2">Розничный клиент</div>
        </div>


        <?
        if(!empty($arParams["~AUTH_RESULT"])):
            $text = str_replace(array("<br>", "<br />"), "\n", $arParams["~AUTH_RESULT"]["MESSAGE"]);
        ?>
            <div class="alert <?=($arParams["~AUTH_RESULT"]["TYPE"] == "OK"? "alert-success":"alert-danger")?>"><?=nl2br(htmlspecialcharsbx($text))?></div>
        <?endif?>

        <?if($arResult["SHOW_EMAIL_SENT_CONFIRMATION"]):?>
            <div class="alert alert-success"><?echo GetMessage("AUTH_EMAIL_SENT")?></div>
        <?endif?>

        <?if(!$arResult["SHOW_EMAIL_SENT_CONFIRMATION"] && $arResult["USE_EMAIL_CONFIRMATION"] === "Y"):?>
            <div class="alert alert-warning"><?echo GetMessage("AUTH_EMAIL_WILL_BE_SENT")?></div>
        <?endif?>

        <?if($arResult["SHOW_SMS_FIELD"] == true):?>

            <form method="post" action="<?=$arResult["AUTH_URL"]?>" name="regform">

                <input type="hidden" name="SIGNED_DATA" value="<?=htmlspecialcharsbx($arResult["SIGNED_DATA"])?>" />

                <div class="bx-authform-formgroup-container">
                    <div class="bx-authform-label-container"><span class="bx-authform-starrequired">*</span><?echo GetMessage("main_register_sms_code")?></div>
                    <div class="bx-authform-input-container">
                        <input type="text" name="SMS_CODE" maxlength="255" value="<?=htmlspecialcharsbx($arResult["SMS_CODE"])?>" autocomplete="off" />
                    </div>
                </div>

                <div class="bx-authform-formgroup-container">
                    <input type="submit" class="btn btn-primary" name="code_submit_button" value="<?echo GetMessage("main_register_sms_send")?>" />
                </div>

            </form>

            <script>
            new BX.PhoneAuth({
                containerId: 'bx_register_resend',
                errorContainerId: 'bx_register_error',
                interval: <?=$arResult["PHONE_CODE_RESEND_INTERVAL"]?>,
                data:
                    <?=CUtil::PhpToJSObject([
                        'signedData' => $arResult["SIGNED_DATA"],
                    ])?>,
                onError:
                    function(response)
                    {
                        var errorNode = BX('bx_register_error');
                        errorNode.innerHTML = '';
                        for(var i = 0; i < response.errors.length; i++)
                        {
                            errorNode.innerHTML = errorNode.innerHTML + BX.util.htmlspecialchars(response.errors[i].message) + '<br />';
                        }
                        errorNode.style.display = '';
                    }
            });
            </script>

            <div id="bx_register_error" style="display:none" class="alert alert-danger"></div>

            <div id="bx_register_resend"></div>

        <?elseif(!$arResult["SHOW_EMAIL_SENT_CONFIRMATION"]):?>

            <form method="post" action="<?=$arResult["AUTH_URL"]?>" name="bform" enctype="multipart/form-data">
                <input type="hidden" name="AUTH_FORM" value="Y" />
                <input type="hidden" name="TYPE" value="REGISTRATION" />

                <?/* Регистрация Оптового Начало */?>
                <div class="reg__pad reg__cont-active uk-animation-slide-right-medium" id="cont-1">
                    <div class="<?/*reg__steps reg__steps-active*/?>" id="step1">

                        <h2 class="auth-form__label"><?=GetMessage("AUTH_REGISTER")?> <span>1 <?=GetMessage("AUTH_STEP")?>/3</span></h2>

                        <div class="auth-form__text">
                            <input type="text" id="opt-name" placeholder="<?=GetMessage("AUTH_NAME")?>*" name="USER_NAME" maxlength="255" value="<?=$arResult["USER_NAME"]?>" required>
                        </div>

                        <div class="auth-form__text">
                            <input type="text" id="opt-patronymic" placeholder="<?=GetMessage("SECOND_NAME")?>*" name="USER_SECOND_NAME" maxlength="255" value="<?=$arResult["USER_SECOND_NAME"]?>" required>
                        </div>

                        <div class="auth-form__text">
                            <input type="text" id="opt-lastname" placeholder="<?=GetMessage("AUTH_LAST_NAME")?>*" name="USER_LAST_NAME" maxlength="255" value="<?=$arResult["USER_LAST_NAME"]?>" required>
                        </div>

                        <div class="link-half lookbook__link auth-form__login reg__next">
                            <?=GetMessage("NEXT_BTTN")?>
                            <picture>
                                <source srcset="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" type="image/webp">
                                <img src="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" class="btn-arrow-cat" alt="">
                            </picture>
                        </div>

                    </div>

                    <div class="<?/*reg__steps uk-animation-slide-right-medium*/?>" id="step2">

                        <h2 class="auth-form__label"><?=GetMessage("AUTH_REGISTER")?> <span>2 <?=GetMessage("AUTH_STEP")?>/3</span></h2>

                        <div class="auth-form__text">
                            <input type="text" id="opt-org" placeholder="<?=GetMessage("AUTH_COMPANY")?>*" name="USER_WORK_COMPANY" maxlength="255" value="<?=$arResult["USER_WORK_COMPANY"]?>" required>
                        </div>

                        <div class="auth-form__text">
                            <input type="text" id="opt-city" placeholder="<?=GetMessage("AUTH_CITY")?>*" name="USER_PERSONAL_CITY" maxlength="255" value="<?=$arResult["USER_PERSONAL_CITY"]?>" required>
                        </div>

                        <div class="auth-form__text">
                            <input type="text" id="opt-inn" placeholder="<?=GetMessage("AUTH_INN")?>*" name="USER_UF_INN" maxlength="255" value="<?=$arResult["USER_UF_INN"]?>" required>
                        </div>

                        <div class="auth-form__text">
                            <input type="text" id="opt-adres" placeholder="<?=GetMessage("AUTH_ADRES")?>" name="USER_PERSONAL_STREET" maxlength="255" value="<?=$arResult["USER_PERSONAL_STREET"]?>">
                        </div>

                        <div class="link-half lookbook__link auth-form__login reg__next">
                            <?=GetMessage("NEXT_BTTN")?>
                            <picture>
                                <source srcset="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" type="image/webp">
                                <img src="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" class="btn-arrow-cat" alt="">
                            </picture>
                        </div>

                    </div>

                    <div class="<?/*reg__steps uk-animation-slide-right-medium*/?>" id="step3">

                        <h2 class="auth-form__label"><?=GetMessage("AUTH_REGISTER")?> <span>3 <?=GetMessage("AUTH_STEP")?>/3</span></h2>

                        <div class="auth-form__text">
                            <input type="text" id="opt-phone" placeholder="<?=GetMessage("AUTH_PHONE")?>*" name="USER_PERSONAL_PHONE" maxlength="255" value="<?=$arResult["USER_PERSONAL_PHONE"]?>" required>
                        </div>
                        <div class="auth-form__text">
                            <input type="text" class="user_email" id="opt-email-input" placeholder="<?=GetMessage("AUTH_EMAIL")?>*" name="USER_EMAIL" maxlength="255" value="<?=$arResult["USER_EMAIL"]?>" required>
                        </div>
                        <div class="auth-form__text">
                            <input type="password" id="opt-pass" placeholder="<?=GetMessage("AUTH_PASSWORD_REQ")?>*" name="USER_PASSWORD" maxlength="255" value="<?=$arResult["USER_PASSWORD"]?>" required>
                        </div>
                        <div class="auth-form__text">
                            <input type="password" id="opt-pass-rep" placeholder="<?=GetMessage("AUTH_CONFIRM_PSSWD")?>*" name="USER_CONFIRM_PASSWORD" maxlength="255" value="<?=$arResult["USER_CONFIRM_PASSWORD"]?>" required>
                        </div>
                        <div class="remember">
                            <div>
                                <input type="checkbox" class="custom-checkbox" name="" id="pers-data" value="">
                                <label for="pers-data" class="footer__label remember__label reg__chechbox">
                                    Даю согласие на <a href="#">обработку данных</a></label>
                            </div>
                        </div>
                        <a href="#" class="link-half lookbook__link auth-form__login reg__reg-btn">
                            <?=GetMessage("AUTH_REGISTER")?>
                            <picture>
                                <source srcset="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" type="image/webp">
                                <img src="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" class="btn-arrow-cat" alt="">
                            </picture>
                        </a>

                        <div class="send_registr_bttn" style="display: none;">
                            <input type="submit" class="btn btn-primary btn_register" name="Register" value="<?=GetMessage("AUTH_REGISTER")?>" />
                            <input type="text" class="user_login" name="USER_LOGIN" maxlength="255" value="<?=$arResult["USER_LOGIN"]?>" />
                        </div>

                    </div>
                </div>
                <?/* Регистрация Оптового Конец */?>

                <?/* Регистрация Розничного начало */?>
                <?/*<div class="reg__pad uk-animation-slide-left-medium" id="cont-2">
                    <div class="reg__steps reg__steps-active" id="roz-step1">

                        <h2 class="auth-form__label"><?=GetMessage("AUTH_REGISTER")?> <span>1 <?=GetMessage("AUTH_STEP")?>/2</span></h2>

                        <div class="auth-form__text">
                            <input type="text" id="roz-name" placeholder="<?=GetMessage("AUTH_NAME")?>*" name="USER_NAME" maxlength="255" value="<?=$arResult["USER_NAME"]?>" required>
                        </div>

                        <div class="auth-form__text">
                            <input type="text" id="roz-patronymic" placeholder="<?=GetMessage("SECOND_NAME")?>*" name="USER_SECOND_NAME" maxlength="255" value="<?=$arResult["USER_SECOND_NAME"]?>" required>
                        </div>

                        <div class="auth-form__text">
                            <input type="text" id="roz-lastname" placeholder="<?=GetMessage("AUTH_LAST_NAME")?>*" name="USER_LAST_NAME" maxlength="255" value="<?=$arResult["USER_LAST_NAME"]?>" required>
                        </div>

                        <div class="auth-form__text">
                            <input type="text" id="roz-phone" placeholder="<?=GetMessage("AUTH_PHONE")?>*" name="USER_PERSONAL_PHONE" maxlength="255" value="<?=$arResult["USER_PERSONAL_PHONE"]?>" required>
                        </div>

                        <div class="link-half lookbook__link auth-form__login reg__next-roz">
                            <?=GetMessage("NEXT_BTTN")?>
                            <picture>
                                <source srcset="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" type="image/webp">
                                <img src="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" class="btn-arrow-cat" alt="">
                            </picture>
                        </div>

                    </div>

                    <div class="reg__steps uk-animation-slide-right-medium" id="roz-step2">

                        <h2 class="auth-form__label"><?=GetMessage("AUTH_REGISTER")?> <span>2 <?=GetMessage("AUTH_STEP")?>/2</span></h2>

                        <div class="auth-form__text">
                            <input type="text" id="roz-email-input" placeholder="<?=GetMessage("AUTH_EMAIL")?>*" name="USER_EMAIL" maxlength="255" value="<?=$arResult["USER_EMAIL"]?>" required>
                        </div>
                        <div class="auth-form__text">
                            <input type="password" id="roz-pass" placeholder="<?=GetMessage("AUTH_PASSWORD_REQ")?>*" name="USER_PASSWORD" maxlength="255" value="<?=$arResult["USER_PASSWORD"]?>" required>
                        </div>
                        <div class="auth-form__text">
                            <input type="password" id="roz-pass-rep" placeholder="<?=GetMessage("AUTH_CONFIRM_PSSWD")?>*" name="USER_CONFIRM_PASSWORD" maxlength="255" value="<?=$arResult["USER_CONFIRM_PASSWORD"]?>" required>
                        </div>
                        <div class="remember">
                            <div>
                                <input type="checkbox" class="custom-checkbox" name="" id="pers-dara-roz" value="">
                                <label for="pers-dara-roz" class="footer__label remember__label reg__chechbox">
                                    Даю согласие на <a href="#">обработку данных</a>
                                </label>
                            </div>
                        </div>

                        <a href="#" class="link-half lookbook__link auth-form__login reg__reg-btn">
                            <?=GetMessage("AUTH_REGISTER")?>
                            <picture>
                                <source srcset="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" type="image/webp">
                                <img src="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" class="btn-arrow-cat" alt="">
                            </picture>
                        </a>

                        <div class="send_registr_bttn" style="display: none;">
                            <input type="submit" class="btn btn-primary" name="Register" value="<?=GetMessage("AUTH_REGISTER")?>" />
                        </div>

                    </div>

                </div>*/?>
                <?/* Регистрация Розничнго конец */?>

                <?/*
                <div class="bx-authform-formgroup-container">
                    <div class="bx-authform-label-container"><?=GetMessage("AUTH_NAME")?></div>
                    <div class="bx-authform-input-container">
                        <input type="text" name="USER_NAME" maxlength="255" value="<?=$arResult["USER_NAME"]?>" />
                    </div>
                </div>

                <div class="bx-authform-formgroup-container">
                    <div class="bx-authform-label-container"><?=GetMessage("AUTH_LAST_NAME")?></div>
                    <div class="bx-authform-input-container">
                        <input type="text" name="USER_LAST_NAME" maxlength="255" value="<?=$arResult["USER_LAST_NAME"]?>" />
                    </div>
                </div>

                <div class="bx-authform-formgroup-container">
                    <div class="bx-authform-label-container"><span class="bx-authform-starrequired">*</span><?=GetMessage("AUTH_LOGIN_MIN")?></div>
                    <div class="bx-authform-input-container">
                        <input type="text" name="USER_LOGIN" maxlength="255" value="<?=$arResult["USER_LOGIN"]?>" />
                    </div>
                </div>

                <div class="bx-authform-formgroup-container">
                    <div class="bx-authform-label-container"><span class="bx-authform-starrequired">*</span><?=GetMessage("AUTH_PASSWORD_REQ")?></div>
                    <div class="bx-authform-input-container">
        <?if($arResult["SECURE_AUTH"]):?>
                        <div class="bx-authform-psw-protected" id="bx_auth_secure" style="display:none"><div class="bx-authform-psw-protected-desc"><span></span><?echo GetMessage("AUTH_SECURE_NOTE")?></div></div>

        <script type="text/javascript">
        document.getElementById('bx_auth_secure').style.display = '';
        </script>
        <?endif?>
                        <input type="password" name="USER_PASSWORD" maxlength="255" value="<?=$arResult["USER_PASSWORD"]?>" autocomplete="off" />
                    </div>
                </div>

                <div class="bx-authform-formgroup-container">
                    <div class="bx-authform-label-container"><span class="bx-authform-starrequired">*</span><?=GetMessage("AUTH_CONFIRM")?></div>
                    <div class="bx-authform-input-container">
        <?if($arResult["SECURE_AUTH"]):?>
                        <div class="bx-authform-psw-protected" id="bx_auth_secure_conf" style="display:none"><div class="bx-authform-psw-protected-desc"><span></span><?echo GetMessage("AUTH_SECURE_NOTE")?></div></div>

        <script type="text/javascript">
        document.getElementById('bx_auth_secure_conf').style.display = '';
        </script>
        <?endif?>
                        <input type="password" name="USER_CONFIRM_PASSWORD" maxlength="255" value="<?=$arResult["USER_CONFIRM_PASSWORD"]?>" autocomplete="off" />
                    </div>
                </div>

        <?if($arResult["EMAIL_REGISTRATION"]):?>
                <div class="bx-authform-formgroup-container">
                    <div class="bx-authform-label-container"><?if($arResult["EMAIL_REQUIRED"]):?><span class="bx-authform-starrequired">*</span><?endif?><?=GetMessage("AUTH_EMAIL")?></div>
                    <div class="bx-authform-input-container">
                        <input type="text" name="USER_EMAIL" maxlength="255" value="<?=$arResult["USER_EMAIL"]?>" />
                    </div>
                </div>
        <?endif?>

        <?if($arResult["PHONE_REGISTRATION"]):?>
                <div class="bx-authform-formgroup-container">
                    <div class="bx-authform-label-container"><?if($arResult["PHONE_REQUIRED"]):?><span class="bx-authform-starrequired">*</span><?endif?><?echo GetMessage("main_register_phone_number")?></div>
                    <div class="bx-authform-input-container">
                        <input type="text" name="USER_PHONE_NUMBER" maxlength="255" value="<?=$arResult["USER_PHONE_NUMBER"]?>" />
                    </div>
                </div>
        <?endif?>

        <?if($arResult["USER_PROPERTIES"]["SHOW"] == "Y"):?>
            <?foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):?>

                <div class="bx-authform-formgroup-container">
                    <div class="bx-authform-label-container"><?if ($arUserField["MANDATORY"]=="Y"):?><span class="bx-authform-starrequired">*</span><?endif?><?=$arUserField["EDIT_FORM_LABEL"]?></div>
                    <div class="bx-authform-input-container">
        <?
        $APPLICATION->IncludeComponent(
            "bitrix:system.field.edit",
            $arUserField["USER_TYPE"]["USER_TYPE_ID"],
            array(
                "bVarsFromForm" => $arResult["bVarsFromForm"],
                "arUserField" => $arUserField,
                "form_name" => "bform"
            ),
            null,
            array("HIDE_ICONS"=>"Y")
        );
        ?>
                    </div>
                </div>

            <?endforeach;?>
        <?endif;?>
        <?if ($arResult["USE_CAPTCHA"] == "Y"):?>
                <input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />

                <div class="bx-authform-formgroup-container">
                    <div class="bx-authform-label-container">
                        <span class="bx-authform-starrequired">*</span><?=GetMessage("CAPTCHA_REGF_PROMT")?>
                    </div>
                    <div class="bx-captcha"><img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" /></div>
                    <div class="bx-authform-input-container">
                        <input type="text" name="captcha_word" maxlength="50" value="" autocomplete="off"/>
                    </div>
                </div>

        <?endif?>
                <div class="bx-authform-formgroup-container">
                    <div class="bx-authform-label-container">
                    </div>
                    <div class="bx-authform-input-container">
                        <?$APPLICATION->IncludeComponent("bitrix:main.userconsent.request", "",
                            array(
                                "ID" => COption::getOptionString("main", "new_user_agreement", ""),
                                "IS_CHECKED" => "Y",
                                "AUTO_SAVE" => "N",
                                "IS_LOADED" => "Y",
                                "ORIGINATOR_ID" => $arResult["AGREEMENT_ORIGINATOR_ID"],
                                "ORIGIN_ID" => $arResult["AGREEMENT_ORIGIN_ID"],
                                "INPUT_NAME" => $arResult["AGREEMENT_INPUT_NAME"],
                                "REPLACE" => array(
                                    "button_caption" => GetMessage("AUTH_REGISTER"),
                                    "fields" => array(
                                        rtrim(GetMessage("AUTH_NAME"), ":"),
                                        rtrim(GetMessage("AUTH_LAST_NAME"), ":"),
                                        rtrim(GetMessage("AUTH_LOGIN_MIN"), ":"),
                                        rtrim(GetMessage("AUTH_PASSWORD_REQ"), ":"),
                                        rtrim(GetMessage("AUTH_EMAIL"), ":"),
                                    )
                                ),
                            )
                        );?>
                    </div>
                </div>
                <div class="bx-authform-formgroup-container">
                    <input type="submit" class="btn btn-primary" name="Register" value="<?=GetMessage("AUTH_REGISTER")?>" />
                </div>

                <hr class="bxe-light">

                <div class="bx-authform-description-container">
                    <?echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"];?>
                </div>

                <div class="bx-authform-description-container">
                    <span class="bx-authform-starrequired">*</span><?=GetMessage("AUTH_REQ")?>
                </div>

                <div class="bx-authform-link-container">
                    <a href="<?=$arResult["AUTH_AUTH_URL"]?>" rel="nofollow"><b><?=GetMessage("AUTH_AUTH")?></b></a>
                </div>
                */?>

                <div class="bx-authform-description-container">
                    <?echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"];?>
                </div>

                <div class="bx-authform-description-container">
                    <span class="bx-authform-starrequired">*</span><?=GetMessage("AUTH_REQ")?>
                </div>

                <div class="bx-authform-link-container">
                    <a href="<?=$arResult["AUTH_AUTH_URL"]?>" rel="nofollow"><b><?=GetMessage("AUTH_AUTH")?></b></a>
                </div>

            </form>

        <?/*<script type="text/javascript">
            document.bform.USER_NAME.focus();
        </script>*/?>

        <?endif?>

    </div>

    <div class="top-info__heart-dots heart-dots auth-dot-top"></div>
    <div class="heart-dots auth-dot-bottom"></div>

</div>
<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if($arResult["SHOW_SMS_FIELD"] == true)
{
	CJSCore::Init('phone_auth');
}
?>
<div class="bx-auth main-content auth-wrap">

    <div class="auth-form reg">

        <?
        ShowMessage($arParams["~AUTH_RESULT"]);
        ?>
        <?if($arResult["SHOW_EMAIL_SENT_CONFIRMATION"]):?>
            <p><?echo GetMessage("AUTH_EMAIL_SENT")?></p>
        <?endif;?>

        <?if(!$arResult["SHOW_EMAIL_SENT_CONFIRMATION"] && $arResult["USE_EMAIL_CONFIRMATION"] === "Y"):?>
            <p><?echo GetMessage("AUTH_EMAIL_WILL_BE_SENT")?></p>
        <?endif?>

        <noindex>

        <?if($arResult["SHOW_SMS_FIELD"] == true):?>

            <form method="post" action="<?=$arResult["AUTH_URL"]?>" name="regform">
                <input type="hidden" name="SIGNED_DATA" value="<?=htmlspecialcharsbx($arResult["SIGNED_DATA"])?>" />
                <table class="data-table bx-registration-table">
                    <tbody>
                        <tr>
                            <td><span class="starrequired">*</span><?echo GetMessage("main_register_sms_code")?></td>
                            <td><input size="30" type="text" name="SMS_CODE" value="<?=htmlspecialcharsbx($arResult["SMS_CODE"])?>" autocomplete="off" /></td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td></td>
                            <td><input type="submit" name="code_submit_button" value="<?echo GetMessage("main_register_sms_send")?>" /></td>
                        </tr>
                    </tfoot>
                </table>
            </form>

            <script>
            new BX.PhoneAuth({
                containerId: 'bx_register_resend',
                errorContainerId: 'bx_register_error',
                interval: <?=$arResult["PHONE_CODE_RESEND_INTERVAL"]?>,
                data:
                    <?=CUtil::PhpToJSObject([
                        'signedData' => $arResult["SIGNED_DATA"],
                    ])?>,
                onError:
                    function(response)
                    {
                        var errorDiv = BX('bx_register_error');
                        var errorNode = BX.findChildByClassName(errorDiv, 'errortext');
                        errorNode.innerHTML = '';
                        for(var i = 0; i < response.errors.length; i++)
                        {
                            errorNode.innerHTML = errorNode.innerHTML + BX.util.htmlspecialchars(response.errors[i].message) + '<br>';
                        }
                        errorDiv.style.display = '';
                    }
            });
            </script>

            <div id="bx_register_error" style="display:none"><?ShowError("error")?></div>

            <div id="bx_register_resend"></div>

        <?elseif(!$arResult["SHOW_EMAIL_SENT_CONFIRMATION"]):?>

            <div class="reg__top-nav">
                <div class="reg__top-item reg__top-active" id="label-1">Оптовый клиент</div>
                <div class="reg__top-item" id="label-2">Розничный клиент</div>
            </div>

            <?/* Регистрация оптового начало */?>
            <div class="reg__pad reg__cont-active uk-animation-slide-right-medium" id="cont-1">
                <form method="post" action="<?=$arResult["AUTH_URL"]?>" name="bform" enctype="multipart/form-data">
                    <input type="hidden" name="AUTH_FORM" value="Y" />
                    <input type="hidden" name="TYPE" value="REGISTRATION" />

                    <div class="reg__steps reg__steps-active" id="step1">

                        <h2 class="auth-form__label"><?=GetMessage("AUTH_REGISTER")?> <span>1 <?=GetMessage("AUTH_STEP")?>/3</span></h2>

                        <div class="auth-form__text">
                            <input type="text" id="opt-name" placeholder="<?=GetMessage("AUTH_NAME")?>*" name="USER_NAME" maxlength="255" value="<?=$arResult["USER_NAME"]?>" required>
                        </div>

                        <div class="auth-form__text">
                            <input type="text" id="opt-patronymic" placeholder="<?=GetMessage("SECOND_NAME")?>*" name="USER_SECOND_NAME" maxlength="255" value="<?=$arResult["USER_SECOND_NAME"]?>" required>
                        </div>

                        <div class="auth-form__text">
                            <input type="text" id="opt-lastname" placeholder="<?=GetMessage("AUTH_LAST_NAME")?>*" name="USER_LAST_NAME" maxlength="255" value="<?=$arResult["USER_LAST_NAME"]?>" required>
                        </div>

                        <div class="link-half lookbook__link auth-form__login reg__next">
                            <?=GetMessage("NEXT_BTTN")?>
                            <picture>
                                <source srcset="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" type="image/webp">
                                <img src="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" class="btn-arrow-cat" alt="">
                            </picture>
                        </div>

                    </div>

                    <div class="reg__steps uk-animation-slide-right-medium" id="step2">

                        <h2 class="auth-form__label"><?=GetMessage("AUTH_REGISTER")?> <span>2 <?=GetMessage("AUTH_STEP")?>/3</span></h2>

                        <div class="auth-form__text">
                            <input type="text" id="opt-org" placeholder="Название вашей организации*" name="USER_WORK_COMPANY" maxlength="255" value="<?=$arResult["USER_WORK_COMPANY"]?>" required>
                        </div>

                        <div class="auth-form__text">
                            <input type="text" id="opt-city" placeholder="Город*" name="USER_PERSONAL_CITY" maxlength="255" value="<?=$arResult["USER_PERSONAL_CITY"]?>" required>
                        </div>

                        <div class="auth-form__text">
                            <input type="text" id="opt-inn" placeholder="ИНН*" name="USER_UF_INN" maxlength="255" value="<?=$arResult["USER_UF_INN"]?>" required>
                        </div>

                        <div class="auth-form__text">
                            <input type="text" id="opt-adres" placeholder="Адрес доставки" name="USER_PERSONAL_STREET" maxlength="255" value="<?=$arResult["USER_PERSONAL_STREET"]?>">
                        </div>

                        <div class="link-half lookbook__link auth-form__login reg__next">
                            <?=GetMessage("NEXT_BTTN")?>
                            <picture>
                                <source srcset="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" type="image/webp">
                                <img src="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" class="btn-arrow-cat" alt="">
                            </picture>
                        </div>

                    </div>

                    <div class="reg__steps uk-animation-slide-right-medium" id="step3">

                        <h2 class="auth-form__label"><?=GetMessage("AUTH_REGISTER")?> <span>3 <?=GetMessage("AUTH_STEP")?>/3</span></h2>

                        <div class="auth-form__text">
                            <input type="text" id="opt-phone" placeholder="Номер телефона*" name="USER_PERSONAL_PHONE" maxlength="255" value="<?=$arResult["USER_PERSONAL_PHONE"]?>" required>
                        </div>
                        <div class="auth-form__text">
                            <input type="text" id="opt-email-input" placeholder="Email*" name="USER_EMAIL" maxlength="255" value="<?=$arResult["USER_EMAIL"]?>" required>
                        </div>
                        <div class="auth-form__text">
                            <input type="password" id="opt-pass" placeholder="Пароль*" name="USER_PASSWORD" maxlength="255" value="<?=$arResult["USER_PASSWORD"]?>" required>
                        </div>
                        <div class="auth-form__text">
                            <input type="password" id="opt-pass-rep" placeholder="Повторите пароль*" name="USER_CONFIRM_PASSWORD" maxlength="255" value="<?=$arResult["USER_CONFIRM_PASSWORD"]?>" required>
                        </div>
                        <div class="remember">
                            <div>
                                <input type="checkbox" class="custom-checkbox" name="" id="pers-data" value="">
                                <label for="pers-data" class="footer__label remember__label reg__chechbox">
                                    Даю согласие на <a href="#">обработку данных</a></label>
                            </div>
                        </div>
                        <a href="#" class="link-half lookbook__link auth-form__login reg__reg-btn">
                            <?=GetMessage("AUTH_REGISTER")?>
                            <picture>
                                <source srcset="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" type="image/webp">
                                <img src="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" class="btn-arrow-cat" alt="">
                            </picture>
                        </a>

                    </div>

                <?/*
                <table class="data-table bx-registration-table">
                    <thead>
                        <tr>
                            <td colspan="2"><b><?=GetMessage("AUTH_REGISTER")?></b></td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><?=GetMessage("AUTH_NAME")?></td>
                            <td><input type="text" name="USER_NAME" maxlength="50" value="<?=$arResult["USER_NAME"]?>" class="bx-auth-input" /></td>
                        </tr>
                        <tr>
                            <td><?=GetMessage("AUTH_LAST_NAME")?></td>
                            <td><input type="text" name="USER_LAST_NAME" maxlength="50" value="<?=$arResult["USER_LAST_NAME"]?>" class="bx-auth-input" /></td>
                        </tr>
                        <tr>
                            <td><span class="starrequired">*</span><?=GetMessage("AUTH_LOGIN_MIN")?></td>
                            <td><input type="text" name="USER_LOGIN" maxlength="50" value="<?=$arResult["USER_LOGIN"]?>" class="bx-auth-input" /></td>
                        </tr>
                        <tr>
                            <td><span class="starrequired">*</span><?=GetMessage("AUTH_PASSWORD_REQ")?></td>
                            <td><input type="password" name="USER_PASSWORD" maxlength="255" value="<?=$arResult["USER_PASSWORD"]?>" class="bx-auth-input" autocomplete="off" />
                <?if($arResult["SECURE_AUTH"]):?>
                                <span class="bx-auth-secure" id="bx_auth_secure" title="<?echo GetMessage("AUTH_SECURE_NOTE")?>" style="display:none">
                                    <div class="bx-auth-secure-icon"></div>
                                </span>
                                <noscript>
                                <span class="bx-auth-secure" title="<?echo GetMessage("AUTH_NONSECURE_NOTE")?>">
                                    <div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
                                </span>
                                </noscript>
                <script type="text/javascript">
                    document.getElementById('bx_auth_secure').style.display = 'inline-block';
                </script>
                <?endif?>
                            </td>
                        </tr>
                        <tr>
                            <td><span class="starrequired">*</span><?=GetMessage("AUTH_CONFIRM")?></td>
                            <td><input type="password" name="USER_CONFIRM_PASSWORD" maxlength="255" value="<?=$arResult["USER_CONFIRM_PASSWORD"]?>" class="bx-auth-input" autocomplete="off" /></td>
                        </tr>

                <?if($arResult["EMAIL_REGISTRATION"]):?>
                        <tr>
                            <td><?if($arResult["EMAIL_REQUIRED"]):?><span class="starrequired">*</span><?endif?><?=GetMessage("AUTH_EMAIL")?></td>
                            <td><input type="text" name="USER_EMAIL" maxlength="255" value="<?=$arResult["USER_EMAIL"]?>" class="bx-auth-input" /></td>
                        </tr>
                <?endif?>

                <?if($arResult["PHONE_REGISTRATION"]):?>
                        <tr>
                            <td><?if($arResult["PHONE_REQUIRED"]):?><span class="starrequired">*</span><?endif?><?echo GetMessage("main_register_phone_number")?></td>
                            <td><input type="text" name="USER_PHONE_NUMBER" maxlength="255" value="<?=$arResult["USER_PHONE_NUMBER"]?>" class="bx-auth-input" /></td>
                        </tr>
                <?endif?>

                <?// ********************* User properties ***************************************************?>
                <?if($arResult["USER_PROPERTIES"]["SHOW"] == "Y"):?>
                    <tr><td colspan="2"><?=strlen(trim($arParams["USER_PROPERTY_NAME"])) > 0 ? $arParams["USER_PROPERTY_NAME"] : GetMessage("USER_TYPE_EDIT_TAB")?></td></tr>
                    <?foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):?>
                    <tr><td><?if ($arUserField["MANDATORY"]=="Y"):?><span class="starrequired">*</span><?endif;
                        ?><?=$arUserField["EDIT_FORM_LABEL"]?>:</td><td>
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:system.field.edit",
                                $arUserField["USER_TYPE"]["USER_TYPE_ID"],
                                array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField, "form_name" => "bform"), null, array("HIDE_ICONS"=>"Y"));?></td></tr>
                    <?endforeach;?>
                <?endif;?>
                <?// ******************** /User properties ***************************************************

                    // CAPTCHA
                    if ($arResult["USE_CAPTCHA"] == "Y")
                    {
                        ?>
                        <tr>
                            <td colspan="2"><b><?=GetMessage("CAPTCHA_REGF_TITLE")?></b></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
                                <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
                            </td>
                        </tr>
                        <tr>
                            <td><span class="starrequired">*</span><?=GetMessage("CAPTCHA_REGF_PROMT")?>:</td>
                            <td><input type="text" name="captcha_word" maxlength="50" value="" autocomplete="off" /></td>
                        </tr>
                        <?
                    }
                    // CAPTCHA
                    ?>
                        <tr>
                            <td></td>
                            <td>
                                <?$APPLICATION->IncludeComponent("bitrix:main.userconsent.request", "",
                                    array(
                                        "ID" => COption::getOptionString("main", "new_user_agreement", ""),
                                        "IS_CHECKED" => "Y",
                                        "AUTO_SAVE" => "N",
                                        "IS_LOADED" => "Y",
                                        "ORIGINATOR_ID" => $arResult["AGREEMENT_ORIGINATOR_ID"],
                                        "ORIGIN_ID" => $arResult["AGREEMENT_ORIGIN_ID"],
                                        "INPUT_NAME" => $arResult["AGREEMENT_INPUT_NAME"],
                                        "REPLACE" => array(
                                            "button_caption" => GetMessage("AUTH_REGISTER"),
                                            "fields" => array(
                                                rtrim(GetMessage("AUTH_NAME"), ":"),
                                                rtrim(GetMessage("AUTH_LAST_NAME"), ":"),
                                                rtrim(GetMessage("AUTH_LOGIN_MIN"), ":"),
                                                rtrim(GetMessage("AUTH_PASSWORD_REQ"), ":"),
                                                rtrim(GetMessage("AUTH_EMAIL"), ":"),
                                            )
                                        ),
                                    )
                                );?>
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td></td>
                            <td><input type="submit" name="Register" value="<?=GetMessage("AUTH_REGISTER")?>" /></td>
                        </tr>
                    </tfoot>
                </table>
                */?>

                </form>
            </div>
            <?/* Регистрация оптового конец */?>

            <?/* Регистрация Розничного начало */?>
            <?/*
            <div class="reg__pad uk-animation-slide-left-medium" id="cont-2">
                <form method="post" action="<?=$arResult["AUTH_URL"]?>" name="bform" enctype="multipart/form-data">
                    <input type="hidden" name="AUTH_FORM" value="Y" />
                    <input type="hidden" name="TYPE" value="REGISTRATION" />

                    <table class="data-table bx-registration-table">
                        <thead>
                        <tr>
                            <td colspan="2"><b><?=GetMessage("AUTH_REGISTER")?></b></td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><?=GetMessage("AUTH_NAME")?></td>
                            <td><input type="text" name="USER_NAME" maxlength="50" value="<?=$arResult["USER_NAME"]?>" class="bx-auth-input" /></td>
                        </tr>
                        <tr>
                            <td><?=GetMessage("AUTH_LAST_NAME")?></td>
                            <td><input type="text" name="USER_LAST_NAME" maxlength="50" value="<?=$arResult["USER_LAST_NAME"]?>" class="bx-auth-input" /></td>
                        </tr>
                        <tr>
                            <td><span class="starrequired">*</span><?=GetMessage("AUTH_LOGIN_MIN")?></td>
                            <td><input type="text" name="USER_LOGIN" maxlength="50" value="<?=$arResult["USER_LOGIN"]?>" class="bx-auth-input" /></td>
                        </tr>
                        <tr>
                            <td><span class="starrequired">*</span><?=GetMessage("AUTH_PASSWORD_REQ")?></td>
                            <td><input type="password" name="USER_PASSWORD" maxlength="255" value="<?=$arResult["USER_PASSWORD"]?>" class="bx-auth-input" autocomplete="off" />
                                <?if($arResult["SECURE_AUTH"]):?>
                                    <span class="bx-auth-secure" id="bx_auth_secure" title="<?echo GetMessage("AUTH_SECURE_NOTE")?>" style="display:none">
                                    <div class="bx-auth-secure-icon"></div>
                                </span>
                                    <noscript>
                                <span class="bx-auth-secure" title="<?echo GetMessage("AUTH_NONSECURE_NOTE")?>">
                                    <div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
                                </span>
                                    </noscript>
                                    <script type="text/javascript">
                                        document.getElementById('bx_auth_secure').style.display = 'inline-block';
                                    </script>
                                <?endif?>
                            </td>
                        </tr>
                        <tr>
                            <td><span class="starrequired">*</span><?=GetMessage("AUTH_CONFIRM")?></td>
                            <td><input type="password" name="USER_CONFIRM_PASSWORD" maxlength="255" value="<?=$arResult["USER_CONFIRM_PASSWORD"]?>" class="bx-auth-input" autocomplete="off" /></td>
                        </tr>

                        <?if($arResult["EMAIL_REGISTRATION"]):?>
                            <tr>
                                <td><?if($arResult["EMAIL_REQUIRED"]):?><span class="starrequired">*</span><?endif?><?=GetMessage("AUTH_EMAIL")?></td>
                                <td><input type="text" name="USER_EMAIL" maxlength="255" value="<?=$arResult["USER_EMAIL"]?>" class="bx-auth-input" /></td>
                            </tr>
                        <?endif?>

                        <?if($arResult["PHONE_REGISTRATION"]):?>
                            <tr>
                                <td><?if($arResult["PHONE_REQUIRED"]):?><span class="starrequired">*</span><?endif?><?echo GetMessage("main_register_phone_number")?></td>
                                <td><input type="text" name="USER_PHONE_NUMBER" maxlength="255" value="<?=$arResult["USER_PHONE_NUMBER"]?>" class="bx-auth-input" /></td>
                            </tr>
                        <?endif?>

                        <?// ********************* User properties ***************************************************?>
                        <?if($arResult["USER_PROPERTIES"]["SHOW"] == "Y"):?>
                            <tr><td colspan="2"><?=strlen(trim($arParams["USER_PROPERTY_NAME"])) > 0 ? $arParams["USER_PROPERTY_NAME"] : GetMessage("USER_TYPE_EDIT_TAB")?></td></tr>
                            <?foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):?>
                                <tr><td><?if ($arUserField["MANDATORY"]=="Y"):?><span class="starrequired">*</span><?endif;
                                        ?><?=$arUserField["EDIT_FORM_LABEL"]?>:</td><td>
                                        <?$APPLICATION->IncludeComponent(
                                            "bitrix:system.field.edit",
                                            $arUserField["USER_TYPE"]["USER_TYPE_ID"],
                                            array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField, "form_name" => "bform"), null, array("HIDE_ICONS"=>"Y"));?></td></tr>
                            <?endforeach;?>
                        <?endif;?>
                        <?// ******************** /User properties ***************************************************

                        // CAPTCHA
                        if ($arResult["USE_CAPTCHA"] == "Y")
                        {
                            ?>
                            <tr>
                                <td colspan="2"><b><?=GetMessage("CAPTCHA_REGF_TITLE")?></b></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
                                    <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
                                </td>
                            </tr>
                            <tr>
                                <td><span class="starrequired">*</span><?=GetMessage("CAPTCHA_REGF_PROMT")?>:</td>
                                <td><input type="text" name="captcha_word" maxlength="50" value="" autocomplete="off" /></td>
                            </tr>
                            <?
                        }
                        // CAPTCHA
                        ?>
                        <tr>
                            <td></td>
                            <td>
                                <?$APPLICATION->IncludeComponent("bitrix:main.userconsent.request", "",
                                    array(
                                        "ID" => COption::getOptionString("main", "new_user_agreement", ""),
                                        "IS_CHECKED" => "Y",
                                        "AUTO_SAVE" => "N",
                                        "IS_LOADED" => "Y",
                                        "ORIGINATOR_ID" => $arResult["AGREEMENT_ORIGINATOR_ID"],
                                        "ORIGIN_ID" => $arResult["AGREEMENT_ORIGIN_ID"],
                                        "INPUT_NAME" => $arResult["AGREEMENT_INPUT_NAME"],
                                        "REPLACE" => array(
                                            "button_caption" => GetMessage("AUTH_REGISTER"),
                                            "fields" => array(
                                                rtrim(GetMessage("AUTH_NAME"), ":"),
                                                rtrim(GetMessage("AUTH_LAST_NAME"), ":"),
                                                rtrim(GetMessage("AUTH_LOGIN_MIN"), ":"),
                                                rtrim(GetMessage("AUTH_PASSWORD_REQ"), ":"),
                                                rtrim(GetMessage("AUTH_EMAIL"), ":"),
                                            )
                                        ),
                                    )
                                );?>
                            </td>
                        </tr>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td></td>
                            <td><input type="submit" name="Register" value="<?=GetMessage("AUTH_REGISTER")?>" /></td>
                        </tr>
                        </tfoot>
                    </table>

                </form>
            </div>*/?>
            <?/* Регистрация Розничнго конец */?>

            <p><?echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"];?></p>
            <p><span class="starrequired">*</span><?=GetMessage("AUTH_REQ")?></p>

            <p><a href="<?=$arResult["AUTH_AUTH_URL"]?>" rel="nofollow"><b><?=GetMessage("AUTH_AUTH")?></b></a></p>

            <script type="text/javascript">
                document.bform.USER_NAME.focus();
            </script>

        <?endif?>

        </noindex>

    </div>

    <div class="top-info__heart-dots heart-dots auth-dot-top"></div>
    <div class="heart-dots auth-dot-bottom"></div>

</div>
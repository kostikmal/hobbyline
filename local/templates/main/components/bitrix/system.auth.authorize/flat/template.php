<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponent $component
 */

//one css for all system.auth.* forms
$APPLICATION->SetAdditionalCSS("/bitrix/css/main/system.auth/flat/style.css");
?>

<div class="main-content auth-wrap">
    <div class="auth-form">

    <?
    if(!empty($arParams["~AUTH_RESULT"])):
        $text = str_replace(array("<br>", "<br />"), "\n", $arParams["~AUTH_RESULT"]["MESSAGE"]);
    ?>
        <div class="alert alert-danger"><?=nl2br(htmlspecialcharsbx($text))?></div>
    <?endif?>

    <?
    if($arResult['ERROR_MESSAGE'] <> ''):
        $text = str_replace(array("<br>", "<br />"), "\n", $arResult['ERROR_MESSAGE']);
    ?>
        <div class="alert alert-danger"><?=nl2br(htmlspecialcharsbx($text))?></div>
    <?endif?>

        <h2 class="auth-form__label"><?=GetMessage("AUTH_PLEASE_AUTH")?></h2>

    <?/*if($arResult["AUTH_SERVICES"]):?>
    <?
    $APPLICATION->IncludeComponent("bitrix:socserv.auth.form",
        "flat",
        array(
            "AUTH_SERVICES" => $arResult["AUTH_SERVICES"],
            "AUTH_URL" => $arResult["AUTH_URL"],
            "POST" => $arResult["POST"],
        ),
        $component,
        array("HIDE_ICONS"=>"Y")
    );
    ?>

        <hr class="bxe-light">
    <?endif*/?>

        <form name="form_auth" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">

            <input type="hidden" name="AUTH_FORM" value="Y" />
            <input type="hidden" name="TYPE" value="AUTH" />
    <?if (strlen($arResult["BACKURL"]) > 0):?>
            <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
    <?endif?>
    <?foreach ($arResult["POST"] as $key => $value):?>
            <input type="hidden" name="<?=$key?>" value="<?=$value?>" />
    <?endforeach?>

            <div class="auth-form__text">
                <input type="text" id="email-input" placeholder="<?=GetMessage("AUTH_LOGIN")?>*" name="USER_LOGIN" maxlength="255" value="<?=$arResult["LAST_LOGIN"]?>" >
            </div>

            <div class="auth-form__text auth-form__password">
                <input type="password" id="password-input" placeholder="<?=GetMessage("AUTH_PASSWORD")?>*" name="USER_PASSWORD" maxlength="255" autocomplete="off">
                <picture>
                    <source srcset="<?=SITE_TEMPLATE_PATH?>/img/ico/eye-pass.svg" type="image/webp">
                    <img src="<?=SITE_TEMPLATE_PATH?>/img/ico/eye-pass.svg" class="auth-form__password-control" alt="">
                </picture>
            </div>

            <div class="remember">
                <div>
                    <input type="checkbox" class="custom-checkbox" id="USER_REMEMBER" name="USER_REMEMBER" value="Y" >
                    <label for="remember" class="footer__label remember__label"><?=GetMessage("AUTH_REMEMBER_ME")?></label>
                </div>
                <a href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>" class="remember__recovery"><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></a>
            </div>

            <a href="#" class="link-half lookbook__link auth-form__login">
                <?=GetMessage("AUTH_AUTHORIZE")?>
                <picture>
                    <img src="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" class="btn-arrow-cat" alt="">
                </picture>
            </a>

            <input type="submit" class="btn btn-primary login_submit" name="Login" value="<?=GetMessage("AUTH_AUTHORIZE")?>" style="display: none;" />

            <p class="auth-form__reg">
                или <a href="<?=$arResult["AUTH_REGISTER_URL"]?>"><?=GetMessage("AUTH_REGISTER")?></a>
            </p>

        </form>

    </div>

    <div class="top-info__heart-dots heart-dots auth-dot-top"></div>
    <div class="heart-dots auth-dot-bottom"></div>

</div>

<script type="text/javascript">
<?if (strlen($arResult["LAST_LOGIN"])>0):?>
try{document.form_auth.USER_PASSWORD.focus();}catch(e){}
<?else:?>
try{document.form_auth.USER_LOGIN.focus();}catch(e){}
<?endif?>

    $(document).on('click', '.auth-form__login', function (e) {
        e.preventDefault();
        $('.login_submit').click();
    });

</script>
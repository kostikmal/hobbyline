<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
//***********************************
//status and unsubscription/activation section
//***********************************
?>

<div class="lk-subscribe__item">

    <form action="<?= $arResult["FORM_ACTION"] ?>" method="get">

        <input type="hidden" name="ID" value="<? echo $arResult["SUBSCRIPTION"]["ID"]; ?>"/>
        <? echo bitrix_sessid_post(); ?>

        <div class="button r" id="button-1">

            <input
                type="checkbox"
                class="checkbox news_subscribe_to_change"
                <?=($arResult["SUBSCRIPTION"]["ACTIVE"]=="Y")?'checked=""':''?>
            >

            <div class="knobs"></div>
            <div class="layer"></div>
        </div>

        <div style="display: none;" class="news_subscribe_change_submit">
            <? if ($arResult["SUBSCRIPTION"]["ACTIVE"] == "Y"): ?>
                <input type="submit" name="unsubscribe" value="<? echo GetMessage("subscr_unsubscr") ?>"/>
                <input type="hidden" name="action" value="unsubscribe"/>
            <? else: ?>
                <input type="submit" name="activate" value="<? echo GetMessage("subscr_activate") ?>"/>
                <input type="hidden" name="action" value="activate"/>
            <? endif; ?>
        </div>

        <div class="lk-subscribe__text">Подписка на новые статьи</div>

    </form>

</div>
<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);

if (empty($arResult["ALL_ITEMS"]))
	return;

//CUtil::InitJSCore();

$menuBlockId = "catalog_menu_".$this->randString();
?>

  <div class="header__fixed" id="fixedHeader" uk-scrollspy="cls: uk-animation-slide-top; target: div; delay: 100; repeat: true">
    <div class="sticky">
      <a href="index.html"><picture><source srcset="<?=SITE_TEMPLATE_PATH?>/img/ico/logo-fixed-header.svg" type="image/webp">
      <img src="<?=SITE_TEMPLATE_PATH?>/img/ico/logo-fixed-header.svg" >
      </picture></a>
      <nav class="nav nav-fixed">
        <ul class="nav__list">
		<?foreach($arResult["MENU_STRUCTURE"] as $itemID => $arColumns):?>     <!-- first level-->
			<?$existPictureDescColomn = ($arResult["ALL_ITEMS"][$itemID]["PARAMS"]["picture_src"] || $arResult["ALL_ITEMS"][$itemID]["PARAMS"]["description"]) ? true : false;?>
			<li class="nav__item" >
				<a class="nav__link" href="<?=$arResult["ALL_ITEMS"][$itemID]["LINK"]?>"><?=htmlspecialcharsbx($arResult["ALL_ITEMS"][$itemID]["TEXT"])?>
				</a>
			<?if (is_array($arColumns) && count($arColumns) > 0):?>
				<div uk-dropdown class="nav__dropdown dropdown">
					<?foreach($arColumns as $key=>$arRow):?>
						<ul class="dropdown__list">
						<?foreach($arRow as $itemIdLevel_2=>$arLevel_3):?>  <!-- second level-->
							<li class="dropdown__item">
								<a class="dropdown__link"
									href="<?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["LINK"]?>"
									<?if ($existPictureDescColomn):?>
										onmouseover="window.obj_<?=$menuBlockId?> && obj_<?=$menuBlockId?>.changeSectionPicure(this, '<?=$itemIdLevel_2?>');"
									<?endif?>
									data-picture="<?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["PARAMS"]["picture_src"]?>"
									<?if($arResult["ALL_ITEMS"][$itemIdLevel_2]["SELECTED"]):?>class="bx-active"<?endif?>
								>
									<span class="bx-nav-2-lvl-link-text"><?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["TEXT"]?></span>
								</a> 
							<?if (is_array($arLevel_3) && count($arLevel_3) > 0):?>
								<ul class="bx-nav-list-3-lvl">
								<?foreach($arLevel_3 as $itemIdLevel_3):?>	<!-- third level-->
									<li class="bx-nav-3-lvl">
										<a
											class="bx-nav-3-lvl-link"
											href="<?=$arResult["ALL_ITEMS"][$itemIdLevel_3]["LINK"]?>"
											<?if ($existPictureDescColomn):?>
												onmouseover="window.obj_<?=$menuBlockId?> && obj_<?=$menuBlockId?>.changeSectionPicure(this, '<?=$itemIdLevel_3?>');return false;"
											<?endif?>
											data-picture="<?=$arResult["ALL_ITEMS"][$itemIdLevel_3]["PARAMS"]["picture_src"]?>"
											<?if($arResult["ALL_ITEMS"][$itemIdLevel_3]["SELECTED"]):?>class="bx-active"<?endif?>
										>
											<span class="bx-nav-3-lvl-link-text"><?=$arResult["ALL_ITEMS"][$itemIdLevel_3]["TEXT"]?></span>
										</a>
									</li>
								<?endforeach;?>
								</ul>
							<?endif?>
							</li>
						<?endforeach;?>
						</ul>
					<?endforeach;?>
					<?if ($existPictureDescColomn):?>
						<div class="bx-nav-list-2-lvl bx-nav-catinfo dbg" data-role="desc-img-block">
							<a class="bx-nav-2-lvl-link-image" href="<?=$arResult["ALL_ITEMS"][$itemID]["LINK"]?>">
								<img src="<?=$arResult["ALL_ITEMS"][$itemID]["PARAMS"]["picture_src"]?>" alt="">
							</a>
							<p><?=$arResult["ALL_ITEMS"][$itemID]["PARAMS"]["description"]?></p>
						</div>
					<?endif?>
				</div>
			<?endif?>
			</li>
		<?endforeach;?>
		</ul>
	</nav>
    
        <div class="header__icon-nav icon-nav icon-nav__fixed">
        <a href="#" class="icon-nav__item"><picture><source srcset="img/ico/call.svg" type="image/webp">
        <?$APPLICATION->IncludeComponent(
        "bitrix:main.include",
        "",
        array(
            "AREA_FILE_SHOW" => "file",
            "PATH" => SITE_TEMPLATE_PATH."/img/ico/call.svg"),
        false
        );?>
        </picture>
          <div uk-dropdown="pos: bottom-justify" class="icon-nav__dropdown uk-text-center dropdown">
            Обратная связь
          </div>
        </a>
        <a href="/personal/subscribe/" class="icon-nav__item"><picture><source srcset="img/ico/like.svg" type="image/webp">
                <?$APPLICATION->IncludeComponent(
        "bitrix:main.include",
        "",
        array(
            "AREA_FILE_SHOW" => "file",
            "PATH" => SITE_TEMPLATE_PATH."/img/ico/like.svg"),
        false
        );?>
        </picture>
          <div uk-dropdown="pos: bottom-justify" class="icon-nav__dropdown uk-text-center dropdown">
            Избранное
          </div>
        </a>
      <a href="/personal/" class="icon-nav__item"><picture><source srcset="img/ico/profile.svg" type="image/webp">
                      <?$APPLICATION->IncludeComponent(
        "bitrix:main.include",
        "",
        array(
            "AREA_FILE_SHOW" => "file",
            "PATH" => SITE_TEMPLATE_PATH."/img/ico/profile.svg"),
        false
        );?>
      </picture>
          <div uk-dropdown="pos: bottom-right" class="icon-nav__dropdown uk-text-center dropdown">
            Личный кабинет
            <!-- <div class="icon-nav__dropdown-wrap">
            <a href="#">Войти</a> или <a href="#">зарегистрироваться</a>
          </div> -->
          </div>
        </a>
        <a href="/personal/cart/" class="icon-nav__item basket-count"><picture><source srcset="img/ico/basket.svg" type="image/webp">
        <?$APPLICATION->IncludeComponent(
        "bitrix:main.include",
        "",
        array(
            "AREA_FILE_SHOW" => "file",
            "PATH" => SITE_TEMPLATE_PATH."/img/ico/basket.svg"),
        false
        );?>

        </picture>
          <div uk-dropdown="pos: bottom-justify" class="icon-nav__dropdown uk-text-center dropdown">
          Корзина
          </div>
          <span class="basket-count__counter">          <?
          if (!CModule::IncludeModule("sale")) return;
          $cntBasketItems = CSaleBasket::GetList(
           array(),
           array( 
              "FUSER_ID" => CSaleBasket::GetBasketUserID(),
              "LID" => "s1",
              "ORDER_ID" => "NULL"
           ), 
           array()
        );
        echo $cntBasketItems;
        ?></span>
        </a>
        <span class="icon-nav__sum">
        <?
        if (!CModule::IncludeModule("sale")) return;

      
      if (CModule::IncludeModule("sale"))
      {

         $arBasketItems = array();

         $dbBasketItems = CSaleBasket::GetList(
                 array(
                         "NAME" => "ASC",
                         "ID" => "ASC"
                     ),
                 array(
                         "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                         "LID" => SITE_ID,
                         "ORDER_ID" => "NULL"
                     ),
                 false,
                 false,
                 array("ID", "QUANTITY", "PRICE")
             );
         while ($arItems = $dbBasketItems->Fetch())
         {
             if (strlen($arItems["CALLBACK_FUNC"]) > 0)
             {
                 CSaleBasket::UpdatePrice($arItems["ID"], 
                                          $arItems["QUANTITY"]);
                 $arItems = CSaleBasket::GetByID($arItems["ID"]);
             }
         
             $arBasketItems[] = $arItems;
         }
         
         $summ = 0;
         
         for ($i=0;$i<=$arResult["NUM_PRODUCTS"];$i++){      
            
            $summ = $summ + $arBasketItems[$i]["PRICE"]*$arBasketItems[$i]["QUANTITY"];
            
         }
   
      }
      echo $summ;

      ?>
 руб.</span>
      </div>
      <a href="#" class="mob-search"><picture><source srcset="<?=SITE_TEMPLATE_PATH?>/img/ico/search-mob.svg" type="image/webp">
      <img src="<?=SITE_TEMPLATE_PATH?>/img/ico/search-mob.svg">

        
      </picture></a>
    </div>
</div>

<script>
	/*BX.ready(function () {
		window.obj_<?=$menuBlockId?> = new BX.Main.MenuComponent.CatalogHorizontal('<?=CUtil::JSEscape($menuBlockId)?>', <?=CUtil::PhpToJSObject($arResult["ITEMS_IMG_DESC"])?>);
	});*/
</script>
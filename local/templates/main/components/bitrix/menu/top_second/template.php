<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arImg = [
    '/' => 'm-main',
    '/catalog/' => 'm-catalog',
    '/personal/cart/' => 'basket',
    '/personal/' => 'like',
    '/personal/subscribe/' => 'profile',
];

if (!empty($arResult)):?>
    <section class="mb-menu"><?
        foreach ($arResult as $arItem) {
            if ($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
                continue;

            $sActiveClass = '';
            $sBasketClass = '';
            if ($arItem["SELECTED"]) {
                $sClass = 'mb-menu__active';
            }
            if ( $arItem["LINK"] == '/personal/cart/' ) {
                $sBasketClass = 'basket-count';
            }

            ?><a href="<?= $arItem["LINK"] ?>" class="mb-menu__item <?=$sActiveClass?> <?=$sBasketClass?>">
                <picture>
                    <source srcset="<?= SITE_TEMPLATE_PATH ?>/img/ico/<?= $arImg[$arItem["LINK"]] ?>.svg" type="image/webp">
                    <img class="img-svg" src="<?= SITE_TEMPLATE_PATH ?>/img/ico/<?= $arImg[$arItem["LINK"]] ?>.svg" alt="">
                </picture>
                <span><?= $arItem["TEXT"] ?></span><?

                if ( $sBasketClass != '' ) {
                    ?><span class="basket-count__counter"><?
                        if (!CModule::IncludeModule("sale")) return;
                        $cntBasketItems = CSaleBasket::GetList(
                            array(),
                            array(
                                "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                                "LID" => "s1",
                                "ORDER_ID" => "NULL"
                            ),
                            array()
                        );
                        echo $cntBasketItems;
                    ?></span><?
                }

            ?></a><?
        }
    ?></section>
<? endif ?>
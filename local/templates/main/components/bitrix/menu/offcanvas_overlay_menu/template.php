<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!empty($arResult)) {

?><nav class="nav-left">
    <ul class="uk-nav-default uk-nav-parent-icon" uk-nav="multiple: true"><?

    $previousLevel = 0;
    foreach($arResult as $arItem) {

        if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel) {
            echo str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));
        }

        if ($arItem["IS_PARENT"]) {

            if ($arItem["DEPTH_LEVEL"] == 1) {
            ?><li class="uk-parent nav-left__li-parent">
                <a href="<?=$arItem["LINK"]?>" class="<?if ($arItem["SELECTED"]):?>root-item-selected<?endif?>">
                    <?=$arItem["TEXT"]?>
                </a>
                <ul class="uk-nav-sub"><?
            }
            else {
                ?><li class="uk-parent nav-left__li-parent <?if ($arItem["SELECTED"]):?> item-selected<?endif?>" >
                    <a href="<?=$arItem["LINK"]?>" class="parent">
                        <?=$arItem["TEXT"]?>
                    </a>
                    <ul class="uk-nav-sub"><?
            }

        }
        else {

            if ($arItem["PERMISSION"] > "D") {

                if ($arItem["DEPTH_LEVEL"] == 1) {
                    ?><li class="nav-left__li-parent">
                        <a href="<?=$arItem["LINK"]?>" class="<?if ($arItem["SELECTED"]):?>root-item-selected<?endif?>">
                            <?=$arItem["TEXT"]?>
                        </a>
                    </li><?
                }
                else {
                    ?><li class=" <?if ($arItem["SELECTED"]):?> item-selected<?endif?>">
                        <a href="<?=$arItem["LINK"]?>" class="">
                            <?=$arItem["TEXT"]?>
                        </a>
                    </li><?
                }

            }
            else {

                if ($arItem["DEPTH_LEVEL"] == 1) {
                    ?><li class="nav-left__li-parent">
                        <a href="" class="<?if ($arItem["SELECTED"]):?>root-item-selected<?endif?>" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>">
                            <?=$arItem["TEXT"]?>
                        </a>
                    </li><?
                }
                else {
                    ?><li class="">
                        <a href="" class="denied" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>">
                            <?=$arItem["TEXT"]?>
                        </a>
                    </li><?
                }

            }

        }

        $previousLevel = $arItem["DEPTH_LEVEL"];

    }

    if ($previousLevel > 1) {//close last item tags
        echo str_repeat("</ul></li>", ($previousLevel-1) );
    }

    ?></ul>
</nav><?
}
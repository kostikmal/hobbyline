<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<nav class="nav">
<ul class="nav__list">

<?
$previousLevel = 0;
foreach($arResult as $arItem):?>

	<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
		<?=str_repeat("</ul></div></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
	<?endif?>

	<?if ($arItem["IS_PARENT"]):?>

		<?if ($arItem["DEPTH_LEVEL"] == 1):?>
			<li class="nav__item">
                <a href="<?=$arItem["LINK"]?>" class="<?if ($arItem["SELECTED"]):?>nav__link selected<?else:?>nav__link<?endif?>">
                    <?=$arItem["TEXT"]?>
                </a>
                <div uk-dropdown class="nav__dropdown dropdown">
				    <ul class="dropdown__list">
		<?else:?>
			<li<?if ($arItem["SELECTED"]):?> class="item-selected"<?endif?>>
                <a href="<?=$arItem["LINK"]?>" class="nav__link">
                    <?=$arItem["TEXT"]?>
                </a>
                <div uk-dropdown class="nav__dropdown dropdown">
				    <ul class="dropdown__list">
		<?endif?>

	<?else:?>

		<?if ($arItem["PERMISSION"] > "D"):?>

			<?if ($arItem["DEPTH_LEVEL"] == 1):?>
				<li class="nav__item">
                    <a href="<?=$arItem["LINK"]?>" class="<?if ($arItem["SELECTED"]):?>nav__link selected<?else:?>nav__link<?endif?>">
                        <?=$arItem["TEXT"]?>
                    </a>
                </li>
			<?else:?>
				<li<?if ($arItem["SELECTED"]):?> class="dropdown__item selected"<?else:?> class="dropdown__item"<?endif?>>
                    <a href="<?=$arItem["LINK"]?>" class="dropdown__link">
                        <?=$arItem["TEXT"]?>
                    </a>
                </li>
			<?endif?>

		<?else:?>

			<?if ($arItem["DEPTH_LEVEL"] == 1):?>
				<li class="nav__item">
                    <a href="" class="<?if ($arItem["SELECTED"]):?>nav__link selected<?else:?>nav__link<?endif?>" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>">
                        <?=$arItem["TEXT"]?>
                    </a>
                </li>
			<?else:?>
				<li class="dropdown__item">
                    <a href="" class="denied dropdown__link" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>">
                        <?=$arItem["TEXT"]?>
                    </a>
                </li>
			<?endif?>

		<?endif?>

	<?endif?>

	<?$previousLevel = $arItem["DEPTH_LEVEL"];?>

<?endforeach?>

<?if ($previousLevel > 1)://close last item tags?>
	<?=str_repeat("</ul></li>", ($previousLevel-1) );?>
<?endif?>

</ul>
</nav>

<?endif?>
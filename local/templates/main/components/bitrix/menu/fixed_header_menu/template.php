<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!empty($arResult)) {

?><nav class="nav nav-fixed">
    <ul class="nav__list"><?

    $previousLevel = 0;
    foreach($arResult as $arItem) {

        if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel) {
            echo str_repeat("</ul></div></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));
        }

        if ($arItem["IS_PARENT"]) {

            if ($arItem["DEPTH_LEVEL"] == 1) {
                ?><li class="nav__item">
                    <a href="<?=$arItem["LINK"]?>" class="<?if ($arItem["SELECTED"]):?>nav__link root-item-selected<?else:?>nav__link<?endif?>">
                        <?=$arItem["TEXT"]?>
                    </a>
                <div uk-dropdown class="nav__dropdown dropdown">
                    <ul class="dropdown__list"><?
            }
            else {
                ?><li class="nav__item <?if ($arItem["SELECTED"]):?> item-selected<?endif?>" >
                        <a href="<?=$arItem["LINK"]?>" class="parent">
                            <?=$arItem["TEXT"]?>
                        </a>
                <div uk-dropdown class="nav__dropdown dropdown">
                    <ul class="dropdown__list"><?
            }

        }
        else {

            if ($arItem["PERMISSION"] > "D") {

                if ($arItem["DEPTH_LEVEL"] == 1) {
                    ?><li class="nav__item">
                        <a href="<?=$arItem["LINK"]?>" class="<?if ($arItem["SELECTED"]):?>nav__link root-item-selected<?else:?>nav__link<?endif?>">
                            <?=$arItem["TEXT"]?>
                        </a>
                    </li><?
                }
                else {
                    ?><li class="dropdown__item <?if ($arItem["SELECTED"]):?> item-selected<?endif?>">
                        <a href="<?=$arItem["LINK"]?>" class="dropdown__link">
                            <?=$arItem["TEXT"]?>
                        </a>
                    </li><?
                }

            }
            else {

                if ($arItem["DEPTH_LEVEL"] == 1) {
                    ?><li class="nav__item">
                        <a href="" class="<?if ($arItem["SELECTED"]):?>nav__link root-item-selected<?else:?>nav__link<?endif?>" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>">
                            <?=$arItem["TEXT"]?>
                        </a>
                    </li><?
                }
                else {
                    ?><li class="dropdown__item">
                        <a href="" class="dropdown__link denied" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>">
                            <?=$arItem["TEXT"]?>
                        </a>
                    </li><?
                }

            }

        }

        $previousLevel = $arItem["DEPTH_LEVEL"];

    }

    if ($previousLevel > 1) {//close last item tags
        echo str_repeat("</ul></div></li>", ($previousLevel-1) );
   }

   ?></ul>
</nav><?
}
<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();
$HlBlockId = 9;

global $USER;

$arResult['PROPERTIES']['LINKED_ELEMENTS']['ELEMENTS'] = [];

if ( is_array($arResult['PROPERTIES']['LINKED_ELEMENTS']['VALUE']) ) {
    $arHLColorFilter = [];
    $res = CIBlockElement::GetList(
        ["SORT"=>"ASC"],
        ['IBLOCK_ID'=>$arParams["IBLOCK_ID"], 'ACTIVE'=>'Y', 'ID'=>$arResult['PROPERTIES']['LINKED_ELEMENTS']['VALUE']],
        false,
        false,
        ['ID', 'IBLOCK_ID', 'CODE', 'NAME', 'DETAIL_PICTURE', 'PROPERTY_COLOR']
    );

    while ( $arRes = $res->GetNext() ) {
        $arResult['PROPERTIES']['LINKED_ELEMENTS']['ELEMENTS'][ $arRes['PROPERTY_COLOR_VALUE'] ] = [
            'NAME' => $arRes['NAME'],
            'LINK' => '/catalog/products/'.$arRes['CODE'].'/',
            'PICTURE' => CFile::ResizeImageGet( $arRes['DETAIL_PICTURE'], ['width'=>77, 'height'=>95], BX_RESIZE_IMAGE_PROPORTIONAL )['src'],
            'COLOR' => $arRes['PROPERTY_COLOR_VALUE'],
        ];
        $arHLColorFilter[] = $arRes['PROPERTY_COLOR_VALUE'];
    }

    $hlblock = Bitrix\Highloadblock\HighloadBlockTable::getById($HlBlockId)->fetch();
    $entity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
    $entityDataClass = $entity->getDataClass();

    $tempObD = $entityDataClass::GetList( [ 'select' => ['ID', 'UF_NAME', 'UF_XML_ID', 'UF_XML_1C'], 'filter' => ['UF_XML_ID'=>$arHLColorFilter] ] );
    while ($tempResD = $tempObD->fetch()) {
        $arResult['PROPERTIES']['LINKED_ELEMENTS']['ELEMENTS'][ $tempResD['UF_XML_ID'] ]['COLOR'] = $tempResD['UF_NAME'];
    }
}

if ( !empty($arResult['OFFERS']) ) {
    foreach ( $arResult['OFFERS'] as $arOffer ) {
        if ( $arOffer['CAN_BUY'] ) {
            $actualItem = $arOffer;
            break;
        }
    }
    if ( !isset($actualItem) ) {
        $actualItem = reset($arResult['OFFERS']);
    }
}
else {
    $actualItem = $arResult;
}

/*$db_res = CPrice::GetList(
    [],
    ["PRODUCT_ID" => $actualItem['ID']]
);
while ($ar_res = $db_res->Fetch()) {
    file_put_contents( $_SERVER['DOCUMENT_ROOT'].'/xxx_element_price.txt', PHP_EOL.print_r($ar_res, true), FILE_APPEND );
}*/

//file_put_contents( $_SERVER['DOCUMENT_ROOT'].'/xxx_element_catalog.txt', print_r($arResult, true) );

// если есть цена по акции
/*
if ( $arResult['ITEM_PRICES'][0]['PRICE_TYPE_ID'] == 4 ) {
    $allProductPrices = \Bitrix\Catalog\PriceTable::getList([
        "select" => ["*"],
        "filter" => [
            "=PRODUCT_ID" => $actualItem['ID'],
            "=CATALOG_GROUP_ID" => 1,
        ],
        "order" => ["CATALOG_GROUP_ID" => "ASC"]
    ])->fetchAll();

    if ( $allProductPrices[0]['PRICE'] != '' ) {
        $arResult['ITEM_PRICES'][1] = [
            'PRINT_RATIO_BASE_PRICE' => $allProductPrices[0]['PRICE'].' руб.',
            'PRICE' => $allProductPrices[0]['PRICE'],
        ];
    }
}
*/

foreach ( $arResult['SKU_PROPS'] as $sPropCode=>&$arSkuProp ) {
    foreach ( $arSkuProp['VALUES'] as $arValue ) {
        $arSkuProp['VALUES_BY_CODE'][ $arValue['XML_ID'] ] = $arValue;
    }
}

$arResult['FAVORITE_PRODUCTS'] = [];

if ( $USER->IsAuthorized() ) {
    $nUserID = $USER->GetID();

    $res = CIBlockElement::GetList(
        ["SORT"=>"ASC"],
        ['IBLOCK_ID'=>FAVORITE_PRODUCTS, 'ACTIVE'=>'Y', 'PROPERTY_USER'=>$nUserID],
        false,
        false,
        ['ID', 'PROPERTY_PRODUCTS']
    );

    if ( $arRes = $res->GetNext() ) {
        $arResult['FAVORITE_PRODUCTS'] = $arRes['PROPERTY_PRODUCTS_VALUE'];
    }
}

if ( !is_array( $arResult['PROPERTIES']['MORE_PHOTO']['VALUE'] ) && intval( $arResult['DETAIL_PICTURE']['ID'] ) > 0 ) {
    $arResult['PROPERTIES']['MORE_PHOTO']['VALUE'][] = $arResult['DETAIL_PICTURE']['ID'];
}

/*if ( is_array( $arResult['PROPERTIES']['MORE_PHOTO']['VALUE'] ) && intval( $arResult['DETAIL_PICTURE']['ID'] ) > 0 ) {
    array_unshift( $arResult['PROPERTIES']['MORE_PHOTO']['VALUE'], $arResult['DETAIL_PICTURE']['ID'] );
}*/

if ( count($arResult['PROPERTIES']['MORE_PHOTO']['VALUE']) > 0 && is_array( $arResult['PROPERTIES']['MORE_PHOTO']['VALUE'] ) ) {
    foreach ( $arResult['PROPERTIES']['MORE_PHOTO']['VALUE'] as $nPictID ) {
        $arResult['PROPERTIES']['MORE_PHOTO']['DISPLAY_SRC'][] = CFile::ResizeImageGet( $nPictID, ['width'=>469, 'height'=>668], BX_RESIZE_IMAGE_PROPORTIONAL )['src'];
        $arResult['PROPERTIES']['MORE_PHOTO']['DISPLAY_SMALL_SRC'][] = CFile::ResizeImageGet( $nPictID, ['width'=>128, 'height'=>180], BX_RESIZE_IMAGE_PROPORTIONAL )['src'];
        $arResult['PROPERTIES']['MORE_PHOTO']['DISPLAY_BIG_SRC'][] = CFile::GetPath( $nPictID );
    }
}
else {
    $arResult['PROPERTIES']['MORE_PHOTO']['DISPLAY_SRC'][] = SITE_TEMPLATE_PATH.'/img/main/hl_prod_no_image.png';
    $arResult['PROPERTIES']['MORE_PHOTO']['DISPLAY_SMALL_SRC'][] = SITE_TEMPLATE_PATH.'/img/main/hl_prod_no_image.png';
    $arResult['PROPERTIES']['MORE_PHOTO']['DISPLAY_BIG_SRC'][] = SITE_TEMPLATE_PATH.'/img/main/hl_prod_no_image.png';
}
?>
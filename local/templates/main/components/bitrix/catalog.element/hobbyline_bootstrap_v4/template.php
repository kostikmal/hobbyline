<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;

Asset::getInstance()->addString('<script src="https://yastatic.net/share2/share.js" async></script>');
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/fancybox_4012.css");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/fancybox_4012.js");

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);

$templateLibrary = array('popup', 'fx');
$currencyList = '';

global $USER;

if (!empty($arResult['CURRENCIES']))
{
	$templateLibrary[] = 'currency';
	$currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}

$templateData = array(
	'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
	'TEMPLATE_LIBRARY' => $templateLibrary,
	'CURRENCIES' => $currencyList,
	'ITEM' => array(
		'ID' => $arResult['ID'],
		'IBLOCK_ID' => $arResult['IBLOCK_ID'],
		'OFFERS_SELECTED' => $arResult['OFFERS_SELECTED'],
		'JS_OFFERS' => $arResult['JS_OFFERS']
	)
);
unset($currencyList, $templateLibrary);

$mainId = $this->GetEditAreaId($arResult['ID']);
$itemIds = array(
	'ID' => $mainId,
	'DISCOUNT_PERCENT_ID' => $mainId.'_dsc_pict',
	'STICKER_ID' => $mainId.'_sticker',
	'BIG_SLIDER_ID' => $mainId.'_big_slider',
	'BIG_IMG_CONT_ID' => $mainId.'_bigimg_cont',
	'SLIDER_CONT_ID' => $mainId.'_slider_cont',
	'OLD_PRICE_ID' => $mainId.'_old_price',
	'PRICE_ID' => $mainId.'_price',
	'DISCOUNT_PRICE_ID' => $mainId.'_price_discount',
	'PRICE_TOTAL' => $mainId.'_price_total',
	'SLIDER_CONT_OF_ID' => $mainId.'_slider_cont_',
	'QUANTITY_ID' => $mainId.'_quantity',
	'QUANTITY_DOWN_ID' => $mainId.'_quant_down',
	'QUANTITY_UP_ID' => $mainId.'_quant_up',
	'QUANTITY_MEASURE' => $mainId.'_quant_measure',
	'QUANTITY_LIMIT' => $mainId.'_quant_limit',
	'BUY_LINK' => $mainId.'_buy_link',
	'ADD_BASKET_LINK' => $mainId.'_add_basket_link',
	'BASKET_ACTIONS_ID' => $mainId.'_basket_actions',
	'NOT_AVAILABLE_MESS' => $mainId.'_not_avail',
	'COMPARE_LINK' => $mainId.'_compare_link',
	'TREE_ID' => $mainId.'_skudiv',
	'DISPLAY_PROP_DIV' => $mainId.'_sku_prop',
	'DISPLAY_MAIN_PROP_DIV' => $mainId.'_main_sku_prop',
	'OFFER_GROUP' => $mainId.'_set_group_',
	'BASKET_PROP_DIV' => $mainId.'_basket_prop',
	'SUBSCRIBE_LINK' => $mainId.'_subscribe',
	'TABS_ID' => $mainId.'_tabs',
	'TAB_CONTAINERS_ID' => $mainId.'_tab_containers',
	'SMALL_CARD_PANEL_ID' => $mainId.'_small_card_panel',
	'TABS_PANEL_ID' => $mainId.'_tabs_panel'
);
$obName = $templateData['JS_OBJ'] = 'ob'.preg_replace('/[^a-zA-Z0-9_]/', 'x', $mainId);
$name = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
	: $arResult['NAME'];
$title = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE']
	: $arResult['NAME'];
$alt = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT']
	: $arResult['NAME'];

$haveOffers = !empty($arResult['OFFERS']);

if ($haveOffers) {
	/*$actualItem = isset($arResult['OFFERS'][$arResult['OFFERS_SELECTED']])
		? $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]
		: reset($arResult['OFFERS']);*/

	if ( isset($arResult['OFFERS'][$arResult['OFFERS_SELECTED']]) ) {
        $actualItem = $arResult['OFFERS'][$arResult['OFFERS_SELECTED']];
    }
	else {
	    foreach ( $arResult['OFFERS'] as $arOffer ) {
	        if ( $arOffer['CAN_BUY'] ) {
                $actualItem = $arOffer;
                break;
            }
        }
	    if ( !isset($actualItem) ) {
            $actualItem = reset($arResult['OFFERS']);
        }
    }

	$showSliderControls = false;

	foreach ($arResult['OFFERS'] as $offer)
	{
		if ($offer['MORE_PHOTO_COUNT'] > 1)
		{
			$showSliderControls = true;
			break;
		}
	}
}
else {
    $actualItem = $arResult;
	$showSliderControls = $arResult['MORE_PHOTO_COUNT'] > 1;
}

// если есть цена по акции
if ( $actualItem['ITEM_PRICES'][0]['PRICE_TYPE_ID'] == 4 ) {
    $allProductPrices = \Bitrix\Catalog\PriceTable::getList([
        "select" => ["*"],
        "filter" => [
            "=PRODUCT_ID" => $actualItem['ID'],
            "=CATALOG_GROUP_ID" => 1,
        ],
        "order" => ["CATALOG_GROUP_ID" => "ASC"]
    ])->fetchAll();

    if ( $allProductPrices[0]['PRICE'] != '' ) {
        $actualItem['ITEM_PRICES'][1] = [
            'PRINT_RATIO_BASE_PRICE' => $allProductPrices[0]['PRICE'].' руб.',
            'PRICE' => $allProductPrices[0]['PRICE'],
        ];
    }
}

$skuProps = array();
$price = $actualItem['ITEM_PRICES'][$actualItem['ITEM_PRICE_SELECTED']];
$measureRatio = $actualItem['ITEM_MEASURE_RATIOS'][$actualItem['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'];
$showDiscount = $price['PERCENT'] > 0;

$showDescription = !empty($arResult['PREVIEW_TEXT']) || !empty($arResult['DETAIL_TEXT']);
$showBuyBtn = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION']);
$buyButtonClassName = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION_PRIMARY']) ? 'btn-primary' : 'btn-link';
$showAddBtn = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION']);
$showButtonClassName = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION_PRIMARY']) ? 'btn-primary' : 'btn-link';
$showSubscribe = $arParams['PRODUCT_SUBSCRIPTION'] === 'Y' && ($arResult['PRODUCT']['SUBSCRIBE'] === 'Y' || $haveOffers);

$arParams['MESS_BTN_BUY'] = $arParams['MESS_BTN_BUY'] ?: Loc::getMessage('CT_BCE_CATALOG_BUY');
$arParams['MESS_BTN_ADD_TO_BASKET'] = $arParams['MESS_BTN_ADD_TO_BASKET'] ?: Loc::getMessage('CT_BCE_CATALOG_ADD');
$arParams['MESS_NOT_AVAILABLE'] = $arParams['MESS_NOT_AVAILABLE'] ?: Loc::getMessage('CT_BCE_CATALOG_NOT_AVAILABLE');
$arParams['MESS_BTN_COMPARE'] = $arParams['MESS_BTN_COMPARE'] ?: Loc::getMessage('CT_BCE_CATALOG_COMPARE');
$arParams['MESS_PRICE_RANGES_TITLE'] = $arParams['MESS_PRICE_RANGES_TITLE'] ?: Loc::getMessage('CT_BCE_CATALOG_PRICE_RANGES_TITLE');
$arParams['MESS_DESCRIPTION_TAB'] = $arParams['MESS_DESCRIPTION_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_DESCRIPTION_TAB');
$arParams['MESS_PROPERTIES_TAB'] = $arParams['MESS_PROPERTIES_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_PROPERTIES_TAB');
$arParams['MESS_COMMENTS_TAB'] = $arParams['MESS_COMMENTS_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_COMMENTS_TAB');
$arParams['MESS_SHOW_MAX_QUANTITY'] = $arParams['MESS_SHOW_MAX_QUANTITY'] ?: Loc::getMessage('CT_BCE_CATALOG_SHOW_MAX_QUANTITY');
$arParams['MESS_RELATIVE_QUANTITY_MANY'] = $arParams['MESS_RELATIVE_QUANTITY_MANY'] ?: Loc::getMessage('CT_BCE_CATALOG_RELATIVE_QUANTITY_MANY');
$arParams['MESS_RELATIVE_QUANTITY_FEW'] = $arParams['MESS_RELATIVE_QUANTITY_FEW'] ?: Loc::getMessage('CT_BCE_CATALOG_RELATIVE_QUANTITY_FEW');

$positionClassMap = array(
	'left' => 'product-item-label-left',
	'center' => 'product-item-label-center',
	'right' => 'product-item-label-right',
	'bottom' => 'product-item-label-bottom',
	'middle' => 'product-item-label-middle',
	'top' => 'product-item-label-top'
);

$discountPositionClass = 'product-item-label-big';
if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y' && !empty($arParams['DISCOUNT_PERCENT_POSITION']))
{
	foreach (explode('-', $arParams['DISCOUNT_PERCENT_POSITION']) as $pos)
	{
		$discountPositionClass .= isset($positionClassMap[$pos]) ? ' '.$positionClassMap[$pos] : '';
	}
}

$labelPositionClass = 'product-item-label-big';
if (!empty($arParams['LABEL_PROP_POSITION']))
{
	foreach (explode('-', $arParams['LABEL_PROP_POSITION']) as $pos)
	{
		$labelPositionClass .= isset($positionClassMap[$pos]) ? ' '.$positionClassMap[$pos] : '';
	}
}

$themeClass = isset($arParams['TEMPLATE_THEME']) ? ' bx-'.$arParams['TEMPLATE_THEME'] : '';

?>

<div class="bx-catalog-element<?=$themeClass?>" id="<?=$itemIds['ID']?>" itemscope itemtype="http://schema.org/Product">

    <div class="hl-card-top">

        <h1 class="hl-card-top__label">
            <?=$name?>
        </h1>

        <div class="hl-card-top__ico-bar">
            <?// ПДЕЛИТЬСЯ?>
            <?/*<div class="ya-share2" data-curtain data-shape="round" data-services="vkontakte,facebook,odnoklassniki"></div>*/?>
            <div class="ya-share2" data-curtain data-shape="round" data-limit="0" data-more-button-type="short" data-services="vkontakte,facebook,odnoklassniki"></div>
            <?/*<div class="hl-circle-btn">
                <picture>
                    <source srcset="<?=SITE_TEMPLATE_PATH?>/img/main/share-card.svg" type="image/webp">
                    <img class="hl-circle-img" src="<?=SITE_TEMPLATE_PATH?>/img/main/share-card.svg" alt="">
                </picture>
                <img class="hl-circle-img__active" src="<?=SITE_TEMPLATE_PATH?>/img/main/share-card-active.svg" alt="">
            </div>*/?>

            <? // *** ИЗБРАННОЕ ***
            if ( $USER->IsAuthorized() ) {
                $sBtnClickActiveClass = '';
                if ( in_array($arResult['ID'], $arResult['FAVORITE_PRODUCTS']) ) {
                    $sBtnClickActiveClass = 'btn-click-active';
                }
                ?>
                <div class="hl-circle-btn btn-click <?=$sBtnClickActiveClass?>" data-product="<?=$arResult['ID']?>">
                    <picture>
                        <source srcset="<?=SITE_TEMPLATE_PATH?>/img/main/izbr-card.svg" type="image/webp">
                        <img class="hl-circle-img" src="<?=SITE_TEMPLATE_PATH?>/img/main/izbr-card.svg" alt="">
                    </picture>
                    <picture>
                        <source srcset="<?=SITE_TEMPLATE_PATH?>/img/main/izbr-card-active.svg" type="image/webp">
                        <img class="hl-circle-img__active" src="<?=SITE_TEMPLATE_PATH?>/img/main/izbr-card-active.svg" alt="">
                    </picture>
                </div><?
            }?>

            <?// ПОСТАВИТЬ ОЦЕНКУ?>
            <div class="rating-wrap"><?

                $APPLICATION->IncludeComponent(
                    'bitrix:iblock.vote',
                    //'hobbyline_bootstrap_v4',
                    'hobbyline_stars',
                    //'stars',
                    array(
                        'CUSTOM_SITE_ID' => isset($arParams['CUSTOM_SITE_ID']) ? $arParams['CUSTOM_SITE_ID'] : null,
                        'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
                        'IBLOCK_ID' => $arParams['IBLOCK_ID'],
                        'ELEMENT_ID' => $arResult['ID'],
                        'ELEMENT_CODE' => '',
                        'MAX_VOTE' => '5',
                        'VOTE_NAMES' => array('1', '2', '3', '4', '5'),
                        'SET_STATUS_404' => 'N',
                        'DISPLAY_AS_RATING' => $arParams['VOTE_DISPLAY_AS_RATING'],
                        //'DISPLAY_AS_RATING' => 'rating',
                        'CACHE_TYPE' => $arParams['CACHE_TYPE'],
                        'CACHE_TIME' => $arParams['CACHE_TIME'],
                        'SHOW_RATING' => 'Y'
                    ),
                    $component,
                    array('HIDE_ICONS' => 'Y')
                );

                ?>
            </div>

        </div>

    </div>

    <div class="hl-card-articul">Арт. <?=$arResult['DISPLAY_PROPERTIES']['ARTNUMBER']['DISPLAY_VALUE']?></div>

    <div class="hl-card">

        <div class="hl-card__left">

            <div class="slider-vertical__prev-card slider-vertical__mob-arr"></div>

            <div class="slider-vertical">

                <div class="nav-slick hl-card__nav-slick">

                    <div class="slider-vertical__prev-card"></div>

                    <div class="slider-vertical-nav-card"><?

                        if (!empty($arResult['PROPERTIES']['MORE_PHOTO']['DISPLAY_SMALL_SRC'])) {
                            foreach ($arResult['PROPERTIES']['MORE_PHOTO']['DISPLAY_SMALL_SRC'] as $key => $photo) {
                                ?><div>
                                    <picture>
                                        <source srcset="<?=$photo?>" type="image/webp">
                                        <img src="<?=$photo?>" alt="">
                                    </picture>
                                </div><?
                            }
                        }

                    ?></div>

                    <div class="slider-vertical__next-card"></div>

                </div>

                <div class="slider-vertical-wrap-card" <?/*data-entity="images-container"*/?>><?

                    if (!empty($arResult['PROPERTIES']['MORE_PHOTO']['DISPLAY_SRC'])) {
                        foreach ($arResult['PROPERTIES']['MORE_PHOTO']['DISPLAY_SRC'] as $key => $photo) {
                            ?><div class="slider-vertical__slide">
                                <div class="slider-vertical__item-card">
                                    <a href="<?=$arResult['PROPERTIES']['MORE_PHOTO']['DISPLAY_BIG_SRC'][$key]?>" data-fancybox>
                                        <picture>
                                            <source srcset="<?=$photo?>" type="image/webp">
                                            <img src="<?=$photo?>" alt="">
                                        </picture>
                                    </a>

                                </div>
                            </div><?
                        }
                    }

                ?></div>

            </div>

            <div class="slider-vertical__next-card slider-vertical__mob-arr"></div>

        </div>

        <div class="hl-card__right">

            <div class="hl-card__price-wrap">

                <div class="hl-card__price-content">

                    <?if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y') {
                        if ($haveOffers) {
                            ?><div class="product-item-label-ring <?=$discountPositionClass?>"id="<?=$itemIds['DISCOUNT_PERCENT_ID']?>"style="display: none;"></div><?
                        }
                        else {
                            if ($price['DISCOUNT'] > 0) {
                                ?><div class="product-item-label-ring <?=$discountPositionClass?>"id="<?=$itemIds['DISCOUNT_PERCENT_ID']?>"title="<?=-$price['PERCENT']?>%">
                                    <span><?=-$price['PERCENT']?>%</span>
                                </div><?
                            }
                        }
                    }?>

                    <?/*<div class="hl-card__price">159 руб.</div>
                    <div class="hl-card__price-through">350 руб.</div>*/?>

                    <div class="hl-card__price" id="<?=$itemIds['PRICE_ID']?>"><?=$price['PRINT_RATIO_PRICE']?></div><?

                    if ($actualItem['ITEM_PRICES'][1]['PRINT_RATIO_BASE_PRICE'] != '') {
                        ?><div class="hl-card__price-through" id="">
                            <?=$actualItem['ITEM_PRICES'][1]['PRINT_RATIO_BASE_PRICE']?>
                        </div><?
                    }
                    ?>

                    <?
                    /*
                    var_dump( $showDiscount );
                    if ($arParams['SHOW_OLD_PRICE'] === 'Y')
                    {
                        ?>
                        <div class="product-item-detail-price-old mb-1"
                             id="<?=$itemIds['OLD_PRICE_ID']?>"
                             <?=($showDiscount ? '' : 'style="display: none;"')?>;
                        >
                            <?=($showDiscount ? $price['PRINT_RATIO_BASE_PRICE'] : '')?>
                        </div>
                        <?
                    }
                    ?>

                    <div class="product-item-detail-price-current mb-1" id="<?=$itemIds['PRICE_ID']?>"><?=$price['PRINT_RATIO_PRICE']?></div>

                    <?
                    if ($arParams['SHOW_OLD_PRICE'] === 'Y')
                    {
                        ?>
                        <div class="product-item-detail-economy-price mb-1"
                             id="<?=$itemIds['DISCOUNT_PRICE_ID']?>"
                            <?=($showDiscount ? '' : 'style="display: none;"')?>
                        ><?
                            if ($showDiscount)
                            {
                                echo Loc::getMessage('CT_BCE_CATALOG_ECONOMY_INFO2', array('#ECONOMY#' => $price['PRINT_RATIO_DISCOUNT']));
                            }
                            ?>
                        </div>
                        <?
                    }*/
                    ?>

                </div>

                <div class="hl-card__please-auth"><?

                    if ( !$USER->IsAuthorized() ) {
                        ?><a href="/personal/">авторизуйтесь</a>, чтобы узнать оптовые цены<?
                    }

                ?></div>

            </div>

            <? if ( count($arResult['PROPERTIES']['LINKED_ELEMENTS']['ELEMENTS']) > 0 ) { ?><div class="hl-card__label">Выбрать цвет:</div><? } ?>

            <div class="color-slider uk-position-relative" uk-slider=""><?

                if ( count($arResult['PROPERTIES']['LINKED_ELEMENTS']['ELEMENTS']) > 0 ) {
                    ?><div class="uk-visible-toggle uk-light uk-slider-container" tabindex="-1">
                        <ul class="uk-slider-items uk-grid color-slider__ul"><?
                            foreach ( $arResult['PROPERTIES']['LINKED_ELEMENTS']['ELEMENTS'] as $arElem ) {
                                ?><li class="color-slider__li">
                                    <a href="<?=$arElem['LINK']?>" class="color-slider__item">
                                        <div class="color-slider__color">
                                            <picture>
                                                <source srcset="<?=$arElem['PICTURE']?>" type="image/webp">
                                                <img src="<?=$arElem['PICTURE']?>" alt="">
                                            </picture>
                                        </div>
                                        <div class="color-slider__label"><?=$arElem['COLOR']?></div>
                                    </a>
                                </li><?
                            }
                        ?></ul>
                    </div>

                    <a class="uk-position-center-left-out uk-position-small color-slider__arr-left" href="#" uk-slider-item="previous"></a>
                    <a class="uk-position-center-right-out uk-position-small color-slider__arr-right" href="#" uk-slider-item="next"></a><?
                }
                ?>

            </div>

            <!-- Выбрать размер начало -->
            <?

            $showOffersBlock = $haveOffers && !empty($arResult['OFFERS_PROP']);
            $mainBlockProperties = array_intersect_key($arResult['DISPLAY_PROPERTIES'], $arParams['MAIN_BLOCK_PROPERTY_CODE']);
            $showPropsBlock = !empty($mainBlockProperties) || $arResult['SHOW_OFFERS_PROPS'];
            $showBlockWithOffersAndProps = $showOffersBlock || $showPropsBlock;

            if ($showBlockWithOffersAndProps) {
                if ($showOffersBlock) {

                    ?><div class="size-table" id="<?=$itemIds['TREE_ID']?>">
                        <?/*foreach ($arResult['SKU_PROPS'] as $skuProperty) {
                            if (!isset($arResult['OFFERS_PROP'][$skuProperty['CODE']]))
                                continue;

                            $propertyId = $skuProperty['ID'];
                            $skuProps[] = array(
                                'ID' => $propertyId,
                                'SHOW_MODE' => $skuProperty['SHOW_MODE'],
                                'VALUES' => $skuProperty['VALUES'],
                                'VALUES_COUNT' => $skuProperty['VALUES_COUNT']
                            );

                            ?><div class="hl-card__labele">Выбрать <?=htmlspecialcharsEx($skuProperty['NAME'])?></div>

                            <form class="products_form" id="product-order-form" action="">

                                <input type="hidden" name="ARTNUMBER" value="<?=$arResult["DISPLAY_PROPERTIES"]["ARTNUMBER"]['DISPLAY_VALUE']?>" >
                                <input type="hidden" name="SKU_PROP_CODE" value="<?=$skuProperty['CODE']?>" >
                                <input type="hidden" name="SKU_PROP_NAME" value="<?=$skuProperty['NAME']?>" >

                                <table class="uk-table uk-table-divider size-table__container">
                                    <thead>
                                    <tr class="">
                                        <th class="size-table__th"></th><?

                                        foreach ($skuProperty['VALUES'] as &$value) {
                                            if ( intval( $value['ID'] ) <= 0 ) {
                                                continue;
                                            }
                                            $value['NAME'] = htmlspecialcharsbx($value['NAME']);

                                            ?><th class="size-table__th" title="<?=$value['NAME']?>"data-treevalue="<?=$propertyId?>_<?=$value['ID']?>"data-onevalue="<?=$value['ID']?>">
                                                <?=$value['NAME']?>
                                            </th><?
                                        }

                                        ?></tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="size-table__label">Поштучно:</td><?

                                        foreach ($skuProperty['VALUES'] as &$value) {
                                            if ( intval( $value['ID'] ) <= 0 ) {
                                                continue;
                                            }
                                            $value['NAME'] = htmlspecialcharsbx($value['NAME']);

                                            ?><td class="size-table__td">
                                                <div class="step">
                                                    <div class=" <?= $arResult['OFFERS'][$value['ID']]['CAN_BUY']?'step__plus':''?> "></div>
                                                        <input
                                                                class="step__input"
                                                                type="text"
                                                                name="OFFERS[<?= $arResult['OFFERS'][$value['ID']]['ID'] ?>][<?=$value['NAME']?>]"
                                                                value="0"
                                                                <?= !$arResult['OFFERS'][$value['ID']]['CAN_BUY']?'disabled':''?>
                                                        >
                                                    <div class=" <?= $arResult['OFFERS'][$value['ID']]['CAN_BUY']?'step__minus':''?> "></div>
                                                </div>
                                            </td><?
                                        }

                                        ?></tr>
                                    </tbody>
                                </table>

                            </form><?

                        }*/?>
                        <div class="hl-card__labele">Выберите вариант ниже:</div>
                        <form class="products_form" id="product-order-form" action="">
                            <input type="hidden" name="ARTNUMBER" value="<?=$arResult["DISPLAY_PROPERTIES"]["ARTNUMBER"]['DISPLAY_VALUE']?>" >
                            <input type="hidden" name="SKU_PROP_CODE" value="<?=$skuProperty['CODE']?>" >
                            <input type="hidden" name="SKU_PROP_NAME" value="<?=$skuProperty['NAME']?>" >

                            <table class="uk-table uk-table-divider size-table__container">
                                <thead>
                                    <tr>
                                        <th class="size-table__th"></th>
                                        <?
                                        foreach ( $arResult['OFFERS'] as $arOffer ) {

                                            //$sColor = $arOffer['PROPERTIES']['COLOR']['VALUE'];
                                            $sColor = $arOffer['PROPERTIES']['TSVET']['VALUE'];
                                            //$sSize = $arOffer['PROPERTIES']['SIZE']['VALUE'];
                                            $sSize = $arOffer['PROPERTIES']['RAZMER']['VALUE'];

                                            //$sColor = trim( $arResult['SKU_PROPS']['COLOR']['VALUES_BY_CODE'][$sColor]['NAME'] );
                                            $sColor = trim( $arResult['SKU_PROPS']['TSVET']['VALUES_BY_CODE'][$sColor]['NAME'] );
                                            //$sSize = trim( $arResult['SKU_PROPS']['SIZE']['VALUES_BY_CODE'][$sSize]['NAME'] );
                                            $sSize = trim( $arResult['SKU_PROPS']['RAZMER']['VALUES_BY_CODE'][$sSize]['NAME'] );

                                            ?><th class="size-table__th" title="<?//=$value['NAME']?>"data-treevalue="<?//=$propertyId?>_<?//=$value['ID']?>"data-onevalue="<?//=$value['ID']?>">
                                                <?=$sColor?><br><?=$sSize?>
                                            </th><?
                                        }
                                        ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="size-table__label">Поштучно:</td>
                                        <?

                                        foreach ( $arResult['OFFERS'] as $arOffer ) {

                                            //$sColor = $arOffer['PROPERTIES']['COLOR']['VALUE'];
                                            $sColor = $arOffer['PROPERTIES']['TSVET']['VALUE'];
                                            //$sSize = $arOffer['PROPERTIES']['SIZE']['VALUE'];
                                            $sSize = $arOffer['PROPERTIES']['RAZMER']['VALUE'];

                                            //$sColor = trim( $arResult['SKU_PROPS']['COLOR']['VALUES_BY_CODE'][$sColor]['NAME'] );
                                            $sColor = trim( $arResult['SKU_PROPS']['TSVET']['VALUES_BY_CODE'][$sColor]['NAME'] );
                                            //$sSize = trim( $arResult['SKU_PROPS']['SIZE']['VALUES_BY_CODE'][$sSize]['NAME'] );
                                            $sSize = trim( $arResult['SKU_PROPS']['RAZMER']['VALUES_BY_CODE'][$sSize]['NAME'] );

                                            $sColor = strlen($sColor)>0?$sColor:'none';
                                            $sSize = strlen($sSize)>0?$sSize:'none';

                                            ?><td class="size-table__td">
                                                <div class="step">
                                                    <div class=" <?= $arOffer['CAN_BUY']?'step__plus':''?> "></div>
                                                        <input
                                                                class="step__input"
                                                                type="text"
                                                                name="OFFERS[<?= $arOffer['ID'] ?>][<?=$sColor?>][<?=$sSize?>]"
                                                                value="0"
                                                                min="0"
                                                                max="<?=$arOffer['PRODUCT']['QUANTITY']?>"
                                                            <?= !$arOffer['CAN_BUY']?'disabled':''?>
                                                        >
                                                    <div class=" <?= $arOffer['CAN_BUY']?'step__minus':''?> "></div>
                                                </div>
                                            </td><?
                                        }
                                        ?>
                                    </tr>
                                </tbody>
                            </table>
                        </form>

                    </div><?

                }
                else {
                    ?><div class="size-table" id="<?=$itemIds['TREE_ID']?>">

                        <div class="hl-card__labele">Выбрать Количество</div>

                        <form class="products_form" id="product-order-form" action="">

                            <input type="hidden" name="ARTNUMBER" value="<?=$arResult["DISPLAY_PROPERTIES"]["ARTNUMBER"]['DISPLAY_VALUE']?>" >
                            <input type="hidden" name="SKU_PROP_CODE" value="GENDER" >
                            <input type="hidden" name="SKU_PROP_NAME" value="<?=$arResult["DISPLAY_PROPERTIES"]["GENDER"]['NAME']?>" >
                            <input type="hidden" name="SKU_PROP_VALUE" value="<?=$arResult["DISPLAY_PROPERTIES"]["GENDER"]['DISPLAY_VALUE']?>" >

                            <table class="uk-table uk-table-divider size-table__container">
                                <tbody>
                                <tr>
                                    <td class="size-table__label">Поштучно:</td>
                                        <td class="size-table__td">
                                        <div class="step">
                                            <div class=" <?= $arResult['CAN_BUY']?'step__plus':''?> "></div>
                                            <input
                                                    class="step__input"
                                                    type="text"
                                                    name="PRODUCT[<?= $arResult['ID'] ?>]"
                                                    value="0"
                                                    min="0"
                                                    max="<?=$arResult['PRODUCT']['QUANTITY']?>"
                                                <?= !$arResult['CAN_BUY']?'disabled':''?>
                                            >
                                            <div class=" <?= $arResult['CAN_BUY']?'step__minus':''?> "></div>
                                        </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                        <input type="hidden" name="IS_AJAX" value="Y">

                        </form>

                    </div><?
                }
            }
            ?>
            <!-- Выбрать размер конец -->

            <?
            switch ( $arResult['PROPERTIES']['TYPE']['VALUE'] ) {
                CASE 'noski':
                CASE 'golfiny':
                CASE 'golfy':
                    switch ( $arResult['PROPERTIES']['GENDER']['VALUE'] ){
                        CASE 'zhenskoe':
                        CASE 'muzhskoe':
                        CASE 'uniseks':
                            ?><a href="#modal-tabel" uk-toggle class="hl-card__bl-link">Таблица размеров</a><?
                            break;
                        CASE 'detskoe':
                            ?><a href="#modal-tabel-child" uk-toggle class="hl-card__bl-link">Таблица размеров детская</a><?
                            break;
                    }
                    break;
                CASE 'varezhki':
                CASE 'perchatki':
                    switch ( $arResult['PROPERTIES']['GENDER']['VALUE'] ){
                        CASE 'zhenskoe':
                        CASE 'muzhskoe':
                        CASE 'uniseks':
                            ?><a href="#modal-tabel-varejki" uk-toggle class="hl-card__bl-link">Размерная сетка варежек и перчаток для взрослых</a><?
                            break;
                        CASE 'detskoe':
                            ?><a href="#modal-tabel-varejki-deti" uk-toggle class="hl-card__bl-link">Размерная сетка варежек и перчаток для детей</a><?
                            break;
                    }
                    break;
                CASE 'kolgotki':
                CASE 'losiny':
                    switch ( $arResult['PROPERTIES']['GENDER']['VALUE'] ){
                        CASE 'zhenskoe':
                        CASE 'muzhskoe':
                        CASE 'uniseks':
                            ?><a href="#modal-tabel-kolgotki" uk-toggle class="hl-card__bl-link">Размерная сетка колгот и лосин для взрослых</a><?
                            break;
                        CASE 'detskoe':
                            ?><a href="#modal-tabel-sizes" uk-toggle class="hl-card__bl-link">Размеры колготок</a><?
                            break;
                    }
                    break;
            }

            ?>

            <div class="hl-card__btn-align"><?

                if ($actualItem['CAN_BUY'] ) {
                    if ($showAddBtn /*&& $showOffersBlock*/) {

                        ?><a class="to_add_product_btn btn <?=$showButtonClassName?>  link-half lookbook__link hl-card__buy-btn"
                             id="<?=$itemIds['ADD_BASKET_LINK']?>"
                             href="javascript:void(0);">
                        <?=$arParams['MESS_BTN_ADD_TO_BASKET']?>
                        <picture>
                            <source srcset="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" type="image/webp">
                            <img src="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" class="btn-arrow-cat" alt="">
                        </picture>
                        </a><?
                    }

                    if ($showBuyBtn /*&& $showOffersBlock*/) {

                        ?><a class="to_add_product_btn btn <?=$buyButtonClassName?> link-half lookbook__link hl-card__buy-btn"
                             id="<?=$itemIds['BUY_LINK']?>"
                             href="javascript:void(0);">
                        <?=$arParams['MESS_BTN_BUY']?>
                        <picture>
                            <source srcset="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" type="image/webp">
                            <img src="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" class="btn-arrow-cat" alt="">
                        </picture>
                        </a><?

                    }
                }
                else {
                    if ($showSubscribe) {
                        $APPLICATION->IncludeComponent(
                            'bitrix:catalog.product.subscribe',
                            'hobbyline_element_default',
                            array(
                                'CUSTOM_SITE_ID' => isset($arParams['CUSTOM_SITE_ID']) ? $arParams['CUSTOM_SITE_ID'] : null,
                                'PRODUCT_ID' => $arResult['ID'],
                                'BUTTON_ID' => $itemIds['SUBSCRIBE_LINK'],
                                'BUTTON_CLASS' => 'btn u-btn-outline-primary product-item-detail-buy-button',
                                'DEFAULT_DISPLAY' => !$actualItem['CAN_BUY'],
                                'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],
                            ),
                            $component,
                            array('HIDE_ICONS' => 'Y')
                        );
                    }
                    else {
                        ?><a class="link-half lookbook__link hl-card__buy-btn not_avail" href="javascript:void(0)" rel="nofollow"><?=$arParams['MESS_NOT_AVAILABLE']?></a><?
                    }
                }

            ?></div>
            <?/*</div>*/?>

        </div>

    </div>

    <!--Описание-->
    <div class="hl-description"><?

        if (!empty($arResult['DISPLAY_PROPERTIES']) || $arResult['SHOW_OFFERS_PROPS']) {
            if (!empty($arResult['DISPLAY_PROPERTIES'])) {

                ?><h3 class="hl-description__label">Описание</h3>

                <div class="hl-description__sets"><?

                    foreach ($arResult['DISPLAY_PROPERTIES'] as $property) {
                        if ( $property['CODE'] == 'LINKED_ELEMENTS' ) {
                            continue;
                        }
                        ?><div class="hl-description__item">
                            <div class="hl-description__set-name"><?=$property['NAME']?>:</div>
                            <div class="hl-description__param">
                                <?=(is_array($property['DISPLAY_VALUE']) ? implode(' / ', $property['DISPLAY_VALUE']) : $property['DISPLAY_VALUE'])?>
                            </div>
                        </div><?
                    }
                    unset($property);

                ?></div><?
            }
        }
        ?>

        <div class="hl-description__more"><?

            if ($showDescription) {
                if (
                    $arResult['PREVIEW_TEXT'] != ''
                    && (
                        $arParams['DISPLAY_PREVIEW_TEXT_MODE'] === 'S'
                        || ($arParams['DISPLAY_PREVIEW_TEXT_MODE'] === 'E' && $arResult['DETAIL_TEXT'] == '')
                    )
                )
                {
                    echo $arResult['PREVIEW_TEXT_TYPE'] === 'html' ? $arResult['PREVIEW_TEXT'] : '<p>'.$arResult['PREVIEW_TEXT'].'</p>';
                }

                if ($arResult['DETAIL_TEXT'] != '') {
                    echo $arResult['DETAIL_TEXT_TYPE'] === 'html' ? $arResult['DETAIL_TEXT'] : '<p>'.$arResult['DETAIL_TEXT'].'</p>';
                }
            }

        ?></div>

    </div>
    <!--конец Описание-->

    <?/*
    if ($arParams['USE_COMMENTS'] === 'Y')
    {
        ?>
        <div class="">
            <?
            $componentCommentsParams = array(
                'ELEMENT_ID' => $arResult['ID'],
                'ELEMENT_CODE' => '',
                'IBLOCK_ID' => $arParams['IBLOCK_ID'],
                'SHOW_DEACTIVATED' => $arParams['SHOW_DEACTIVATED'],
                'URL_TO_COMMENT' => '',
                'WIDTH' => '',
                'COMMENTS_COUNT' => '5',
                'BLOG_USE' => $arParams['BLOG_USE'],
                'FB_USE' => $arParams['FB_USE'],
                'FB_APP_ID' => $arParams['FB_APP_ID'],
                'VK_USE' => $arParams['VK_USE'],
                'VK_API_ID' => $arParams['VK_API_ID'],
                'CACHE_TYPE' => $arParams['CACHE_TYPE'],
                'CACHE_TIME' => $arParams['CACHE_TIME'],
                'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
                'BLOG_TITLE' => '',
                'BLOG_URL' => $arParams['BLOG_URL'],
                'PATH_TO_SMILE' => '',
                'EMAIL_NOTIFY' => $arParams['BLOG_EMAIL_NOTIFY'],
                'AJAX_POST' => 'Y',
                'SHOW_SPAM' => 'Y',
                'SHOW_RATING' => 'N',
                'FB_TITLE' => '',
                'FB_USER_ADMIN_ID' => '',
                'FB_COLORSCHEME' => 'light',
                'FB_ORDER_BY' => 'reverse_time',
                'VK_TITLE' => '',
                'TEMPLATE_THEME' => $arParams['~TEMPLATE_THEME']
            );
            if(isset($arParams["USER_CONSENT"]))
                $componentCommentsParams["USER_CONSENT"] = $arParams["USER_CONSENT"];
            if(isset($arParams["USER_CONSENT_ID"]))
                $componentCommentsParams["USER_CONSENT_ID"] = $arParams["USER_CONSENT_ID"];
            if(isset($arParams["USER_CONSENT_IS_CHECKED"]))
                $componentCommentsParams["USER_CONSENT_IS_CHECKED"] = $arParams["USER_CONSENT_IS_CHECKED"];
            if(isset($arParams["USER_CONSENT_IS_LOADED"]))
                $componentCommentsParams["USER_CONSENT_IS_LOADED"] = $arParams["USER_CONSENT_IS_LOADED"];

            $APPLICATION->IncludeComponent(
                'bitrix:catalog.comments',
                '',
                $componentCommentsParams,
                $component,
                array('HIDE_ICONS' => 'Y')
            );
            ?>
        </div>
        <?
    }*/
    ?>

	<meta itemprop="name" content="<?=$name?>" />
	<meta itemprop="category" content="<?=$arResult['CATEGORY_PATH']?>" />

	<?
	if ($haveOffers) {
		foreach ($arResult['JS_OFFERS'] as $offer)
		{
			$currentOffersList = array();

			if (!empty($offer['TREE']) && is_array($offer['TREE']))
			{
				foreach ($offer['TREE'] as $propName => $skuId)
				{
					$propId = (int)substr($propName, 5);

					foreach ($skuProps as $prop)
					{
						if ($prop['ID'] == $propId)
						{
							foreach ($prop['VALUES'] as $propId => $propValue)
							{
								if ($propId == $skuId)
								{
									$currentOffersList[] = $propValue['NAME'];
									break;
								}
							}
						}
					}
				}
			}

			$offerPrice = $offer['ITEM_PRICES'][$offer['ITEM_PRICE_SELECTED']];
			?>
			<span itemprop="offers" itemscope itemtype="http://schema.org/Offer">
				<meta itemprop="sku" content="<?=htmlspecialcharsbx(implode('/', $currentOffersList))?>" />
				<meta itemprop="price" content="<?=$offerPrice['RATIO_PRICE']?>" />
				<meta itemprop="priceCurrency" content="<?=$offerPrice['CURRENCY']?>" />
				<link itemprop="availability" href="http://schema.org/<?=($offer['CAN_BUY'] ? 'InStock' : 'OutOfStock')?>" />
			</span>
			<?
		}

		unset($offerPrice, $currentOffersList);
	}
	else {
		?>
		<span itemprop="offers" itemscope itemtype="http://schema.org/Offer">
			<meta itemprop="price" content="<?=$price['RATIO_PRICE']?>" />
			<meta itemprop="priceCurrency" content="<?=$price['CURRENCY']?>" />
			<link itemprop="availability" href="http://schema.org/<?=($actualItem['CAN_BUY'] ? 'InStock' : 'OutOfStock')?>" />
		</span>
		<?
	}
	?>

	<?
	if ($haveOffers) {
		$offerIds = array();
		$offerCodes = array();

		$useRatio = $arParams['USE_RATIO_IN_RANGES'] === 'Y';

		foreach ($arResult['JS_OFFERS'] as $ind => &$jsOffer)
		{
			$offerIds[] = (int)$jsOffer['ID'];
			$offerCodes[] = $jsOffer['CODE'];

			$fullOffer = $arResult['OFFERS'][$ind];
			$measureName = $fullOffer['ITEM_MEASURE']['TITLE'];

			$strAllProps = '';
			$strMainProps = '';
			$strPriceRangesRatio = '';
			$strPriceRanges = '';

			if ($arResult['SHOW_OFFERS_PROPS'])
			{
				if (!empty($jsOffer['DISPLAY_PROPERTIES']))
				{
					foreach ($jsOffer['DISPLAY_PROPERTIES'] as $property)
					{
						$current = '<li class="product-item-detail-properties-item">
						<span class="product-item-detail-properties-name">'.$property['NAME'].'</span>
						<span class="product-item-detail-properties-dots"></span>
						<span class="product-item-detail-properties-value">'.(
							is_array($property['VALUE'])
								? implode(' / ', $property['VALUE'])
								: $property['VALUE']
							).'</span></li>';
						$strAllProps .= $current;

						if (isset($arParams['MAIN_BLOCK_OFFERS_PROPERTY_CODE'][$property['CODE']]))
						{
							$strMainProps .= $current;
						}
					}

					unset($current);
				}
			}

			if ($arParams['USE_PRICE_COUNT'] && count($jsOffer['ITEM_QUANTITY_RANGES']) > 1)
			{
				$strPriceRangesRatio = '('.Loc::getMessage(
						'CT_BCE_CATALOG_RATIO_PRICE',
						array('#RATIO#' => ($useRatio
								? $fullOffer['ITEM_MEASURE_RATIOS'][$fullOffer['ITEM_MEASURE_RATIO_SELECTED']]['RATIO']
								: '1'
							).' '.$measureName)
					).')';

				foreach ($jsOffer['ITEM_QUANTITY_RANGES'] as $range)
				{
					if ($range['HASH'] !== 'ZERO-INF')
					{
						$itemPrice = false;

						foreach ($jsOffer['ITEM_PRICES'] as $itemPrice)
						{
							if ($itemPrice['QUANTITY_HASH'] === $range['HASH'])
							{
								break;
							}
						}

						if ($itemPrice)
						{
							$strPriceRanges .= '<dt>'.Loc::getMessage(
									'CT_BCE_CATALOG_RANGE_FROM',
									array('#FROM#' => $range['SORT_FROM'].' '.$measureName)
								).' ';

							if (is_infinite($range['SORT_TO']))
							{
								$strPriceRanges .= Loc::getMessage('CT_BCE_CATALOG_RANGE_MORE');
							}
							else
							{
								$strPriceRanges .= Loc::getMessage(
									'CT_BCE_CATALOG_RANGE_TO',
									array('#TO#' => $range['SORT_TO'].' '.$measureName)
								);
							}

							$strPriceRanges .= '</dt><dd>'.($useRatio ? $itemPrice['PRINT_RATIO_PRICE'] : $itemPrice['PRINT_PRICE']).'</dd>';
						}
					}
				}

				unset($range, $itemPrice);
			}

			$jsOffer['DISPLAY_PROPERTIES'] = $strAllProps;
			$jsOffer['DISPLAY_PROPERTIES_MAIN_BLOCK'] = $strMainProps;
			$jsOffer['PRICE_RANGES_RATIO_HTML'] = $strPriceRangesRatio;
			$jsOffer['PRICE_RANGES_HTML'] = $strPriceRanges;
		}

		$templateData['OFFER_IDS'] = $offerIds;
		$templateData['OFFER_CODES'] = $offerCodes;
		unset($jsOffer, $strAllProps, $strMainProps, $strPriceRanges, $strPriceRangesRatio, $useRatio);

		$jsParams = array(
			'CONFIG' => array(
				'USE_CATALOG' => $arResult['CATALOG'],
				'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
				'SHOW_PRICE' => true,
				'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'] === 'Y',
				'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'] === 'Y',
				'USE_PRICE_COUNT' => $arParams['USE_PRICE_COUNT'],
				'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
				'SHOW_SKU_PROPS' => $arResult['SHOW_OFFERS_PROPS'],
				'OFFER_GROUP' => $arResult['OFFER_GROUP'],
				'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
				'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
				'SHOW_CLOSE_POPUP' => $arParams['SHOW_CLOSE_POPUP'] === 'Y',
				'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
				'RELATIVE_QUANTITY_FACTOR' => $arParams['RELATIVE_QUANTITY_FACTOR'],
				'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
				'USE_STICKERS' => true,
				'USE_SUBSCRIBE' => $showSubscribe,
				'SHOW_SLIDER' => $arParams['SHOW_SLIDER'],
				'SLIDER_INTERVAL' => $arParams['SLIDER_INTERVAL'],
				'ALT' => $alt,
				'TITLE' => $title,
				'MAGNIFIER_ZOOM_PERCENT' => 200,
				'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
				'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
				'BRAND_PROPERTY' => !empty($arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']])
					? $arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']]['DISPLAY_VALUE']
					: null
			),
			'PRODUCT_TYPE' => $arResult['PRODUCT']['TYPE'],
			'VISUAL' => $itemIds,
			'DEFAULT_PICTURE' => array(
				'PREVIEW_PICTURE' => $arResult['DEFAULT_PICTURE'],
				'DETAIL_PICTURE' => $arResult['DEFAULT_PICTURE']
			),
			'PRODUCT' => array(
				'ID' => $arResult['ID'],
				'ACTIVE' => $arResult['ACTIVE'],
				'NAME' => $arResult['~NAME'],
				'CATEGORY' => $arResult['CATEGORY_PATH']
			),
			'BASKET' => array(
				'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
				'BASKET_URL' => $arParams['BASKET_URL'],
				'SKU_PROPS' => $arResult['OFFERS_PROP_CODES'],
				'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
				'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
			),
			'OFFERS' => $arResult['JS_OFFERS'],
			'OFFER_SELECTED' => $arResult['OFFERS_SELECTED'],
			'TREE_PROPS' => $skuProps
		);
	}
	else {
		$emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
		if ($arParams['ADD_PROPERTIES_TO_BASKET'] === 'Y' && !$emptyProductProperties)
		{
			?>
			<div id="<?=$itemIds['BASKET_PROP_DIV']?>" style="display: none;">
				<?
				if (!empty($arResult['PRODUCT_PROPERTIES_FILL']))
				{
					foreach ($arResult['PRODUCT_PROPERTIES_FILL'] as $propId => $propInfo)
					{
						?>
						<input type="hidden" name="<?=$arParams['PRODUCT_PROPS_VARIABLE']?>[<?=$propId?>]" value="<?=htmlspecialcharsbx($propInfo['ID'])?>">
						<?
						unset($arResult['PRODUCT_PROPERTIES'][$propId]);
					}
				}

				$emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
				if (!$emptyProductProperties)
				{
					?>
					<table>
						<?
						foreach ($arResult['PRODUCT_PROPERTIES'] as $propId => $propInfo)
						{
							?>
							<tr>
								<td><?=$arResult['PROPERTIES'][$propId]['NAME']?></td>
								<td>
									<?
									if (
										$arResult['PROPERTIES'][$propId]['PROPERTY_TYPE'] === 'L'
										&& $arResult['PROPERTIES'][$propId]['LIST_TYPE'] === 'C'
									)
									{
										foreach ($propInfo['VALUES'] as $valueId => $value)
										{
											?>
											<label>
												<input type="radio" name="<?=$arParams['PRODUCT_PROPS_VARIABLE']?>[<?=$propId?>]"
													value="<?=$valueId?>" <?=($valueId == $propInfo['SELECTED'] ? '"checked"' : '')?>>
												<?=$value?>
											</label>
											<br>
											<?
										}
									}
									else
									{
										?>
										<select name="<?=$arParams['PRODUCT_PROPS_VARIABLE']?>[<?=$propId?>]">
											<?
											foreach ($propInfo['VALUES'] as $valueId => $value)
											{
												?>
												<option value="<?=$valueId?>" <?=($valueId == $propInfo['SELECTED'] ? '"selected"' : '')?>>
													<?=$value?>
												</option>
												<?
											}
											?>
										</select>
										<?
									}
									?>
								</td>
							</tr>
							<?
						}
						?>
					</table>
					<?
				}
				?>
			</div>
			<?
		}

		$jsParams = array(
			'CONFIG' => array(
				'USE_CATALOG' => $arResult['CATALOG'],
				'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
				'SHOW_PRICE' => !empty($arResult['ITEM_PRICES']),
				'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'] === 'Y',
				'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'] === 'Y',
				'USE_PRICE_COUNT' => $arParams['USE_PRICE_COUNT'],
				'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
				'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
				'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
				'SHOW_CLOSE_POPUP' => $arParams['SHOW_CLOSE_POPUP'] === 'Y',
				'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
				'RELATIVE_QUANTITY_FACTOR' => $arParams['RELATIVE_QUANTITY_FACTOR'],
				'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
				'USE_STICKERS' => true,
				'USE_SUBSCRIBE' => $showSubscribe,
				'SHOW_SLIDER' => $arParams['SHOW_SLIDER'],
				'SLIDER_INTERVAL' => $arParams['SLIDER_INTERVAL'],
				'ALT' => $alt,
				'TITLE' => $title,
				'MAGNIFIER_ZOOM_PERCENT' => 200,
				'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
				'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
				'BRAND_PROPERTY' => !empty($arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']])
					? $arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']]['DISPLAY_VALUE']
					: null
			),
			'VISUAL' => $itemIds,
			'PRODUCT_TYPE' => $arResult['PRODUCT']['TYPE'],
			'PRODUCT' => array(
				'ID' => $arResult['ID'],
				'ACTIVE' => $arResult['ACTIVE'],
				'PICT' => reset($arResult['MORE_PHOTO']),
				'NAME' => $arResult['~NAME'],
				'SUBSCRIPTION' => true,
				'ITEM_PRICE_MODE' => $arResult['ITEM_PRICE_MODE'],
				'ITEM_PRICES' => $arResult['ITEM_PRICES'],
				'ITEM_PRICE_SELECTED' => $arResult['ITEM_PRICE_SELECTED'],
				'ITEM_QUANTITY_RANGES' => $arResult['ITEM_QUANTITY_RANGES'],
				'ITEM_QUANTITY_RANGE_SELECTED' => $arResult['ITEM_QUANTITY_RANGE_SELECTED'],
				'ITEM_MEASURE_RATIOS' => $arResult['ITEM_MEASURE_RATIOS'],
				'ITEM_MEASURE_RATIO_SELECTED' => $arResult['ITEM_MEASURE_RATIO_SELECTED'],
				'SLIDER_COUNT' => $arResult['MORE_PHOTO_COUNT'],
				'SLIDER' => $arResult['MORE_PHOTO'],
				'CAN_BUY' => $arResult['CAN_BUY'],
				'CHECK_QUANTITY' => $arResult['CHECK_QUANTITY'],
				'QUANTITY_FLOAT' => is_float($arResult['ITEM_MEASURE_RATIOS'][$arResult['ITEM_MEASURE_RATIO_SELECTED']]['RATIO']),
				'MAX_QUANTITY' => $arResult['PRODUCT']['QUANTITY'],
				'STEP_QUANTITY' => $arResult['ITEM_MEASURE_RATIOS'][$arResult['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'],
				'CATEGORY' => $arResult['CATEGORY_PATH']
			),
			'BASKET' => array(
				'ADD_PROPS' => $arParams['ADD_PROPERTIES_TO_BASKET'] === 'Y',
				'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
				'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
				'EMPTY_PROPS' => $emptyProductProperties,
				'BASKET_URL' => $arParams['BASKET_URL'],
				'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
				'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
			)
		);
		unset($emptyProductProperties);
	}

	if ($arParams['DISPLAY_COMPARE']) {
		$jsParams['COMPARE'] = array(
			'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
			'COMPARE_DELETE_URL_TEMPLATE' => $arResult['~COMPARE_DELETE_URL_TEMPLATE'],
			'COMPARE_PATH' => $arParams['COMPARE_PATH']
		);
	}
	?>
</div>

<!-- Попап таблицы размеров -->

<?
switch ( $arResult['PROPERTIES']['TYPE']['VALUE'] ) {
    CASE 'noski':
    CASE 'golfiny':
    CASE 'golfy':
        switch ( $arResult['PROPERTIES']['GENDER']['VALUE'] ){
            CASE 'zhenskoe':
            CASE 'muzhskoe':
            CASE 'uniseks':
                ?><div id="modal-tabel" uk-modal>
            <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical hl-modal__body">
                <button class="uk-modal-close-outside hl-modal__close" type="button">
                    <picture>
                        <source srcset="<?=SITE_TEMPLATE_PATH?>/img/main/ha-modal-plus.svg" type="image/webp">
                        <img src="<?=SITE_TEMPLATE_PATH?>/img/main/ha-modal-plus.svg" alt=""></picture>
                </button>
                <h2 class="uk-modal-title hl-modal__label">Размерная сетка</h2>
                <div class="popup-table-scroll">
                    <table class="uk-table uk-table-divider hl-modal__tabel">
                        <thead>
                        <tr>
                            <th>Размер обуви</th>
                            <th>Длина стопы (см)</th>
                            <th>Длина стельки (см)</th>
                            <th>Размер носков (РФ)</th>
                        </tr>
                        </thead>
                        <tbody>
                        <!-- 3cols -->
                        <tr>
                            <td>35</td>
                            <td>21,3-21,9</td>
                            <td>21,8-22,4</td>
                            <td rowspan="3" class="hl-modal__rowspan">23</td>
                        </tr>
                        <tr>
                            <td>36</td>
                            <td>21,9-22,6</td>
                            <td>22,4-23,1</td>

                        </tr>
                        <tr>
                            <td>37</td>
                            <td>22,6-23,3</td>
                            <td>23,1-23,8</td>
                        </tr>
                        <!-- end 3cols -->
                        <!-- 3cols -->
                        <tr>
                            <td>38</td>
                            <td>23,3-23,9</td>
                            <td>23,8-24,5</td>
                            <td rowspan="3" class="hl-modal__rowspan">25</td>
                        </tr>
                        <tr>
                            <td>39</td>
                            <td>23,9-24,6</td>
                            <td>24,5-25,2</td>

                        </tr>
                        <tr>
                            <td>40</td>
                            <td>24,6-25,3</td>
                            <td>25,2-25,9</td>
                        </tr>
                        <!-- end 3cols -->
                        <!-- 3cols -->
                        <tr>
                            <td>41</td>
                            <td>25,3-26,0</td>
                            <td>25,9-26,7</td>
                            <td rowspan="3" class="hl-modal__rowspan">27</td>
                        </tr>
                        <tr>
                            <td>42</td>
                            <td>26,0-26,7</td>
                            <td>26,7-27,4</td>

                        </tr>
                        <tr>
                            <td>43</td>
                            <td>26,7-27,3</td>
                            <td>26,7-27,3</td>
                        </tr>
                        <!-- end 3cols -->
                        <!-- 3cols -->
                        <tr>
                            <td>44</td>
                            <td>27,3-28,0</td>
                            <td>28,0-28,8</td>
                            <td rowspan="3" class="hl-modal__rowspan">29</td>
                        </tr>
                        <tr>
                            <td>45</td>
                            <td>28,0-28,8</td>
                            <td>28,8-29,6</td>

                        </tr>
                        <tr>
                            <td>46</td>
                            <td>28,8-29,7</td>
                            <td>29,6-30,5</td>
                        </tr>
                        <!-- end 3cols -->
                        <!-- 2cols -->
                        <tr>
                            <td>47</td>
                            <td>29,7-30,6</td>
                            <td>30,5-31,5</td>
                            <td rowspan="2" class="hl-modal__rowspan">31</td>
                        </tr>
                        <tr>
                            <td>48</td>
                            <td>30,6-31,6</td>
                            <td>31,5-32,5</td>
                        </tr>
                        <!-- end 3cols -->
                        </tbody>
                    </table>
                </div>
            </div>
        </div><?
                break;
            CASE 'detskoe':
                ?><div id="modal-tabel-child" uk-modal>
                <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical hl-modal__body">
                    <button class="uk-modal-close-outside hl-modal__close" type="button">
                        <picture>
                            <source srcset="<?=SITE_TEMPLATE_PATH?>/img/main/ha-modal-plus.svg" type="image/webp">
                            <img src="<?=SITE_TEMPLATE_PATH?>/img/main/ha-modal-plus.svg" alt=""></picture>
                    </button>
                    <h2 class="uk-modal-title hl-modal__label">Детская размерная сетка</h2>
                    <div class="popup-table-scroll">
                        <table class="uk-table uk-table-divider hl-modal__tabel">
                            <thead>
                            <tr>
                                <th>Размер детских носков</th>
                                <th>Размер обуви</th>
                                <th>Длина стопы</th>
                                <th>Возраст</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>11 (10-12)</td>
                                <td>18-19</td>
                                <td>10-12</td>
                                <td>6 мес. - 1 год</td>
                            </tr>
                            <tr>
                                <td>13 (12-14)</td>
                                <td>20-22</td>
                                <td>12-14 см</td>
                                <td>1 - 2 года</td>
                            </tr>
                            <tr>
                                <td>15 (14-16)</td>
                                <td>23-25</td>
                                <td>14-16 см</td>
                                <td>3 - 4 года</td>
                            </tr>
                            <tr>
                                <td>17 (16-18)</td>
                                <td>26-28</td>
                                <td>16-18 см</td>
                                <td>4 - 5 лет</td>
                            </tr>
                            <tr>
                                <td>19 (18-20)</td>
                                <td>29-31</td>
                                <td>18-20 см</td>
                                <td>5 - 7 лет</td>
                            </tr>
                            <tr>
                                <td>21 (20-22)</td>
                                <td>32-34</td>
                                <td>20-22 см</td>
                                <td>7 - 9 лет</td>
                            </tr>
                            <tr>
                                <td>23 (22-24)</td>
                                <td>35-38</td>
                                <td>22-24 см</td>
                                <td>10 - 12 лет</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div><?
                break;
        }
        break;
    CASE 'varezhki':
    CASE 'perchatki':
        switch ( $arResult['PROPERTIES']['GENDER']['VALUE'] ){
            CASE 'zhenskoe':
            CASE 'muzhskoe':
            CASE 'uniseks':
                ?><div id="modal-tabel-varejki" uk-modal>
            <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical hl-modal__body">
                <button class="uk-modal-close-outside hl-modal__close" type="button">
                    <picture>
                        <source srcset="<?=SITE_TEMPLATE_PATH?>/img/main/ha-modal-plus.svg" type="image/webp">
                        <img src="<?=SITE_TEMPLATE_PATH?>/img/main/ha-modal-plus.svg" alt=""></picture>
                </button>
                <h2 class="uk-modal-title hl-modal__label">Размерная сетка варежек и перчаток для взрослых</h2>
                <div class="popup-double">
                    <div class="popup-table-scroll">
                        <table class="uk-table uk-table-divider hl-modal__tabel table-mini">
                            <thead>
                            <tr>
                                <th>Size/размер</th>
                                <th>Обхват ладони, см</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>6</td>
                                <td>15,2</td>
                            </tr>
                            <tr>
                                <td>6,5</td>
                                <td>16,5</td>
                            </tr>
                            <tr>
                                <td>7</td>
                                <td>17,8</td>
                            </tr>
                            <tr>
                                <td>7,5</td>
                                <td>19</td>
                            </tr>
                            <tr>
                                <td>8</td>
                                <td>20,3</td>
                            </tr>
                            <tr>
                                <td>8,5</td>
                                <td>21,6</td>
                            </tr>
                            <tr>
                                <td>9</td>
                                <td>22,9</td>
                            </tr>
                            <tr>
                                <td>9,5</td>
                                <td>24</td>
                            </tr>
                            <tr>
                                <td>10</td>
                                <td>25</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="popup-right-info">
                        <h4 class="popup-right-info__label">Как измерить обхват ладони</h4>
                        <picture>
                            <source srcset="<?=SITE_TEMPLATE_PATH?>/img/main/hand.webp" type="image/webp">
                            <img src="<?=SITE_TEMPLATE_PATH?>/img/main/hand.jpg" alt=""></picture>
                        <p>
                            Для определения размера измерьте
                            окружность ладони сантиметровой
                            лентой над большим пальцем.
                            По таблице определите необходимый размер
                        </p>
                    </div>
                </div>
            </div>
        </div><?
                break;
            CASE 'detskoe':
                ?><div id="modal-tabel-varejki-deti" uk-modal>
                <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical hl-modal__body">
                    <button class="uk-modal-close-outside hl-modal__close" type="button">
                        <picture>
                            <source srcset="<?=SITE_TEMPLATE_PATH?>/img/main/ha-modal-plus.svg" type="image/webp">
                            <img src="<?=SITE_TEMPLATE_PATH?>/img/main/ha-modal-plus.svg" alt=""></picture>
                    </button>
                    <h2 class="uk-modal-title hl-modal__label">Размерная сетка варежек и перчаток для детей</h2>
                    <div class="popup-double">
                        <div class="popup-table-scroll">
                            <table class="uk-table uk-table-divider hl-modal__tabel table-mini">
                                <thead>
                                <tr>
                                    <th>Примерный
                                        возраст
                                    </th>
                                    <th>Обхват ладони, см</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>1-2 года</td>
                                    <td>12</td>
                                </tr>
                                <tr>
                                    <td>2-3 года</td>
                                    <td>13</td>
                                </tr>
                                <tr>
                                    <td>4-6 лет</td>
                                    <td>14</td>
                                </tr>
                                <tr>
                                    <td>7-8 лет</td>
                                    <td>15</td>
                                </tr>
                                <tr>
                                    <td>9-10 лет</td>
                                    <td>16</td>
                                </tr>
                                <tr>
                                    <td>11-12 лет</td>
                                    <td>17</td>
                                </tr>
                                <tr>
                                    <td>13 лет</td>
                                    <td>18</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="popup-right-info">
                            <h4 class="popup-right-info__label">Как измерить обхват ладони</h4>
                            <picture>
                                <source srcset="<?=SITE_TEMPLATE_PATH?>/img/main/hand.webp" type="image/webp">
                                <img src="<?=SITE_TEMPLATE_PATH?>/img/main/hand.jpg" alt=""></picture>
                            <p>
                                Для определения размера измерьте
                                окружность ладони сантиметровой
                                лентой над большим пальцем.
                                По таблице определите необходимый размер
                            </p>
                        </div>
                    </div>
                </div>
            </div><?
                break;
        }
        break;
    CASE 'kolgotki':
    CASE 'losiny':
        switch ( $arResult['PROPERTIES']['GENDER']['VALUE'] ){
            CASE 'zhenskoe':
            CASE 'muzhskoe':
            CASE 'uniseks':
                ?><div id="modal-tabel-kolgotki" uk-modal>
            <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical hl-modal__body">
                <button class="uk-modal-close-outside hl-modal__close" type="button">
                    <picture>
                        <source srcset="<?=SITE_TEMPLATE_PATH?>/img/main/ha-modal-plus.svg" type="image/webp">
                        <img src="<?=SITE_TEMPLATE_PATH?>/img/main/ha-modal-plus.svg" alt=""></picture>
                </button>
                <h2 class="uk-modal-title hl-modal__label">Размерная сетка колгот и лосин для взрослых</h2>
                <div class="popup-table-scroll">
                    <picture>
                        <source srcset="<?=SITE_TEMPLATE_PATH?>/img/main/tabel-colgotki.webp" type="image/webp">
                        <img src="<?=SITE_TEMPLATE_PATH?>/img/main/tabel-colgotki.jpg" alt=""></picture>
                </div>
            </div>
        </div><?
                break;
            CASE 'detskoe':
                ?><div id="modal-tabel-sizes" uk-modal>
                <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical hl-modal__body">
                    <button class="uk-modal-close-outside hl-modal__close" type="button">
                        <picture>
                            <source srcset="<?=SITE_TEMPLATE_PATH?>/img/main/ha-modal-plus.svg" type="image/webp">
                            <img src="<?=SITE_TEMPLATE_PATH?>/img/main/ha-modal-plus.svg" alt=""></picture>
                    </button>
                    <h2 class="uk-modal-title hl-modal__label">Размеры колготок</h2>
                    <div class="popup-table-scroll">
                        <table class="uk-table uk-table-divider hl-modal__tabel">
                            <thead>
                            <tr>
                                <th>Size/размер</th>
                                <th>M</th>
                                <th>L</th>
                                <th>XL</th>
                                <th>XXL</th>
                                <th>XXXL</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Chest/Обхват груди</td>
                                <td>52-56</td>
                                <td>60-64</td>
                                <td>68-72</td>
                                <td>76</td>
                                <td>80</td>
                            </tr>
                            <tr>
                                <td>Height/Рост</td>
                                <td>104-116</td>
                                <td>116-128</td>
                                <td>128-140</td>
                                <td>140-152</td>
                                <td>152-158</td>
                            </tr>
                            <tr>
                                <td>Age/Возраст</td>
                                <td>3-4</td>
                                <td>5-7</td>
                                <td>8-10</td>
                                <td>11-12</td>
                                <td>13-14</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div><?
                break;
        }
        break;
}
?>

<script>
	BX.message({
		ECONOMY_INFO_MESSAGE: '<?=GetMessageJS('CT_BCE_CATALOG_ECONOMY_INFO2')?>',
		TITLE_ERROR: '<?=GetMessageJS('CT_BCE_CATALOG_TITLE_ERROR')?>',
		TITLE_BASKET_PROPS: '<?=GetMessageJS('CT_BCE_CATALOG_TITLE_BASKET_PROPS')?>',
		BASKET_UNKNOWN_ERROR: '<?=GetMessageJS('CT_BCE_CATALOG_BASKET_UNKNOWN_ERROR')?>',
		BTN_SEND_PROPS: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_SEND_PROPS')?>',
		BTN_MESSAGE_BASKET_REDIRECT: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_BASKET_REDIRECT')?>',
		BTN_MESSAGE_CLOSE: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE')?>',
		BTN_MESSAGE_CLOSE_POPUP: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE_POPUP')?>',
		TITLE_SUCCESSFUL: '<?=GetMessageJS('CT_BCE_CATALOG_ADD_TO_BASKET_OK')?>',
		COMPARE_MESSAGE_OK: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_OK')?>',
		COMPARE_UNKNOWN_ERROR: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_UNKNOWN_ERROR')?>',
		COMPARE_TITLE: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_TITLE')?>',
		BTN_MESSAGE_COMPARE_REDIRECT: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT')?>',
		PRODUCT_GIFT_LABEL: '<?=GetMessageJS('CT_BCE_CATALOG_PRODUCT_GIFT_LABEL')?>',
		PRICE_TOTAL_PREFIX: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_PRICE_TOTAL_PREFIX')?>',
		RELATIVE_QUANTITY_MANY: '<?=CUtil::JSEscape($arParams['MESS_RELATIVE_QUANTITY_MANY'])?>',
		RELATIVE_QUANTITY_FEW: '<?=CUtil::JSEscape($arParams['MESS_RELATIVE_QUANTITY_FEW'])?>',
		SITE_ID: '<?=CUtil::JSEscape($component->getSiteId())?>'
	});

	var <?=$obName?> = new JCCatalogElement(<?=CUtil::PhpToJSObject($jsParams, false, true)?>);
</script>
<?
unset($actualItem, $itemIds, $jsParams);
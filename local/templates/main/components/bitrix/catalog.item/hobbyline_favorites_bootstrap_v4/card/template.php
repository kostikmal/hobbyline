<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $item
 * @var array $actualItem
 * @var array $minOffer
 * @var array $itemIds
 * @var array $price
 * @var array $measureRatio
 * @var bool $haveOffers
 * @var bool $showSubscribe
 * @var array $morePhoto
 * @var bool $showSlider
 * @var bool $itemHasDetailUrl
 * @var string $imgTitle
 * @var string $productTitle
 * @var string $buttonSizeClass
 * @var CatalogSectionComponent $component
 */

global $USER;

if ( in_array(NEW_PRODUCT_PROPERTY, $item['PROPERTIES']['SPECIAL_CONDITIONS']['VALUE'] ) ) {
    ?>
    <div class="cat__tab-wrap">
        <div class="cat__tab">Новинка</div>
    </div><?
}
if ( in_array(SALE_PROPERTY, $item['PROPERTIES']['SPECIAL_CONDITIONS']['VALUE'] ) ) {
    ?>
    <div class="cat__tab-wrap">
        <div class="cat__tab cat__tab_pink">SALE</div>
    </div><?
}
if ( in_array(BESTSELLER_PROPERTY, $item['PROPERTIES']['SPECIAL_CONDITIONS']['VALUE'] ) ) {
    ?>
    <div class="cat__tab-wrap">
        <div class="cat__tab cat__tab_green">ХИТ</div>
    </div><?
}
?>

<a href="<?= $item['DETAIL_PAGE_URL'] ?>" class="cat__img">
    <picture><?
        if ( strlen($item['DETAIL_PICTURE']['SRC']) > 0 ) {
            ?><source srcset="<?= $item['DETAIL_PICTURE']['SRC'] ?>" type="image/webp">
            <img src="<?= $item['DETAIL_PICTURE']['SRC'] ?>" alt=""><?
        }
        else {
            ?><source srcset="<?=SITE_TEMPLATE_PATH?>/img/main/hl_prod_no_image.png" type="image/webp">
            <img src="<?=SITE_TEMPLATE_PATH?>/img/main/hl_prod_no_image.png" alt=""><?
        }
    ?></picture>
</a>
<div class="cat__info">
    <div class="cat__label">
        <?= $item['NAME'] ?>
    </div>
    <div class="cat__art">
        Арт. <?= $item['PROPERTIES']['ARTNUMBER']['VALUE'] ?>
    </div>
    <div class="cat__price">
        <span class="cat__price-main"><?

            if (!empty($price)) {
                if ($arParams['PRODUCT_DISPLAY_MODE'] === 'N' && $haveOffers) {
                    echo Loc::getMessage(
                        'CT_BCI_TPL_MESS_PRICE_SIMPLE_MODE',
                        array(
                            '#PRICE#' => $price['PRINT_RATIO_PRICE'],
                            '#VALUE#' => $measureRatio,
                            '#UNIT#' => $minOffer['ITEM_MEASURE']['TITLE']
                        )
                    );
                } else {
                    echo $price['PRINT_RATIO_PRICE'];
                }
            }
            ?></span><?

        if ($arParams['SHOW_OLD_PRICE'] === 'Y') {
            ?><span
            class="cat__price-through" <?= ($price['RATIO_PRICE'] >= $price['RATIO_BASE_PRICE'] ? 'style="display: none;"' : '') ?>>
            <?= $price['PRINT_RATIO_BASE_PRICE'] ?>
            </span>&nbsp;<?
        }

        ?></div>
</div>

<div class="izbr-nav">
    <?
    if (!$haveOffers) {
        if ( $actualItem['CAN_BUY'] ) {
            ?><a href="#modal-tabel-korz_<?= $item['ID'] ?>" class="izbr__basket" uk-toggle></a><?
        }
        else {
            /*?><div class="">
                <?
                if ($showSubscribe) {
                    $APPLICATION->IncludeComponent(
                        'bitrix:catalog.product.subscribe',
                        'hobbyline_default',
                        array(
                            'PRODUCT_ID' => $actualItem['ID'],
                            'BUTTON_ID' => $itemIds['SUBSCRIBE_LINK'],
                            'BUTTON_CLASS' => 'btn btn-primary ' . $buttonSizeClass,
                            'DEFAULT_DISPLAY' => true,
                            'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],
                        ),
                        $component,
                        array('HIDE_ICONS' => 'Y')
                    );
                }
                ?>
                <a href="javascript:void(0);" class="btn-korz uk-margin-top not_avail 333" >
                    <?=$arParams['MESS_NOT_AVAILABLE']?>
                </a>
            </div><?*/
        }
    }
    else {
        if ($arParams['PRODUCT_DISPLAY_MODE'] === 'Y') {
            ?><div class=""><?
                /*if ($showSubscribe) {
                    $APPLICATION->IncludeComponent(
                        'bitrix:catalog.product.subscribe',
                        '',
                        array(
                            'PRODUCT_ID' => $item['ID'],
                            'BUTTON_ID' => $itemIds['SUBSCRIBE_LINK'],
                            'BUTTON_CLASS' => 'btn btn-primary ' . $buttonSizeClass,
                            'DEFAULT_DISPLAY' => !$actualItem['CAN_BUY'],
                            'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],
                        ),
                        $component,
                        array('HIDE_ICONS' => 'Y')
                    );
                }*/

                ?><?/*<a href="javascript:void(0);" class="btn-korz uk-margin-top not_avail 123"  <?= ($actualItem['CAN_BUY'] ? 'style="display: none;"' : '') ?> >
                    <?=$arParams['MESS_NOT_AVAILABLE']?>
                </a>*/?>
                <div id="<?= $itemIds['BASKET_ACTIONS'] ?>" <? echo ($actualItem['CAN_BUY']?'':'style="display: none;"') ?> >
                    <a href="#modal-tabel-korz_<?= $item['ID'] ?>" class="izbr__basket" uk-toggle></a>
                </div>
            </div><?
        }
        else {
            /*?><div class="">
                <a class="btn btn-primary <?= $buttonSizeClass ?>" href="<?= $item['DETAIL_PAGE_URL'] ?>">
                    <?= $arParams['MESS_BTN_DETAIL'] ?>
                    Подробнее
                </a>
            </div><?*/
        }
    }
    ?>

    <?/*<a href="#modal-tabel-korz" class="izbr__basket" uk-toggle></a>*/?>

    <div class="izbr__basket izbr__doted"></div>

    <div uk-drop="mode: click; pos: top-center" class="izbr-drop-wrap">
        <div class="uk-card uk-card-body uk-card-default izbr-drop">
            <a href="#" class="izbr-drop__link">Показать похожие</a>
            <a href="#" class="izbr-drop__link" data-product="<?= $item['ID'] ?>">Удалить из избранного</a>
        </div>
    </div>

</div>

<!-- Попап добавить в корзину-->
<div id="modal-tabel-korz_<?= $item['ID'] ?>" uk-modal>
    <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical hl-modal__body hl-modal__mini">

        <button class="uk-modal-close-outside hl-modal__close" type="button">
            <picture>
                <source srcset="<?= SITE_TEMPLATE_PATH ?>/img/main/ha-modal-plus.svg" type="image/webp">
                <img src="<?= SITE_TEMPLATE_PATH ?>/img/main/ha-modal-plus.svg" alt=""></picture>
        </button>

        <h2 class="uk-modal-title hl-modal__label">Добавить в корзину</h2>

        <div class="product-item">

            <div style="display: none;">
                <? if ($itemHasDetailUrl): ?>
                    <a class="product-item-image-wrapper" href="<?=$item['DETAIL_PAGE_URL']?>" title="<?=$imgTitle?>" data-entity="image-wrapper">
                <? else: ?>
                    <span class="product-item-image-wrapper" data-entity="image-wrapper">
                <? endif; ?>
                    <span class="product-item-image-slider-slide-container slide" id="<?=$itemIds['PICT_SLIDER']?>"<?=($showSlider ? '' : 'style="display: none;"')?>data-slider-interval="<?=$arParams['SLIDER_INTERVAL']?>" data-slider-wrap="true"><?
                        if ($showSlider) {
                            foreach ($morePhoto as $key => $photo) {
                                ?><span class="product-item-image-slide item <?=($key == 0 ? 'active' : '')?>" style="background-image: url('<?=$photo['SRC']?>');"></span><?
                            }
                        }
                    ?></span>
                    <span class="product-item-image-original" id="<?=$itemIds['PICT']?>" style="background-image: url('<?=$item['PREVIEW_PICTURE']['SRC']?>'); <?=($showSlider ? 'display: none;' : '')?>"></span><?
                    if ($item['SECOND_PICT']) {
                        $bgImage = !empty($item['PREVIEW_PICTURE_SECOND']) ? $item['PREVIEW_PICTURE_SECOND']['SRC'] : $item['PREVIEW_PICTURE']['SRC'];
                        ?><span class="product-item-image-alternative" id="<?=$itemIds['SECOND_PICT']?>" style="background-image: url('<?=$bgImage?>'); <?=($showSlider ? 'display: none;' : '')?>"></span><?
                    }

                    if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y') {
                        ?><div class="product-item-label-ring <?=$discountPositionClass?>" id="<?=$itemIds['DSC_PERC']?>"
                            <?=($price['PERCENT'] > 0 ? '' : 'style="display: none;"')?>>
                            <span><?=-$price['PERCENT']?>%</span>
                        </div><?
                    }

                    if ($item['LABEL']) {
                        ?><div class="product-item-label-text <?=$labelPositionClass?>" id="<?=$itemIds['STICKER_ID']?>"><?
                            if (!empty($item['LABEL_ARRAY_VALUE'])) {
                                foreach ($item['LABEL_ARRAY_VALUE'] as $code => $value) {
                                    ?><div<?=(!isset($item['LABEL_PROP_MOBILE'][$code]) ? ' class="hidden-xs"' : '')?>>
                                        <span title="<?=$value?>"><?=$value?></span>
                                    </div><?
                                }
                            }
                        ?></div><?
                    }
                    ?><span class="product-item-image-slider-control-container" id="<?=$itemIds['PICT_SLIDER']?>_indicator"<?=($showSlider ? '' : 'style="display: none;"')?>><?
                        if ($showSlider) {
                            foreach ($morePhoto as $key => $photo) {
                                ?><span class="product-item-image-slider-control<?=($key == 0 ? ' active' : '')?>" data-go-to="<?=$key?>"></span><?
                            }
                        }
                    ?></span><?
                    if ($arParams['SLIDER_PROGRESS'] === 'Y') {
                        ?><span class="product-item-image-slider-progress-bar-container">
                            <span class="product-item-image-slider-progress-bar" id="<?=$itemIds['PICT_SLIDER']?>_progress_bar" style="width: 0;"></span>
                        </span><?
                    }
                    ?>
                <? if ($itemHasDetailUrl): ?>
                    </a>
                <? else: ?>
                    </span>
                <? endif; ?>
            </div>

            <?/*<span class="product-item-image-wrapper" data-entity="image-wrapper" style="">
                <span class="product-item-image-slider-slide-container slide" id="<?= $itemIds['PICT_SLIDER'] ?>" style="display: none;" data-slider-interval="<?= $arParams['SLIDER_INTERVAL'] ?>" data-slider-wrap="true"></span>
                <span class="product-item-image-original" id="<?= $itemIds['PICT'] ?>" style="background-image: url('<?= $item['PREVIEW_PICTURE']['SRC'] ?>'); display: none;"></span>
            </span>*/?>

            <div class="popup-info">

                <div class="popup-info__img">
                    <picture><?
                        if ( strlen($item['DETAIL_PICTURE']['SRC']) > 0 ) {
                            ?><source srcset="<?= $item['DETAIL_PICTURE']['SRC'] ?>" type="image/webp">
                            <img src="<?= $item['DETAIL_PICTURE']['SRC'] ?>" alt=""><?
                        }
                        else {
                            ?><source srcset="<?=SITE_TEMPLATE_PATH?>/img/main/hl_prod_no_image.png" type="image/webp">
                            <img src="<?=SITE_TEMPLATE_PATH?>/img/main/hl_prod_no_image.png" alt=""><?
                        }
                    ?></picture>
                </div>

                <div class="popup-info__content">

                    <div class="popup-info__title">
                        <?=$productTitle?>
                    </div><?

                    if (!empty($arParams['PRODUCT_BLOCKS_ORDER'])) {
                        ?><div class="popup-info__item" data-entity="price-block">
                            <span class="popup-info__label">Цена за шт.:</span>
                            <span class="popup-info__curent" id="<?= $itemIds['PRICE'] ?>"><?
                                if (!empty($price)) {
                                    if ($arParams['PRODUCT_DISPLAY_MODE'] === 'N' && $haveOffers) {
                                        echo Loc::getMessage(
                                            'CT_BCI_TPL_MESS_PRICE_SIMPLE_MODE',
                                            array(
                                                '#PRICE#' => $price['PRINT_RATIO_PRICE'],
                                                '#VALUE#' => $measureRatio,
                                                '#UNIT#' => $minOffer['ITEM_MEASURE']['TITLE']
                                            )
                                        );
                                    } else {
                                        echo $price['PRINT_RATIO_PRICE'];
                                    }
                                }
                            ?></span>
                        </div>

                        <div data-entity="props-block">
                            <?
                            foreach ($item['DISPLAY_PROPERTIES'] as $code => $displayProperty) {
                                if ( $code == 'LINKED_ELEMENTS' ) {continue;}
                                ?><div class="popup-info__item">
                                    <span class="popup-info__label<?= (!isset($item['PROPERTY_CODE_MOBILE'][$code]) ? ' d-none d-sm-block' : '') ?>">
                                        <?= $displayProperty['NAME'] ?>:
                                    </span>
                                    <span class="popup-info__curent<?= (!isset($item['PROPERTY_CODE_MOBILE'][$code]) ? ' d-none d-sm-block' : '') ?>">
                                        <?= (is_array($displayProperty['DISPLAY_VALUE'])
                                            ? implode(' / ', $displayProperty['DISPLAY_VALUE'])
                                            : $displayProperty['DISPLAY_VALUE']) ?>
                                    </span>
                                </div><?

                                if ($arParams['ADD_PROPERTIES_TO_BASKET'] === 'Y' && !empty($item['PRODUCT_PROPERTIES'])) {
                                    ?>
                                    <div id="<?= $itemIds['BASKET_PROP_DIV'] ?>" style="display: none;">
                                        <?
                                        if (!empty($item['PRODUCT_PROPERTIES_FILL'])) {
                                            foreach ($item['PRODUCT_PROPERTIES_FILL'] as $propID => $propInfo) {
                                                ?>
                                                <input type="hidden"
                                                       name="<?= $arParams['PRODUCT_PROPS_VARIABLE'] ?>[<?= $propID ?>]"
                                                       value="<?= htmlspecialcharsbx($propInfo['ID']) ?>">
                                                <?
                                                unset($item['PRODUCT_PROPERTIES'][$propID]);
                                            }
                                        }

                                        if (!empty($item['PRODUCT_PROPERTIES'])) {
                                            ?>
                                            <table>
                                                <?
                                                foreach ($item['PRODUCT_PROPERTIES'] as $propID => $propInfo) {
                                                    ?>
                                                    <tr>
                                                        <td><?= $item['PROPERTIES'][$propID]['NAME'] ?></td>
                                                        <td>
                                                            <?
                                                            if (
                                                                $item['PROPERTIES'][$propID]['PROPERTY_TYPE'] === 'L'
                                                                && $item['PROPERTIES'][$propID]['LIST_TYPE'] === 'C'
                                                            ) {
                                                                foreach ($propInfo['VALUES'] as $valueID => $value) {
                                                                    ?>
                                                                    <label>
                                                                        <? $checked = $valueID === $propInfo['SELECTED'] ? 'checked' : ''; ?>
                                                                        <input type="radio"
                                                                               name="<?= $arParams['PRODUCT_PROPS_VARIABLE'] ?>[<?= $propID ?>]"
                                                                               value="<?= $valueID ?>" <?= $checked ?>>
                                                                        <?= $value ?>
                                                                    </label>
                                                                    <br/>
                                                                    <?
                                                                }
                                                            } else {
                                                                ?>
                                                                <select name="<?= $arParams['PRODUCT_PROPS_VARIABLE'] ?>[<?= $propID ?>]">
                                                                    <?
                                                                    foreach ($propInfo['VALUES'] as $valueID => $value) {
                                                                        $selected = $valueID === $propInfo['SELECTED'] ? 'selected' : '';
                                                                        ?>
                                                                        <option value="<?= $valueID ?>" <?= $selected ?>>
                                                                            <?= $value ?>
                                                                        </option>
                                                                        <?
                                                                    }
                                                                    ?>
                                                                </select>
                                                                <?
                                                            }
                                                            ?>
                                                        </td>
                                                    </tr>
                                                    <?
                                                }
                                                ?>
                                            </table>
                                            <?
                                        }
                                        ?>
                                    </div>
                                    <?
                                }
                            }
                            ?>
                        </div><?

                        if ($arParams['SHOW_MAX_QUANTITY'] !== 'N' && false) {
                            if ($haveOffers) {
                                if ($arParams['PRODUCT_DISPLAY_MODE'] === 'Y') {
                                    ?>
                                    <div class="popup-info__item"
                                         id="<?= $itemIds['QUANTITY_LIMIT'] ?>"
                                         style="display: none;"
                                         data-entity="quantity-limit-block">

                                        <span class="popup-info__label">
                                            <?= $arParams['MESS_SHOW_MAX_QUANTITY'] ?>:
                                        </span>
                                        <span class="popup-info__curent"
                                              data-entity="quantity-limit-value">
                                        </span>

                                    </div>
                                    <?
                                }
                            }
                            else {
                                if (
                                    $measureRatio
                                    && (float)$actualItem['CATALOG_QUANTITY'] > 0
                                    && $actualItem['CATALOG_QUANTITY_TRACE'] === 'Y'
                                    && $actualItem['CATALOG_CAN_BUY_ZERO'] === 'N'
                                )
                                {
                                    ?>
                                    <div class="popup-info__item"
                                         id="<?= $itemIds['QUANTITY_LIMIT'] ?>">

                                        <span class="popup-info__label">
                                            <?= $arParams['MESS_SHOW_MAX_QUANTITY'] ?>:
                                        </span>
                                        <span class="popup-info__curent"
                                              data-entity="quantity-limit-value">
                                                    <?
                                                    if ($arParams['SHOW_MAX_QUANTITY'] === 'M') {
                                                        if ((float)$actualItem['CATALOG_QUANTITY'] / $measureRatio >= $arParams['RELATIVE_QUANTITY_FACTOR']) {
                                                            echo $arParams['MESS_RELATIVE_QUANTITY_MANY'];
                                                        } else {
                                                            echo $arParams['MESS_RELATIVE_QUANTITY_FEW'];
                                                        }
                                                    } else {
                                                        echo $actualItem['CATALOG_QUANTITY'] . ' ' . $actualItem['ITEM_MEASURE']['TITLE'];
                                                    }
                                                    ?>
                                        </span>

                                    </div>
                                    <?
                                }
                            }
                        }

                    }

                ?></div>

            </div>

            <!-- Выбрать размер начало -->

            <?
            if ($arParams['PRODUCT_DISPLAY_MODE'] === 'Y' && $haveOffers && !empty($item['OFFERS_PROP'])) {
                ?>
                <div class="size-table" id="<?= $itemIds['PROP_DIV'] ?>">
                    <div class="hl-card__label">Выбрать вариант:</div>
                    <?/*
                    foreach ($arParams['SKU_PROPS'] as $skuProperty) {
                        $propertyId = $skuProperty['ID'];
                        $skuProperty['NAME'] = htmlspecialcharsbx($skuProperty['NAME']);
                        if (!isset($item['SKU_TREE_VALUES'][$propertyId]))
                            continue;
                        ?>
                        <form id="product-order-form-<?=$item['ID']?>">

                            <input type="hidden" name="ARTNUMBER" value="<?=$item["DISPLAY_PROPERTIES"]["ARTNUMBER"]['DISPLAY_VALUE']?>" >
                            <input type="hidden" name="SKU_PROP_CODE" value="<?=$skuProperty['CODE']?>" >
                            <input type="hidden" name="SKU_PROP_NAME" value="<?=$skuProperty['NAME']?>" >

                        <table
                                class="uk-table uk-table-divider size-table__container"
                                data-entity="sku-block"
                        >
                            <thead>
                            <tr data-entity="sku-line-block">
                                <th class="size-table__th"></th>
                                <?
                                foreach ($skuProperty['VALUES'] as $value) {
                                    if (!isset($item['SKU_TREE_VALUES'][$propertyId][$value['ID']]))
                                        continue;

                                    $value['NAME'] = htmlspecialcharsbx($value['NAME']);

                                    if ($skuProperty['SHOW_MODE'] === 'PICT' && false) {
                                        ?>
                                        <th class="size-table__th"
                                            title="<?= $value['NAME'] ?>"
                                            data-treevalue="<?= $propertyId ?>_<?= $value['ID'] ?>"
                                            data-onevalue="<?= $value['ID'] ?>">
                                            <div class="product-item-scu-item-color-block">
                                                <div class="product-item-scu-item-color"
                                                     title="<?= $value['NAME'] ?>"
                                                     style="background-image: url('<?= $value['PICT']['SRC'] ?>');"></div>
                                            </div>
                                        </th>
                                        <?
                                    }
                                    else {
                                        ?>
                                        <th class="size-table__th"
                                            title="<?= $value['NAME'] ?>"
                                            data-treevalue="<?= $propertyId ?>_<?= $value['ID'] ?>"
                                            data-onevalue="<?= $value['ID'] ?>">
                                            <div class="product-item-scu-item-text-block">
                                                <div class="product-item-scu-item-text"><?= $value['NAME'] ?></div>
                                            </div>
                                        </th>
                                        <?
                                    }
                                }
                                ?>
                            </tr>
                            </thead>

                            <tbody>
                                <tr>
                                    <td class="size-table__label">Поштучно:</td>
                                    <?
                                    foreach ($skuProperty['VALUES'] as $value) {
                                        if (!isset($item['SKU_TREE_VALUES'][$propertyId][$value['ID']]))
                                            continue;

                                        $value['NAME'] = htmlspecialcharsbx($value['NAME']);

                                        ?>
                                        <td class="size-table__td"
                                            title="<?//= $value['NAME'] ?>"
                                            data-treevalue="<?= $propertyId ?>_<?= $value['ID'] ?>"
                                            data-onevalue="<?= $value['ID'] ?>">

                                            <div class="step">
                                                <div class=" <?= $arResult['ITEM']['OFFERS'][$value['ID']]['CAN_BUY']?'step__plus':''?> "></div>
                                                <input
                                                    class="step__input"
                                                    type="text"
                                                    name="OFFERS[<?= $item['OFFERS'][$value['ID']]['ID'] ?>][<?=$value['NAME']?>]"
                                                    value="0"
                                                    <?= !$arResult['ITEM']['OFFERS'][$value['ID']]['CAN_BUY']?'disabled':''?>
                                                >
                                                <div class=" <?= $arResult['ITEM']['OFFERS'][$value['ID']]['CAN_BUY']?'step__minus':''?> "></div>
                                            </div>

                                        </td>
                                        <?

                                    }
                                    ?>
                                </tr>
                            </tbody>

                        </table>
                        </form>
                        <?
                    }*/
                    ?>

                    <form id="product-order-form-<?=$item['ID']?>">
                        <input type="hidden" name="ARTNUMBER" value="<?=$item["DISPLAY_PROPERTIES"]["ARTNUMBER"]['DISPLAY_VALUE']?>" >
                        <?/*<input type="hidden" name="SKU_PROP_CODE" value="<?=$skuProperty['CODE']?>" >
                        <input type="hidden" name="SKU_PROP_NAME" value="<?=$skuProperty['NAME']?>" >*/?>

                        <table
                            class="uk-table uk-table-divider size-table__container"
                            data-entity="sku-block"
                        >
                            <thead>
                                <tr data-entity="sku-line-block">
                                    <th class="size-table__th"></th><?
                                    foreach ( $arResult['ITEM']['OFFERS'] as $arOffer ) {

                                        $sColor = $arOffer['PROPERTIES']['COLOR']['VALUE'];
                                        $sSize = $arOffer['PROPERTIES']['SIZE']['VALUE'];

                                        $sColor = $arParams['SKU_PROPS']['COLOR']['VALUES_BY_CODE'][$sColor]['NAME'];
                                        $sSize = $arParams['SKU_PROPS']['SIZE']['VALUES_BY_CODE'][$sSize]['NAME'];

                                        ?><th class="size-table__th"
                                              title="<?= $value['NAME'] ?>"
                                              data-treevalue="<?//= $propertyId ?>_<?//= $value['ID'] ?>"
                                              data-onevalue="<?//= $value['ID'] ?>">
                                            <div class="product-item-scu-item-text-block">
                                                <div class="product-item-scu-item-text"><?
                                                    echo $sColor;
                                                    echo '<br>';
                                                    echo $sSize;
                                                ?></div>
                                            </div>
                                        </th><?
                                    }
                                ?></tr>
                            </thead>

                            <tbody>
                                <tr>
                                    <td class="size-table__label">Поштучно:</td><?

                                    foreach ( $arResult['ITEM']['OFFERS'] as $arOffer ) {

                                        $sColor = $arOffer['PROPERTIES']['COLOR']['VALUE'];
                                        $sSize = $arOffer['PROPERTIES']['SIZE']['VALUE'];

                                        $sColor = trim( $arParams['SKU_PROPS']['COLOR']['VALUES_BY_CODE'][$sColor]['NAME'] );
                                        $sSize = trim( $arParams['SKU_PROPS']['SIZE']['VALUES_BY_CODE'][$sSize]['NAME'] );

                                        $sColor = strlen($sColor)>0?$sColor:'none';
                                        $sSize = strlen($sSize)>0?$sSize:'none';

                                        ?><td class="size-table__td"
                                              title="<?//= $value['NAME'] ?>"
                                              data-treevalue="<?//= $propertyId ?>_<?//= $value['ID'] ?>"
                                              data-onevalue="<?//= $value['ID'] ?>">

                                            <div class="step">
                                                <div class=" <?= $arOffer['CAN_BUY']?'step__plus':''?> "></div>
                                                <input
                                                    class="step__input"
                                                    type="text"
                                                    name="OFFERS[<?= $arOffer['ID'] ?>][<?=$sColor?>][<?=$sSize?>]"
                                                    value="0"
                                                    max="<?= $arOffer['PRODUCT']['QUANTITY']?>"
                                                    <?= !$arOffer['CAN_BUY']?'disabled':''?>
                                                >
                                                <div class=" <?= $arOffer['CAN_BUY']?'step__minus':''?> "></div>
                                            </div>

                                        </td><?
                                    }

                                ?></tr>
                            </tbody>

                        </table>
                        <?



                    ?></form>

                </div>
                <?
                foreach ($arParams['SKU_PROPS'] as $skuProperty) {
                    if (!isset($item['OFFERS_PROP'][$skuProperty['CODE']]))
                        continue;

                    $skuProps[] = array(
                        'ID' => $skuProperty['ID'],
                        'SHOW_MODE' => $skuProperty['SHOW_MODE'],
                        'VALUES' => $skuProperty['VALUES'],
                        'VALUES_COUNT' => $skuProperty['VALUES_COUNT']
                    );
                }

                unset($skuProperty, $value);

                if ($item['OFFERS_PROPS_DISPLAY']) {
                    foreach ($item['JS_OFFERS'] as $keyOffer => $jsOffer) {
                        $strProps = '';

                        if (!empty($jsOffer['DISPLAY_PROPERTIES'])) {
                            foreach ($jsOffer['DISPLAY_PROPERTIES'] as $displayProperty) {
                                $strProps .= '<dt>' . $displayProperty['NAME'] . '</dt><dd>'
                                    . (is_array($displayProperty['VALUE'])
                                        ? implode(' / ', $displayProperty['VALUE'])
                                        : $displayProperty['VALUE'])
                                    . '</dd>';
                            }
                        }

                        $item['JS_OFFERS'][$keyOffer]['DISPLAY_PROPERTIES'] = $strProps;
                    }
                    unset($jsOffer, $strProps);
                }
            }
            elseif ( $arParams['PRODUCT_DISPLAY_MODE'] === 'Y' && !$haveOffers ) {
                ?><form id="product-order-form-<?=$item['ID']?>">

                <input type="hidden" name="ARTNUMBER" value="<?=$item["DISPLAY_PROPERTIES"]["ARTNUMBER"]['DISPLAY_VALUE']?>" >

                <table
                    class="uk-table uk-table-divider size-table__container"
                    data-entity="sku-block"
                >
                    <thead>
                        <tr data-entity="sku-line-block">
                            <th class="size-table__th"></th>
                                <th class="size-table__th">
                                    <div class="product-item-scu-item-text-block">
                                        <div class="product-item-scu-item-text">Количество</div>
                                    </div>
                                </th>
                        </tr>
                    </thead>

                    <tbody>
                    <tr>
                        <td class="size-table__label">Поштучно:</td>

                            <td class="size-table__td">

                                <div class="step">
                                    <div class=" <?= $actualItem['CAN_BUY']?'step__plus':''?> "></div>
                                    <input
                                        class="step__input"
                                        type="text"
                                        name="PRODUCT[<?= $actualItem['ID'] ?>]"
                                        value="0"
                                        <?= !$actualItem['CAN_BUY']?'disabled':''?>
                                    >
                                    <div class=" <?= $actualItem['CAN_BUY']?'step__minus':''?> "></div>
                                </div>

                            </td>

                    </tr>
                    </tbody>

                </table>
                </form><?
            }
            ?>

            <!-- Выбрать размер конец -->

            <?
            if (!empty($arParams['PRODUCT_BLOCKS_ORDER'])) {
                foreach ($arParams['PRODUCT_BLOCKS_ORDER'] as $blockName) {
                    switch ($blockName) {
                        case 'price':
                            /*?><div class="product-item-info-container product-item-price-container"
                                 data-entity="price-block">
                                <?
                                if ($arParams['SHOW_OLD_PRICE'] === 'Y') {
                                    ?>
                                    <span class="product-item-price-old" id="<?= $itemIds['PRICE_OLD'] ?>"
                                            <?= ($price['RATIO_PRICE'] >= $price['RATIO_BASE_PRICE'] ? 'style="display: none;"' : '') ?>>
                                            <?= $price['PRINT_RATIO_BASE_PRICE'] ?>
                                        </span>&nbsp;
                                    <?
                                }
                                ?>
                                <span class="product-item-price-current" id="<?= $itemIds['PRICE'] ?>">
                                        <?
                                        if (!empty($price)) {
                                            if ($arParams['PRODUCT_DISPLAY_MODE'] === 'N' && $haveOffers) {
                                                echo Loc::getMessage(
                                                    'CT_BCI_TPL_MESS_PRICE_SIMPLE_MODE',
                                                    array(
                                                        '#PRICE#' => $price['PRINT_RATIO_PRICE'],
                                                        '#VALUE#' => $measureRatio,
                                                        '#UNIT#' => $minOffer['ITEM_MEASURE']['TITLE']
                                                    )
                                                );
                                            } else {
                                                echo $price['PRINT_RATIO_PRICE'];
                                            }
                                        }
                                        ?>
                                    </span>
                            </div><?*/
                            break;

                        case 'quantityLimit':
                            /*if ($arParams['SHOW_MAX_QUANTITY'] !== 'N') {
                                if ($haveOffers) {
                                    if ($arParams['PRODUCT_DISPLAY_MODE'] === 'Y') {
                                        ?>
                                        <div class="product-item-info-container product-item-hidden"
                                             id="<?= $itemIds['QUANTITY_LIMIT'] ?>"
                                             style="display: none;"
                                             data-entity="quantity-limit-block">
                                            <div class="product-item-info-container-title text-muted">
                                                <?= $arParams['MESS_SHOW_MAX_QUANTITY'] ?>:
                                                <span class="product-item-quantity text-dark"
                                                      data-entity="quantity-limit-value"></span>
                                            </div>
                                        </div>
                                        <?
                                    }
                                } else {
                                    if (
                                        $measureRatio
                                        && (float)$actualItem['CATALOG_QUANTITY'] > 0
                                        && $actualItem['CATALOG_QUANTITY_TRACE'] === 'Y'
                                        && $actualItem['CATALOG_CAN_BUY_ZERO'] === 'N'
                                    ) {
                                        ?>
                                        <div class="product-item-info-container product-item-hidden"
                                             id="<?= $itemIds['QUANTITY_LIMIT'] ?>">
                                            <div class="product-item-info-container-title text-muted">
                                                <?= $arParams['MESS_SHOW_MAX_QUANTITY'] ?>:
                                                <span class="product-item-quantity text-dark"
                                                      data-entity="quantity-limit-value">
                                                    <?
                                                    if ($arParams['SHOW_MAX_QUANTITY'] === 'M') {
                                                        if ((float)$actualItem['CATALOG_QUANTITY'] / $measureRatio >= $arParams['RELATIVE_QUANTITY_FACTOR']) {
                                                            echo $arParams['MESS_RELATIVE_QUANTITY_MANY'];
                                                        } else {
                                                            echo $arParams['MESS_RELATIVE_QUANTITY_FEW'];
                                                        }
                                                    } else {
                                                        echo $actualItem['CATALOG_QUANTITY'] . ' ' . $actualItem['ITEM_MEASURE']['TITLE'];
                                                    }
                                                    ?>
                                                </span>
                                            </div>
                                        </div>
                                        <?
                                    }
                                }
                            }*/
                            break;

                        case 'quantity':
                            /*if (!$haveOffers) {
                                if ($actualItem['CAN_BUY'] && $arParams['USE_PRODUCT_QUANTITY']) {
                                    ?>
                                    <div class="product-item-info-container product-item-hidden"
                                         data-entity="quantity-block">
                                        <div class="product-item-amount">
                                            <div class="product-item-amount-field-container">
                                                        <span class="product-item-amount-field-btn-minus no-select"
                                                              id="<?= $itemIds['QUANTITY_DOWN'] ?>"></span>
                                                <input class="product-item-amount-field"
                                                       id="<?= $itemIds['QUANTITY'] ?>"
                                                       type="number"
                                                       name="<?= $arParams['PRODUCT_QUANTITY_VARIABLE'] ?>"
                                                       value="<?= $measureRatio ?>">
                                                <span class="product-item-amount-field-btn-plus no-select"
                                                      id="<?= $itemIds['QUANTITY_UP'] ?>"></span>
                                                <span class="product-item-amount-description-container">
                                                        <span id="<?= $itemIds['QUANTITY_MEASURE'] ?>">
                                                            <?= $actualItem['ITEM_MEASURE']['TITLE'] ?>
                                                        </span>
                                                        <span id="<?= $itemIds['PRICE_TOTAL'] ?>"></span>
                                                    </span>
                                            </div>
                                        </div>
                                    </div>
                                    <?
                                }
                            }
                            elseif ($arParams['PRODUCT_DISPLAY_MODE'] === 'Y') {
                                if ($arParams['USE_PRODUCT_QUANTITY']) {
                                    ?>
                                    <div class="product-item-info-container product-item-hidden"
                                         data-entity="quantity-block">
                                        <div class="product-item-amount">
                                            <div class="product-item-amount-field-container">
                                                        <span class="product-item-amount-field-btn-minus no-select"
                                                              id="<?= $itemIds['QUANTITY_DOWN'] ?>"></span>
                                                <input class="product-item-amount-field"
                                                       id="<?= $itemIds['QUANTITY'] ?>"
                                                       type="number"
                                                       name="<?= $arParams['PRODUCT_QUANTITY_VARIABLE'] ?>"
                                                       value="<?= $measureRatio ?>">
                                                <span class="product-item-amount-field-btn-plus no-select"
                                                      id="<?= $itemIds['QUANTITY_UP'] ?>"></span>
                                                <span class="product-item-amount-description-container">
                                                        <span id="<?= $itemIds['QUANTITY_MEASURE'] ?>"><?= $actualItem['ITEM_MEASURE']['TITLE'] ?></span>
                                                        <span id="<?= $itemIds['PRICE_TOTAL'] ?>"></span>
                                                    </span>
                                            </div>
                                        </div>
                                    </div>
                                    <?
                                }
                            }*/
                            break;

                        case 'buttons':
                            ?><div class="product-item-info-container product-item-hidden" data-entity="buttons-block">
                                <?
                                if (!$haveOffers) {
                                    if ($actualItem['CAN_BUY']) {
                                        ?>
                                        <div class=""
                                             id="<?= $itemIds['BASKET_ACTIONS'] ?>">
                                            <a class="btn btn-primary <?= $buttonSizeClass ?> btn-korz send-product-order"
                                                    id="<?= $itemIds['BUY_LINK'] ?>"
                                                    data-id="<?=$item['ID']?>"
                                                    href="javascript:void(0)" rel="nofollow">
                                                <?= ($arParams['ADD_TO_BASKET_ACTION'] === 'BUY' ? $arParams['MESS_BTN_BUY'] : $arParams['MESS_BTN_ADD_TO_BASKET']) ?>
                                            </a>
                                        </div>
                                        <?
                                    }
                                    else {
                                        ?>
                                        <div class="">
                                            <?
                                            if ($showSubscribe) {
                                                $APPLICATION->IncludeComponent(
                                                    'bitrix:catalog.product.subscribe',
                                                    '',
                                                    array(
                                                        'PRODUCT_ID' => $actualItem['ID'],
                                                        'BUTTON_ID' => $itemIds['SUBSCRIBE_LINK'],
                                                        'BUTTON_CLASS' => 'btn btn-primary ' . $buttonSizeClass,
                                                        'DEFAULT_DISPLAY' => true,
                                                        'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],
                                                    ),
                                                    $component,
                                                    array('HIDE_ICONS' => 'Y')
                                                );
                                            }
                                            ?>
                                            <a class="btn btn-link <?= $buttonSizeClass ?> not_avail"
                                                    id="<?= $itemIds['NOT_AVAILABLE_MESS'] ?>" href="javascript:void(0)"
                                                    rel="nofollow">
                                                <?= $arParams['MESS_NOT_AVAILABLE'] ?>
                                            </a>
                                        </div>
                                        <?
                                    }
                                }
                                else {
                                    if ($arParams['PRODUCT_DISPLAY_MODE'] === 'Y') {
                                        ?>
                                        <div class="">
                                            <?
                                            if ($showSubscribe) {
                                                $APPLICATION->IncludeComponent(
                                                    'bitrix:catalog.product.subscribe',
                                                    '',
                                                    array(
                                                        'PRODUCT_ID' => $item['ID'],
                                                        'BUTTON_ID' => $itemIds['SUBSCRIBE_LINK'],
                                                        'BUTTON_CLASS' => 'btn btn-primary ' . $buttonSizeClass,
                                                        'DEFAULT_DISPLAY' => !$actualItem['CAN_BUY'],
                                                        'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],
                                                    ),
                                                    $component,
                                                    array('HIDE_ICONS' => 'Y')
                                                );
                                            }
                                            ?>
                                            <a class="btn btn-link <?= $buttonSizeClass ?> not_avail"
                                                    id="<?= $itemIds['NOT_AVAILABLE_MESS'] ?>"
                                                    href="javascript:void(0)"
                                                    rel="nofollow"
                                                <?= ($actualItem['CAN_BUY'] ? 'style="display: none;"' : '') ?>>
                                                <?= $arParams['MESS_NOT_AVAILABLE'] ?>
                                            </a>

                                            <div id="<?= $itemIds['BASKET_ACTIONS'] ?>" <?= ($actualItem['CAN_BUY'] ? '' : 'style="display: none;"') ?>>
                                                <a class="btn btn-primary <?= $buttonSizeClass ?> btn-korz send-product-order"
                                                        id="<?= $itemIds['BUY_LINK'] ?>"
                                                        data-id="<?=$item['ID']?>"
                                                        href="javascript:void(0)" rel="nofollow">
                                                    <?= ($arParams['ADD_TO_BASKET_ACTION'] === 'BUY' ? $arParams['MESS_BTN_BUY'] : $arParams['MESS_BTN_ADD_TO_BASKET']) ?>
                                                </a>
                                            </div>
                                        </div>
                                        <?
                                    }
                                    else {
                                        ?>
                                        <div class="product-item-button-container">
                                            <button class="btn btn-primary <?= $buttonSizeClass ?>"
                                                    href="<?= $item['DETAIL_PAGE_URL'] ?>">
                                                <?= $arParams['MESS_BTN_DETAIL'] ?>
                                            </button>
                                        </div>
                                        <?
                                    }
                                }
                                ?>
                            </div><?
                            break;

                        case 'props':
                            /*if (!$haveOffers) {
                                if (!empty($item['DISPLAY_PROPERTIES'])) {
                                    ?>
                                    <div class="product-item-info-container product-item-hidden"
                                         data-entity="props-block">
                                        <dl class="product-item-properties">
                                            <?
                                            foreach ($item['DISPLAY_PROPERTIES'] as $code => $displayProperty) {
                                                ?>
                                                <dt class="text-muted<?= (!isset($item['PROPERTY_CODE_MOBILE'][$code]) ? ' d-none d-sm-block' : '') ?>">
                                                    <?= $displayProperty['NAME'] ?>
                                                </dt>
                                                <dd class="text-dark<?= (!isset($item['PROPERTY_CODE_MOBILE'][$code]) ? ' d-none d-sm-block' : '') ?>">
                                                    <?= (is_array($displayProperty['DISPLAY_VALUE'])
                                                        ? implode(' / ', $displayProperty['DISPLAY_VALUE'])
                                                        : $displayProperty['DISPLAY_VALUE']) ?>
                                                </dd>
                                                <?
                                            }
                                            ?>
                                        </dl>
                                    </div>
                                    <?
                                }

                                if ($arParams['ADD_PROPERTIES_TO_BASKET'] === 'Y' && !empty($item['PRODUCT_PROPERTIES'])) {
                                    ?>
                                    <div id="<?= $itemIds['BASKET_PROP_DIV'] ?>" style="display: none;">
                                        <?
                                        if (!empty($item['PRODUCT_PROPERTIES_FILL'])) {
                                            foreach ($item['PRODUCT_PROPERTIES_FILL'] as $propID => $propInfo) {
                                                ?>
                                                <input type="hidden"
                                                       name="<?= $arParams['PRODUCT_PROPS_VARIABLE'] ?>[<?= $propID ?>]"
                                                       value="<?= htmlspecialcharsbx($propInfo['ID']) ?>">
                                                <?
                                                unset($item['PRODUCT_PROPERTIES'][$propID]);
                                            }
                                        }

                                        if (!empty($item['PRODUCT_PROPERTIES'])) {
                                            ?>
                                            <table>
                                                <?
                                                foreach ($item['PRODUCT_PROPERTIES'] as $propID => $propInfo) {
                                                    ?>
                                                    <tr>
                                                        <td><?= $item['PROPERTIES'][$propID]['NAME'] ?></td>
                                                        <td>
                                                            <?
                                                            if (
                                                                $item['PROPERTIES'][$propID]['PROPERTY_TYPE'] === 'L'
                                                                && $item['PROPERTIES'][$propID]['LIST_TYPE'] === 'C'
                                                            ) {
                                                                foreach ($propInfo['VALUES'] as $valueID => $value) {
                                                                    ?>
                                                                    <label>
                                                                        <? $checked = $valueID === $propInfo['SELECTED'] ? 'checked' : ''; ?>
                                                                        <input type="radio"
                                                                               name="<?= $arParams['PRODUCT_PROPS_VARIABLE'] ?>[<?= $propID ?>]"
                                                                               value="<?= $valueID ?>" <?= $checked ?>>
                                                                        <?= $value ?>
                                                                    </label>
                                                                    <br/>
                                                                    <?
                                                                }
                                                            } else {
                                                                ?>
                                                                <select name="<?= $arParams['PRODUCT_PROPS_VARIABLE'] ?>[<?= $propID ?>]">
                                                                    <?
                                                                    foreach ($propInfo['VALUES'] as $valueID => $value) {
                                                                        $selected = $valueID === $propInfo['SELECTED'] ? 'selected' : '';
                                                                        ?>
                                                                        <option value="<?= $valueID ?>" <?= $selected ?>>
                                                                            <?= $value ?>
                                                                        </option>
                                                                        <?
                                                                    }
                                                                    ?>
                                                                </select>
                                                                <?
                                                            }
                                                            ?>
                                                        </td>
                                                    </tr>
                                                    <?
                                                }
                                                ?>
                                            </table>
                                            <?
                                        }
                                        ?>
                                    </div>
                                    <?
                                }
                            }
                            else {
                                $showProductProps = !empty($item['DISPLAY_PROPERTIES']);
                                $showOfferProps = $arParams['PRODUCT_DISPLAY_MODE'] === 'Y' && $item['OFFERS_PROPS_DISPLAY'];

                                if ($showProductProps || $showOfferProps) {
                                    ?>
                                    <div class="product-item-info-container product-item-hidden"
                                         data-entity="props-block">
                                        <dl class="product-item-properties">
                                            <?
                                            if ($showProductProps) {
                                                foreach ($item['DISPLAY_PROPERTIES'] as $code => $displayProperty) {
                                                    ?>
                                                    <dt class="text-muted<?= (!isset($item['PROPERTY_CODE_MOBILE'][$code]) ? ' d-none d-sm-block' : '') ?>">
                                                        <?= $displayProperty['NAME'] ?>
                                                    </dt>
                                                    <dd class="text-dark<?= (!isset($item['PROPERTY_CODE_MOBILE'][$code]) ? ' d-none d-sm-block' : '') ?>">
                                                        <?= (is_array($displayProperty['DISPLAY_VALUE'])
                                                            ? implode(' / ', $displayProperty['DISPLAY_VALUE'])
                                                            : $displayProperty['DISPLAY_VALUE']) ?>
                                                    </dd>
                                                    <?
                                                }
                                            }

                                            if ($showOfferProps) {
                                                ?>
                                                <span id="<?= $itemIds['DISPLAY_PROP_DIV'] ?>"
                                                      style="display: none;"></span>
                                                <?
                                            }
                                            ?>
                                        </dl>
                                    </div>
                                    <?
                                }
                            }*/
                            break;

                        case 'sku':
                            if ($arParams['PRODUCT_DISPLAY_MODE'] === 'Y' && $haveOffers && !empty($item['OFFERS_PROP'])) {
                                ?>
                                <div class="product-item-info-container product-item-hidden"
                                     id="<?= $itemIds['PROP_DIV'] ?>"
                                     style="display: none;"
                                >
                                    <?
                                    foreach ($arParams['SKU_PROPS'] as $skuProperty) {
                                        $propertyId = $skuProperty['ID'];
                                        $skuProperty['NAME'] = htmlspecialcharsbx($skuProperty['NAME']);
                                        if (!isset($item['SKU_TREE_VALUES'][$propertyId]))
                                            continue;
                                        ?>
                                        <div data-entity="sku-block">
                                            <div class="product-item-scu-container" data-entity="sku-line-block">
                                                <div class="product-item-scu-block-title text-muted"><?= $skuProperty['NAME'] ?></div>
                                                <div class="product-item-scu-block">
                                                    <div class="product-item-scu-list">
                                                        <ul class="product-item-scu-item-list">
                                                            <?
                                                            foreach ($skuProperty['VALUES'] as $value) {
                                                                if (!isset($item['SKU_TREE_VALUES'][$propertyId][$value['ID']]))
                                                                    continue;

                                                                $value['NAME'] = htmlspecialcharsbx($value['NAME']);

                                                                if ($skuProperty['SHOW_MODE'] === 'PICT') {
                                                                    ?>
                                                                    <li class="product-item-scu-item-color-container"
                                                                        title="<?= $value['NAME'] ?>"
                                                                        data-treevalue="<?= $propertyId ?>_<?= $value['ID'] ?>"
                                                                        data-onevalue="<?= $value['ID'] ?>">
                                                                        <div class="product-item-scu-item-color-block">
                                                                            <div class="product-item-scu-item-color"
                                                                                 title="<?= $value['NAME'] ?>"
                                                                                 style="background-image: url('<?= $value['PICT']['SRC'] ?>');"></div>
                                                                        </div>
                                                                    </li>
                                                                    <?
                                                                } else {
                                                                    ?>
                                                                    <li class="product-item-scu-item-text-container"
                                                                        title="<?= $value['NAME'] ?>"
                                                                        data-treevalue="<?= $propertyId ?>_<?= $value['ID'] ?>"
                                                                        data-onevalue="<?= $value['ID'] ?>">
                                                                        <div class="product-item-scu-item-text-block">
                                                                            <div class="product-item-scu-item-text"><?= $value['NAME'] ?></div>
                                                                        </div>
                                                                    </li>
                                                                    <?
                                                                }
                                                            }
                                                            ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?
                                    }
                                    ?>
                                </div>
                                <?
                                foreach ($arParams['SKU_PROPS'] as $skuProperty) {
                                    if (!isset($item['OFFERS_PROP'][$skuProperty['CODE']]))
                                        continue;

                                    $skuProps[] = array(
                                        'ID' => $skuProperty['ID'],
                                        'SHOW_MODE' => $skuProperty['SHOW_MODE'],
                                        'VALUES' => $skuProperty['VALUES'],
                                        'VALUES_COUNT' => $skuProperty['VALUES_COUNT']
                                    );
                                }

                                unset($skuProperty, $value);

                                if ($item['OFFERS_PROPS_DISPLAY']) {
                                    foreach ($item['JS_OFFERS'] as $keyOffer => $jsOffer) {
                                        $strProps = '';

                                        if (!empty($jsOffer['DISPLAY_PROPERTIES'])) {
                                            foreach ($jsOffer['DISPLAY_PROPERTIES'] as $displayProperty) {
                                                $strProps .= '<dt>' . $displayProperty['NAME'] . '</dt><dd>'
                                                    . (is_array($displayProperty['VALUE'])
                                                        ? implode(' / ', $displayProperty['VALUE'])
                                                        : $displayProperty['VALUE'])
                                                    . '</dd>';
                                            }
                                        }

                                        $item['JS_OFFERS'][$keyOffer]['DISPLAY_PROPERTIES'] = $strProps;
                                    }
                                    unset($jsOffer, $strProps);
                                }
                            }
                            break;
                    }
                }
            }
            ?>

        </div>

    </div>
</div>
<?php

if ( count( $arResult['ITEM']['PROPERTIES']['MORE_PHOTO']['VALUE'] ) <= 0 && intval( $arResult['ITEM']['DETAIL_PICTURE']['ID'] ) > 0 ) {
    $arResult['ITEM']['PROPERTIES']['MORE_PHOTO']['VALUE'][] = $arResult['ITEM']['DETAIL_PICTURE']['ID'];
}

foreach ( $arResult['ITEM']['PROPERTIES']['MORE_PHOTO']['VALUE'] as $nPictID ) {
    $arResult['ITEM']['PROPERTIES']['MORE_PHOTO']['DISPLAY_SRC'][] = CFile::ResizeImageGet( $nPictID, ['width'=>295, 'height'=>394], BX_RESIZE_IMAGE_EXACT )['src'];
}

var_dump( $arResult['ITEM']['PROPERTIES']['MORE_PHOTO']['DISPLAY_SRC'] );

?>
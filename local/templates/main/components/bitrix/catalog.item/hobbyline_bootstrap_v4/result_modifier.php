<?php

global $USER;

if ( !is_array( $arResult['ITEM']['PROPERTIES']['MORE_PHOTO']['VALUE'] ) && intval( $arResult['ITEM']['DETAIL_PICTURE']['ID'] ) > 0 ) {
    $arResult['ITEM']['PROPERTIES']['MORE_PHOTO']['VALUE'][] = $arResult['ITEM']['DETAIL_PICTURE']['ID'];
}

if ( is_array( $arResult['ITEM']['PROPERTIES']['MORE_PHOTO']['VALUE'] ) && intval( $arResult['ITEM']['DETAIL_PICTURE']['ID'] ) <= 0 ) {
    $arResult['ITEM']['DETAIL_PICTURE']['SRC'] = CFile::ResizeImageGet( $arResult['ITEM']['PROPERTIES']['MORE_PHOTO']['VALUE'][0], ['width'=>295, 'height'=>394], BX_RESIZE_IMAGE_PROPORTIONAL )['src'];
}

foreach ( $arResult['ITEM']['PROPERTIES']['MORE_PHOTO']['VALUE'] as $nPictID ) {
    $arResult['ITEM']['PROPERTIES']['MORE_PHOTO']['DISPLAY_SRC'][] = CFile::ResizeImageGet( $nPictID, ['width'=>295, 'height'=>394], BX_RESIZE_IMAGE_PROPORTIONAL )['src'];
}

if ( count( $arResult['ITEM']['PROPERTIES']['LINKED_ELEMENTS']['VALUE'] ) > 0 ) {

    $resExist = CIBlockElement::GetList(
        ["ID"=>"ASC"],
        ['IBLOCK_ID'=>$arResult['ITEM']['PROPERTIES']['LINKED_ELEMENTS']['IBLOCK_ID'], 'ACTIVE'=>'Y', 'ID'=>$arResult['ITEM']['PROPERTIES']['LINKED_ELEMENTS']['VALUE']],
        false,
        false,
        ['ID', 'NAME', 'CODE', 'DETAIL_PICTURE']
    );

    while ( $arResExist = $resExist->GetNext() ) {

        $arResult['ITEM']['PROPERTIES']['LINKED_ELEMENTS']['ELEMENTS'][] = [
            'NAME' => $arResExist['NAME'],
            'LINK' => '/catalog/products/'.$arResExist['CODE'].'/',
            'PICTURE' => CFile::ResizeImageGet( $arResExist['DETAIL_PICTURE'], ['width'=>70, 'height'=>110], BX_RESIZE_IMAGE_PROPORTIONAL )['src'],
        ];

    }

}

//file_put_contents( $_SERVER['DOCUMENT_ROOT'].'/xxx_catalog_item_offers_'.$arResult['ITEM']['ID'].'.txt', print_r($arResult['ITEM']['OFFERS'], true) );

foreach ( $arParams['SKU_PROPS'] as $sPropCode=>&$arSkuProp ) {
    foreach ( $arSkuProp['VALUES'] as $arValue ) {
        $arSkuProp['VALUES_BY_CODE'][ $arValue['XML_ID'] ] = $arValue;
    }
}

$arResult['FAVORITE_PRODUCTS'] = [];

if ( $USER->IsAuthorized() ) {
    $nUserID = $USER->GetID();

    $res = CIBlockElement::GetList(
        ["SORT"=>"ASC"],
        ['IBLOCK_ID'=>FAVORITE_PRODUCTS, 'ACTIVE'=>'Y', 'PROPERTY_USER'=>$nUserID],
        false,
        false,
        ['ID', 'PROPERTY_PRODUCTS']
    );

    if ( $arRes = $res->GetNext() ) {
        $arResult['FAVORITE_PRODUCTS'] = $arRes['PROPERTY_PRODUCTS_VALUE'];
    }
}

?>
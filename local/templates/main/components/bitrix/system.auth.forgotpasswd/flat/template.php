<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 */

//one css for all system.auth.* forms
$APPLICATION->SetAdditionalCSS("/bitrix/css/main/system.auth/flat/style.css");
?>

<div class="main-content auth-wrap">
    <div class="auth-form">

<?
if(!empty($arParams["~AUTH_RESULT"])):
	$text = str_replace(array("<br>", "<br />"), "\n", $arParams["~AUTH_RESULT"]["MESSAGE"]);
?>
	<div class="alert <?=($arParams["~AUTH_RESULT"]["TYPE"] == "OK"? "alert-success":"alert-danger")?>"><?=nl2br(htmlspecialcharsbx($text))?></div>
<?endif?>

        <h2 class="auth-form__label"><?=GetMessage("AUTH_GET_CHECK_STRING")?></h2>

	<?/*<p class="bx-authform-content-container"><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></p>*/?>

	<form name="bform" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
<?if($arResult["BACKURL"] <> ''):?>
		<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
<?endif?>
		<input type="hidden" name="AUTH_FORM" value="Y">
		<input type="hidden" name="TYPE" value="SEND_PWD">

        <div class="auth-form__text">
            <input type="text" id="email-input" placeholder="<?echo GetMessage("AUTH_LOGIN_EMAIL")?>*" name="USER_LOGIN" maxlength="255" value="<?=$arResult["LAST_LOGIN"]?>">
            <input type="hidden" name="USER_EMAIL" />
        </div>

        <a href="#" class="link-half lookbook__link auth-form__rec-pass-btn">
            <?=GetMessage("REQUEST_PSSWD")?>
            <picture>
                <img src="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" class="btn-arrow-cat" alt="">
            </picture>
        </a>

        <input type="submit" class="btn btn-primary frgtpsswd_submit" name="send_account_info" value="<?=GetMessage("AUTH_SEND")?>" style="display: none;"/>

	</form>

        <div class="top-info__heart-dots heart-dots auth-dot-top"></div>
        <div class="heart-dots auth-dot-bottom"></div>

    </div>
</div>

<script type="text/javascript">
document.bform.onsubmit = function(){document.bform.USER_EMAIL.value = document.bform.USER_LOGIN.value;};
document.bform.USER_LOGIN.focus();

$(document).on('click', '.auth-form__rec-pass-btn', function (e) {
    e.preventDefault();
    $('.frgtpsswd_submit').click();
});

</script>

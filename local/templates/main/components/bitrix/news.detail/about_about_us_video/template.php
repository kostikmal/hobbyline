<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

?><h2 class="title video__label"><?=$arResult['PREVIEW_TEXT']?></h2>
<div class="video__wrap">
    <video src="<?=$arResult['PROPERTIES']['LINKS']['VALUE'][0]?>" class="video__video" controls playsinline uk-video="automute: true"></video>
    <div class="video__bg"></div>
</div>
<div class="video__heart-dots heart-dots"></div>

<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

?><div class="opt__content">
    <h2 class="opt__title title"><?=$arResult['PREVIEW_TEXT']?></h2>
    <p><?=$arResult['DETAIL_TEXT']?></p>
    <a href="<?=$arResult['PROPERTIES']['LINKS']['VALUE'][0]?>" class="link-half opt__link">ПОЛУЧИТЬ ДОСТУП К ОПТОВЫМ ЦЕНАМ
        <picture>
            <source srcset="<?= SITE_TEMPLATE_PATH ?>/img/main/arrow-rg.svg" type="image/webp">
            <img src="<?= SITE_TEMPLATE_PATH ?>/img/main/arrow-rg.svg" class="btn-arrow-cat" alt="">
        </picture>
    </a>
</div>
<div class="opt__slider slider">
    <div class="opt__slider-wrap">
        <div class="uk-position-relative uk-visible-toggle uk-light slider__container opt__slider-container" tabindex="-1" uk-slider>
            <ul class="uk-slider-items uk-grid"><?
                foreach($arResult['PROPERTIES']['PICTURE']['SRC'] as $sSRC) {
                    ?><li class="uk-width-4-4 slider__item opt__slider-item">
                        <div class="uk-panel">
                            <picture>
                                <source srcset="<?= $sSRC ?>" type="image/webp">
                                <img src="<?= $sSRC ?>" alt="">
                            </picture>
                        </div>
                    </li><?
                }
            ?></ul>
            <div class="slider__wrap-arrows">
                <a class="slider__prev" href="#" uk-slider-item="previous"></a>
                <a class="slider__next" href="#" uk-slider-item="next"></a>
            </div>
        </div>
        <div class="slider__bg opt__slider-bg"></div>
    </div>
</div>
<h2 class="opt__title-mob title"><?=$arResult['PREVIEW_TEXT']?></h2>
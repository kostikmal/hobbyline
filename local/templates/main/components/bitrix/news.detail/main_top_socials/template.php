<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

?><div class="top-info__content">

    <h1 class="main-title"><?=$arResult["PREVIEW_TEXT"]?> -</h1>
    <p class="main-sub-title"><?=$arResult["DETAIL_TEXT"];?></p>
    <a href="/catalog/" class="top-info__link btn-anim">В каталог</a>

    <div class="top-info__socials socials"><?
        foreach($arResult['PROPERTIES']['PICTURE']['VALUE'] as $key=>$nID) {
            $sSRC = CFile::GetPath($nID);
            ?><a href="<?=$arResult['PROPERTIES']['PICTURE']['DESCRIPTION'][$key]?>" class="socials__item" target="_blank">
                <picture>
                    <source srcset="<?= $sSRC ?>" type="image/webp">
                    <img src="<?= $sSRC ?>" alt="">
                </picture>
            </a><?
        }
    ?></div>
</div>
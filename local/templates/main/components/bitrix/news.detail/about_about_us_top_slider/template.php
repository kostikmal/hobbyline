<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

?><div class="top-info__slider slider production__slider">
    <div class="top-info__slider-wrap">

        <div class="uk-position-relative uk-visible-toggle uk-light slider__container" tabindex="-1" uk-slider>
            <ul class="uk-slider-items uk-grid"><?
                foreach($arResult['PROPERTIES']['PICTURE']['SRC'] as $sSRC) {
                    ?><li class="uk-width-4-4">
                        <div class="uk-panel slider__item">
                            <picture>
                                <source srcset="<?= $sSRC ?>" type="image/webp">
                                <img src="<?= $sSRC ?>" alt="">
                            </picture>
                        </div>
                    </li><?
                }
            ?></ul>

            <div class="slider__wrap-arrows">
                <a class="slider__prev" href="#" uk-slider-item="previous"></a>
                <a class="slider__next" href="#" uk-slider-item="next"></a>
            </div>
        </div>

        <div class="slider__bg top-info__bg production__bg us__bg"></div>
    </div>
</div>
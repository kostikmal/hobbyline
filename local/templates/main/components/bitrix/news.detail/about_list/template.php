<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

$arCategories = [
    '',
    'category__green',
    'category__yellow',
];

?><section class="category main-content"><?

    foreach($arResult['PROPERTIES']['PICTURE']['SRC'] as $key=>$sSRC) {
        ?><a href="<?=$arResult['PROPERTIES']['LINKS']['VALUE'][$key]?>" class="category__item">
            <div class="category__img">
                <picture>
                    <source srcset="<?= $sSRC ?>" type="image/webp">
                    <img src="<?= $sSRC ?>" alt="">
                </picture>
            </div>
            <div class="category__link <?=$arCategories[$key]?>">
                <?=$arResult['PROPERTIES']['PICTURE']['DESCRIPTION'][$key]?>
                <picture>
                    <source srcset="<?= SITE_TEMPLATE_PATH ?>/img/ico/arrow-right.svg" type="image/webp">
                    <img src="<?= SITE_TEMPLATE_PATH ?>/img/ico/arrow-right.svg" alt="">
                </picture>
            </div>
        </a><?
    }

?></section>
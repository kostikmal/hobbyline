<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

?><h2 class="about__title title"><?=$arResult["PREVIEW_TEXT"]?></h2>
<div class="about__list" uk-grid uk-scrollspy="cls: uk-animation-slide-bottom; target: .about__item; delay: 300; repeat: false" offset-top="-200"><?
    foreach($arResult['PROPERTIES']['PICTURE']['VALUE'] as $key=>$nID) {
        $sSRC = CFile::GetPath($nID);
        ?><div class="about__item">
            <div class="about__img">
                <picture>
                    <source srcset="<?= $sSRC ?>" type="image/webp">
                    <img src="<?= $sSRC ?>" alt="">
                </picture>
            </div>
            <p><?=$arResult['PROPERTIES']['PICTURE']['DESCRIPTION'][$key]?></p>
        </div><?
    }
?></div>
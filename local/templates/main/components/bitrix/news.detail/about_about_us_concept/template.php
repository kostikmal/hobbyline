<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

?><div class="top-info minify-top-info production__bottom-info" uk-scrollspy="cls: uk-animation-fade; target: div; delay: 100; repeat: false">
    <div class="top-info__wrap main-content">

        <div class="top-info__content production__content">
            <h2 class="top-title">
                <?=$arResult['PREVIEW_TEXT']?>
            </h2>
            <p class="top-info__col">
                <?=$arResult['DETAIL_TEXT']?>
            </p>
        </div>

        <div class="top-info__slider slider production__slider production__bottom-wrap">
            <div class="top-info__slider-wrap">
                <div class="uk-position-relative uk-visible-toggle uk-light slider__container" tabindex="-1" uk-slider>

                    <ul class="uk-slider-items uk-grid"><?
                        foreach($arResult['PROPERTIES']['PICTURE']['SRC'] as $sSRC) {
                            ?><li class="uk-width-4-4 slider__item">
                                <div class="uk-panel">
                                    <picture>
                                        <source srcset="<?= $sSRC ?>" type="image/webp">
                                        <img src="<?= $sSRC ?>" alt="">
                                    </picture>
                                </div>
                            </li><?
                        }
                    ?></ul>
                    <div class="slider__wrap-arrows">
                        <a class="slider__prev" href="#" uk-slider-item="previous"></a>
                        <a class="slider__next" href="#" uk-slider-item="next"></a>
                    </div>
                </div>
                <div class="slider__bg top-info__bg production__bg us__bg-second"></div>
            </div>
        </div>

        <h2 class="top-title-mob concept-work__title">
            <?=$arResult['PREVIEW_TEXT']?>
        </h2>

    </div>

    <div class="top-info__heart-dots heart-dots production__bg-bottom"></div>
</div>
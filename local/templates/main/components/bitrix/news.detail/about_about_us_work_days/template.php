<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

?><section class="gallery main-content">
    <h2 class="title gallery__title"><?=$arResult['PREVIEW_TEXT']?></h2>

    <div class="slider-vertical">

        <div class="slider-vertical-wrap"><?
            foreach($arResult['PROPERTIES']['PICTURE']['SRC'] as $sSRC) {
                ?><div class="slider-vertical__slide">
                    <div class="slider-vertical__item">
                        <picture>
                            <source srcset="<?= $sSRC ?>" type="image/webp">
                            <img src="<?= $sSRC ?>" alt="">
                        </picture>
                    </div>
                </div><?
            }
            ?></div>

        <div class="nav-slick">
            <div class="slider-vertical__prev"></div>

            <div class="slider-vertical-nav"><?
                foreach($arResult['PROPERTIES']['PICTURE']['SRC'] as $sSRC) {
                    ?><div>
                        <picture>
                            <source srcset="<?= $sSRC ?>" type="image/webp">
                            <img src="<?= $sSRC ?>" alt="">
                        </picture>
                    </div><?
                }
            ?></div>

            <div class="slider-vertical__next"></div>
        </div>

    </div>

    <div class="uk-position-relative uk-visible-toggle uk-light gallery__mobile-slider" tabindex="-1" uk-slider autoplay="true">

        <ul class="uk-slider-items uk-child-width-1-1 uk-child-width-1-2@s uk-child-width-1-3@m uk-grid"><?
            foreach($arResult['PROPERTIES']['PICTURE']['SRC'] as $sSRC) {
                ?><li>
                    <div class="uk-panel">
                        <picture>
                            <source srcset="<?= $sSRC ?>" type="image/webp">
                            <img src="<?= $sSRC ?>" alt=""></picture>
                    </div>
                </li><?
            }
        ?></ul>

        <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
        <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>
    </div>

</section>
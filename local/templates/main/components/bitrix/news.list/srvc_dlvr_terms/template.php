<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

foreach ( $arResult['ITEMS'] as $arItem ) {
    ?><h4 class="sf-terms__title"><?=$arItem['NAME']?></h4><?
    if ( strlen($arItem['PREVIEW_TEXT']) > 0 ) {
        ?><p class="sf-terms__paragraph uk-margin-small-top"><?=$arItem['PREVIEW_TEXT']?></p><?
    }
    ?><ul class="sf-terms__list circle-list"><?
        foreach ( $arItem['PROPERTIES']['LIST']['VALUE'] as $val ) {
            ?><li class="sf-terms__list-item circle-list__item"><?
                echo $val;
            ?></li><?
        }
    ?></ul><?
}?>
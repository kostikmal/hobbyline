<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

foreach ( $arResult['ITEMS'] as $arItem ) {
    ?><div class="sf-mini-label"><?=$arItem['NAME']?></div>
    <ul class="sf-terms__list circle-list"><?
        foreach ( $arItem['PROPERTIES']['LIST']['VALUE'] as $val ) {
            ?><li class="sf-terms__list-item circle-list__item"><?
                echo $val;
            ?></li><?
        }
    ?></ul><?
}?>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<section class="main-content staff" uk-scrollspy="cls: uk-animation-fade; target: div; delay: 0; repeat: false" offset-top="0"><?

    foreach ( $arResult['DEPARTMENTS'] as $arDepartment ) {

        if ( count($arDepartment['ITEMS']) <= 0 ) {
            continue;
        }

        ?><h3 class="title staff__title"><?=$arDepartment['NAME']?></h3><?

        ?><div class="staff__row"><?

            foreach ( $arDepartment['ITEMS'] as $arItem ) {
                ?><div class="staff__item">
                    <div class="staff__img-wrap">
                        <div class="staff__img">
                            <picture>
                                <source srcset="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" type="image/webp">
                                <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="">
                            </picture>
                        </div>
                        <div class="staff__img-bg"></div>
                    </div>
                    <div class="staff__content">
                        <h4 class="staff__label">
                            <?=$arItem['PROPERTIES']['LAST_NAME']['VALUE']?>
                            <div><?=$arItem['PROPERTIES']['NAME']['VALUE'].' '.$arItem['PROPERTIES']['SECOND_NAME']['VALUE']?></div>
                        </h4>
                        <div class="staff__rang">
                            <?=$arItem['PROPERTIES']['POSITION']['VALUE']?>
                        </div>
                        <!-- <div class="staff__info">
                          <picture><source srcset="img/main/mini-phone.svg" type="image/webp"><img src="img/main/mini-phone.svg" alt=""></picture>
                          <a href="tel:+74212751700" class="staff__link">+7 4212 75-17-00</a>
                        </div> -->
                        <div class="staff__info">
                            <picture>
                                <source srcset="<?=SITE_TEMPLATE_PATH?>/img/main/message.svg" type="image/webp">
                                <img src="<?=SITE_TEMPLATE_PATH?>/img/main/message.svg" alt="">
                            </picture>
                            <a href="mailto:<?=$arItem['PROPERTIES']['EMAIL']['VALUE']?>" class="staff__link"><?=$arItem['PROPERTIES']['EMAIL']['VALUE']?></a>
                        </div>
                    </div>
                </div><?
            }

        ?></div><?
    }

?></section>
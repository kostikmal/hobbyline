<?php
$arResult['DEPARTMENTS'] = [];

$resSect = CIBlockSection::GetList(
    ["SORT"=>"ASC"],
    ['IBLOCK_ID'=>$arParams['IBLOCK_ID'], 'ACTIVE'=>'Y', /*'ID'=>array_keys($arResult['DEPARTMENTS'])*/],
    false,
    ['NAME', 'ID'],
    false
);

while ( $arSect = $resSect->GetNext() ) {
    $arResult['DEPARTMENTS'][ $arSect['ID'] ]['NAME'] = $arSect['NAME'];
}

foreach ( $arResult['ITEMS'] as $arItem ) {
    $arResult['DEPARTMENTS'][ $arItem['IBLOCK_SECTION_ID'] ]['ITEMS'][] = $arItem;
}

?>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?><div class="footer__socials socials"><?
    foreach ( $arResult["ITEMS"] as $key=>$arItem ) {
        ?><a href="<?=$arItem['~PREVIEW_TEXT']?>" class="socials__item" target="_blank">
            <picture>
                <source srcset="<?=$arItem['DISPLAY_PROPERTIES']['FILES']['FILE_VALUE']['SRC']?>" type="image/webp">
                <img src="<?=$arItem['DISPLAY_PROPERTIES']['FILES']['FILE_VALUE']['SRC']?>" alt="">
            </picture>
        </a><?
    }
?></div>
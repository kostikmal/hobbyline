<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

foreach ( $arResult['ITEMS'] as $arItem ) {
    ?><div class="staff__item">
        <div class="staff__img-wrap">
            <div class="staff__img">
                <picture>
                    <source srcset="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" type="image/webp">
                    <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="">
                </picture>
            </div>
            <div class="staff__img-bg"></div>
        </div>
        <div class="staff__content">
            <h4 class="staff__label">
                <?=$arItem['PROPERTIES']['LAST_NAME']['VALUE']?>
                <div><?=$arItem['PROPERTIES']['NAME']['VALUE'].' '.$arItem['PROPERTIES']['SECOND_NAME']['VALUE']?></div>
            </h4>
            <div class="staff__rang">
                <?=$arItem['PROPERTIES']['POSITION']['VALUE']?>
            </div>
            <!-- <div class="staff__info">
              <picture><source srcset="img/main/mini-phone.svg" type="image/webp"><img src="img/main/mini-phone.svg" alt=""></picture>
              <a href="tel:+74212751700" class="staff__link">+7 4212 75-17-00</a>
            </div> -->
            <div class="staff__info">
                <picture>
                    <source srcset="<?=SITE_TEMPLATE_PATH?>/img/main/message.svg" type="image/webp">
                    <img src="<?=SITE_TEMPLATE_PATH?>/img/main/message.svg" alt="">
                </picture>
                <a href="mailto:<?=$arItem['PROPERTIES']['EMAIL']['VALUE']?>" class="staff__link"><?=$arItem['PROPERTIES']['EMAIL']['VALUE']?></a>
            </div>
        </div>
    </div><?
}

?>
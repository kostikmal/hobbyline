<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="accordion">
    <ul uk-accordion><?

        foreach ( $arResult["ITEMS"] as $key=>$arItem ) {
            ?><li class="accordion__item <?=$key==0?'uk-open':''?>">
                <a class="uk-accordion-title" href="#">
                    <?=$arItem['NAME']?>
                </a>
                <div class="uk-accordion-content">

                    <h3 class="accordion__label">
                        <?=$arItem['NAME']?>
                    </h3>

                    <?/*<h6 class="accordion__mini-label">
                        Хочется найти:
                    </h6>*/?>
                    <p class="accordion__article">
                        <?=$arItem['PREVIEW_TEXT']?>
                    </p>

                    <h6 class="accordion__mini-label">Обязанности:</h6>

                    <ul class="circle-list"><?
                        foreach ( $arItem['PROPERTIES']['DUTIES']['VALUE'] as $sItem ) {
                            ?><li class="circle-list__item"><?=$sItem?></li><?
                        }
                    ?></ul>

                    <h6 class="accordion__mini-label">Требования:</h6>

                    <ul class="circle-list"><?
                        foreach ( $arItem['PROPERTIES']['REQUIREMENTS']['VALUE'] as $sItem ) {
                            ?><li class="circle-list__item"><?=$sItem?></li><?
                        }
                    ?></ul>

                    <h6 class="accordion__mini-label">Личные качества:</h6>

                    <ul class="circle-list"><?
                        foreach ( $arItem['PROPERTIES']['PERSONAL_QUALITIES']['VALUE'] as $sItem ) {
                            ?><li class="circle-list__item"><?=$sItem?></li><?
                        }
                        ?></ul>

                    <h6 class="accordion__mini-label">Условия:</h6>

                    <ul class="circle-list"><?
                        foreach ( $arItem['PROPERTIES']['TERMS']['VALUE'] as $sItem ) {
                            ?><li class="circle-list__item"><?=$sItem?></li><?
                        }
                        ?></ul>

                    <h6 class="accordion__mini-label">Адрес:</h6>
                    <p class="vacancies__top-article"><?=$arItem['PROPERTIES']['ADRES']['VALUE']?></p>

                    <h6 class="accordion__mini-label">Тип занятости:</h6>
                    <p class="vacancies__top-article"><?=implode(',', $arItem['PROPERTIES']['EMPLOYMENT_TYPE']['VALUE'])?></p>
                </div>
            </li><?
        }

    ?></ul>
</div>
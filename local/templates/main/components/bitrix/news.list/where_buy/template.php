<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<div class="select main-content">
    <form class="uk-form-horizontal">
        <fieldset class="uk-fieldset">
            <div class="uk-margin uk-flex uk-flex-middle select-mob-next-line">
                <label class="uk-form-label select__label uk-margin-remove" for="form-horizontal-select">
                    <?=GetMessage('SELECT_CITY')?>:
                </label>
                <div class="uk-form-controls select__item-wrap">
                    <select class="uk-select select__item" id="form-horizontal-select"><?
                        foreach ( $arResult['CITIES'] as $key=>$sCity ) {
                            ?><option value="m<?=$key?>"><?=$sCity?></option><?
                        }
                    ?></select>
                </div>
            </div>
        </fieldset>
    </form>
</div>

<?
$i=0;
foreach ( $arResult['SHOPS'] as $nCityID=>$arShops ) {
    ?><div class="select-container selected-city <?echo $i==0?'selected-active':''?>" id="m<?=$nCityID?>"><?
        $i++;
        foreach ( $arShops as $arItem ) {
            ?><div class="top-info select-top-info minify-top-info production__bottom-info">
                <div class="top-info__wrap main-content">
                    <div class="top-info__content map-info">

                        <div class="map-contacts">
                            <div class="map-contacts__item">
                                <div class="map-contacts__ico-wrap">
                                    <picture>
                                        <source srcset="<?=SITE_TEMPLATE_PATH?>/img/ico/loc.svg" type="image/webp">
                                        <img class="map-contacts__img" src="<?=SITE_TEMPLATE_PATH?>/img/ico/loc.svg" alt="">
                                    </picture>
                                </div>
                                <span class="map-contacts__text">
                                    <?=$arItem['PROPERTIES']['ADRES']['VALUE']?>
                                </span>
                            </div>
                            <div class="map-contacts__item">
                                <div class="map-contacts__ico-wrap">
                                    <picture>
                                        <source srcset="<?=SITE_TEMPLATE_PATH?>/img/ico/phone-map.svg" type="image/webp">
                                        <img class="map-contacts__img" src="<?=SITE_TEMPLATE_PATH?>/img/ico/phone-map.svg" alt="">
                                    </picture>
                                </div><?

                                foreach ( $arItem['PROPERTIES']['PHONE']['VALUE'] as $val ) {
                                    ?><a href="tel:<?=$val?>" class="map-contacts__text"><?=$val?></a><br><?
                                }

                            ?></div>
                            <?if ( strlen($arItem['PROPERTIES']['SITE']['VALUE']) > 0 ) {
                                ?><div class="map-contacts__item">
                                    <div class="map-contacts__ico-wrap">
                                        <picture>
                                            <source srcset="<?=SITE_TEMPLATE_PATH?>/img/ico/mail-map.svg" type="image/webp">
                                            <img class="map-contacts__img" src="<?=SITE_TEMPLATE_PATH?>/img/ico/mail-map.svg" alt="">
                                        </picture>
                                    </div>
                                    <a href="<?=$arItem['PROPERTIES']['SITE']['VALUE']?>" target="_blank" class="map-contacts__text">
                                        <?=$arItem['PROPERTIES']['SITE']['VALUE']?>
                                    </a>
                                </div><?
                            }?>
                            <div class="map-contacts__item">
                                <div class="map-contacts__ico-wrap">
                                    <picture>
                                        <source srcset="<?=SITE_TEMPLATE_PATH?>/img/ico/mail-map.svg" type="image/webp">
                                        <img class="map-contacts__img" src="<?=SITE_TEMPLATE_PATH?>/img/ico/mail-map.svg" alt="">
                                    </picture>
                                </div><?

                                foreach ( $arItem['PROPERTIES']['EMAIL']['VALUE'] as $val ) {
                                    ?><a href="mailto:<?=$val?>" class="map-contacts__text"><?=$val?></a><br><?
                                }

                            ?></div>
                            <div class="map-contacts__item">
                                <div class="map-contacts__ico-wrap">
                                    <picture>
                                        <source srcset="<?=SITE_TEMPLATE_PATH?>/img/ico/clock-map.svg" type="image/webp">
                                        <img class="map-contacts__img" src="<?=SITE_TEMPLATE_PATH?>/img/ico/clock-map.svg" alt="">
                                    </picture>
                                </div>
                                <span class="map-contacts__text"><?
                                    foreach ( $arItem['PROPERTIES']['SHEDULE']['VALUE'] as $val ) {
                                        echo $val.'<br>';
                                    }
                                ?></span>
                            </div>
                        </div>

                        <div class="map__tower">
                            <? if ( strlen($arItem["PREVIEW_PICTURE"]["SRC"]) > 0 ) {
                                ?><div class="map__tower-item">
                                    <picture>
                                        <source srcset="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" type="image/webp">
                                        <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="">
                                    </picture>
                                </div><?
                            } ?>
                            <? if ( strlen($arItem["DETAIL_PICTURE"]["SRC"]) > 0 ) {
                                ?><div class="map__tower-item">
                                    <picture>
                                        <source srcset="<?=$arItem["DETAIL_PICTURE"]["SRC"]?>" type="image/webp">
                                        <img src="<?=$arItem["DETAIL_PICTURE"]["SRC"]?>" alt="">
                                    </picture>
                                </div><?
                            } ?>
                        </div>

                    </div>

                    <div class="top-info__slider slider map">
                        <div class="map-rel">
                            <div class="map__wrap">
                                <?/*<iframe
                                        src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d1991.3494660929093!2d30.33256955890285!3d60.059009581231436!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1zItCa0L7RiNC60LjQvSDQtNC-0LwiINCzLiDQodCw0L3QutGCLdCf0LXRgtC10YDQsdGD0YDQsywg0K3QvdCz0LXQu9GM0YHQsCDQv9GALdC60YIuLCDQtC4gMTU0LCDQotCg0JogItCT0YDQsNC90LQt0JrQsNC90YzQvtC9IiwgNCDRjdGC0LDQtg!5e0!3m2!1sru!2sru!4v1600865864233!5m2!1sru!2sru"
                                        width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen=""
                                        aria-hidden="false" tabindex="0">

                                </iframe>*/?>
                                <?//echo $arItem['DISPLAY_PROPERTIES']['COORDINATES']['DISPLAY_VALUE'];?>
                                <?echo $arItem['~PREVIEW_TEXT'];?>
                            </div>
                            <div class="map__wrap-bg"></div>
                        </div>
                    </div>

                    <div class="top-info__heart-dots heart-dots production__bg-bottom"></div>

                </div>
            </div><?
        }
    ?></div><?php
}

?>
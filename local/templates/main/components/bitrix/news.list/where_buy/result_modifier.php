<?
$arResult['SHOPS'] = [];
$arResult['CITIES'] = ['1' => 'Все города'];

foreach ( $arResult['ITEMS'] as $arItem ) {
    $arResult['SHOPS'][ 1 ][] = $arItem;
    $arResult['SHOPS'][ $arItem['IBLOCK_SECTION_ID'] ][] = $arItem;
}

$resSect = CIBlockSection::GetList(
    ["SORT"=>"ASC"],
    ['IBLOCK_ID'=>$arParams['IBLOCK_ID'], 'ACTIVE'=>'Y', /*'ID'=>array_keys($arResult['DEPARTMENTS'])*/],
    false,
    ['NAME', 'ID'],
    false
);

while ( $arSect = $resSect->GetNext() ) {
    $arResult['CITIES'][ $arSect['ID'] ] = $arSect['NAME'];
}
?>
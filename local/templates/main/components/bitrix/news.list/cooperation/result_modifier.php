<?php
foreach ( $arResult['ITEMS'] as &$arItem ) {
    $arSize = ['width'=> 672, 'height'=> 315];
    if ( $arItem['PROPERTIES']['TYPE']['VALUE_XML_ID'] == 'wide' ) {
        $arSize = ['width'=> 1372, 'height'=> 315];
    }
    $arItem['PREVIEW_PICTURE']['SRC'] = CFile::ResizeImageGet( $arItem['PREVIEW_PICTURE']['ID'], $arSize, BX_RESIZE_IMAGE_EXACT )['src'];
}
?>
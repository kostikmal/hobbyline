<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?><section class="category servicep main-content"><?

    foreach ( $arResult['ITEMS'] as $arItem ) {

        ?><a
            href="<?=$arItem['PROPERTIES']['LINK']['VALUE']?>/"
            class="category__item  <?echo $arItem['PROPERTIES']['TYPE']['VALUE_XML_ID']=='wide'?'servicep__item-full-width':'servicep__item';?>"
        >
            <div class="category__img">
                <picture>
                    <source srcset="<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>" type="image/webp">
                    <img src="<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>" alt="">
                </picture>
            </div>
            <div class="category__link">
                <?=$arItem['NAME']?>
                <picture>
                    <source srcset="<?= SITE_TEMPLATE_PATH ?>/img/ico/arrow-right.svg" type="image/webp">
                    <img src="<?= SITE_TEMPLATE_PATH ?>/img/ico/arrow-right.svg" alt="">
                </picture>
            </div>
        </a><?
    }

    ?><div class="servicep__heart-top heart-dots"></div>
    <div class="servicep__heart-right heart-dots"></div>
</section>
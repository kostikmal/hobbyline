<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

LocalRedirect('/personal/private/');

/*
use Bitrix\Main\Localization\Loc;

$sCurPageUrl = $APPLICATION->GetCurPage(false);

if (strlen($arParams["MAIN_CHAIN_NAME"]) > 0)
{
	$APPLICATION->AddChainItem(htmlspecialcharsbx($arParams["MAIN_CHAIN_NAME"]), $arResult['SEF_FOLDER']);
}

$theme = Bitrix\Main\Config\Option::get("main", "wizard_eshop_bootstrap_theme_id", "blue", SITE_ID);

$availablePages = array();

if ($arParams['SHOW_PRIVATE_PAGE'] === 'Y')
{
    $availablePages[] = array(
        "path" => $arResult['PATH_TO_PRIVATE'],
        "name" => Loc::getMessage("SPS_PERSONAL_PAGE_NAME"),
        "icon" => '<i class="fa fa-user-secret"></i>'
    );
}

if ($arParams['SHOW_ORDER_PAGE'] === 'Y')
{
	$availablePages[] = array(
		"path" => $arResult['PATH_TO_ORDERS'],
		"name" => Loc::getMessage("SPS_ORDER_PAGE_HISTORY"),
		"icon" => '<i class="fa fa-list-alt"></i>'
	);
}

if ($arParams['SHOW_PRODUCTS_LIST_PAGE'] === 'Y')
{
    $availablePages[] = array(
        "path" => $arResult['PATH_TO_PRODUCTS_LIST'],
        "name" => Loc::getMessage("SPS_PRODUCTS_LIST_PAGE_NAME"),
        "icon" => '<i class="fa fa-info-circle"></i>'
    );
}

$customPagesList = CUtil::JsObjectToPhp($arParams['~CUSTOM_PAGES']);
if ($customPagesList)
{
	foreach ($customPagesList as $page)
	{
		$availablePages[] = array(
			"path" => $page[0],
			"name" => $page[1],
			"icon" => (strlen($page[2])) ? '<i class="fa '.htmlspecialcharsbx($page[2]).'"></i>' : ""
		);
	}
}

if (empty($availablePages))
{
	ShowError(Loc::getMessage("SPS_ERROR_NOT_CHOSEN_ELEMENT"));
}
else {
	?>
        <div class="lk-sidebar">


            <input type="file" id="logo-load">
            <div class="lk-sidebar__logo">
                <picture>
                    <source srcset="<?=SITE_TEMPLATE_PATH?>/img/ico/lk-logo.svg" type="image/webp">
                    <img class="lk-sidebar__logo-img" src="<?=SITE_TEMPLATE_PATH?>/img/ico/lk-logo.svg" alt="">
                </picture>
                <div class="lk-sidebar__redact uk-animation-fade">
                </div>
            </div>


            <ul class="lk-menu"><?
                foreach ($availablePages as $blockElement) {
                    if ( $sCurPageUrl == $blockElement['path'] ) {
                        $sActiveClass = 'lk-menu__active';
                    }
                    else {
                        $sActiveClass = '';
                    }
                    ?><li class="lk-menu__item <?=$sActiveClass?>">
                        <a class="" href="<?=htmlspecialcharsbx($blockElement['path'])?>">
                            <?=htmlspecialcharsbx($blockElement['name'])?>
                        </a>
                    </li><?
                }
            ?></ul>

		</div>

    <?
    $APPLICATION->IncludeComponent(
        "glab:main.profile",
        "",
        Array(
            "SET_TITLE" =>$arParams["SET_TITLE"],
            "AJAX_MODE" => $arParams['AJAX_MODE_PRIVATE'],
            "SEND_INFO" => $arParams["SEND_INFO_PRIVATE"],
            "CHECK_RIGHTS" => $arParams['CHECK_RIGHTS_PRIVATE'],
            "EDITABLE_EXTERNAL_AUTH_ID" => $arParams['EDITABLE_EXTERNAL_AUTH_ID'],
            "USER_PROPERTY" => ['PERSONAL_STREET', 'PERSONAL_CITY', 'PERSONAL_PHONE', 'PERSONAL_ZIP'],
            "USER_PROPERTY_NAME" => "Additional",
        ),
        $component
    );

}*/
?>
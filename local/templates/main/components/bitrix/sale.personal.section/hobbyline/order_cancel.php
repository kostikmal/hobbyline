<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Localization\Loc;

if ($arParams['SHOW_ORDER_PAGE'] !== 'Y')
{
	LocalRedirect($arParams['SEF_FOLDER']);
}
elseif ($arParams['ORDER_DISALLOW_CANCEL'] === 'Y')
{
	LocalRedirect($arResult['PATH_TO_ORDERS']);
}
if (strlen($arParams["MAIN_CHAIN_NAME"]) > 0)
{
	$APPLICATION->AddChainItem(htmlspecialcharsbx($arParams["MAIN_CHAIN_NAME"]), $arResult['SEF_FOLDER']);
}
//$APPLICATION->AddChainItem(Loc::getMessage("SPS_CHAIN_ORDERS"), $arResult['PATH_TO_ORDERS']);
$APPLICATION->AddChainItem(Loc::getMessage("SPS_CHAIN_ORDER_DETAIL", array("#ID#" => $arResult["VARIABLES"]["ID"])));

?><section class="main-content">
    <div class="lk">
        <div class="lk-sidebar">
            <div class="lk-sidebar__logo">
                <picture>
                    <source srcset="<?=SITE_TEMPLATE_PATH?>/img/ico/lk-logo.svg" type="image/webp">
                    <img class="lk-sidebar__logo-img" src="<?=SITE_TEMPLATE_PATH?>/img/ico/lk-logo.svg" alt="">
                </picture>
                <div class="lk-sidebar__redact uk-animation-fade">
                </div>
            </div>

            <ul class="lk-menu">
                <li class="lk-menu__item ">
                    <a class="" href="/personal/">Личные данные</a>
                </li>
                <li class="lk-menu__item lk-menu__active">
                    <a class="" href="/personal/orders/">История заказов</a>
                </li>
                <li class="lk-menu__item ">
                    <a class="" href="/personal/products_list/">Список покупок</a>
                </li>
            </ul>
        </div>

        <?$APPLICATION->IncludeComponent(
            "bitrix:sale.personal.order.cancel",
            "hobbyline",
            array(
                "PATH_TO_LIST" => $arResult["PATH_TO_ORDERS"],
                "PATH_TO_DETAIL" => $arResult["PATH_TO_ORDER_DETAIL"],
                "SET_TITLE" =>$arParams["SET_TITLE"],
                "ID" => $arResult["VARIABLES"]["ID"],
            ),
            $component
        );?>

    </div>
</section>
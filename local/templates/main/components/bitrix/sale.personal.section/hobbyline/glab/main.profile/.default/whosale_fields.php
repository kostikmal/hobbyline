<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<div class="lk-profile__col">
    <div class="lk-profile__item">
        <div class="lk-profile__label">Юридическое название</div>
        <div class="lk-profile__val">
            <input
                class="lk-profile__input"
                placeholder="Моя Компания"
                type="text"
                name="WORK_COMPANY"
                value=""
            >
        </div>
    </div>

    <div class="lk-profile__item">
        <div class="lk-profile__label">ИНН</div>
        <div class="lk-profile__val">
            <input
                class="lk-profile__input"
                placeholder="1234567890"
                type="number"
                name="UF_INN"
                value=""
            >
        </div>
    </div>
</div>

<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use Bitrix\Main\Localization\Loc;

?>

<form method="post" name="form1" action="<?= POST_FORM_ACTION_URI ?>" enctype="multipart/form-data" role="form" style="display: flex; width: 100%;">
    <?= $arResult["BX_SESSION_CHECK"] ?>
    <div class="lk-sidebar">

        <input type="file" id="logo-load" name="PERSONAL_PHOTO" value="">
        <div class="lk-sidebar__logo">
            <picture>
                <source srcset="<?= $arResult["arUser"]['PERSONAL_PHOTO_SRC'] ?>" type="image/webp">
                <img class="lk-sidebar__logo-img" src="<?= $arResult["arUser"]['PERSONAL_PHOTO_SRC'] ?>" alt="">
            </picture>
            <div class="lk-sidebar__redact uk-animation-fade">
            </div>
        </div>

        <ul class="lk-menu">
            <li class="lk-menu__item lk-menu__active"><a href="/personal/private/">Личный кабинет</a></li>
            <li class="lk-menu__item"><a href="/personal/orders/">История заказов</a></li>
            <li class="lk-menu__item"><a href="/personal/products_list/">Список покупок</a></li>
            <li class="lk-menu__item"><a href="/?logout=yes">ВЫХОД</a></li>
            <?/*<li class="lk-menu__item"><a href="#modal-change-pass" uk-toggle="">Смена пароля</a></li>*/?>
        </ul>

    </div>

    <div class="lk-content" id="user_div_reg">
        <div class="lk-profile"><?

            ShowError($arResult["strProfileError"]);

            if ($arResult['DATA_SAVED'] == 'Y') {
                ShowNote(Loc::getMessage('PROFILE_DATA_SAVED'));
            }

            ?><input type="hidden" name="lang" value="<?= LANG ?>"/>
            <input type="hidden" name="ID" value="<?= $arResult["ID"] ?>"/>
            <input type="hidden" name="LOGIN" value="<?= $arResult["arUser"]["LOGIN"] ?>"/>

            <div class="lk-profile__title">Персональные данные</div>

            <div class="lk-profile__data">

                <div class="lk-profile__col">
                    <div class="lk-profile__item">
                        <div class="lk-profile__label">ФИО</div>
                        <div class="lk-profile__val">
                            <input
                                    class="lk-profile__input"
                                    placeholder="Алексеев Алексей Иванович"
                                    type="text"
                                    name="NAME"
                                    value="<?=trim($arResult["arUser"]["LAST_NAME"].' '.$arResult["arUser"]["NAME"].' '.$arResult["arUser"]["SECOND_NAME"])?>"
                            >
                        </div>
                    </div>

                    <div class="lk-profile__item">
                        <div class="lk-profile__label">Адрес доставки</div>
                        <div class="lk-profile__val">
                            <input
                                    class="lk-profile__input"
                                    placeholder="Москва, ул. Проспект Мира, д.54, кв.68"
                                    type="text"
                                    name="PERSONAL_STREET"
                                    value="<?= $arResult["arUser"]["PERSONAL_STREET"] ?>"
                            >
                        </div>
                    </div>
                </div>

                <div class="lk-profile__col">
                    <div class="lk-profile__item">
                        <div class="lk-profile__label">E-mail</div>
                        <div class="lk-profile__val">
                            <input
                                    class="lk-profile__input"
                                    placeholder="alexeev@mail.ru"
                                    type="text"
                                    name="EMAIL"
                                    maxlength="50" id="main-profile-email"
                                    value="<?= $arResult["arUser"]["EMAIL"] ?>"
                            >
                        </div>
                    </div>

                    <div class="lk-profile__item">
                        <div class="lk-profile__label">Индекс</div>
                        <div class="lk-profile__val">
                            <input
                                    class="lk-profile__input"
                                    placeholder="112414"
                                    type="number"
                                    name="PERSONAL_ZIP"
                                    maxlength="6"
                                    value="<?= $arResult["arUser"]["PERSONAL_ZIP"] ?>"
                            >
                        </div>
                    </div>
                </div>

                <div class="lk-profile__col">
                    <div class="lk-profile__item">
                        <div class="lk-profile__label">Город</div>
                        <div class="lk-profile__val">
                            <input
                                    class="lk-profile__input"
                                    placeholder="Москва"
                                    type="text"
                                    name="PERSONAL_CITY"
                                    maxlength="50"
                                    value="<?= $arResult["arUser"]["PERSONAL_CITY"] ?>"
                            >
                        </div>
                    </div>

                    <div class="lk-profile__item">
                        <div class="lk-profile__label">Номер телефона</div>
                        <div class="lk-profile__val">
                            <input
                                    class="lk-profile__input"
                                    placeholder="+7(999)999-99-99"
                                    type="text"
                                    name="PERSONAL_PHONE"
                                    maxlength="50"
                                    value="<?= $arResult["arUser"]["PERSONAL_PHONE"] ?>"
                            >
                        </div>
                    </div>
                </div>

                <?
                /*
                    if ($arResult['CAN_EDIT_PASSWORD']) {
                        ?>
                        <div class="form-group">
                            <p class="main-profile-form-password-annotation col-sm-9 col-sm-offset-3 small">
                                <?echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"];?>
                            </p>
                        </div>
                        <div class="form-group">
                            <label class="main-profile-form-label col-sm-12 col-md-3 text-md-right" for="main-profile-password"><?=Loc::getMessage('NEW_PASSWORD_REQ')?></label>
                            <div class="col-sm-12">
                                <input class=" form-control bx-auth-input main-profile-password" type="password" name="NEW_PASSWORD" maxlength="50" id="main-profile-password" value="" autocomplete="off"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="main-profile-form-label main-profile-password col-sm-12 col-md-3 text-md-right" for="main-profile-password-confirm">
                                <?=Loc::getMessage('NEW_PASSWORD_CONFIRM')?>
                            </label>
                            <div class="col-sm-12">
                                <input class="form-control" type="password" name="NEW_PASSWORD_CONFIRM" maxlength="50" value="" id="main-profile-password-confirm" autocomplete="off" />
                            </div>
                        </div>
                        <?
                    }*/
                ?>
            </div>

            <div class="lk-profile__title">Данные организации</div><?

            if ( in_array( WHOSALE_USER_GROUP, $arResult['arUser']['GROUPS']) ) {
                ?><div class="lk-profile__data">

                    <div class="lk-profile__col">
                        <div class="lk-profile__item">
                            <div class="lk-profile__label">Юридическое название</div>
                            <div class="lk-profile__val">
                                <input
                                        class="lk-profile__input"
                                        placeholder="Моя Компания"
                                        type="text"
                                        name="WORK_COMPANY"
                                        value="<?=trim($arResult["arUser"]["WORK_COMPANY"])?>"
                                >
                            </div>
                        </div>

                        <div class="lk-profile__item">
                            <div class="lk-profile__label">ИНН</div>
                            <div class="lk-profile__val">
                                <input
                                        class="lk-profile__input"
                                        placeholder="1234567890"
                                        type="number"
                                        name="UF_INN"
                                        value="<?= $arResult["arUser"]["UF_INN"] ?>"
                                >
                            </div>
                        </div>
                    </div>

                </div><?
            }
            else {
                ?><a href="#" class="lk-profile__red-link">Стать оптовым клиентом</a>

                <div class="lk-profile__data whosale_fields"></div> <?
            }

            ?><label class="link-half hl-filter__btn">
                <input
                        type="submit"
                        name="save"
                        class="btn btn-themes btn-default btn-md main-profile-submit"
                        value="<?= (($arResult["ID"] > 0) ? Loc::getMessage("MAIN_SAVE") : Loc::getMessage("MAIN_ADD")) ?>"
                >
                <picture>
                    <source srcset="<?=SITE_TEMPLATE_PATH?>/img/ico/filter-arr-right.svg" type="image/webp">
                    <img src="<?=SITE_TEMPLATE_PATH?>/img/ico/filter-arr-right.svg" class="btn-arrow-cat" alt="">
                </picture>
            </label>

            <div class="lk-profile__title  uk-margin-large-top">Управление подписками</div>

            <div class="lk-subscribe">

                <?/*
                <div class="lk-subscribe__item">
                    <div class="button r" id="button-1">
                        <input type="checkbox" class="checkbox" checked="">
                        <div class="knobs"></div>
                        <div class="layer"></div>
                    </div>
                    <div class="lk-subscribe__text">Подписка на акции и спецпредложения</div>
                </div>*/?>

            </div>

            <?
            $APPLICATION->IncludeComponent(
                    "bitrix:subscribe.edit",
                    "hobbyline_default",
                    Array(
                    "AJAX_MODE" => "N",
                    "SHOW_HIDDEN" => "Y",
                    "ALLOW_ANONYMOUS" => "Y",
                    "SHOW_AUTH_LINKS" => "Y",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "3600",
                    "SET_TITLE" => "Y",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N"
                )
            );
            ?>

            <?
            $APPLICATION->IncludeComponent(
                'bitrix:catalog.product.subscribe.list',
                'hobbyline_bootstrap_v4',
                array(
                    'SET_TITLE' => $arParams['SET_TITLE'],
                    'DETAIL_URL' => $arParams['SUBSCRIBE_DETAIL_URL'],
                ),
                $component
            );
            ?>

        </div>
    </div>
</form>

<script>
    BX.Sale.PrivateProfileComponent.init();

    $(document).on('click', '.lk-profile__red-link', function (e) {
        e.preventDefault();
        var ulr_file = '<?=$templateFolder?>' + '/whosale_fields.php';
        var block = '.lk-profile__data.whosale_fields';
        $(this).css('display', 'none');
        $(block).css('display', 'block').load( ulr_file );
    });

</script>
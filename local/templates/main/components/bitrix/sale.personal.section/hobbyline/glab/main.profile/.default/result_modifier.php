<?php

if ( intval($arResult["arUser"]['PERSONAL_PHOTO']) > 0 ) {
    $arResult["arUser"]['PERSONAL_PHOTO_SRC'] = CFile::ResizeImageGet( $arResult["arUser"]['PERSONAL_PHOTO'], ['width'=>86, 'height'=>103], BX_RESIZE_IMAGE_EXACT)['src'];
}
else {
    $arResult["arUser"]['PERSONAL_PHOTO_SRC'] = SITE_TEMPLATE_PATH.'/img/ico/lk-logo.svg';
}

$arResult["arUser"]['GROUPS'] = CUser::GetUserGroup( $arResult["arUser"]['ID'] );

?>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

/*$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"",
	Array(
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"NEWS_COUNT" => $arParams["NEWS_COUNT"],
		"SORT_BY1" => $arParams["SORT_BY1"],
		"SORT_ORDER1" => $arParams["SORT_ORDER1"],
		"SORT_BY2" => $arParams["SORT_BY2"],
		"SORT_ORDER2" => $arParams["SORT_ORDER2"],
		"FIELD_CODE" => $arParams["LIST_FIELD_CODE"],
		"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
		"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
		"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
		"IBLOCK_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"],
		"DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
		"SET_TITLE" => $arParams["SET_TITLE"],
		"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
		"MESSAGE_404" => $arParams["MESSAGE_404"],
		"SET_STATUS_404" => $arParams["SET_STATUS_404"],
		"SHOW_404" => $arParams["SHOW_404"],
		"FILE_404" => $arParams["FILE_404"],
		"INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_FILTER" => $arParams["CACHE_FILTER"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
		"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
		"PAGER_TITLE" => $arParams["PAGER_TITLE"],
		"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
		"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
		"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
		"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
		"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
		"PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
		"PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
		"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
		"DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
		"DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
		"PREVIEW_TRUNCATE_LEN" => $arParams["PREVIEW_TRUNCATE_LEN"],
		"ACTIVE_DATE_FORMAT" => $arParams["LIST_ACTIVE_DATE_FORMAT"],
		"USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
		"GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
		"FILTER_NAME" => $arParams["FILTER_NAME"],
		"HIDE_LINK_WHEN_NO_DETAIL" => $arParams["HIDE_LINK_WHEN_NO_DETAIL"],
		"CHECK_DATES" => $arParams["CHECK_DATES"],
	),
	$component
);*/?>

<section class="lb-page main-content">

    <h1 class="title lb-page__title">LOOKBOOK</h1>

    <div class="lb-page__grid" uk-scrollspy="cls: uk-animation-fade; target: div; delay: 100; repeat: false">

        <div class="lb-page__main">

            <div class="lb-page__col lb-page__col-first"><?
                $arItem = getLookBookElemByID( 19182, [442, 278]);
                ?>
                <style>
                    .lb-page__bg1 {
                        background: url(<?=$arItem['PICTURE']?>) center center no-repeat;
                    }
                </style>
                <a href="/lookbook/<?=$arItem['URL']?>/" class="lb-page__item-mini lb-page__bg1">
                    <h3 class="lb-page__label"><?=$arItem['NAME']?></h3>
                </a><?

                $arItem = getLookBookElemByID( 19183, [442, 586]);
                ?>
                <style>
                    .lb-page__bg2 {
                        background: url(<?=$arItem['PICTURE']?>) center center no-repeat;
                    }
                </style>
                <a href="/lookbook/<?=$arItem['URL']?>/" class="lb-page__item-midle lb-page__bg2 lb-page__bottom-text">
                    <h3 class="lb-page__label"><?=$arItem['NAME']?></h3>
                </a>

            </div>

            <div class="lb-page__col lb-page__col-second"><?
                $arItem = getLookBookElemByID( 19184, [442, 586]);
                ?>
                <style>
                    .lb-page__bg3 {
                        background: url(<?=$arItem['PICTURE']?>) center center no-repeat;
                    }
                </style>
                <a href="/lookbook/<?=$arItem['URL']?>/" class="lb-page__item-midle lb-page__bg3 lb-page__top-text">
                    <h3 class="lb-page__label"><?=$arItem['NAME']?></h3>
                </a><?

                $arItem = getLookBookElemByID( 19185, [442, 278]);
                ?>
                <style>
                    .lb-page__bg4 {
                        background: url(<?=$arItem['PICTURE']?>) center center no-repeat;
                    }
                </style>
                <a href="/lookbook/<?=$arItem['URL']?>/" class="lb-page__item-mini lb-page__bg4">
                    <h3 class="lb-page__label"><?=$arItem['NAME']?></h3>
                </a>

            </div>

            <div class="lb-page__full-width"><?

                $arItem = getLookBookElemByID( 19186, [906, 314]);
                ?>
                <style>
                    .lb-page__bg5 {
                        background: url(<?=$arItem['PICTURE']?>) center center no-repeat;
                    }
                </style>
                <a href="/lookbook/<?=$arItem['URL']?>/" class="lb-page__item-mini lb-page__bg5">
                    <h3 class="lb-page__label"><?=$arItem['NAME']?></h3>
                </a>

            </div>

        </div>

        <div class="lb-page_right">
            <div class="lb-page__col lb-page__col-last"><?

                $arItem = getLookBookElemByID( 19187, [442, 278]);
                ?>
                <style>
                    .lb-page__bg6 {
                        background: url(<?=$arItem['PICTURE']?>) center center no-repeat;
                    }
                </style>
                <a href="/lookbook/<?=$arItem['URL']?>/" class="lb-page__item-mini lb-page__bg6">
                    <h3 class="lb-page__label"><?=$arItem['NAME']?></h3>
                </a><?

                $arItem = getLookBookElemByID( 19188, [442, 278]);
                ?>
                <style>
                    .lb-page__bg7 {
                        background: url(<?=$arItem['PICTURE']?>) center center no-repeat;
                    }
                </style>
                <a href="/lookbook/<?=$arItem['URL']?>/" class="lb-page__item-mini lb-page__bg7">
                    <h3 class="lb-page__label"><?=$arItem['NAME']?></h3>
                </a><?

                $arItem = getLookBookElemByID( 19189, [442, 618]);
                ?>
                <style>
                    .lb-page__bg8 {
                        background: url(<?=$arItem['PICTURE']?>) center center no-repeat;
                    }
                </style>
                <a href="/lookbook/<?=$arItem['URL']?>/" class="lb-page__item-midle lb-page__bg8 lb-page__top-text">
                    <h3 class="lb-page__label"><?=$arItem['NAME']?></h3>
                </a>

            </div>

        </div>
    </div>

    <div class="lb-page__heart-top heart-dots"></div>
    <div class="lb-page__heart-bottom heart-dots"></div>
    <div class="lb-page__heart-right heart-dots"></div>
</section>
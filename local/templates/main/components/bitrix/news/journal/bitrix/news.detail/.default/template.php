<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

use Bitrix\Main\Localization\Loc;

Bitrix\Main\Page\Asset::getInstance()->addString('<script src="https://yastatic.net/share2/share.js" async></script>');
?>

<div class="top-info minify-top-info articlep__top-info" uk-scrollspy="cls: uk-animation-fade; target: div; delay: 100; repeat: false">
    <div class="top-info__wrap main-content articlep-top-info__wrap">
        <div class="top-info__content articlep__content-top">
            <div class="articlep__title-wrap">

                <h1 class="title articlep__title">
                    <?=$arResult["NAME"]?>
                </h1>

                <?/*<a href="#" class="articlep__subscribe"></a>*/?>
                <div class="ya-share2 article__share" data-curtain data-shape="normal" data-limit="0" data-more-button-type="short" data-services="vkontakte,facebook,odnoklassniki"></div>
            </div>
            <p class="articlep__paragraph">
                <?echo $arResult["PREVIEW_TEXT"];?>
            </p>
        </div>
        <div class="top-info__slider slider about-page__slider articlep__slider">
            <div class="articlep__top-banner">
                <div class="articlep__img">
                    <picture>
                        <source srcset="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>" type="image/webp">
                        <img src="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>" alt="">
                    </picture>
                </div>
                <div class="bg-left-bottom articlep__bg-top"></div>
            </div>
        </div>
    </div>
    <div class="top-info__heart-dots heart-dots"></div>
</div>

<section class="articlep main-content">
    <div class="articlep__content">
        <?echo $arResult["DETAIL_TEXT"];?>
    </div>
    <div class="lb-page__heart-right heart-dots"></div>
</section>


<?php/*
if(array_key_exists("USE_SHARE", $arParams) && $arParams["USE_SHARE"] == "Y")
	{
		?>
		<div class="news-detail-share">
			<noindex>
			<?
			$APPLICATION->IncludeComponent("bitrix:main.share", "", array(
					"HANDLERS" => $arParams["SHARE_HANDLERS"],
					"PAGE_URL" => $arResult["~DETAIL_PAGE_URL"],
					"PAGE_TITLE" => $arResult["~NAME"],
					"SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
					"SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
					"HIDE" => $arParams["SHARE_HIDE"],
				),
				$component,
				array("HIDE_ICONS" => "Y")
			);
			?>
			</noindex>
		</div>
		<?
	}
*/?>
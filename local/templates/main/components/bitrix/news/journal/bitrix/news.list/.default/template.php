<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<section class="magazinep main-content">

    <h1 class="title">Журнал</h1>

    <div class="magazinep__news">
        <?
        foreach ( $arResult["ITEMS"] as $arItem ) {
            ?><div class="magazinep__item">
                <div class="magazinep__img">
                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="">
                        <picture>
                            <source srcset="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" type="image/webp">
                            <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="">
                        </picture>
                    </a>
                </div>
                <div class="magazinep__content">
                    <div>
                        <h5 class="magazinep__label">
                            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class=""><?=$arItem["NAME"]?></a>
                        </h5>
                        <p class="magazinep__article">
                            <?=$arItem["PREVIEW_TEXT"]?>
                        </p>
                    </div>
                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="link-half magazinep__link">
                        <?=GetMessage('GO_TO_LINK')?>
                        <picture>
                            <source srcset="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" type="image/webp">
                            <img src="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" class="btn-arrow-cat" alt="">
                        </picture>
                    </a>
                </div>
            </div><?
        }
        ?>

    </div>

    <?=$arResult["NAV_STRING"]?>

    <div class="servicep__heart-top heart-dots"></div>
    <div class="servicep__heart-right heart-dots"></div>

</section>
<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogSectionComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

$arResult['ITEMS_BY_TYPE'] = [];

foreach ( $arResult['ITEMS'] as $arItem ) {
    $arResult['ITEMS_BY_TYPE'][ $arItem['PROPERTIES']['TYPE']['VALUE'] ][] = $arItem;
}

$HlBlockId = HLB_PROD_TYPE;
$hlblock = Bitrix\Highloadblock\HighloadBlockTable::getById($HlBlockId)->fetch();
$entity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
$entityDataClass = $entity->getDataClass();

$tempObD = $entityDataClass::GetList( [ 'select' => ['ID', 'UF_NAME', 'UF_XML_ID'], 'filter' => [] ] );
while ($tempResD = $tempObD->fetch()) {
    $arResult['TYPE_OF_ITEMS'][ $tempResD['UF_XML_ID'] ] = $tempResD['UF_NAME'];
}
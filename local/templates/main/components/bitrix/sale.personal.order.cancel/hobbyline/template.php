<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="lk-content">

    <div class="lk-history">
        <div class="lk-sw-content">
            <div class="lk-sw-content__item lk-sw-content__active uk-animation-slide-left-medium">

                <div class="lk-date">

                    <div class="lk-date__top">
                        <div class="lk-date__label">
                            Отменить заказ № <?=$arResult["ID"]?>
                        </div>
                    </div>

                    <div class="lk-date__content">

                        <div class="lk-date__item">
                            <?if(strlen($arResult["ERROR_MESSAGE"])<=0):?>
                                <form method="post" action="<?=POST_FORM_ACTION_URI?>">

                                    <input type="hidden" name="CANCEL" value="Y">
                                    <?=bitrix_sessid_post()?>
                                    <input type="hidden" name="ID" value="<?=$arResult["ID"]?>">

                                    <?=GetMessage("SALE_CANCEL_ORDER1") ?>

                                    <a href="<?=$arResult["URL_TO_DETAIL"]?>"><?=GetMessage("SALE_CANCEL_ORDER2")?> #<?=$arResult["ACCOUNT_NUMBER"]?></a>?
                                    <br />
                                    <b><?= GetMessage("SALE_CANCEL_ORDER3") ?></b>
                                    <br /><br />
                                    <?= GetMessage("SALE_CANCEL_ORDER4") ?>:
                                    <br />

                                    <textarea name="REASON_CANCELED" class="hl-review__textarea"></textarea>
                                    <br /><br />

                                    <label class="link-half hl-filter__btn">
                                        <input type="submit" name="action" value="<?=GetMessage("SALE_CANCEL_ORDER_BTN") ?>">
                                        <picture>
                                            <source srcset="/local/templates/main/img/ico/filter-arr-right.svg" type="image/webp">
                                            <img src="/local/templates/main/img/ico/filter-arr-right.svg" class="btn-arrow-cat" alt="">
                                        </picture>
                                    </label>

                                </form>
                            <?else:?>
                                <?=ShowError($arResult["ERROR_MESSAGE"]);?>
                            <?endif;?>
                        </div>

                        <div class="lk-date__btn-wrap">
                            <a href="<?=$arResult["URL_TO_LIST"]?>" class="lk-date__btn"><?=GetMessage("SALE_RECORDS_LIST")?></a>
                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div>
    
</div>
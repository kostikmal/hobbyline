<?

foreach ( $arResult['ORDERS'] as $key_order=>$order ) {
    foreach ( $order['BASKET_ITEMS'] as $key_item=>$item ) {
        $item['PRODUCT_ID'];

        $offer = CIBlockElement::GetList(
            ["ID"=>"ASC"],
            ['IBLOCK_ID'=>OFFERS_IBLOCK, 'ID'=>$item['PRODUCT_ID']],
            false,
            false,
            ['IBLOCK-ID', 'ID', 'PROPERTY_CML2_LINK']
        );

        while ( $arOffer = $offer->GetNext() ) {

            $product = CIBlockElement::GetList(
                ["ID"=>"ASC"],
                ['IBLOCK_ID'=>PRODUCTS_IBLOCK, 'ID'=>$arOffer['PROPERTY_CML2_LINK_VALUE']],
                false,
                false,
                ['IBLOCK-ID', 'ID', 'DETAIL_PICTURE']
            );

            while ( $arProd = $product->GetNext() ) {
                $arResult['ORDERS'][$key_order]['BASKET_ITEMS'][$key_item]['IMAGE'] = CFile::ResizeImageGet( $arProd['DETAIL_PICTURE'], ['width'=>79, 'height'=>79], BX_RESIZE_IMAGE_EXACT )['src'];
            }

        }
    }
}

?>
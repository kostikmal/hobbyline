<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="auth-form__text" id="main_<?= $arParams["arUserField"]["FIELD_NAME"] ?>"><?
    foreach ($arResult["VALUE"] as $res):
        ?>
        <div class="fields string"><?
        if ($arParams["arUserField"]["SETTINGS"]["ROWS"] < 2):
            ?><input type="text" name="<?= $arParams["arUserField"]["FIELD_NAME"] ?>" value="<?= $res ?>"<?
            if (intVal($arParams["arUserField"]["SETTINGS"]["SIZE"]) > 0):
                ?> size="<?= $arParams["arUserField"]["SETTINGS"]["SIZE"] ?>"<?
            endif;
            if (intVal($arParams["arUserField"]["SETTINGS"]["MAX_LENGTH"]) > 0):
                ?> maxlength="<?= $arParams["arUserField"]["SETTINGS"]["MAX_LENGTH"] ?>"<?
            endif;
            if ($arParams["arUserField"]["EDIT_IN_LIST"] != "Y"):
                ?> disabled="disabled"<?
            endif;
            ?> class="hl-require fields string"
               placeholder="<?=$arParams["arUserField"]['EDIT_FORM_LABEL']?>*"
               required><?
        endif;
        ?></div><?
    endforeach;
?></div>
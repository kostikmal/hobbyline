<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @var array $arParams
 * @var array $arResult
 * @var SaleOrderAjax $component
 */

$component = $this->__component;
$component::scaleImages($arResult['JS_DATA'], $arParams['SERVICES_IMAGES_SCALING']);

if ( $arResult['JS_DATA']['TOTAL']['DELIVERY_PRICE'] == 1 ) {
    $arResult['JS_DATA']['TOTAL']['ORDER_TOTAL_PRICE'] = $arResult['JS_DATA']['TOTAL']['ORDER_PRICE'];
    $arResult['JS_DATA']['TOTAL']['ORDER_TOTAL_PRICE_FORMATED'] = $arResult['JS_DATA']['TOTAL']['ORDER_PRICE_FORMATED'];
}
file_put_contents( $_SERVER['DOCUMENT_ROOT'].'/xxx_JS_DATA.txt', print_r($arResult['JS_DATA'], true) );

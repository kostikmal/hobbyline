<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/**
 * @var array $arParams
 */
?>
<script id="basket-total-template" type="text/html">
	<div class="" data-entity="basket-checkout-aligner">
		<?
		if ( $arParams['HIDE_COUPON'] !== 'Y' )
		{
			?>
			<div class="basket-coupon-section">
				<div class="basket-coupon-block-field">
					<div class="basket-coupon-block-field-description">
						<?=Loc::getMessage('SBB_COUPON_ENTER')?>:
					</div>
					<div class="form">
						<div class="form-group" style="position: relative;">
							<input type="text" class="form-control" id="" placeholder="" data-entity="basket-coupon-input">
							<span class="basket-coupon-block-coupon-btn"></span>
						</div>
					</div>
				</div>
			</div>
			<?
		}
		?>

        <h4 class="basket-info__label"><?=Loc::getMessage('SBB_TOTAL')?>:</h4>

        {{#DISCOUNT_PRICE_FORMATED}}
            <div class="basket-info__line">
                <span class="basket-info__line-name">Сумма покупки:</span>
                <span class="basket-info__line-cur">{{{PRICE_WITHOUT_DISCOUNT_FORMATED}}}</span>
            </div>
        {{/DISCOUNT_PRICE_FORMATED}}

        {{#DISCOUNT_PRICE_FORMATED}}
            <div class="basket-info__line">
                <span class="basket-info__line-name">Скидка:</span>
                <?=Loc::getMessage('SBB_BASKET_ITEM_ECONOMY')?>
                <span class="basket-info__line-cur">{{{DISCOUNT_PRICE_FORMATED}}}</span>
            </div>
        {{/DISCOUNT_PRICE_FORMATED}}

        <div class="basket-divider"></div>

        <div class="basket-info__line" data-entity="basket-total-price">
            <span class="basket-info__line-name">Общая стоимость:</span>
            <span class="basket-info__line-cur">{{{PRICE_FORMATED}}}</span>
        </div>

        <div class="basket-info__auth">
            <button class="link-half btn btn-lg basket-btn-checkout{{#DISABLE_CHECKOUT}} disabled{{/DISABLE_CHECKOUT}}"
                    data-entity="basket-checkout-button">
                <?=Loc::getMessage('SBB_ORDER')?>
                <picture>
                    <source srcset="<?=SITE_TEMPLATE_PATH?>/img/ico/filter-arr-right.svg" type="image/webp">
                    <img src="<?=SITE_TEMPLATE_PATH?>/img/ico/filter-arr-right.svg" class="btn-arrow-cat" alt="">
                </picture>
            </button>
        </div>


		<div class="basket-checkout-section" style="display: none;">
			<div class="basket-checkout-section-inner">
				<div class="basket-checkout-block basket-checkout-block-total">
					<div class="basket-checkout-block-total-inner">

						<div class="basket-checkout-block-total-description">
							{{#WEIGHT_FORMATED}}
								<?=Loc::getMessage('SBB_WEIGHT')?>: {{{WEIGHT_FORMATED}}}
								{{#SHOW_VAT}}<br>{{/SHOW_VAT}}
							{{/WEIGHT_FORMATED}}
							{{#SHOW_VAT}}
								<?=Loc::getMessage('SBB_VAT')?>: {{{VAT_SUM_FORMATED}}}
							{{/SHOW_VAT}}
						</div>
					</div>
				</div>

				<div class="basket-checkout-block basket-checkout-block-total-price">
					<div class="basket-checkout-block-total-price-inner">
						<?/*{{#DISCOUNT_PRICE_FORMATED}}
							<div class="basket-coupon-block-total-price-old">
								{{{PRICE_WITHOUT_DISCOUNT_FORMATED}}}
							</div>
						{{/DISCOUNT_PRICE_FORMATED}}*/?>

						<?/*<div class="basket-coupon-block-total-price-current" data-entity="basket-total-price">
							{{{PRICE_FORMATED}}}
						</div>*/?>

						<?/*{{#DISCOUNT_PRICE_FORMATED}}
							<div class="basket-coupon-block-total-price-difference">
								<?=Loc::getMessage('SBB_BASKET_ITEM_ECONOMY')?>
								<span style="white-space: nowrap;">{{{DISCOUNT_PRICE_FORMATED}}}</span>
							</div>
						{{/DISCOUNT_PRICE_FORMATED}}*/?>
					</div>
				</div>

				<?/*<div class="basket-checkout-block basket-checkout-block-btn">
					<button class="btn btn-lg btn-default basket-btn-checkout{{#DISABLE_CHECKOUT}} disabled{{/DISABLE_CHECKOUT}}"
						data-entity="basket-checkout-button">
						<?=Loc::getMessage('SBB_ORDER')?>
					</button>
				</div>*/?>
			</div>
		</div>

		<?
		if ($arParams['HIDE_COUPON'] !== 'Y')
		{
		?>
			<div class="basket-coupon-alert-section">
				<div class="basket-coupon-alert-inner">
					{{#COUPON_LIST}}
					<div class="basket-coupon-alert text-{{CLASS}}">
						<span class="basket-coupon-text">
							<strong>{{COUPON}}</strong> - <?=Loc::getMessage('SBB_COUPON')?> {{JS_CHECK_CODE}}
							{{#DISCOUNT_NAME}}({{DISCOUNT_NAME}}){{/DISCOUNT_NAME}}
						</span>
						<span class="close-link" data-entity="basket-coupon-delete" data-coupon="{{COUPON}}">
							<?=Loc::getMessage('SBB_DELETE')?>
						</span>
					</div>
					{{/COUPON_LIST}}
				</div>
			</div>
			<?
		}
		?>
	</div>
</script>
<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
use Bitrix\Main;

$defaultParams = array(
	'TEMPLATE_THEME' => 'blue'
);
$arParams = array_merge($defaultParams, $arParams);
unset($defaultParams);

$arParams['TEMPLATE_THEME'] = (string)($arParams['TEMPLATE_THEME']);
if ('' != $arParams['TEMPLATE_THEME'])
{
	$arParams['TEMPLATE_THEME'] = preg_replace('/[^a-zA-Z0-9_\-\(\)\!]/', '', $arParams['TEMPLATE_THEME']);
	if ('site' == $arParams['TEMPLATE_THEME'])
	{
		$templateId = (string)Main\Config\Option::get('main', 'wizard_template_id', 'eshop_bootstrap', SITE_ID);
		$templateId = (preg_match("/^eshop_adapt/", $templateId)) ? 'eshop_adapt' : $templateId;
		$arParams['TEMPLATE_THEME'] = (string)Main\Config\Option::get('main', 'wizard_'.$templateId.'_theme_id', 'blue', SITE_ID);
	}
	if ('' != $arParams['TEMPLATE_THEME'])
	{
		if (!is_file($_SERVER['DOCUMENT_ROOT'].$this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css'))
			$arParams['TEMPLATE_THEME'] = '';
	}
}
if ('' == $arParams['TEMPLATE_THEME'])
	$arParams['TEMPLATE_THEME'] = 'blue';

//$arResult['STOCK_AMOUNT'] = 100;

foreach ( $arResult['BASKET_ITEM_RENDER_DATA'] as $key=>$arBasketItem ) {
    $resProd = CIBlockElement::GetList( ["ID"=>"ASC"], ['IBLOCK_ID'=>PRODUCTS_IBLOCK, 'NAME'=>$arBasketItem['NAME'], 'ACTIVE'=>'Y' ], false, false, ['IBLOCK_ID', 'ID', 'PROPERTY_ARTNUMBER'] );
    while ( $arProd = $resProd->GetNext() ) {
        $arResult['BASKET_ITEM_RENDER_DATA'][$key]['NAME'] = $arProd['PROPERTY_ARTNUMBER_VALUE'].' '.$arBasketItem['NAME'];
        $arResult['BASKET_ITEM_RENDER_DATA'][$key]['~NAME'] = $arProd['PROPERTY_ARTNUMBER_VALUE'].' '.$arBasketItem['~NAME'];
    }
}

//file_put_contents( $_SERVER['DOCUMENT_ROOT'].'/xxx_basket_items.txt', print_r( $arResult, true ) );
<?

global $USER;

$arResult['BASKET_ITEMS'] = [];

foreach ( $arResult['ORDERS'] as $key_order=>$order ) {
    foreach ( $order['BASKET_ITEMS'] as $key_item=>&$item ) {

        $offer = CIBlockElement::GetList(
            ["ID"=>"ASC"],
            ['IBLOCK_ID'=>OFFERS_IBLOCK, 'ID'=>$item['PRODUCT_ID']],
            false,
            false,
            ['IBLOCK-ID', 'ID', 'PROPERTY_CML2_LINK', 'PROPERTY_COLOR', 'PROPERTY_SIZE']
        );

        if ( $arOffer = $offer->GetNext() ) {

            $item['PROD_SIZE'] = init_getHLElems( HLB_OFFERS_SIZE, $arOffer['PROPERTY_SIZE_VALUE'] );
            $item['PROD_COLOR'] = init_getHLElems( HLB_OFFERS_COLOR, $arOffer['PROPERTY_COLOR_VALUE'] );

            $product = CIBlockElement::GetList(
                ["ID"=>"ASC"],
                ['IBLOCK_ID'=>PRODUCTS_IBLOCK, 'ID'=>$arOffer['PROPERTY_CML2_LINK_VALUE']],
                false,
                false,
                ['IBLOCK-ID', 'ID', 'DETAIL_PICTURE', 'PROPERTY_ARTNUMBER']
            );

            while ( $arProd = $product->GetNext() ) {
                $item['ARTNUMBER'] = $arProd['PROPERTY_ARTNUMBER_VALUE'];
                $item['PROD_TYPE'] = 'OFFERS';
                if ( intval($arProd['DETAIL_PICTURE']) > 0 ) {
                    $item['IMAGE'] = CFile::ResizeImageGet( $arProd['DETAIL_PICTURE'], ['width'=>79, 'height'=>79], BX_RESIZE_IMAGE_EXACT )['src'];
                }
                else {
                    $item['IMAGE'] = SITE_TEMPLATE_PATH.'/img/main/hl_prod_no_image.png';
                }
            }

        }
        else {
            $product = CIBlockElement::GetList(
                ["ID"=>"ASC"],
                ['IBLOCK_ID'=>PRODUCTS_IBLOCK, 'ID'=>$item['PRODUCT_ID']],
                false,
                false,
                ['IBLOCK-ID', 'ID', 'DETAIL_PICTURE', 'PROPERTY_ARTNUMBER']
            );

            while ( $arProd = $product->GetNext() ) {



                $item['ARTNUMBER'] = $arProd['PROPERTY_ARTNUMBER_VALUE'];
                $item['PROD_TYPE'] = 'PRODUCT';
                if ( intval($arProd['DETAIL_PICTURE']) > 0 ) {
                    $item['IMAGE'] = CFile::ResizeImageGet( $arProd['DETAIL_PICTURE'], ['width'=>79, 'height'=>79], BX_RESIZE_IMAGE_EXACT )['src'];
                }
                else {
                    $item['IMAGE'] = SITE_TEMPLATE_PATH.'/img/main/hl_prod_no_image.png';
                }
            }
        }

        if ( !array_key_exists( $item['PRODUCT_ID'], $arResult['BASKET_ITEMS'] ) ) {
            $arResult['BASKET_ITEMS'][ $item['PRODUCT_ID'] ] = $item;
            $arResult['BASKET_ITEMS'][ $item['PRODUCT_ID'] ]['QUANTITY'] = floatval($item['QUANTITY']);
        }
        else {
            $arResult['BASKET_ITEMS'][ $item['PRODUCT_ID'] ]['QUANTITY'] += floatval($item['QUANTITY']);
        }
    }
}

?>
<?

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main,
	Bitrix\Main\Localization\Loc,
	Bitrix\Main\Page\Asset;

Asset::getInstance()->addJs("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/script.js");
Asset::getInstance()->addCss("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/style.css");
$this->addExternalCss("/bitrix/css/main/bootstrap.css");
CJSCore::Init(array('clipboard', 'fx'));

Loc::loadMessages(__FILE__);

//file_put_contents( $_SERVER['DOCUMENT_ROOT'].'/xxx_ORDERS_.txt', print_r($arResult['ORDERS'], true));

?><div class="lk-content"><?

        if (!empty($arResult['ERRORS']['FATAL'])) {
            foreach($arResult['ERRORS']['FATAL'] as $error) {
                ShowError($error);
            }
            $component = $this->__component;
            if ($arParams['AUTH_FORM_IN_TEMPLATE'] && isset($arResult['ERRORS']['FATAL'][$component::E_NOT_AUTHORIZED])) {
                $APPLICATION->AuthForm('', false, false, 'N', false);
            }

        }
        else {
            if (!empty($arResult['ERRORS']['NONFATAL'])) {
                foreach($arResult['ERRORS']['NONFATAL'] as $error) {
                    ShowError($error);
                }
            }

                ?><div class="lk-sw-content__item lk-sw-content__active uk-animation-slide-left-medium"><?

                    $paymentChangeData = array();
                    $orderHeaderStatus = null;

                        ?>
                        <div class="lk-date lk-buy-list">

                            <div class="lk-date__content lk-buy-list__content"><?

                                foreach ( $arResult['BASKET_ITEMS'] as $item ) {
                                    ?><div class="lk-date__item">
                                        <div class="lk-date__left-side">
                                            <div class="lk-date__img">
                                                <picture>
                                                    <source srcset="<?=$item['IMAGE']?>" type="image/webp">
                                                    <img src="<?=$item['IMAGE']?>" alt="">
                                                </picture>
                                            </div>

                                            <div class="lk-date__title-wrap">
                                                <div class="lk-date__title">
                                                    <?=$item['NAME']?>
                                                </div>
                                                <div class="lk-date__art">
                                                    <?=$item['ARTNUMBER']?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="lk-date__right-side">
                                            <div class="lk-date__info lk-date__count">
                                                <?=$item['QUANTITY']?> <?=$item['MEASURE_TEXT']?>
                                            </div>
                                            <div class="lk-date__info lk-date__basket">
                                                <form id="order_form_<?=$order['ORDER']['ACCOUNT_NUMBER']?>_<?=$item['PRODUCT_ID']?>">
                                                    <input type="hidden" name="ARTNUMBER" value="<?=$item['ARTNUMBER']?>"><?

                                                    if ( $item['PROD_TYPE'] == 'OFFERS' ) {
                                                        ?><input type="hidden" name="OFFERS[<?=$item['PRODUCT_ID']?>][<?=$item['PROD_COLOR']?>][<?=$item['PROD_SIZE']?>]" value="1"><?
                                                    }
                                                    else {
                                                        ?><input type="hidden" name="PRODUCT[<?=$item['PRODUCT_ID']?>]" value="1"><?
                                                    }

                                                    ?></form>
                                                <a href="#" data-product="<?=$item['PRODUCT_ID']?>" data-order="<?=$order['ORDER']['ACCOUNT_NUMBER']?>">
                                                    <picture>
                                                        <source srcset="<?=SITE_TEMPLATE_PATH?>/img/ico/basket-h.svg" type="image/webp">
                                                        <img src="<?=SITE_TEMPLATE_PATH?>/img/ico/basket-h.svg" alt="">
                                                    </picture>
                                                </a>
                                            </div>
                                        </div>
                                    </div><?
                                }

                            ?></div>

                        </div><?

                ?></div><?

                echo $arResult["NAV_STRING"];

                if ($_REQUEST["filter_history"] !== 'Y') {
                    $javascriptParams = array(
                        "url" => CUtil::JSEscape($this->__component->GetPath().'/ajax.php'),
                        "templateFolder" => CUtil::JSEscape($templateFolder),
                        "templateName" => $this->__component->GetTemplateName(),
                        "paymentList" => $paymentChangeData
                    );
                    $javascriptParams = CUtil::PhpToJSObject($javascriptParams);
                    ?>
                    <script>
                        BX.Sale.PersonalOrderComponent.PersonalOrderList.init(<?=$javascriptParams?>);
                    </script>
                    <?
                }
        }
    ?>
</div>

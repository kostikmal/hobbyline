<?

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main,
	Bitrix\Main\Localization\Loc,
	Bitrix\Main\Page\Asset;

Asset::getInstance()->addJs("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/script.js");
Asset::getInstance()->addCss("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/style.css");
$this->addExternalCss("/bitrix/css/main/bootstrap.css");
CJSCore::Init(array('clipboard', 'fx'));

Loc::loadMessages(__FILE__);

//file_put_contents( $_SERVER['DOCUMENT_ROOT'].'/xxx_ORDERS_.txt', print_r($arResult['ORDERS'], true));

?><div class="lk-content">
    <div class="lk-history"><?

        if (!empty($arResult['ERRORS']['FATAL'])) {
            foreach($arResult['ERRORS']['FATAL'] as $error) {
                ShowError($error);
            }
            $component = $this->__component;
            if ($arParams['AUTH_FORM_IN_TEMPLATE'] && isset($arResult['ERRORS']['FATAL'][$component::E_NOT_AUTHORIZED])) {
                $APPLICATION->AuthForm('', false, false, 'N', false);
            }

        }
        else {
            if (!empty($arResult['ERRORS']['NONFATAL'])) {
                foreach($arResult['ERRORS']['NONFATAL'] as $error) {
                    ShowError($error);
                }
            }
            ?>

            <ul class="lk-switcher">
                <li class="lk-switcher__item lk-switcher__item-active">Все</li>
                <li class="lk-switcher__item">Текущие</li>
                <li class="lk-switcher__item">Завершенные</li>
                <li class="lk-switcher__item">Отмененные</li>
            </ul>

            <div class="lk-sw-content"><?

                // Все заказы START
                ?><div class="lk-sw-content__item lk-sw-content__active uk-animation-slide-left-medium"><?

                    $paymentChangeData = array();
                    $orderHeaderStatus = null;

                    foreach ($arResult['ORDERS'] as $key => $order) {
                        if ($orderHeaderStatus !== $order['ORDER']['STATUS_ID'] && $arResult['SORT_TYPE'] == 'STATUS') {
                            $orderHeaderStatus = $order['ORDER']['STATUS_ID'];
                        }
                        ?>
                        <div class="lk-date">

                            <div class="lk-date__top">
                                <div class="lk-date__label">
                                    <?=Loc::getMessage('SPOL_TPL_ORDER')?> <?=Loc::getMessage('SPOL_TPL_FROM_DATE')?> <?=$order['ORDER']['DATE_INSERT_FORMATED']?> • <?=Loc::getMessage('SPOL_TPL_NUMBER_SIGN').$order['ORDER']['ACCOUNT_NUMBER']?>
                                </div>
                                <div class="lk-date__label-price">
                                    <?=$order['ORDER']['FORMATED_PRICE']?>
                                </div>
                            </div>

                            <div class="lk-date__content"><?

                                foreach ( $order['BASKET_ITEMS'] as $item ) {
                                    ?><div class="lk-date__item">
                                        <div class="lk-date__left-side">
                                            <div class="lk-date__img">
                                                <picture>
                                                    <source srcset="<?=$item['IMAGE']?>" type="image/webp">
                                                    <img src="<?=$item['IMAGE']?>" alt="">
                                                </picture>
                                            </div>
                                            <div class="lk-date__title-wrap">
                                                <div class="lk-date__title">
                                                    <?=$item['NAME']?>
                                                </div>
                                                <div class="lk-date__art">
                                                    <?=$item['ARTNUMBER']?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="lk-date__right-side">
                                            <div class="lk-date__info lk-date__count">
                                                <?=$item['QUANTITY']?> <?=$item['MEASURE_TEXT']?>
                                            </div>
                                            <div class="lk-date__info lk-date__price">
                                                <? echo floatval($item['PRICE']) * floatval($item['QUANTITY']) ?> руб.
                                            </div>
                                            <div class="lk-date__info lk-date__basket">
                                                <form id="order_form_<?=$order['ORDER']['ACCOUNT_NUMBER']?>_<?=$item['PRODUCT_ID']?>">
                                                    <input type="hidden" name="ARTNUMBER" value="<?=$item['ARTNUMBER']?>"><?

                                                    if ( $item['PROD_TYPE'] == 'OFFERS' ) {
                                                        ?><input type="hidden" name="OFFERS[<?=$item['PRODUCT_ID']?>][<?=$item['PROD_COLOR']?>][<?=$item['PROD_SIZE']?>]" value="1"><?
                                                    }
                                                    else {
                                                        ?><input type="hidden" name="PRODUCT[<?=$item['PRODUCT_ID']?>]" value="1"><?
                                                    }

                                                ?></form>
                                                <a href="#" data-product="<?=$item['PRODUCT_ID']?>" data-order="<?=$order['ORDER']['ACCOUNT_NUMBER']?>">
                                                    <picture>
                                                        <source srcset="<?=SITE_TEMPLATE_PATH?>/img/ico/basket-h.svg" type="image/webp">
                                                        <img src="<?=SITE_TEMPLATE_PATH?>/img/ico/basket-h.svg" alt="">
                                                    </picture>
                                                </a>
                                            </div>
                                        </div>
                                    </div><?
                                }

                                if ( $order['ORDER']['CANCELED'] != 'Y' ) {
                                    ?><div class="lk-date__btn-wrap">
                                        <a href="<?=htmlspecialcharsbx($order["ORDER"]["URL_TO_CANCEL"])?>" class="lk-date__btn">Отменить заказ</a>
                                    </div><?
                                }?>

                            </div>

                        </div><?
                    }

                ?></div><?
                // Все заказы FINISH

                // Текущие заказы START
                ?><div class="lk-sw-content__item uk-animation-slide-left-medium"><?

                    $paymentChangeData = array();
                    $orderHeaderStatus = null;

                    foreach ($arResult['ORDERS'] as $key => $order) {

                        if ( $order['ORDER']['STATUS_ID'] == 'F' || $order['ORDER']['CANCELED'] == 'Y' ) {
                            continue;
                        }

                        if ( $orderHeaderStatus !== $order['ORDER']['STATUS_ID'] && $arResult['SORT_TYPE'] == 'STATUS' ) {
                            $orderHeaderStatus = $order['ORDER']['STATUS_ID'];
                        }
                        ?>
                        <div class="lk-date">

                            <div class="lk-date__top">
                                <div class="lk-date__label">
                                    <?=Loc::getMessage('SPOL_TPL_ORDER')?> <?=Loc::getMessage('SPOL_TPL_FROM_DATE')?> <?=$order['ORDER']['DATE_INSERT_FORMATED']?> • <?=Loc::getMessage('SPOL_TPL_NUMBER_SIGN').$order['ORDER']['ACCOUNT_NUMBER']?>
                                </div>
                            <div class="lk-date__label-price">
                                <?=$order['ORDER']['FORMATED_PRICE']?>
                            </div>
                        </div>

                            <div class="lk-date__content"><?

                                foreach ( $order['BASKET_ITEMS'] as $item ) {

                                    ?><div class="lk-date__item">
                                        <div class="lk-date__left-side">
                                            <div class="lk-date__img">
                                                <picture>
                                                    <source srcset="<?=$item['IMAGE']?>" type="image/webp">
                                                    <img src="<?=$item['IMAGE']?>" alt="">
                                                </picture>
                                            </div>
                                            <div class="lk-date__title-wrap">
                                                <div class="lk-date__title">
                                                    <?=$item['NAME']?>
                                                </div>
                                                <div class="lk-date__art">
                                                    <?=$item['ARTNUMBER']?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="lk-date__right-side">
                                            <div class="lk-date__info lk-date__count">
                                                <?=$item['QUANTITY']?> <?=$item['MEASURE_TEXT']?>
                                            </div>
                                            <div class="lk-date__info lk-date__price">
                                                <? echo floatval($item['PRICE']) * floatval($item['QUANTITY']) ?> руб.
                                            </div>
                                            <div class="lk-date__info lk-date__basket">
                                                <form id="order_form_<?=$order['ORDER']['ACCOUNT_NUMBER']?>_<?=$item['PRODUCT_ID']?>">
                                                    <input type="hidden" name="ARTNUMBER" value="<?=$item['ARTNUMBER']?>"><?

                                                    if ( $item['PROD_TYPE'] == 'OFFERS' ) {
                                                        ?><input type="hidden" name="OFFERS[<?=$item['PRODUCT_ID']?>][<?=$item['PROD_COLOR']?>][<?=$item['PROD_SIZE']?>]" value="1"><?
                                                    }
                                                    else {
                                                        ?><input type="hidden" name="PRODUCT[<?=$item['PRODUCT_ID']?>]" value="1"><?
                                                    }

                                                    ?></form>
                                                <a href="#" data-product="<?=$item['PRODUCT_ID']?>" data-order="<?=$order['ORDER']['ACCOUNT_NUMBER']?>">
                                                    <picture>
                                                        <source srcset="<?=SITE_TEMPLATE_PATH?>/img/ico/basket-h.svg" type="image/webp">
                                                        <img src="<?=SITE_TEMPLATE_PATH?>/img/ico/basket-h.svg" alt="">
                                                    </picture>
                                                </a>
                                            </div>
                                        </div>
                                    </div><?
                                }

                                if ( $order['ORDER']['CANCELED'] != 'Y' ) {
                                    ?><div class="lk-date__btn-wrap">
                                        <a href="<?=htmlspecialcharsbx($order["ORDER"]["URL_TO_CANCEL"])?>" class="lk-date__btn">Отменить заказ</a>
                                    </div><?
                                }?>

                            </div>

                        </div><?
                    }

                ?></div><?
                // Текущие заказы FINISH

                // Завершенные заказы START
                ?><div class="lk-sw-content__item uk-animation-slide-left-medium"><?

                $paymentChangeData = array();
                $orderHeaderStatus = null;

                foreach ($arResult['ORDERS'] as $key => $order) {

                    if ( $order['ORDER']['STATUS_ID'] != 'F' ) {
                        continue;
                    }

                    if ( $orderHeaderStatus !== $order['ORDER']['STATUS_ID'] && $arResult['SORT_TYPE'] == 'STATUS' ) {
                        $orderHeaderStatus = $order['ORDER']['STATUS_ID'];
                    }
                    ?>
                    <div class="lk-date">

                    <div class="lk-date__top">
                        <div class="lk-date__label">
                            <?=Loc::getMessage('SPOL_TPL_ORDER')?> <?=Loc::getMessage('SPOL_TPL_FROM_DATE')?> <?=$order['ORDER']['DATE_INSERT_FORMATED']?> • <?=Loc::getMessage('SPOL_TPL_NUMBER_SIGN').$order['ORDER']['ACCOUNT_NUMBER']?>
                        </div>
                        <div class="lk-date__label-price">
                            <?=$order['ORDER']['FORMATED_PRICE']?>
                        </div>
                    </div>

                    <div class="lk-date__content"><?

                        foreach ( $order['BASKET_ITEMS'] as $item ) {

                            ?><div class="lk-date__item">
                            <div class="lk-date__left-side">
                                <div class="lk-date__img">
                                    <picture>
                                        <source srcset="<?=$item['IMAGE']?>" type="image/webp">
                                        <img src="<?=$item['IMAGE']?>" alt="">
                                    </picture>
                                </div>
                                <div class="lk-date__title-wrap">
                                    <div class="lk-date__title">
                                        <?=$item['NAME']?>
                                    </div>
                                    <div class="lk-date__art">
                                        <?=$item['ARTNUMBER']?>
                                    </div>
                                </div>
                            </div>

                            <div class="lk-date__right-side">
                                <div class="lk-date__info lk-date__count">
                                    <?=$item['QUANTITY']?> <?=$item['MEASURE_TEXT']?>
                                </div>
                                <div class="lk-date__info lk-date__price">
                                    <? echo floatval($item['PRICE']) * floatval($item['QUANTITY']) ?> руб.
                                </div>
                                <div class="lk-date__info lk-date__basket">
                                    <form id="order_form_<?=$order['ORDER']['ACCOUNT_NUMBER']?>_<?=$item['PRODUCT_ID']?>">
                                        <input type="hidden" name="ARTNUMBER" value="<?=$item['ARTNUMBER']?>"><?

                                        if ( $item['PROD_TYPE'] == 'OFFERS' ) {
                                            ?><input type="hidden" name="OFFERS[<?=$item['PRODUCT_ID']?>][<?=$item['PROD_COLOR']?>][<?=$item['PROD_SIZE']?>]" value="1"><?
                                        }
                                        else {
                                            ?><input type="hidden" name="PRODUCT[<?=$item['PRODUCT_ID']?>]" value="1"><?
                                        }

                                        ?></form>
                                    <a href="#" data-product="<?=$item['PRODUCT_ID']?>" data-order="<?=$order['ORDER']['ACCOUNT_NUMBER']?>">
                                        <picture>
                                            <source srcset="<?=SITE_TEMPLATE_PATH?>/img/ico/basket-h.svg" type="image/webp">
                                            <img src="<?=SITE_TEMPLATE_PATH?>/img/ico/basket-h.svg" alt="">
                                        </picture>
                                    </a>
                                </div>
                            </div>
                            </div><?
                        }

                        if ( $order['ORDER']['CANCELED'] != 'Y' ) {
                            ?><div class="lk-date__btn-wrap">
                                <a href="<?=htmlspecialcharsbx($order["ORDER"]["URL_TO_CANCEL"])?>" class="lk-date__btn">Отменить заказ</a>
                            </div><?
                        }?>

                    </div>

                    </div><?
                }

                ?></div><?
                // Завершенные заказы FINISH

                // Отмененные заказы START
                ?><div class="lk-sw-content__item uk-animation-slide-left-medium"><?

                $paymentChangeData = array();
                $orderHeaderStatus = null;

                foreach ($arResult['ORDERS'] as $key => $order) {

                    if ( $order['ORDER']['CANCELED'] != 'Y' ) {
                        continue;
                    }

                    if ( $orderHeaderStatus !== $order['ORDER']['STATUS_ID'] && $arResult['SORT_TYPE'] == 'STATUS' ) {
                        $orderHeaderStatus = $order['ORDER']['STATUS_ID'];
                    }
                    ?>
                    <div class="lk-date">

                    <div class="lk-date__top">
                        <div class="lk-date__label">
                            <?=Loc::getMessage('SPOL_TPL_ORDER')?> <?=Loc::getMessage('SPOL_TPL_FROM_DATE')?> <?=$order['ORDER']['DATE_INSERT_FORMATED']?> • <?=Loc::getMessage('SPOL_TPL_NUMBER_SIGN').$order['ORDER']['ACCOUNT_NUMBER']?>
                        </div>
                        <div class="lk-date__label-price">
                            <?=$order['ORDER']['FORMATED_PRICE']?>
                        </div>
                    </div>

                    <div class="lk-date__content"><?

                        foreach ( $order['BASKET_ITEMS'] as $item ) {

                            ?><div class="lk-date__item">
                            <div class="lk-date__left-side">
                                <div class="lk-date__img">
                                    <picture>
                                        <source srcset="<?=$item['IMAGE']?>" type="image/webp">
                                        <img src="<?=$item['IMAGE']?>" alt="">
                                    </picture>
                                </div>
                                <div class="lk-date__title-wrap">
                                    <div class="lk-date__title">
                                        <?=$item['NAME']?>
                                    </div>
                                    <div class="lk-date__art">
                                        <?=$item['ARTNUMBER']?>
                                    </div>
                                </div>
                            </div>

                            <div class="lk-date__right-side">
                                <div class="lk-date__info lk-date__count">
                                    <?=$item['QUANTITY']?> <?=$item['MEASURE_TEXT']?>
                                </div>
                                <div class="lk-date__info lk-date__price">
                                    <? echo floatval($item['PRICE']) * floatval($item['QUANTITY']) ?> руб.
                                </div>
                                <div class="lk-date__info lk-date__basket">
                                    <form id="order_form_<?=$order['ORDER']['ACCOUNT_NUMBER']?>_<?=$item['PRODUCT_ID']?>">
                                        <input type="hidden" name="ARTNUMBER" value="<?=$item['ARTNUMBER']?>"><?

                                        if ( $item['PROD_TYPE'] == 'OFFERS' ) {
                                            ?><input type="hidden" name="OFFERS[<?=$item['PRODUCT_ID']?>][<?=$item['PROD_COLOR']?>][<?=$item['PROD_SIZE']?>]" value="1"><?
                                        }
                                        else {
                                            ?><input type="hidden" name="PRODUCT[<?=$item['PRODUCT_ID']?>]" value="1"><?
                                        }

                                        ?></form>
                                    <a href="#" data-product="<?=$item['PRODUCT_ID']?>" data-order="<?=$order['ORDER']['ACCOUNT_NUMBER']?>">
                                        <picture>
                                            <source srcset="<?=SITE_TEMPLATE_PATH?>/img/ico/basket-h.svg" type="image/webp">
                                            <img src="<?=SITE_TEMPLATE_PATH?>/img/ico/basket-h.svg" alt="">
                                        </picture>
                                    </a>
                                </div>
                            </div>
                            </div><?
                        }

                        /*?><div class="lk-date__btn-wrap">
                            <a href="#" class="lk-date__btn">Отменить заказ</a>
                        </div><?*/

                    ?></div>

                    </div><?
                }

                ?></div><?
                // Отмененные заказы FINISH

                echo $arResult["NAV_STRING"];

                if ($_REQUEST["filter_history"] !== 'Y') {
                    $javascriptParams = array(
                        "url" => CUtil::JSEscape($this->__component->GetPath().'/ajax.php'),
                        "templateFolder" => CUtil::JSEscape($templateFolder),
                        "templateName" => $this->__component->GetTemplateName(),
                        "paymentList" => $paymentChangeData
                    );
                    $javascriptParams = CUtil::PhpToJSObject($javascriptParams);
                    ?>
                    <script>
                        BX.Sale.PersonalOrderComponent.PersonalOrderList.init(<?=$javascriptParams?>);
                    </script>
                    <?
                }

            ?></div><?
        }
    ?></div>
</div>

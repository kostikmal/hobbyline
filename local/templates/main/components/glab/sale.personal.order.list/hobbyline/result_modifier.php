<?

foreach ( $arResult['ORDERS'] as $key_order=>$order ) {

    foreach ( $order['BASKET_ITEMS'] as $key_item=>$item ) {

        $offer = CIBlockElement::GetList(
            ["ID"=>"ASC"],
            ['IBLOCK_ID'=>OFFERS_IBLOCK, 'ID'=>$item['PRODUCT_ID']],
            false,
            false,
            ['IBLOCK-ID', 'ID', 'PROPERTY_CML2_LINK', 'PROPERTY_COLOR', 'PROPERTY_SIZE']
        );

        if ( $arOffer = $offer->GetNext() ) {

            $arResult['ORDERS'][$key_order]['BASKET_ITEMS'][$key_item]['PROD_SIZE'] = init_getHLElems( HLB_OFFERS_SIZE, $arOffer['PROPERTY_SIZE_VALUE'] );
            $arResult['ORDERS'][$key_order]['BASKET_ITEMS'][$key_item]['PROD_COLOR'] = init_getHLElems( HLB_OFFERS_COLOR, $arOffer['PROPERTY_COLOR_VALUE'] );

            $product = CIBlockElement::GetList(
                ["ID"=>"ASC"],
                ['IBLOCK_ID'=>PRODUCTS_IBLOCK, 'ID'=>$arOffer['PROPERTY_CML2_LINK_VALUE']],
                false,
                false,
                ['IBLOCK-ID', 'ID', 'DETAIL_PICTURE', 'PROPERTY_ARTNUMBER']
            );

            while ( $arProd = $product->GetNext() ) {
                $arResult['ORDERS'][$key_order]['BASKET_ITEMS'][$key_item]['ARTNUMBER'] = $arProd['PROPERTY_ARTNUMBER_VALUE'];
                $arResult['ORDERS'][$key_order]['BASKET_ITEMS'][$key_item]['PROD_TYPE'] = 'OFFERS';
                if ( intval($arProd['DETAIL_PICTURE']) > 0 ) {
                    $arResult['ORDERS'][$key_order]['BASKET_ITEMS'][$key_item]['IMAGE'] = CFile::ResizeImageGet( $arProd['DETAIL_PICTURE'], ['width'=>79, 'height'=>79], BX_RESIZE_IMAGE_EXACT )['src'];
                }
                else {
                    $arResult['ORDERS'][$key_order]['BASKET_ITEMS'][$key_item]['IMAGE'] = SITE_TEMPLATE_PATH.'/img/main/hl_prod_no_image.png';
                }
            }

        }
        else {
            $product = CIBlockElement::GetList(
                ["ID"=>"ASC"],
                ['IBLOCK_ID'=>PRODUCTS_IBLOCK, 'ID'=>$item['PRODUCT_ID']],
                false,
                false,
                ['IBLOCK-ID', 'ID', 'DETAIL_PICTURE', 'PROPERTY_ARTNUMBER']
            );

            while ( $arProd = $product->GetNext() ) {
                $arResult['ORDERS'][$key_order]['BASKET_ITEMS'][$key_item]['ARTNUMBER'] = $arProd['PROPERTY_ARTNUMBER_VALUE'];
                $arResult['ORDERS'][$key_order]['BASKET_ITEMS'][$key_item]['PROD_TYPE'] = 'PRODUCT';
                if ( intval($arProd['DETAIL_PICTURE']) > 0 ) {
                    $arResult['ORDERS'][$key_order]['BASKET_ITEMS'][$key_item]['IMAGE'] = CFile::ResizeImageGet( $arProd['DETAIL_PICTURE'], ['width'=>79, 'height'=>79], BX_RESIZE_IMAGE_EXACT )['src'];
                }
                else {
                    $arResult['ORDERS'][$key_order]['BASKET_ITEMS'][$key_item]['IMAGE'] = SITE_TEMPLATE_PATH.'/img/main/hl_prod_no_image.png';
                }
            }
        }
    }
}

?>
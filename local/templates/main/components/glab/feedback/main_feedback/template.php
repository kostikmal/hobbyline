<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */

?><div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical p-call__body">
    <button class="uk-modal-close-outside hl-modal__close" type="button">
        <picture>
            <source srcset="<?=SITE_TEMPLATE_PATH?>/img/main/ha-modal-plus.svg" type="image/webp">
            <img src="<?=SITE_TEMPLATE_PATH?>/img/main/ha-modal-plus.svg" alt="">
        </picture>
    </button>

    <div class="p-call__double">
        <div class="p-call__img">
            <picture>
                <source srcset="<?=SITE_TEMPLATE_PATH?>/img/main/p-call.webp" type="image/webp">
                <img src="<?=SITE_TEMPLATE_PATH?>/img/main/p-call.jpg" alt="">
            </picture>
        </div>

        <div class="p-call__right"><?

            if(!empty($arResult["ERROR_MESSAGE"])) {
                foreach($arResult["ERROR_MESSAGE"] as $v)
                    ShowError($v);
            }

            if(strlen($arResult["OK_MESSAGE"]) > 0) {
                ?><h2 class="uk-modal-title hl-modal__label"><?=$arResult["OK_MESSAGE"]?></h2>
                <a href="<?=$APPLICATION->GetCurPageParam('', ['success'], false); ?>" class="link-half lookbook__link order-btn-get">
                    ЗАКРЫТЬ
                    <picture>
                        <source srcset="<?=SITE_TEMPLATE_PATH?>/img/ico/filter-arr-right.svg" type="image/webp">
                        <img src="<?=SITE_TEMPLATE_PATH?>/img/ico/filter-arr-right.svg" class="btn-arrow-cat" alt="">
                    </picture>
                </a>
                <script>
                    $(document).on('click', '.p-call__body .hl-modal__close', function (e) {
                        e.preventDefault();
                        $('.p-call__body a.order-btn-get').click();
                    });
                    UIkit.modal('#modal-call').show();
                </script>
                <style>
                    .p-call__body .hl-modal__close {
                        display: none;
                    }
                </style><?
            }
            else {
                ?><h2 class="uk-modal-title hl-modal__label">
                    Мы свяжемся с Вами и ответим на все Ваши вопросы!
                </h2>

                <form action="<?=POST_FORM_ACTION_URI?>" method="POST" class="main_feedback">

                    <?=bitrix_sessid_post()?>

                    <div class="auth-form__text">
                        <input type="text" class="hl-require" id="opt-name" placeholder="Ваше имя" name="user_name" required>
                    </div>

                    <div class="auth-form__text">
                        <input type="text" class="hl-require" id="opt-name" placeholder="Ваш номер телефона" name="user_phone" required>
                    </div>

                    <div class="auth-form__text">
                        <input type="text" class="hl-require" id="opt-name" placeholder="Ваш город" name="user_city" required>
                    </div>

                    <div class="p-call__pers">
                        <input type="checkbox" class="custom-checkbox" name="" id="pers-data" value="">
                        <label for="pers-data" class="footer__label remember__label reg__chechbox">
                            <a href="<?=PROCESSING_PERSONAL_DATA_LINK?>" target="_blank">Согласие на обработку персональных данных </a>
                        </label>
                    </div>

                    <a href="#" class="link-half lookbook__link order-btn-get p-call__btn">
                        ПЕРЕЗВОНИТЕ МНЕ!
                        <picture>
                            <source srcset="<?=SITE_TEMPLATE_PATH?>/img/ico/filter-arr-right.svg" type="image/webp">
                            <img src="<?=SITE_TEMPLATE_PATH?>/img/ico/filter-arr-right.svg" class="btn-arrow-cat" alt="">
                        </picture>
                    </a>

                    <input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">
                    <input type="submit" name="submit" value="<?=GetMessage("MFT_SUBMIT")?>" class="to_submit_main_feedback">

                </form><?
            }

        ?></div>
    </div>
</div>
<script>
    $('a.p-call__btn').on('click', function (e) {
        e.preventDefault();
        $('.to_submit_main_feedback').click();
    });
</script>
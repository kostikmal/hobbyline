<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */

$arVacancies = [];

$res = CIBlockElement::GetList(
    ['ID'=>'ASC'],
    ['IBLOCK_ID'=>10, 'ACTIVE'=>'Y'],
    false,
    false,
    ['ID', 'NAME']
);

while ( $arRes = $res->GetNext() ) {
    $arVacancies[] = $arRes['NAME'];
}

?>

<?if(!empty($arResult["ERROR_MESSAGE"])) {
	foreach($arResult["ERROR_MESSAGE"] as $v)
		ShowError($v);
}
if(strlen($arResult["OK_MESSAGE"]) > 0) {
	?><div class="mf-ok-text"><?=$arResult["OK_MESSAGE"]?></div><?
}
?>

<form action="<?=POST_FORM_ACTION_URI?>" method="POST" class="vacancies">

    <?=bitrix_sessid_post()?>

    <div class="form__row">
        <input type="text" class="form__input" name="user_name" value="" placeholder="Имя*" required>
        <?/*<input type="text" class="form__input" name="user_last_name" value="" placeholder="Фамилия*" required>
        <input type="text" class="form__input" name="user_phone" value="" placeholder="Номер телефона*" required>*/?>
        <input type="text" class="form__input" name="user_email" value="" placeholder="E-mail*" required>

        <?/*<div class="__select" data-state="">
            <div class="__select__title" data-default="Option 0">Выбрать вакансию</div>
            <div class="__select__content">
                <input id="singleSelect0" class="__select__input" type="radio" name="singleSelect" checked/>
                <label for="singleSelect0" class="__select__label"></label><?

                foreach ( $arVacancies as $sVac ) {
                    ?><input id="singleSelect1" class="__select__input" type="radio" name="singleSelect"/>
                    <label for="singleSelect1" class="__select__label"><?=$sVac?></label><?
                }

            ?></div>
        </div>*/?>

    </div>

    <textarea name="MESSAGE" class="form__textarea cp-form__textarea" placeholder="Ваши вопросы, пожелания и предложения"></textarea>

    <div class="uk-flex uk-flex-between form__btn-wrap">
        <div class="uk-margin">
            <?/*<div uk-form-custom class="form__file-wrap">
                <input type="file" name="CV_FILE">
                <span class="form__file">ПРИКРЕПИТЬ РЕЗЮМЕ</span>
                <picture>
                    <source srcset="<?=SITE_TEMPLATE_PATH?>/img/ico/file.svg" type="image/webp">
                    <img src="<?=SITE_TEMPLATE_PATH?>/img/ico/file.svg" alt="">
                </picture>
            </div>*/?>
        </div>

        <div class="mf-captcha">
            <div class="mf-text"><?=GetMessage("MFT_CAPTCHA")?></div>
            <input type="hidden" name="captcha_sid" value="<?=$arResult["capCode"]?>">
            <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["capCode"]?>" width="180" height="40" alt="CAPTCHA">
            <div class="mf-text"><?=GetMessage("MFT_CAPTCHA_CODE")?><span class="mf-req">*</span></div>
            <input type="text" name="captcha_word" size="30" maxlength="50" value="">
        </div>

        <button class="link-half form__link">
            ОТПРАВИТЬ
            <picture>
                <source srcset="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" type="image/webp">
                <img src="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" class="btn-arrow-cat" alt="">
            </picture>
        </button>

        <input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">
        <input type="hidden" name="submit" value="<?=GetMessage("MFT_SUBMIT")?>">

    </div>

</form>
<style>
    .mf-ok-text {
        text-align: center;
        font-family: Glober-free;
        font-weight: 700;
        font-size: 34px;
        color: #e10486;
    }
    }
</style>
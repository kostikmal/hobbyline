<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Интернет-магазин \"Одежда\"");
?>

<div class="top-info" uk-scrollspy="cls: uk-animation-fade; target: div; delay: 100; repeat: false">
    <div class="top-info__wrap main-content">
      <div class="top-info__content">
        <h1 class="main-title">Носки, колготки и перчатки -</h1>
        <p class="main-sub-title">забота о Вас и радость для глаз!</p>
        <a href="#" class="top-info__link btn-anim">В каталог</a>
        <div class="top-info__socials socials">
          <a href="#" class="socials__item"><picture><source srcset="img/ico/inst.svg" type="image/webp"><img src="img/ico/inst.svg" alt=""></picture></a>
          <a href="#" class="socials__item"><picture><source srcset="img/ico/vk.svg" type="image/webp"><img src="img/ico/vk.svg" alt=""></picture></a>
          <a href="#" class="socials__item"><picture><source srcset="img/ico/fb.svg" type="image/webp"><img src="img/ico/fb.svg" alt=""></picture></a>
        </div>
      </div>
      <div class="top-info__slider slider">
        <div class="top-info__slider-wrap">
          <div class="uk-position-relative uk-visible-toggle uk-light slider__container" tabindex="-1" uk-slider>

            <ul class="uk-slider-items uk-grid">
              <li class="uk-width-4-4">
                <div class="uk-panel slider__item">
                  <picture><source srcset="img/main/f1.webp" type="image/webp"><img src="img/main/f1.jpg" alt=""></picture>
                </div>
              </li>
              <li class="uk-width-4-4 slider__item">
                <div class="uk-panel">
                  <picture><source srcset="img/main/f1.webp" type="image/webp"><img src="img/main/f1.jpg" alt=""></picture>
                </div>
              </li>
              <li class="uk-width-4-4 slider__item">
                <div class="uk-panel">
                  <picture><source srcset="img/main/f1.webp" type="image/webp"><img src="img/main/f1.jpg" alt=""></picture>
                </div>
              </li>
              <li class="uk-width-4-4 slider__item">
                <div class="uk-panel">
                  <picture><source srcset="img/main/f1.webp" type="image/webp"><img src="img/main/f1.jpg" alt=""></picture>
                </div>
              </li>
              <li class="uk-width-4-4 slider__item">
                <div class="uk-panel">
                  <picture><source srcset="img/main/f1.webp" type="image/webp"><img src="img/main/f1.jpg" alt=""></picture>
                </div>
              </li>
            </ul>
            <div class="slider__wrap-arrows">
              <a class="slider__prev" href="#" uk-slider-item="previous"></a>
              <a class="slider__next" href="#" uk-slider-item="next"></a>
            </div>
          </div>
          <div class="slider__bg top-info__bg"></div>
        </div>
      </div>
      <div class="label-mob-wrap">
        <div class="main-title__mob">Носки, колготки и перчатки -</div>
        <p class="main-sub-title__mob">забота о Вас и радость для глаз!</p>
      </div>
    </div>
    <div class="top-info__heart-dots heart-dots"></div>
  </div>

  <section class="about">
    <div class="about__wrap main-content">
      <h2 class="about__title title">О нас</h2>
      <div class="about__list" uk-grid uk-scrollspy="cls: uk-animation-slide-bottom; target: .about__item; delay: 300; repeat: false" offset-top="-200">
        <div class="about__item">
          <div class="about__img">
            <picture><source srcset="img/main/a1.svg" type="image/webp"><img src="img/main/a1.svg" alt=""></picture>
          </div>
          <p>лет на рынке</p>
        </div>
        <div class="about__item">
          <div class="about__img">
            <picture><source srcset="img/main/a2.svg" type="image/webp"><img src="img/main/a2.svg" alt=""></picture>
          </div>
          <p>Высокий уровень сервиса</p>
        </div>
        <div class="about__item">
          <div class="about__img">
            <picture><source srcset="img/main/a3.svg" type="image/webp"><img src="img/main/a3.svg" alt=""></picture>
          </div>
          <p>Удобная доставка</p>
        </div>
        <div class="about__item">
          <div class="about__img">
            <picture><source srcset="img/main/a4.svg" type="image/webp"><img src="img/main/a4.svg" alt=""></picture>
          </div>
          <p>Собственное производство</p>
        </div>
        <div class="about__item">
          <div class="about__img">
            <picture><source srcset="img/main/a5.svg" type="image/webp"><img src="img/main/a5.svg" alt=""></picture>
          </div>
          <p>Первоклассное качество</p>
        </div>
      </div>

      <div class="about__double double" uk-scrollspy="cls: uk-animation-fade; target: div; delay: 300; repeat: false" offset-top="-200">
        <div class="double__content">
          <p>
            <!-- Добро пожаловать на сайт HOBBY LINE – один из лидеров России по широчайшему ассортименту чулочно-носочных изделий и перчаток на любой вкус! В нашем каталоге вы найдете огромный выбор изделий – от ярких и необычных моделей носков до
            цветных и стильных колготок, в которых вас обязательно заметят. Познакомьтесь с нами ближе – жмите «Подробнее о нас»! -->
            Добро пожаловать на сайт HOBBY LINE – компании №1 в России по производству чулочно-носочных изделий и перчаток на любой вкус! В нашем каталоге вы найдете огромный выбор изделий – от ярких и необычных моделей носков до цветных и стильных
            колготок, в которых вас обязательно заметят. <br> Познакомьтесь с нами ближе – жмите
          </p>
        </div>
        <div class="double__more">
          <a href="#" class="double__btn btn-arrow">
            <span> ПОДРОБНЕЕ О НАС
              <picture><source srcset="img/main/arrow-rg.svg" type="image/webp"><img src="img/main/arrow-rg.svg" alt=""></picture>
            </span>
          </a>
          <picture><source srcset="img/main/about-img.svg" type="image/webp"><img src="img/main/about-img.svg" class="double__img" alt=""></picture>
        </div>
      </div>
    </div>

    <div class="about-left__heart-dots heart-dots "></div>
    <div class="about-right__heart-dots heart-dots "></div>
  </section>

  <section class="catalog main-content">
    <h2 class="catalog__title title">Каталог</h2>
    <div class="catalog__wrap">
      <div class="catalog__row" uk-scrollspy="cls: uk-animation-fade; target: .btn-img; delay: 300; repeat: false" offset-top="-200">
        <div class="btn-img">
          <div class="btn-img__btn woman-cat">
            <a href="#" class="btn-anim">
              <span> ЖЕНСКИЕ НОСКИ
                <picture><source srcset="img/main/arrow-rg.svg" type="image/webp"><img src="img/main/arrow-rg.svg" class="btn-arrow-cat" alt=""></picture>
              </span>
            </a>
            <picture><source srcset="img/main/woman.svg" type="image/webp"><img src="img/main/woman.svg" class="btn-img__img" alt=""></picture>
          </div>
        </div>
        <div class="btn-img man-cat">
          <div class="btn-img__btn">
            <a href="#" class="btn-anim">
              <span> МУЖСКИЕ НОСКИ
                <picture><source srcset="img/main/arrow-rg.svg" type="image/webp"><img src="img/main/arrow-rg.svg" class="btn-arrow-cat" alt=""></picture>
              </span>
            </a>
            <picture><source srcset="img/main/man.svg" type="image/webp"><img src="img/main/man.svg" class="btn-img__img" alt=""></picture>
          </div>
        </div>
      </div>
      <div class="catalog__row triple-row" uk-scrollspy="cls: uk-animation-fade; target: .btn-img; delay: 100; repeat: false" offset-top="-200">
        <div class="btn-img gloves-cat">
          <div class="btn-img__btn">
            <a href="#" class="btn-anim">
              <span> ВАРЕЖКИ И ПЕРЧАТКИ
                <picture><source srcset="img/main/arrow-rg.svg" type="image/webp"><img src="img/main/arrow-rg.svg" class="btn-arrow-cat" alt=""></picture>
              </span>
            </a>
            <picture><source srcset="img/main/gloves.svg" type="image/webp"><img src="img/main/gloves.svg" class="btn-img__img" alt=""></picture>
          </div>
        </div>

        <div class="catalog__double">
          <div class="btn-img tights-cat">
            <div class="btn-img__btn">
              <a href="#" class="btn-anim-span">
                <span> ПОДСЛЕДНИКИ<br> И ГОЛЬФИНЫ<br>
                  <picture><source srcset="img/main/arrow-rg.svg" type="image/webp"><img src="img/main/arrow-rg.svg" class="btn-arrow-cat" alt=""></picture>
                </span>
              </a>
              <picture><source srcset="img/main/tights.svg" type="image/webp"><img src="img/main/tights.svg" class="btn-img__img" alt=""></picture>
            </div>
          </div>
          <div class="btn-img childs-cat">
            <div class="btn-img__btn">
              <a href="#" class="btn-anim">
                <span> ДЕТСКИЕ НОСКИ<br>
                  <picture><source srcset="img/main/arrow-rg.svg" type="image/webp"><img src="img/main/arrow-rg.svg" class="btn-arrow-cat" alt=""></picture>
                </span>
              </a>
              <picture><source srcset="img/main/childs.svg" type="image/webp"><img src="img/main/childs.svg" class="btn-img__img" alt=""></picture>
            </div>
          </div>
        </div>

        <div class="btn-img leggings-cat">
          <div class="btn-img__btn">
            <a href="#" class="btn-anim-span">
              <span> КОЛГОТКИ И<br> ЛОСИНЫ<br>
                <picture><source srcset="img/main/arrow-rg.svg" type="image/webp"><img src="img/main/arrow-rg.svg" class="btn-arrow-cat" alt=""></picture>
              </span>
            </a>
            <picture><source srcset="img/main/leggings.svg" type="image/webp"><img src="img/main/leggings.svg" class="btn-img__img" alt=""></picture>
          </div>
        </div>
      </div>

      <div class="catalog__row" uk-scrollspy="cls: uk-animation-fade; target: .btn-img; delay: 300; repeat: false" offset-top="-100">
        <div class="btn-img specials-cat">
          <div class="btn-img__btn ">
            <a href="#" class="btn-anim">
              <span> СПЕЦПРЕДЛОЖЕНИЯ<br> И СКИДКИ
              </span>
              <picture><source srcset="img/main/arrow-rg.svg" type="image/webp"><img src="img/main/arrow-rg.svg" class="btn-arrow-cat" alt=""></picture>
            </a>
            <picture><source srcset="img/main/proc.svg" type="image/webp"><img src="img/main/proc.svg" class="btn-img__img" alt=""></picture>
          </div>
        </div>
        <div class="btn-img specials-cat full-cat">
          <div class="btn-img__btn ">
            <a href="#" class="btn-anim">
              <span> ПЕРЕЙТИ В ПОЛНЫЙ КАТАЛОГ
                <picture><source srcset="img/main/arrow-rg.svg" type="image/webp"><img src="img/main/arrow-rg.svg" class="btn-arrow-cat" alt=""></picture>
              </span>
            </a>
          </div>
        </div>
      </div>
    </div>

    <div class="catalog-top__heart-dots heart-dots "></div>
    <div class="catalog-right__heart-dots heart-dots "></div>
  </section>

  <section class="lookbook" uk-scrollspy="cls: uk-animation-fade; target: .lookbook__item; delay: 300; repeat: false" offset-top="-200">
    <div class="lookbook__item">
      <div class="lookbook__img-wrap lookbook__l1">
        <picture><source srcset="img/main/l1.webp" type="image/webp"><img src="img/main/l1.jpg" class="lookbook__img-top" alt=""></picture>
      </div>
      <div class="lookbook__img-wrap lookbook__l2">
        <picture><source srcset="img/main/l2.webp" type="image/webp"><img src="img/main/l2.jpg" class="lookbook__img-bottom" alt=""></picture>
      </div>
    </div>
    <div class="lookbook__item uk-flex-between">
      <div class="lookbook__content">
        <h2 class="lookbook__title title">LOOKBOOK</h2>
        <p>
          Вдохновляйтесь нашими образами, расширяйте ассортимент ваших магазинов и черпайте идеи для своего гардероба!
        </p>
        <a href="#" class="link-half lookbook__link">ПЕРЕЙТИ В LOOKBOOK <picture><source srcset="img/main/arrow-rg.svg" type="image/webp"><img src="img/main/arrow-rg.svg" class="btn-arrow-cat" alt=""></picture></a>
      </div>
      <div class="lookbook__img-wrap lookbook__l3">
        <picture><source srcset="img/main/l3.webp" type="image/webp"><img src="img/main/l3.jpg" alt=""></picture>
      </div>
    </div>
    <div class="lookbook__item">
      <div class="lookbook__row">
        <div class="lookbook__img-wrap lookbook__l4">
          <picture><source srcset="img/main/l4.webp" type="image/webp"><img src="img/main/l4.jpg" alt=""></picture>
        </div>
        <div class="lookbook__img-wrap lookbook__l5">
          <picture><source srcset="img/main/l5.webp" type="image/webp"><img src="img/main/l5.jpg" alt=""></picture>
        </div>
        <div class="lookbook__img-wrap lookbook__l6">
          <picture><source srcset="img/main/l6.webp" type="image/webp"><img src="img/main/l6.jpg" alt=""></picture>
        </div>
      </div>
      <div class="lookbook__row">
        <div class="lookbook__img-wrap lookbook__l7">
          <picture><source srcset="img/main/l7.webp" type="image/webp"><img src="img/main/l7.jpg" alt=""></picture>
        </div>
        <div class="lookbook__img-wrap lookbook__l8">
          <picture><source srcset="img/main/l8.webp" type="image/webp"><img src="img/main/l8.jpg" alt=""></picture>
        </div>
      </div>
    </div>
  </section>

  <section class="opt" uk-scrollspy="cls: uk-animation-fade; target: .opt__wrap; delay: 100; repeat: false" offset-top="-200">
    <div class="opt__wrap main-content">
      <div class="opt__content">
        <h2 class="opt__title title">Приглашаем к сотрудничеству оптовиков</h2>
        <p>
          Мы работаем с качественным сырьем и материалами, успешно обеспечивая необходимой продукцией нас и наших партнеров. HOBBY LINE заботится о каждом своем клиенте, предлагая выгодные условия сотрудничества и обеспечивая четкое выполнение
          каждого заказа в срок.
        </p>
        <a href="#" class="link-half opt__link">ПОЛУЧИТЬ ДОСТУП К ОПТОВЫМ ЦЕНАМ <picture><source srcset="img/main/arrow-rg.svg" type="image/webp"><img src="img/main/arrow-rg.svg" class="btn-arrow-cat" alt=""></picture></a>
      </div>
      <div class="opt__slider slider">
        <div class="opt__slider-wrap">
          <div class="uk-position-relative uk-visible-toggle uk-light slider__container opt__slider-container" tabindex="-1" uk-slider>

            <ul class="uk-slider-items uk-grid">
              <li class="uk-width-4-4 slider__item opt__slider-item">
                <div class="uk-panel">
                  <picture><source srcset="img/main/opt.webp" type="image/webp"><img src="img/main/opt.jpg" alt=""></picture>
                </div>
              </li>
              <li class="uk-width-4-4 slider__item opt__slider-item">
                <div class="uk-panel">
                  <picture><source srcset="img/main/f1.webp" type="image/webp"><img src="img/main/f1.jpg" alt=""></picture>
                </div>
              </li>
              <li class="uk-width-4-4 slider__item opt__slider-item">
                <div class="uk-panel">
                  <picture><source srcset="img/main/f1.webp" type="image/webp"><img src="img/main/f1.jpg" alt=""></picture>
                </div>
              </li>
              <li class="uk-width-4-4 slider__item opt__slider-item">
                <div class="uk-panel">
                  <picture><source srcset="img/main/f1.webp" type="image/webp"><img src="img/main/f1.jpg" alt=""></picture>
                </div>
              </li>
              <li class="uk-width-4-4 slider__item opt__slider-item">
                <div class="uk-panel">
                  <picture><source srcset="img/main/f1.webp" type="image/webp"><img src="img/main/f1.jpg" alt=""></picture>
                </div>
              </li>
            </ul>
            <div class="slider__wrap-arrows">
              <a class="slider__prev" href="#" uk-slider-item="previous"></a>
              <a class="slider__next" href="#" uk-slider-item="next"></a>
            </div>
          </div>
          <div class="slider__bg opt__slider-bg"></div>
        </div>
      </div>
      <h2 class="opt__title-mob title">Приглашаем к сотрудничеству оптовиков</h2>
    </div>
    <div class="top-info__heart-dots heart-dots"></div>
  </section>

  <section class="magazine">
    <div class="magazine__wrap main-content">
      <h2 class="magazine__title title">Журнал</h2>
      <div class="magazine__list" uk-scrollspy="cls: uk-animation-fade; target: div; delay: 0; repeat: false;" offset-top="-200">
        <a href="#" class="magazine__wrap-link">
          <div class="magazine__item">
            <div class="magazine__img">
              <picture><source srcset="img/main/j1.webp" type="image/webp"><img src="img/main/j1.jpg" alt=""></picture>
            </div>
            <div class="magazine__content-wrap">
              <h3 class="magazine__label">HOBBY LINE представляет обновленный логотип бренда</h3>
              <p class="magazine__content">
                Сегодня HOBBY LINE предлагает вам широчайший выбор носков, колготок, лосин и других изделий, включая варежки и перчатки. Мы выходим на новый...
              </p>
              <a href="#" class="magazine__lnk">Читать далее</a>
            </div>
          </div>
        </a>
        <a href="#" class="magazine__wrap-link">
          <div class="magazine__item">
            <div class="magazine__img">
              <picture><source srcset="img/main/j2.webp" type="image/webp"><img src="img/main/j2.jpg" alt=""></picture>
            </div>
            <div class="magazine__content-wrap">
              <h3 class="magazine__label">HOBBY LINE запускает новый сайт!</h3>
              <p class="magazine__content">
                ТМ HOBBY LINE анонсирует замечательное событие – бренд
                запускает новый сайт www . hobbyline . ru , где будет
                представлен полный ассортимент чулочно-носочных...
              </p>
              <a href="#" class="magazine__lnk">Читать далее</a>
            </div>
          </div>
        </a>
        <a href="#" class="magazine__wrap-link">
          <div class="magazine__item">
            <div class="magazine__img">
              <picture><source srcset="img/main/j3.webp" type="image/webp"><img src="img/main/j3.jpg" alt=""></picture>
            </div>
            <div class="magazine__content-wrap">
              <h3 class="magazine__label">Встречаемся 27-29 августа на Lingerie Show-Forum!</h3>
              <p class="magazine__content">
                ТМ HOBBY LINE - крупнейший поставщик носочно-чулочных
                изделий в России, с нетерпением ждет гостей на выставке
                Lingerie Show-Forum в августе 2020! Lingerie Show-Forum...
              </p>
              <a href="#" class="magazine__lnk">Читать далее</a>
            </div>
          </div>
        </a>
      </div>

      <!--Mobile news slider -->
      <div class="uk-position-relative uk-visible-toggle uk-light magazine-mob" tabindex="-1" uk-slider="autoplay: true; autoplay-interval: 3000">

        <ul class="uk-slider-items uk-child-width-1-2 uk-child-width-1-3@m uk-grid">
          <li>
            <div class="uk-panel">
              <a href="#" class="magazine-mob__wrap-link">
                <div class="magazine-mob__item">
                  <div class="magazine-mob__img">
                    <picture><source srcset="img/main/j2.webp" type="image/webp"><img src="img/main/j2.jpg" alt=""></picture>
                  </div>
                  <div class="magazine-mob__content-wrap">
                    <h3 class="magazine-mob__label">HOBBY LINE запускает новый сайт!</h3>
                    <p class="magazine-mob__content">
                      ТМ HOBBY LINE анонсирует замечательное событие – бренд
                      запускает новый сайт www . hobbyline . ru , где будет
                      представлен полный ассортимент чулочно-носочных...
                    </p>
                    <a href="#" class="magazine-mob__lnk">Читать далее</a>
                  </div>
                </div>
              </a>
            </div>
          </li>
          <li>
            <div class="uk-panel">
              <a href="#" class="magazine-mob__wrap-link">
                <div class="magazine-mob__item">
                  <div class="magazine-mob__img">
                    <picture><source srcset="img/main/j2.webp" type="image/webp"><img src="img/main/j2.jpg" alt=""></picture>
                  </div>
                  <div class="magazine-mob__content-wrap">
                    <h3 class="magazine-mob__label">HOBBY LINE запускает новый сайт!</h3>
                    <p class="magazine-mob__content">
                      ТМ HOBBY LINE анонсирует замечательное событие – бренд
                      запускает новый сайт www . hobbyline . ru , где будет
                      представлен полный ассортимент чулочно-носочных...
                    </p>
                    <a href="#" class="magazine-mob__lnk">Читать далее</a>
                  </div>
                </div>
              </a>
            </div>
          </li>
          <li>
            <div class="uk-panel">
              <a href="#" class="magazine-mob__wrap-link">
                <div class="magazine-mob__item">
                  <div class="magazine-mob__img">
                    <picture><source srcset="img/main/j2.webp" type="image/webp"><img src="img/main/j2.jpg" alt=""></picture>
                  </div>
                  <div class="magazine-mob__content-wrap">
                    <h3 class="magazine-mob__label">HOBBY LINE запускает новый сайт!</h3>
                    <p class="magazine-mob__content">
                      ТМ HOBBY LINE анонсирует замечательное событие – бренд
                      запускает новый сайт www . hobbyline . ru , где будет
                      представлен полный ассортимент чулочно-носочных...
                    </p>
                    <a href="#" class="magazine-mob__lnk">Читать далее</a>
                  </div>
                </div>
              </a>
            </div>
          </li>
          <li>
            <div class="uk-panel">
              <a href="#" class="magazine-mob__wrap-link">
                <div class="magazine-mob__item">
                  <div class="magazine-mob__img">
                    <picture><source srcset="img/main/j2.webp" type="image/webp"><img src="img/main/j2.jpg" alt=""></picture>
                  </div>
                  <div class="magazine-mob__content-wrap">
                    <h3 class="magazine-mob__label">HOBBY LINE запускает новый сайт!</h3>
                    <p class="magazine-mob__content">
                      ТМ HOBBY LINE анонсирует замечательное событие – бренд
                      запускает новый сайт www . hobbyline . ru , где будет
                      представлен полный ассортимент чулочно-носочных...
                    </p>
                    <a href="#" class="magazine-mob__lnk">Читать далее</a>
                  </div>
                </div>
              </a>
            </div>
          </li>
          <li>
            <div class="uk-panel">
              <a href="#" class="magazine-mob__wrap-link">
                <div class="magazine-mob__item">
                  <div class="magazine-mob__img">
                    <picture><source srcset="img/main/j2.webp" type="image/webp"><img src="img/main/j2.jpg" alt=""></picture>
                  </div>
                  <div class="magazine-mob__content-wrap">
                    <h3 class="magazine-mob__label">HOBBY LINE запускает новый сайт!</h3>
                    <p class="magazine-mob__content">
                      ТМ HOBBY LINE анонсирует замечательное событие – бренд
                      запускает новый сайт www . hobbyline . ru , где будет
                      представлен полный ассортимент чулочно-носочных...
                    </p>
                    <a href="#" class="magazine-mob__lnk">Читать далее</a>
                  </div>
                </div>
              </a>
            </div>
          </li>
        </ul>

        <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
        <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>

      </div>
      <!--end Mobile News slider -->

      <div class="uk-flex uk-flex-center">
        <a href="#" class="link-half magazine__half-lnk">ВСЕ СТАТЬИ И НОВОСТИ<picture><source srcset="img/main/arrow-rg.svg" type="image/webp"><img src="img/main/arrow-rg.svg" class="btn-arrow-cat" alt=""></picture></a>
      </div>
    </div>
  </section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
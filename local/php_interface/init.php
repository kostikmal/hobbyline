<?php

define("PRODUCTS_IBLOCK", 19);
define("OFFERS_IBLOCK", 20);
define("HLB_PROD_TYPE", 52);
define("HLB_PROD_GENDER", 54);
define("HLB_OFFERS_SIZE", 64);
define("HLB_OFFERS_COLOR", 65);
define("FAVORITE_PRODUCTS", 17);
define("LOOKBOOK_IBLOCK", 18);
define("WHOSALE_USER_GROUP", 9);
define("PRODUCTS_SECTION", 466);
define("NEW_YEAR_SOCKS_SECTION", 366);
define("NEW_YEAR_SOCKS_CHILDREN_SECTION", 367);
define("NEW_YEAR_SOCKS_SYMBOL_SECTION", 436);
define("SELFMADE_SECTION", 433);
define("COLLECTION_PROPERTY_ID", 186);
define("ACTION_PRICE_ID", 4);
define("SALE_PROPERTY", 'sale');
define("NEW_PRODUCT_PROPERTY", 'new');
define("BESTSELLER_PROPERTY", 'bestseller');
define("SPECIAL_CONDITIONS_PROPERTY", 192);
define("PROCESSING_PERSONAL_DATA_LINK", '/personal/personal_data/');
define("SORT_OPTIONS", ['shows'=>'Популярности', 'price'=>'Цене', 'price_asc'=>'Цене по возрастанию', 'price_desc'=>'Цене по убыванию', 'color'=>'Цвету', 'color_asc'=>'Цвету', 'color_desc'=>'Цвету']);

// require_once('include/GoogleReCaptcha.php');

AddEventHandler("main", "OnBeforeUserRegister", Array("MyClassHobbyline", "OnBeforeUserRegisterHandler"));
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", Array("MyClassHobbyline", "OnBeforeIBlockElementUpdateHandler"));
AddEventHandler("iblock", "OnBeforeIBlockElementAdd", Array("MyClassHobbyline", "OnBeforeIBlockElementAddHandler"));
AddEventHandler('catalog', 'OnSuccessCatalogImport1C', Array("MyClassHobbyline", "customCatalogImportStep"));
AddEventHandler('catalog', 'OnBeforePriceUpdate', Array("MyClassHobbyline", "OnBeforePriceUpdateHandler"));
//AddEventHandler('catalog', 'OnBeforeProductPriceDelete', Array("MyClassHobbyline", "OnBeforeProductPriceDeleteHandler"));
//AddEventHandler("iblock", "OnBeforeIBlockSectionUpdate", Array("MyClassHobbyline", "OnBeforeIBlockSectionUpdateHandler"));

class MyClassHobbyline
{
    protected static $arPropIDtoHLBID = [
        179 => 52, //TYPE
        180 => 53, // SEASON
        181 => 54, // GENDER
        178 => 55, // MATERIAL
        176 => 56, // COLOR
        177 => 57, // PICTURE
        182 => 58, // DENSITY
        183 => 59, // INNER_LAYER
        184 => 60, // OUTER_LAYER
        175 => 61, // SIZE
        185 => 62, // FEATURES
        //197 => 68, // NEW_PRODUCT
        //198 => 69, // SALE
        //199 => 70, // HIT
    ];

    protected static $arNewPropIDtoPropID = [
        // TYPE
        179 => [142,],

        // SEASON
        180 => [124,],

        // GENDER
        181 => [125,],

        // MATERIAL
        178 => [126,],

        // COLOR
        176 => [134, 137, 148, 149, 150, 151, 152, 153, 154, 155,],

        // PICTURE
        177 => [127, 138, 139, 144,],

        // DENSITY
        182 => [130,],

        // INNER_LAYER
        183 => [131,],

        // OUTER_LAYER
        184 => [132,],

        // SIZE
        175 => [141, 145, 146, 147, 156, 157,],

        // FEATURES
        185 => [140,],


        // NEW PRODUCT
        //197 => [],

        // SALE
        //198 => [],

        // HIT
        //199 => [],

    ];

    function GetDirectoryDataClass( $HlBlockId ) {
        if (empty($HlBlockId) || $HlBlockId < 1) {
            return false;
        }
        $hlblock = Bitrix\Highloadblock\HighloadBlockTable::getById($HlBlockId)->fetch();
        $entity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
        $entityDataClass = $entity->getDataClass();
        return $entityDataClass;
    }

    function getHLElement( $idHiloadBlock, $sUF_XML_1C, $sXML_CODE = 'UF_XML_1C' ) {

        $arReturn = [
            'UF_NAME' => '',
            'UF_XML_ID' => '',
            'UF_XML_1C' => '',
        ];

        $entityDataClass = self::GetDirectoryDataClass($idHiloadBlock);

        $tempObD = $entityDataClass::GetList( [ 'select' => ['ID', 'UF_NAME', 'UF_XML_ID', 'UF_XML_1C'], 'filter' => [$sXML_CODE=>$sUF_XML_1C] ] );
        if ($tempResD = $tempObD->fetch()) {
            $arReturn = [
                'UF_NAME' => $tempResD['UF_NAME'],
                'UF_XML_ID' => $tempResD['UF_XML_ID'],
                'UF_XML_1C' => $tempResD['UF_XML_1C'],
            ];
        }

        return $arReturn;
    }

    function OnBeforeUserRegisterHandler( &$arFields ) {
        if ( strlen($arFields["UF_INN"]) > 0 ) {
            $arFields["GROUP_ID"][] = WHOSALE_USER_GROUP;
        }
    }

    function OnBeforeIBlockElementUpdateHandler ( &$arFields ) {
        if ( $arFields['IBLOCK_ID'] == 19 ) {
            self::UpdateElementFields( $arFields );
        }
    }

    function OnBeforeIBlockElementAddHandler ( &$arFields ) {
        if ( $arFields['IBLOCK_ID'] == 19 ) {
            self::UpdateElementFields( $arFields );
        }
    }

    function UpdateElementFields ( &$arFields ) {

        //file_put_contents( $_SERVER['DOCUMENT_ROOT'].'/xxx_UpdateElementFields_'.$arFields['ID'].'.txt', print_r($arFields, true));
        //file_put_contents( $_SERVER['DOCUMENT_ROOT'].'/xxx_element_update.txt', PHP_EOL.'UpdateElementFields', FILE_APPEND );

        $arFields['PREVIEW_TEXT'] = trim($arFields['PREVIEW_TEXT']);

        if ( strlen( $arFields['PREVIEW_TEXT'] ) > 0 ) {
            $arFields['CODE'] = Cutil::translit( $arFields['PREVIEW_TEXT'], 'ru');
        }
        else {
            $sArtNumber = '';
            foreach ( $arFields['PROPERTY_VALUES'][113] as $arVal ) {
                $sArtNumber = $arVal['VALUE'];
            }
            $arFields['CODE'] = Cutil::translit( $sArtNumber.'-'.$arFields['NAME'], 'ru');
        }

        // коллекции "Новый год", "Собственное производство"
        $nCollectionPropValue = '';
        if ( $arFields['IBLOCK_SECTION_ID'] == SELFMADE_SECTION || in_array(SELFMADE_SECTION, $arFields['IBLOCK_SECTION']) ) {
            $nCollectionPropValue = 'selfmade';
        }
        elseif (
            $arFields['IBLOCK_SECTION_ID'] == NEW_YEAR_SOCKS_SECTION || $arFields['IBLOCK_SECTION_ID'] == NEW_YEAR_SOCKS_CHILDREN_SECTION || $arFields['IBLOCK_SECTION_ID'] == NEW_YEAR_SOCKS_SYMBOL_SECTION
            ||
            in_array(NEW_YEAR_SOCKS_SECTION, $arFields['IBLOCK_SECTION']) || in_array(NEW_YEAR_SOCKS_CHILDREN_SECTION, $arFields['IBLOCK_SECTION']) || in_array(NEW_YEAR_SOCKS_SYMBOL_SECTION, $arFields['IBLOCK_SECTION'])
        )
        {
            $nCollectionPropValue = 'new_year';
        }

        if ( $nCollectionPropValue != '' ) {
            $arFields['PROPERTY_VALUES'][COLLECTION_PROPERTY_ID]['n0']['VALUE'] = $nCollectionPropValue;
        }

        $arFields['IBLOCK_SECTION'] = [PRODUCTS_SECTION];
        $arFields['IBLOCK_SECTION_ID'] = PRODUCTS_SECTION;

        // ARTNUMBER
        unset( $arFields['PROPERTY_VALUES'][187] );
        foreach ( $arFields['PROPERTY_VALUES'][113] as $arValue ) {
            if ( strlen(trim($arValue['VALUE'])) > 0 ) {
                $arFields['PROPERTY_VALUES'][187]['n0']['VALUE'] = trim( str_replace('ВЫВОДИМ', '', $arValue['VALUE']) );
            }
        }

        // НОВИНКА - NEWPRODUCT
        /*
        if ( intval( $arFields['ID'] ) > 0 ) {
            $resItem = CIBlockElement::GetByID( $arFields['ID'] );
            if ( $arItem = $resItem->GetNext() ) {

                $origin = date_create($arItem['DATE_CREATE']);
                $target = date_create('now');
                $interval = date_diff($target, $origin);
                if ( $interval->format('%a') <= 30 ) {

                    $arExistValues = [];

                    if ( isset( $arFields['PROPERTY_VALUES'][SPECIAL_CONDITIONS_PROPERTY] ) ) {
                        foreach ( $arFields['PROPERTY_VALUES'][SPECIAL_CONDITIONS_PROPERTY] as $arVal ) {
                            $arExistValues[] = $arVal['VALUE'];
                        }
                    }
                    else {
                        $arExistValues = NULL;
                    }

                    if ( is_null($arExistValues) ) {
                        $arExistValues = [];
                        $resElem = CIBlockElement::GetList( ["ID"=>"ASC"], ['IBLOCK_ID'=>$arItem['IBLOCK_ID'], 'ID'=>$arItem['ID']], false, false, ['ID', 'IBLOCK_ID', 'PROPERTY_SPECIAL_CONDITIONS'] );
                        while ( $arElem = $resElem->GetNext() ) {
                            if ( $arElem['PROPERTY_SPECIAL_CONDITIONS_VALUE'] != '' ) {
                                $arExistValues[] = $arElem['PROPERTY_SPECIAL_CONDITIONS_VALUE'];
                            }
                        }
                        $arExistValues[] = NEW_PRODUCT_PROPERTY;
                        $arExistValues = array_unique($arExistValues);
                    }
                    else {
                        $arExistValues[] = NEW_PRODUCT_PROPERTY;
                        $arExistValues = array_unique($arExistValues);
                    }

                    if ( !empty( $arExistValues ) ) {
                        $i = 0;
                        $arFields['PROPERTY_VALUES'][SPECIAL_CONDITIONS_PROPERTY] = [];
                        foreach ( $arExistValues as $newVal ) {
                            $arFields['PROPERTY_VALUES'][SPECIAL_CONDITIONS_PROPERTY]['n'.$i]['VALUE'] = $newVal;
                            $i++;
                        }
                    }

                }
            }
        }
        */

        foreach ( self::$arNewPropIDtoPropID as $nNewPropID=>$arPropsIDs ) {
            unset( $arFields['PROPERTY_VALUES'][$nNewPropID] );
            $i = 0;
            // обновим значения свойств, если надо
            foreach ( $arPropsIDs as $nOldPropID ) {
                foreach ( $arFields['PROPERTY_VALUES'][$nOldPropID] as $arOldPropVal ) {
                    if ( strlen(trim($arOldPropVal['VALUE'])) > 0 ) {
                        // найдем Это_Значение_Свойства в Новом_Справочнике
                        $sXML1C = trim($arOldPropVal['VALUE']);
                        $sNewPropValue = self::getHLElement( self::$arPropIDtoHLBID[$nNewPropID], $sXML1C, 'UF_XML_1C' )['UF_XML_ID'];
                        if ( $sNewPropValue != '' ) {
                            $arFields['PROPERTY_VALUES'][$nNewPropID]['n'.$i]['VALUE'] = $sNewPropValue;
                            $i++;
                        }
                    }
                }
            }
        }

        // NEW_PRODUCT, SALE, HIT
        $arFields['PROPERTY_VALUES'][SPECIAL_CONDITIONS_PROPERTY] = [
            'n0'=> ['VALUE' => '', 'DESCRIPTION' => ''],
            'n1' => ['VALUE' => '', 'DESCRIPTION' => ''],
            'n2' => ['VALUE' => '', 'DESCRIPTION' => ''],
        ];
        $iSC = 0;

        //unset( $arFields['PROPERTY_VALUES'][$nNewPropID] );

        // NEW_PRODUCT
        foreach ( $arFields['PROPERTY_VALUES'][197] as $arOldPropVal ) {
            if ( $arOldPropVal['VALUE'] == 'true' ) {
                $arFields['PROPERTY_VALUES'][SPECIAL_CONDITIONS_PROPERTY]['n'.$iSC]['VALUE'] = 'new';
                $iSC++;
            }
        }

        // SALE
        foreach ( $arFields['PROPERTY_VALUES'][198] as $arOldPropVal ) {
            if ( $arOldPropVal['VALUE'] == 'true' ) {
                $arFields['PROPERTY_VALUES'][SPECIAL_CONDITIONS_PROPERTY]['n'.$iSC]['VALUE'] = 'sale';
                $iSC++;
            }
        }

        // HIT
        foreach ( $arFields['PROPERTY_VALUES'][199] as $arOldPropVal ) {
            if ( $arOldPropVal['VALUE'] == 'true' ) {
                $arFields['PROPERTY_VALUES'][SPECIAL_CONDITIONS_PROPERTY]['n'.$iSC]['VALUE'] = 'bestseller';
                $iSC++;
            }
        }

        //file_put_contents( $_SERVER['DOCUMENT_ROOT'].'/xxx_UpdateElementFields_new_'.$arFields['ID'].'.txt', print_r($arFields, true));

    }

    function customCatalogImportStep () {

        file_put_contents( $_SERVER['DOCUMENT_ROOT'].'/xxx_customCatalogImportStep_request.txt', PHP_EOL.date('Y.m.d H:i:s'), FILE_APPEND );
        file_put_contents( $_SERVER['DOCUMENT_ROOT'].'/xxx_customCatalogImportStep_request.txt', PHP_EOL.print_r($_REQUEST, true), FILE_APPEND );

        if ( /*strpos($_REQUEST['filename'], 'import') !== false*/ $_REQUEST['type'] == 'catalog' && $_REQUEST['mode'] == 'import' ) {
            $bs = new CIBlockSection;
            $arFields = Array(
                "ACTIVE" => 'Y',
                //"IBLOCK_SECTION_ID" => PRODUCTS_SECTION,
                "IBLOCK_ID" => PRODUCTS_IBLOCK,
            );
            $resUpdSec = $bs->Update(PRODUCTS_SECTION, $arFields);

            file_put_contents( $_SERVER['DOCUMENT_ROOT'].'/xxx_customCatalogImportStep_request.txt', PHP_EOL.'resUpdSec = '.print_r($resUpdSec, true), FILE_APPEND );
        }
    }

    function OnBeforePriceUpdateHandler( $PRICE_ID, &$arFields ) {

        if ( $arFields['CATALOG_GROUP_ID'] == ACTION_PRICE_ID ) {
            $arItem = CCatalogProduct::GetByIDEx( $arFields['PRODUCT_ID'] );
            $nProdID = 0;
            if ( $arItem['IBLOCK_ID'] == OFFERS_IBLOCK ) {
                // если торговое предложение
                $resElemTP = CIBlockElement::GetList( ["ID"=>"ASC"], ['IBLOCK_ID'=>$arItem['IBLOCK_ID'], 'ID'=>$arItem['ID']], false, false, ['ID', 'IBLOCK_ID', 'PROPERTY_CML2_LINK'] );
                if ( $arElemTP = $resElemTP->GetNext() ) {
                    if ( intval($arElemTP['PROPERTY_CML2_LINK_VALUE']) > 0 ) {
                        // найдем товар к этому ТП
                        $resProd = CIBlockElement::GetList( ["ID"=>"ASC"], ['IBLOCK_ID'=>PRODUCTS_IBLOCK, 'ID'=>$arElemTP['PROPERTY_CML2_LINK_VALUE']], false, false, ['ID', 'IBLOCK_ID'] );
                        if ( $arProdTemp = $resProd->GetNext() ) {
                            $nProdID = $arProdTemp['ID'];
                        }
                    }
                }
            }
            elseif ( $arItem['IBLOCK_ID'] == PRODUCTS_IBLOCK ) {
                $nProdID = $arItem['ID'];
            }

            if ( $nProdID > 0 ) {
                // если есть товар
                $arExistValues = [];
                $arFieldsSCP = [];
                $i = 0;
                $resElem = CIBlockElement::GetList( ["ID"=>"ASC"], ['IBLOCK_ID'=>PRODUCTS_IBLOCK, 'ID'=>$nProdID], false, false, ['ID', 'IBLOCK_ID', 'PROPERTY_SPECIAL_CONDITIONS'] );
                while ( $arElem = $resElem->GetNext() ) {
                    if ( $arElem['PROPERTY_SPECIAL_CONDITIONS_VALUE'] != '' ) {
                        $arExistValues[] = $arElem['PROPERTY_SPECIAL_CONDITIONS_VALUE'];
                    }
                }
                $arExistValues[] = SALE_PROPERTY;
                $arExistValues = array_unique( $arExistValues );
                foreach ( $arExistValues as $newVal ) {
                    $arFieldsSCP['n'.$i]['VALUE'] = $newVal;
                    $i++;
                }
                CIBlockElement::SetPropertyValuesEx($nProdID, PRODUCTS_IBLOCK, array('SPECIAL_CONDITIONS' => $arFieldsSCP));
            }

        }
    }

}

function getLookBookElemByID( $nElemID, $arSizes ) {
    $arReturn = [];
    $res = CIBlockElement::GetList(
        ["ID"=>"ASC"],
        ['IBLOCK_ID'=>LOOKBOOK_IBLOCK, 'ACTIVE'=>'Y', 'ID'=>$nElemID],
        false,
        false,
        ['NAME', 'PREVIEW_PICTURE', 'CODE']
    );
    if ( $arRes = $res->GetNext() ) {
        $arReturn = [
            'NAME' => $arRes['NAME'],
            'PICTURE' => CFile::ResizeImageGet( $arRes['PREVIEW_PICTURE'], ['width'=>$arSizes[0], 'height'=>$arSizes[1]], BX_RESIZE_IMAGE_EXACT)['src'],
            'URL' => $arRes['CODE']
        ];
    }
    return $arReturn;
}

function init_GetDirectoryDataClass( $HlBlockId ) {
    if (empty($HlBlockId) || $HlBlockId < 1) {
        return false;
    }
    $hlblock = Bitrix\Highloadblock\HighloadBlockTable::getById($HlBlockId)->fetch();
    $entity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
    $entityDataClass = $entity->getDataClass();
    return $entityDataClass;
}

function init_getHLElems( $idHiloadBlock, $sXML ) {
    $sReturn = '';

    $entityDataClass = init_GetDirectoryDataClass($idHiloadBlock);

    $tempObD = $entityDataClass::GetList( [ 'select' => ['ID', 'UF_NAME'], 'filter' => ['UF_XML_ID'=>$sXML] ] );
    while ($tempResD = $tempObD->fetch()) {
        $sReturn = $tempResD['UF_NAME'];
    }

    return $sReturn;
}
?>
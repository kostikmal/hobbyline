<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$arResult = [
    'SUCCESS' => false,
];

if ( Bitrix\Main\Loader::includeModule("iblock") ) {

    if ( strlen( trim($_REQUEST['review_text']) ) > 0 ) {
        global $USER;
        $sUserName = $USER->GetFullName();
        $el = new CIBlockElement;

        $PROP = [
            'RATING' => $_REQUEST['review_rating'],
            'PROFIT_YES' => 0,
            'PROFIT_NO' => 0,
            'PRODUCT_ID' => $arParams['PRODUCT_ID'],
            'USER_NAME' => $sUserName,
        ];

        $arTempFiles = [];
        $i = 0;

        foreach ( $_FILES['files']['name'] as $key=>$sFileName ) {
            if ( $_FILES['files']['error'][ $key ] == 0 ) {
                $arFile = [
                    'name' => $_FILES['files']['name'][ $key ],
                    'type' => $_FILES['files']['type'][ $key ],
                    'tmp_name' => $_FILES['files']['tmp_name'][ $key ],
                    'size' => $_FILES['files']['size'][ $key ],
                    'MODULE_ID' => 'iblock',
                ];
                $arTempFiles['n'.$i++] = ['VALUE'=>CFile::SaveFile($arFile, 'iblock')];
            }
        }

        if ( !empty($arTempFiles) ) {
            $PROP['FILES'] = $arTempFiles;
        }

        $arReviewArray = [
            "IBLOCK_ID"      => $arParams['IBLOCK_ID'],
            "PROPERTY_VALUES"=> $PROP,
            "NAME"           => $arParams['PRODUCT_ID'].' - '.$sUserName,
            "ACTIVE"         => $arParams['ADD_ACTIVE'],            // активен
            "DETAIL_TEXT"    => htmlspecialcharsbx( $_REQUEST['review_text'] ),
        ];

        $el->Add($arReviewArray);
    }

    if ( intval($_REQUEST['LIKE']) > 0 || intval($_REQUEST['DISLIKE']) > 0 ) {

        $arPropUpdate = [];

        if ( intval($_REQUEST['LIKE']) > 0 ) {
            $arPropUpdate = ['PROFIT_YES'=>intval($_REQUEST['LIKE'])];
        }
        if ( intval($_REQUEST['DISLIKE']) > 0 ) {
            $arPropUpdate = ['PROFIT_NO'=>intval($_REQUEST['DISLIKE'])];
        }

        CIBlockElement::SetPropertyValuesEx( $_REQUEST['REVIEW_ID'], $arParams['IBLOCK_ID'], $arPropUpdate );
    }

    $arResult = [
        'ITEMS' => [],
        'SUCCESS' => true,
    ];

    $res = CIBlockElement::GetList(
        ["ID"=>"ASC"],
        ['IBLOCK_ID'=>$arParams['IBLOCK_ID'], 'ACTIVE'=>'Y', 'PROPERTY_PRODUCT_ID'=>$arParams['PRODUCT_ID']],
        false,
        false,
        ['ID', 'NAME', 'PREVIEW_TEXT', 'DETAIL_TEXT', 'PROPERTY_RATING', 'PROPERTY_PROFIT_YES', 'PROPERTY_PROFIT_NO', 'PROPERTY_USER_NAME', 'PROPERTY_FILES']
    );

    while ( $arRes = $res->GetNext() ) {
        $arPictures = [];
        $arPreview = [];

        foreach ( $arRes['PROPERTY_FILES_VALUE'] as $fileID ) {
            $arPictures[] = CFile::GetPath($fileID);
            $arPreview[] = CFile::ResizeImageGet( $fileID, ['width'=>92, 'height'=>90], BX_RESIZE_IMAGE_EXACT )['src'];
        }

        $arResult['ITEMS'][] = [
            'DATE_CREATE' => $arRes['DATE_CREATE'],
            'ANSWER' => $arRes['PREVIEW_TEXT'],
            'REVIEW' => $arRes['DETAIL_TEXT'],
            'RATING' => $arRes['PROPERTY_RATING_VALUE'],
            'PROFIT_YES' => $arRes['PROPERTY_PROFIT_YES_VALUE'],
            'PROFIT_NO' => $arRes['PROPERTY_PROFIT_NO_VALUE'],
            'USER_NAME' => $arRes['PROPERTY_USER_NAME_VALUE'],
            'ID' => $arRes['ID'],
            'PICTURES' => $arPictures,
            'PREVIEW' => $arPreview,
        ];
    }

    if ( $_REQUEST['AJAX'] == 'Y' ) {
        $arResult['AJAX'] = 'Y';
    }

}

$this->IncludeComponentTemplate();
?>
/*$(document).on('click', '.hl-review .button-wrap a', function(e) {
  e.preventDefault();
  var formData = $(".review_form").serialize();
  $.ajax({
    url: $('.review_url').val(),
    data: formData,
    type: "GET",
    dataType: 'html',
    success: function(msg) {
      if (msg) {
        $('.products_reviews_block').html(msg);
      }
      $('.rating-count').text(initRating);
    }
  });
});*/

$(document).on('click', '.add_like', function (e) {
    e.preventDefault();

    $.ajax({
        url: $('.review_url').val(),
        data: {
            'REVIEW_ID': $(this).data('review'),
            //'AJAX': 'Y',
            'LIKE': $(this).data('like')
        },
        type: "GET",
        dataType: 'html',
        success: function (msg) {
            if (msg) {
                //$('.products_reviews_block').html(msg);
              document.location.reload();
            }
        }
    });

});

$(document).on('click', '.add_dislike', function (e) {
    e.preventDefault();

    $.ajax({
        url: $('.review_url').val(),
        data: {
            'REVIEW_ID': $(this).data('review'),
            //'AJAX': 'Y',
            'DISLIKE': $(this).data('dislike')
        },
        type: "GET",
        dataType: 'html',
        success: function (msg) {
            if (msg) {
                //$('.products_reviews_block').html(msg);
              document.location.reload();
            }
        }
    });

});

$(document).ready(function () {
    function handleFileSelect(evt) {
        var files = evt.target.files; // FileList object

        $('#files').clone().removeAttr('id').appendTo('.send__photo-wrap');

        // Loop through the FileList and render image files as thumbnails.
        for (var i = 0, f; f = files[i]; i++) {

            // Only process image files.
            if (!f.type.match('image.*')) {
                continue;
            }

            var reader = new FileReader();

            // Closure to capture the file information.
            reader.onload = (function (theFile) {
                return function (e) {
                    // Render thumbnail.
                    var span = document.createElement('span');
                    span.innerHTML = ['<img class="thumb" src="', e.target.result,
                        '" title="', theFile.name, '"/>'
                    ].join('');
                    document.getElementById('list').insertBefore(span, null);
                };
            })(f);

            // Read in the image file as a data URL.
            reader.readAsDataURL(f);
        }

        $('#files').val('');

    }

    document.getElementById('files').addEventListener('change', handleFileSelect, false);

});

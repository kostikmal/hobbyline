<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if ( $arResult['AJAX'] == 'Y' ) {
    $APPLICATION->RestartBuffer();
}
?>
<div class="products_reviews_block">
<h3 class="hl-card__title">Отзыв о товаре:</h3>

<form action="" class="review_form" enctype="multipart/form-data" method="post">
    <div class="hl-review">
        <textarea name="review_text" class="hl-review__textarea" placeholder="Введите ваш комментарий о товаре"></textarea>
        <?/*<input type="hidden" class="review_url" value="<?=$arParams['URL']?>">
        <input type="hidden" name="AJAX" class="" value="Y">*/?>
        <div class="hl-review__right">
            <div class="rating-wrap">
                <span class="hl-review__label">Ваша оценка:</span>
                <div class="my-rating-7"></div>
                <input type="hidden" name="review_rating" value="4" class="my-rating-7_value">
            </div>

            <div class="send__photo-wrap">
              <label for="files" class="send__photo">Прикрепить фото</label>
                <input type="file" id="files" name="files[]" class="photo_files" multiple />
                <output id="list"></output>
                <!-- <input type="file" name="review_pics[]">
                <input type="file" name="review_pics[]">
                <input type="file" name="review_pics[]"> -->
            </div>

            <div class="button-wrap">
                <?/*<a class="link-half lookbook__link hl-card__buy-btn" href="#">
                    Отправить
                    <picture>
                        <source srcset="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" type="image/webp">
                        <img src="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" class="btn-arrow-cat" alt="">
                    </picture>
                </a>*/?>
                <button class="link-half lookbook__link hl-card__buy-btn" type="submit">
                    Отправить
                    <picture>
                        <source srcset="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" type="image/webp">
                        <img src="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" class="btn-arrow-cat" alt="">
                    </picture>
                </button>
            </div>
        </div>
    </div>
</form><?

if ( count( $arResult['ITEMS'] ) > 0 ) {
    ?><h3 class="hl-card__title all-rev__title">Все отзывы о товаре:</h3>

    <div class="all-rev"><?

        foreach ( $arResult['ITEMS'] as $arItem ) {
            ?><div class="all-rev__item">
                <div class="uk-flex">
                    <div class="all-rev__name"><?=$arItem['USER_NAME']?></div>
                    <div class="all-rev__date"><?=$arItem['DATE_CREATE']?></div>
                </div>
                <div class="rating-wrap">
                    <div class="my-rating-8 all-rev__stars" data-rating="<?=$arItem['RATING']?>"></div>
                    <!--Инициализация в скрипте происходит -->
                </div>

                <div class="all-rev__content">
                    <?=$arItem['REVIEW']?>
                </div>

                <div class="all-rev__files">
                    <div class="all-rev__photo">
                        <div class="" uk-grid uk-lightbox="animation: slide"><?

                            foreach ( $arItem['PICTURES'] as $key=>$sPictPath ) {
                                ?><div class="all-rev__card">
                                    <a class="uk-inline all-rev__photo-item" href="<?=$sPictPath?>" data-caption="Caption 1">
                                        <picture>
                                            <source srcset="<?=$arItem['PREVIEW'][$key]?>" type="image/webp">
                                            <img src="<?=$arItem['PREVIEW'][$key]?>" alt="">
                                        </picture>
                                    </a>
                                </div><?
                            }

                        ?></div>
                    </div>
                    <div class="all-rev__like-wrap">
                        <div>Отзыв полезен?</div>
                        <div class="all-rev__like">
                            <picture class="add_like" data-review="<?=$arItem['ID']?>" data-like="<?=$arItem['PROFIT_YES']+1?>">
                                <source srcset="<?=SITE_TEMPLATE_PATH?>/img/main/all-rev-like.svg" type="image/webp">
                                <img class="all-rev__default" src="<?=SITE_TEMPLATE_PATH?>/img/main/all-rev-like.svg" alt="">
                            </picture>
                            <picture>
                                <source srcset="<?=SITE_TEMPLATE_PATH?>/img/main/all-rev-like-active.svg" type="image/webp">
                                <img class="all-rev__active" src="<?=SITE_TEMPLATE_PATH?>/img/main/all-rev-like-active.svg" alt="">
                            </picture>
                            <span><?=$arItem['PROFIT_YES']?></span>
                        </div>
                        <div class="all-rev__like">
                            <picture class="add_dislike" data-review="<?=$arItem['ID']?>" data-dislike="<?=$arItem['PROFIT_NO']+1?>">
                                <source srcset="<?=SITE_TEMPLATE_PATH?>/img/main/dislike.svg" type="image/webp">
                                <img class="all-rev__default" src="<?=SITE_TEMPLATE_PATH?>/img/main/dislike.svg" alt="">
                            </picture>
                            <picture>
                                <source srcset="<?=SITE_TEMPLATE_PATH?>/img/main/dislike-active.svg" type="image/webp">
                                <img class="all-rev__active" src="<?=SITE_TEMPLATE_PATH?>/img/main/dislike-active.svg" alt="">
                            </picture>
                            <span><?=$arItem['PROFIT_NO']?></span>
                        </div>
                    </div>
                </div><?
                if ( strlen( $arItem['ANSWER'] ) > 0 ) {
                    ?><div class="all-rev__answer">
                        <div class="all-rev__answer_title">Ответ компании</div>
                        <p class="all-rev__answer_p">
                            <?=$arItem['ANSWER']?>
                        </p>
                    </div><?
                }
            ?></div><?
        }

    ?></div><?
}

?></div><?
if ( $arResult['AJAX'] == 'Y' ) {
    die();
}?>

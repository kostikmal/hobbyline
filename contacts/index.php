<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Hobbyline");
?>

<div class="top-info select-top-info minify-top-info production__bottom-info">
    <div class="top-info__wrap main-content">
        <div class="top-info__content map-info">
            <div class="map-contacts">
                <h1 class="map-top-label">
                    <?
                    $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => SITE_DIR . "include/contacts_contacts_title.php",
                            "AREA_FILE_RECURSIVE" => "N",
                            "EDIT_MODE" => "html",
                        ),
                        false,
                        Array('HIDE_ICONS' => 'Y')
                    );
                    ?>
                </h1>
                <div class="map-contacts__item">
                    <div class="map-contacts__ico-wrap">
                        <picture>
                            <source srcset="<?=SITE_TEMPLATE_PATH?>/img/ico/loc.svg" type="image/webp">
                            <img class="map-contacts__img" src="<?=SITE_TEMPLATE_PATH?>/img/ico/loc.svg" alt="">
                        </picture>
                    </div>
                    <span class="map-contacts__text">
                        <?
                        $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "file",
                                "PATH" => SITE_DIR . "include/contacts_contacts_adres.php",
                                "AREA_FILE_RECURSIVE" => "N",
                                "EDIT_MODE" => "html",
                            ),
                            false,
                            Array('HIDE_ICONS' => 'Y')
                        );
                        ?>
                    </span>
                </div>
                <div class="map-contacts__item">
                    <div class="map-contacts__ico-wrap">
                        <picture>
                            <source srcset="<?=SITE_TEMPLATE_PATH?>/img/ico/phone-map.svg" type="image/webp">
                            <img class="map-contacts__img" src="<?=SITE_TEMPLATE_PATH?>/img/ico/phone-map.svg" alt="">
                        </picture>
                    </div>
                    <a href="tel:+74212751700" class="map-contacts__text">+7 4212 75 17 00
                        <?/*
                        $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "file",
                                "PATH" => SITE_DIR . "include/contacts_contacts_phone.php",
                                "AREA_FILE_RECURSIVE" => "N",
                                "EDIT_MODE" => "html",
                            ),
                            false,
                            Array('HIDE_ICONS' => 'Y')
                        );
                        */?>
                    </a>
                    <a href="tel:+74212751005" class="map-contacts__text">+7 4212 75 10 05
                        <?/*
                        $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "file",
                                "PATH" => SITE_DIR . "include/contacts_contacts_phone.php",
                                "AREA_FILE_RECURSIVE" => "N",
                                "EDIT_MODE" => "html",
                            ),
                            false,
                            Array('HIDE_ICONS' => 'Y')
                        );
                        */?>
                    </a>
                    <a href="tel:+74212751740" class="map-contacts__text">+7 4212 75 17 40
                        <?/*
                        $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "file",
                                "PATH" => SITE_DIR . "include/contacts_contacts_phone.php",
                                "AREA_FILE_RECURSIVE" => "N",
                                "EDIT_MODE" => "html",
                            ),
                            false,
                            Array('HIDE_ICONS' => 'Y')
                        );
                        */?>
                    </a>
                </div>
                <div class="map-contacts__item">
                    <div class="map-contacts__ico-wrap">
                        <picture>
                            <source srcset="<?=SITE_TEMPLATE_PATH?>/img/ico/mail-map.svg" type="image/webp">
                            <img class="map-contacts__img" src="<?=SITE_TEMPLATE_PATH?>/img/ico/mail-map.svg" alt="">
                        </picture>
                    </div>
                    <a href="mailto:star-way@mail.ru" class="map-contacts__text">
                        <?
                        $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "file",
                                "PATH" => SITE_DIR . "include/contacts_contacts_email.php",
                                "AREA_FILE_RECURSIVE" => "N",
                                "EDIT_MODE" => "html",
                            ),
                            false,
                            Array('HIDE_ICONS' => 'Y')
                        );
                        ?>
                    </a>
                </div>
                <div class="map-contacts__item">
                    <div class="map-contacts__ico-wrap">
                        <picture>
                            <source srcset="<?=SITE_TEMPLATE_PATH?>/img/ico/clock-map.svg" type="image/webp">
                            <img class="map-contacts__img" src="<?=SITE_TEMPLATE_PATH?>/img/ico/clock-map.svg" alt="">
                        </picture>
                    </div>
                    <span class="map-contacts__text">
                        <?
                        $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "file",
                                "PATH" => SITE_DIR . "include/contacts_contacts_shedule.php",
                                "AREA_FILE_RECURSIVE" => "N",
                                "EDIT_MODE" => "html",
                            ),
                            false,
                            Array('HIDE_ICONS' => 'Y')
                        );
                        ?>
                    </span>
                </div>

                <div class="map-contacts__item">
                    <a href="/about/team/" class="link-half lookbook__link cp-info__btn">
                        <?
                        $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "file",
                                "PATH" => SITE_DIR . "include/button_meet_us.php",
                                "AREA_FILE_RECURSIVE" => "N",
                                "EDIT_MODE" => "html",
                            ),
                            false,
                            Array('HIDE_ICONS' => 'Y')
                        );
                        ?>
                        <picture>
                            <source srcset="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" type="image/webp">
                            <img src="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" class="btn-arrow-cat" alt="">
                        </picture>
                    </a>
                </div>

            </div>
            <div class="top-info__slider slider map">
                <div class="map-rel">
                    <div class="map__wrap">
                        <iframe src="https://yandex.ru/map-widget/v1/-/CCUiMVhrDD" width="100%" height="100%" frameborder="1" allowfullscreen="true" style="position:relative;"></iframe>
                    </div>
                    <div class="map__wrap-bg"></div>
                </div>
            </div>
            <div class="top-info__heart-dots heart-dots production__bg-bottom"></div>
        </div>
    </div>
</div>

<?php/*
    <section class="main-content staff" uk-scrollspy="cls: uk-animation-fade; target: div; delay: 0; repeat: false" offset-top="0">
    <h3 class="title staff__title">
        <?
        $APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "",
            Array(
                "AREA_FILE_SHOW" => "file",
                "PATH" => SITE_DIR . "include/contacts_staff_title.php",
                "AREA_FILE_RECURSIVE" => "N",
                "EDIT_MODE" => "html",
            ),
            false,
            Array('HIDE_ICONS' => 'Y')
        );
        ?>
    </h3>

    <div class="staff__row">
        <?
        $arFilterContactsStaff = ['SECTION_ID' => 163];
        $APPLICATION->IncludeComponent("bitrix:news.list","contacts_team",Array(
                "DISPLAY_DATE" => "N",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "AJAX_MODE" => "N",
                "IBLOCK_TYPE" => "content",
                "IBLOCK_ID" => 9,
                "NEWS_COUNT" => 100,
                "SORT_BY1" => "SORT",
                "SORT_ORDER1" => "ASC",
                "SORT_BY2" => "ID",
                "SORT_ORDER2" => "ASC",
                "FILTER_NAME" => "arFilterContactsStaff",
                "FIELD_CODE" => Array("ID", "NAME", "PREVIEW_TEXT", "DETAIL_TEXT", "PREVIEW_PICTURE"),
                "PROPERTY_CODE" => Array('NAME', 'LAST_NAME', 'SECOND_NAME', 'EMAIL', 'POSITION'),
                "CHECK_DATES" => "N",
                "DETAIL_URL" => "",
                "PREVIEW_TRUNCATE_LEN" => "",
                "ACTIVE_DATE_FORMAT" => "",
                "SET_TITLE" => "N",
                "SET_BROWSER_TITLE" => "N",
                "SET_META_KEYWORDS" => "N",
                "SET_META_DESCRIPTION" => "N",
                "SET_LAST_MODIFIED" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "INCLUDE_SUBSECTIONS" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "86400",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => "",
                "PAGER_DESC_NUMBERING" => "Y",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "PAGER_BASE_LINK_ENABLE" => "Y",
                "SET_STATUS_404" => "N",
                "SHOW_404" => "N",
                "MESSAGE_404" => "",
                "PAGER_BASE_LINK" => "",
                "PAGER_PARAMS_NAME" => "arrPager",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_ADDITIONAL" => ""
            )
        );
        ?>

        <div class="cp-info">
            <p class="cp-info__text">
                <?
                $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "PATH" => SITE_DIR . "include/contacts_distributor_text.php",
                        "AREA_FILE_RECURSIVE" => "N",
                        "EDIT_MODE" => "html",
                    ),
                    false,
                    Array('HIDE_ICONS' => 'Y')
                );
                ?>
            </p>
            <a href="#" class="link-half lookbook__link cp-info__btn">
                <?
                $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "PATH" => SITE_DIR . "include/button_meet_us.php",
                        "AREA_FILE_RECURSIVE" => "N",
                        "EDIT_MODE" => "html",
                    ),
                    false,
                    Array('HIDE_ICONS' => 'Y')
                );
                ?>
                <picture>
                    <source srcset="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" type="image/webp">
                    <img src="<?=SITE_TEMPLATE_PATH?>/img/main/arrow-rg.svg" class="btn-arrow-cat" alt="">
                </picture>
            </a>
        </div>

    </div>
</section>
*/?>

<section class="vacancies__form form main-content">
    <div class="form__wrap cp-form__wrap">
        <div class="cp-form__label">
            <?
            $APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "AREA_FILE_SHOW" => "file",
                    "PATH" => SITE_DIR . "include/contacts_feedback_title.php",
                    "AREA_FILE_RECURSIVE" => "N",
                    "EDIT_MODE" => "html",
                ),
                false,
                Array('HIDE_ICONS' => 'Y')
            );
            ?>
        </div>

        <?
        $APPLICATION->IncludeComponent(
	"glab:feedback", 
	"contacts_feedback", 
	array(
		"USE_CAPTCHA" => "Y",
		"OK_TEXT" => "Ваше сообщение отправлено. Спасибо.",
		"EMAIL_TO" => "info@hobbyline.ru",
		"REQUIRED_FIELDS" => array(
			0 => "NAME",
			1 => "EMAIL",
		),
		"EVENT_MESSAGE_ID" => array(
		),
		"EVENT_NAME" => "FEEDBACK_FORM",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_SHADOW" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"COMPONENT_TEMPLATE" => "contacts_feedback"
	),
	false
);

        ?>

    </div>
</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
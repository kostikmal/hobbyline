<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Hobbyline");
?>
    <div class="main-content">
        <ul class="uk-breadcrumb breadcrumb">
            <li><a href="index.html">Главная</a></li>
            <li><a href="lookbook.html">LookBook</a></li>
            <li><span>Коллекция лето 2020</span></li>
        </ul>
    </div>
    <section class="lb-album main-content">
        <h1 class="title lb-album__title">LOOKBOOK / Коллекция Лето 2020</h1>

        <p class="lb-album__article">
            Как отличить оптимиста от реалиста? Доставая из стиральной машинки носок без пары, реалист думает о том, что одного не хватает, а оптимист - о том, что нашел лишний!
        </p>

        <div class="lb-album__banner">
            <div class="lb-album__banner-img">
                <picture>
                    <img src="/upload/medialibrary/cd7/cd7dbc22df5c30250f3f2bfd717ec891.jpg" alt=""></picture>
            </div>
            <div class="bg-left-bottom"></div>
        </div>

        <p class="lb-album__article uk-padding-medium-top">
            Мы не сомневаемся, что с изделиями Hobby Line у вас есть все шансы стать стопроцентными оптимистами. Ведь в нашей новой коллекции мы приготовили для вас замечательные модели с надписями, которые никого не оставят равнодушными.
        </p>

        <div class="lb-album__double">

            <div class="lb-album__double-item">
                <div class="lb-album__double-img">
                    <picture>
                        <img src="/upload/medialibrary/692/692baebb14ff95834563e6689ad847ef.JPG" alt="">
                    </picture>
                </div>
                <div class="bg-left-bottom bg-green"></div>
            </div>

            <div class="lb-album__double-item">
                <div class="lb-album__double-img">
                    <picture>
                        <img src="/upload/medialibrary/8d8/8d87953c2d985ee891389cecabac2ad6.jpg" alt="">
                    </picture>
                </div>
                <div class="bg-left-bottom bg-yellow"></div>
            </div>

        </div>

        <div class="lb-album__banner">
            <div class="lb-album__banner-img">
                <picture>
                    <img src="/upload/medialibrary/e6b/e6b2e98dab78b663310960ca745eb707.JPG" alt="">
                </picture>
            </div>
            <div class="bg-left-bottom bg-green2"></div>
        </div>

        <p class="lb-album__article uk-padding-medium-top">
            Почему именно надписи? Потому что такие неброские на первый взгляд изделия позволяют выделиться из толпы и привлечь к себе внимание. Забавная и саркастическая надпись - тенденция вне времени, которая никогда не выйдет из моды.
        </p>

        <div class="lb-album__double">
            <div class="lb-album__double-item">
                <div class="lb-album__double-img">
                    <picture>
                        <img src="/upload/medialibrary/461/461965d7be492c8f29578eaa965a219b.jpg" alt="">
                    </picture>
                </div>
                <div class="bg-left-bottom bg-blue"></div>
            </div>
            <div class="lb-album__double-item">
                <div class="lb-album__double-img">
                    <picture>
                        <img src="/upload/medialibrary/417/41795528d3ac5183c7ca3001867ca7a4.JPG" alt="">
                    </picture>
                </div>
                <div class="bg-left-bottom"></div>
            </div>
        </div>

        <p class="lb-album__article uk-padding-medium-top">
            Вдохновляйтесь нашими образами и выбирайте ту модель, которая лучше всего отразит ваше мировоззрение и ваше настроение - переходите в каталог прямо сейчас!
        </p>

        <?/*<div class="lb-album__banner">
            <div class="lb-album__banner-img">
                <picture>
                    <img src="/upload/medialibrary/25c/25cb6c620fe6d4a7bf38d1e47bca70a0.jpg" alt=""></picture>
            </div>
            <div class="bg-left-bottom bg-green2"></div>
        </div>*/?>


        <div class="lb-page__heart-top heart-dots"></div>
        <div class="lb-page__heart-bottom lb-album-heart-bottom heart-dots"></div>
        <div class="lb-page__heart-right  lb-album-heart-right heart-dots"></div>
        <div class="lb-page__heart-right lb-album-heart-bottom-right heart-dots"></div>


    </section>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Hobbyline");
?>
    <div class="main-content">
        <ul class="uk-breadcrumb breadcrumb">
            <li><a href="index.html">Главная</a></li>
            <li><a href="lookbook.html">LookBook</a></li>
            <li><span>Коллекция лето 2020</span></li>
        </ul>
    </div>
    <section class="lb-album main-content">
        <h1 class="title lb-album__title">LOOKBOOK / БЛАНК </h1>





        <p class="lb-album__article">
            Будни кажутся серыми, и хочется добавить яркий штрих?
            <br>
            Выбор броского и небанального аксессуара - отличный способ зарядиться позитивом с самого утра!
        </p>

        <div class="lb-album__banner">
            <div class="lb-album__banner-img">
                <picture>
                    <img src="/upload/medialibrary/495/495ef34416466af18b22ffadfc0ac40c.jpg" alt=""></picture>
            </div>
            <div class="bg-left-bottom"></div>
        </div>

        <p class="lb-album__article uk-padding-medium-top">
            Специально для вас мы создали коллекцию носков с принтами, в которой вы найдете все - от цветочных узоров и абстрактных фигур до разноцветных полосатых пар и изделий с портретами выдающихся мировых личностей.
        </p>

        <div class="lb-album__double">

            <div class="lb-album__double-item">
                <div class="lb-album__double-img">
                    <picture>
                        <img src="/upload/medialibrary/655/65583195c64b576103232899c1adcbf6.jpg" alt="">
                    </picture>
                </div>
                <div class="bg-left-bottom bg-green"></div>
            </div>

            <div class="lb-album__double-item">
                <div class="lb-album__double-img">
                    <picture>
                        <img src="/upload/medialibrary/a7c/a7c26a7cf611f42393f2b50799ba8a13.jpg" alt="">
                    </picture>
                </div>
                <div class="bg-left-bottom bg-yellow"></div>
            </div>

        </div>

        <div class="lb-album__banner">
            <div class="lb-album__banner-img">
                <picture>
                    <img src="/upload/medialibrary/276/276ce7ede3576e3c179b020b9db1d7d1.jpg" alt="">
                </picture>
            </div>
            <div class="bg-left-bottom bg-green2"></div>
        </div>

        <p class="lb-album__article uk-padding-medium-top">
            Эффектные, оригинальные, неисправимо позитивные - наши носки не дадут вам заскучать даже когда нет никаких поводов для радости и веселья.
        </p>

        <?/*<div class="lb-album__double">
            <div class="lb-album__double-item">
                <div class="lb-album__double-img">
                    <picture>
                        <img src="" alt="">
                    </picture>
                </div>
                <div class="bg-left-bottom bg-blue"></div>
            </div>
            <div class="lb-album__double-item">
                <div class="lb-album__double-img">
                    <picture>
                        <img src="" alt="">
                    </picture>
                </div>
                <div class="bg-left-bottom"></div>
            </div>
        </div>*/?>

        <div class="lb-album__banner">
            <div class="lb-album__banner-img">
                <picture>
                    <img src="/upload/medialibrary/873/8739c2867146abbcfbfa4dc3b45b2824.jpg" alt=""></picture>
            </div>
            <div class="bg-left-bottom bg-green2"></div>
        </div>

        <p class="lb-album__article uk-padding-medium-top">
            Просто загляните в каталог и выберите свою пару. А может быть, и не одну - у нас отличные предложения для оптовых покупателей!
        </p>

        <div class="lb-album__banner">
            <div class="lb-album__banner-img">
                <picture>
                    <img src="/upload/medialibrary/8f7/8f7437a1a97f8d25b892b75fba6834a4.jpg" alt=""></picture>
            </div>
            <div class="bg-left-bottom bg-green2"></div>
        </div>


        <div class="lb-page__heart-top heart-dots"></div>
        <div class="lb-page__heart-bottom lb-album-heart-bottom heart-dots"></div>
        <div class="lb-page__heart-right  lb-album-heart-right heart-dots"></div>
        <div class="lb-page__heart-right lb-album-heart-bottom-right heart-dots"></div>



    </section>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
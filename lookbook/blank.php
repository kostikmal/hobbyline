<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Hobbyline");
?>
    <div class="main-content">
        <ul class="uk-breadcrumb breadcrumb">
            <li><a href="index.html">Главная</a></li>
            <li><a href="lookbook.html">LookBook</a></li>
            <li><span>Коллекция лето 2020</span></li>
        </ul>
    </div>
    <section class="lb-album main-content">
        <h1 class="title lb-album__title">LOOKBOOK / БЛАНК </h1>





        <p class="lb-album__article">

        </p>

        <div class="lb-album__banner">
            <div class="lb-album__banner-img">
                <picture>
                    <img src="" alt=""></picture>
            </div>
            <div class="bg-left-bottom"></div>
        </div>

        <p class="lb-album__article uk-padding-medium-top">

        </p>

        <div class="lb-album__double">

            <div class="lb-album__double-item">
                <div class="lb-album__double-img">
                    <picture>
                        <img src="" alt="">
                    </picture>
                </div>
                <div class="bg-left-bottom bg-green"></div>
            </div>

            <div class="lb-album__double-item">
                <div class="lb-album__double-img">
                    <picture>
                        <img src="" alt="">
                    </picture>
                </div>
                <div class="bg-left-bottom bg-yellow"></div>
            </div>

        </div>

        <div class="lb-album__banner">
            <div class="lb-album__banner-img">
                <picture>
                    <img src="" alt="">
                </picture>
            </div>
            <div class="bg-left-bottom bg-green2"></div>
        </div>

        <p class="lb-album__article uk-padding-medium-top">

        </p>

        <div class="lb-album__double">
            <div class="lb-album__double-item">
                <div class="lb-album__double-img">
                    <picture>
                        <img src="" alt="">
                    </picture>
                </div>
                <div class="bg-left-bottom bg-blue"></div>
            </div>
            <div class="lb-album__double-item">
                <div class="lb-album__double-img">
                    <picture>
                        <img src="" alt="">
                    </picture>
                </div>
                <div class="bg-left-bottom"></div>
            </div>
        </div>

        <p class="lb-album__article uk-padding-medium-top">

        </p>

        <div class="lb-album__banner">
            <div class="lb-album__banner-img">
                <picture>
                    <img src="" alt=""></picture>
            </div>
            <div class="bg-left-bottom bg-green2"></div>
        </div>


        <div class="lb-page__heart-top heart-dots"></div>
        <div class="lb-page__heart-bottom lb-album-heart-bottom heart-dots"></div>
        <div class="lb-page__heart-right  lb-album-heart-right heart-dots"></div>
        <div class="lb-page__heart-right lb-album-heart-bottom-right heart-dots"></div>



    </section>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
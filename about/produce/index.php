<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("О магазине");
?>

    <div class="top-info minify-top-info" uk-scrollspy="cls: uk-animation-fade; target: div; delay: 100; repeat: false">
        <div class="top-info__wrap main-content">
            <div class="top-info__content production__content">

                <h2 class="top-title">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => SITE_DIR . "include/about_produce_production_title.php",
                            "AREA_FILE_RECURSIVE" => "N",
                            "EDIT_MODE" => "html",
                        ),
                        false,
                        Array('HIDE_ICONS' => 'Y')
                    );?>
                </h2>

                <p class="top-info__col">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => SITE_DIR . "include/about_produce_production_text.php",
                            "AREA_FILE_RECURSIVE" => "N",
                            "EDIT_MODE" => "html",
                        ),
                        false,
                        Array('HIDE_ICONS' => 'Y')
                    );?>
                </p>
            </div>

            <div class="top-info__slider slider production__slider">

                <?
                $APPLICATION->IncludeComponent("bitrix:news.detail","top-info__slider-wrap",Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "N",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "USE_SHARE" => "N",
                        "SHARE_HIDE" => "N",
                        "SHARE_TEMPLATE" => "",
                        "SHARE_HANDLERS" => array("delicious"),
                        "SHARE_SHORTEN_URL_LOGIN" => "",
                        "SHARE_SHORTEN_URL_KEY" => "",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "content",
                        "IBLOCK_ID" => 7,
                        "ELEMENT_ID" => 4304,
                        "ELEMENT_CODE" => "",
                        "CHECK_DATES" => "N",
                        "FIELD_CODE" => Array("ID", "NAME", "PREVIEW_TEXT", "DETAIL_TEXT"),
                        "PROPERTY_CODE" => Array("PICTURE", "BACKGROUND"),
                        "IBLOCK_URL" => "news.php?ID=#IBLOCK_ID#\"",
                        "DETAIL_URL" => "",
                        "SET_TITLE" => "N",
                        "SET_CANONICAL_URL" => "N",
                        "SET_BROWSER_TITLE" => "N",
                        "BROWSER_TITLE" => "-",
                        "SET_META_KEYWORDS" => "N",
                        "META_KEYWORDS" => "-",
                        "SET_META_DESCRIPTION" => "N",
                        "META_DESCRIPTION" => "-",
                        "SET_STATUS_404" => "N",
                        "SET_LAST_MODIFIED" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "ADD_ELEMENT_CHAIN" => "N",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "USE_PERMISSIONS" => "N",
                        "GROUP_PERMISSIONS" => Array(),
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "86400",
                        "CACHE_GROUPS" => "N",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "Страница",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_SHOW_ALL" => "N",
                        "PAGER_BASE_LINK_ENABLE" => "Y",
                        "SHOW_404" => "N",
                        "MESSAGE_404" => "",
                        "STRICT_SECTION_CHECK" => "N",
                        "PAGER_BASE_LINK" => "",
                        "PAGER_PARAMS_NAME" => "arrPager",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "N",
                        "AJAX_OPTION_HISTORY" => "N"
                    )
                );
                ?>

            </div>
            <h2 class="top-title-mob">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "PATH" => SITE_DIR . "include/about_produce_production_title.php",
                        "AREA_FILE_RECURSIVE" => "N",
                        "EDIT_MODE" => "html",
                    ),
                    false,
                    Array('HIDE_ICONS' => 'Y')
                );?>
            </h2>
        </div>
        <div class="top-info__heart-dots heart-dots"></div>
    </div>

    <section class="video production__video main-content" uk-scrollspy="cls: uk-animation-fade; target: div; delay: 100; repeat: false" offset-top="-200">
        <h2 class="title video__label">Ролик о производстве</h2>
        <div class="video__wrap">
			
            <img src="/upload/medialibrary/5ef/5ef29cd73cbc0fa48a1880fe5e70d5e8.jpg" style="border-radius: 28px;">
			<p style="position: absolute;bottom: 2em;left: 6em;right: 6em;text-align: center;font-size: 40px;font-weight: bold;">Ролик в производстве и скоро здесь появится</p>
            <?/*
            $APPLICATION->IncludeComponent(
                "bitrix:player",
                "about_us",
                array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "PLAYER_TYPE" => "videojs",
                    "PATH" => "/upload/medialibrary/0f6/0f6ec564507ca2989afba61da0038d69.mp4",
                    "SIZE_TYPE" => "absolute",
                    "WIDTH" => "1372",
                    "HEIGHT" => "772",
                    "AUTOSTART" => "N",
                    "AUTOSTART_ON_SCROLL" => "Y",
                    "REPEAT" => "none",
                    "VOLUME" => "90",
                    "MUTE" => "N",
                    "START_TIME" => "0",
                    "PLAYBACK_RATE" => "1",
                    "PRELOAD" => "Y",
                    "SHOW_CONTROLS" => "Y",
                    "SKIN_PATH" => "/bitrix/js/fileman/player/videojs/skins",
                    "SKIN" => "sublime.css",
                    "ADVANCED_MODE_SETTINGS" => "Y",
                    "PLAYER_ID" => "",
                    "USE_PLAYLIST" => "N",
                    "PREVIEW" => "",
                    "TYPE" => ""
                ),
                false
            );
            */?>
            <div class="video__bg"></div>
        </div>
        <div class="video__heart-dots heart-dots"></div>

    </section>

    <div class="top-info minify-top-info production__bottom-info prod-tech" uk-scrollspy="cls: uk-animation-fade; target: div; delay: 100; repeat: false">
        <div class="top-info__wrap main-content">
            <div class="top-info__content production__content">

                <h2 class="top-title our-tech-mob-label">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => SITE_DIR . "include/about_produce_technologies_title.php",
                            "AREA_FILE_RECURSIVE" => "N",
                            "EDIT_MODE" => "html",
                        ),
                        false,
                        Array('HIDE_ICONS' => 'Y')
                    );?>
                </h2>

                <p class="top-info__col">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => SITE_DIR . "include/about_produce_technologies_text.php",
                            "AREA_FILE_RECURSIVE" => "N",
                            "EDIT_MODE" => "html",
                        ),
                        false,
                        Array('HIDE_ICONS' => 'Y')
                    );?>
                </p>

            </div>

            <div class="top-info__slider slider production__slider production__bottom-wrap">

                <?
                $APPLICATION->IncludeComponent("bitrix:news.detail","top-info__slider-wrap",Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "N",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "USE_SHARE" => "N",
                        "SHARE_HIDE" => "N",
                        "SHARE_TEMPLATE" => "",
                        "SHARE_HANDLERS" => array("delicious"),
                        "SHARE_SHORTEN_URL_LOGIN" => "",
                        "SHARE_SHORTEN_URL_KEY" => "",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "content",
                        "IBLOCK_ID" => 7,
                        "ELEMENT_ID" => 4306,
                        "ELEMENT_CODE" => "",
                        "CHECK_DATES" => "N",
                        "FIELD_CODE" => Array("ID", "NAME", "PREVIEW_TEXT", "DETAIL_TEXT"),
                        "PROPERTY_CODE" => Array("PICTURE", "BACKGROUND"),
                        "IBLOCK_URL" => "news.php?ID=#IBLOCK_ID#\"",
                        "DETAIL_URL" => "",
                        "SET_TITLE" => "N",
                        "SET_CANONICAL_URL" => "N",
                        "SET_BROWSER_TITLE" => "N",
                        "BROWSER_TITLE" => "-",
                        "SET_META_KEYWORDS" => "N",
                        "META_KEYWORDS" => "-",
                        "SET_META_DESCRIPTION" => "N",
                        "META_DESCRIPTION" => "-",
                        "SET_STATUS_404" => "N",
                        "SET_LAST_MODIFIED" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "ADD_ELEMENT_CHAIN" => "N",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "USE_PERMISSIONS" => "N",
                        "GROUP_PERMISSIONS" => Array(),
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "86400",
                        "CACHE_GROUPS" => "N",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "Страница",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_SHOW_ALL" => "N",
                        "PAGER_BASE_LINK_ENABLE" => "Y",
                        "SHOW_404" => "N",
                        "MESSAGE_404" => "",
                        "STRICT_SECTION_CHECK" => "N",
                        "PAGER_BASE_LINK" => "",
                        "PAGER_PARAMS_NAME" => "arrPager",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "N",
                        "AJAX_OPTION_HISTORY" => "N"
                    )
                );
                ?>

            </div>

            <h2 class="top-title-mob our-tech-mob-label">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "PATH" => SITE_DIR . "include/about_produce_technologies_title.php",
                        "AREA_FILE_RECURSIVE" => "N",
                        "EDIT_MODE" => "html",
                    ),
                    false,
                    Array('HIDE_ICONS' => 'Y')
                );?>
            </h2>

        </div>
        <div class="top-info__heart-dots heart-dots production__bg-bottom"></div>
    </div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
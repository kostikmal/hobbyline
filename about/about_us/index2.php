<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("О магазине");
?><div class="main-content">
	<ul class="uk-breadcrumb breadcrumb">
		<li><a href="index.html">Главная</a></li>
		<li><a href="about-company.html">О компании</a></li>
		<li>О нас</li>
	</ul>
</div>
<div class="top-info minify-top-info" uk-scrollspy="cls: uk-animation-fade; target: div; delay: 100; repeat: false">
	<div class="top-info__wrap main-content">
		<div class="top-info__content production__content">
			<h2 class="top-title">О нашей компании</h2>
			<p class="top-info__col">
				Мы хотим, чтобы каждый покупатель нашел у нас то, что ему нужно - от классических носков до ярких, веселых и необычных моделей; от варианта на каждый день до носков “выходного дня”. Продукция ТМ HOBBY LINE - это широчайший выбор чулочно-носочных изделий, в которых будет тепло и уютно в любую погоду, и которые всегда доступны на наших складах в требуемых объемах. Одевайтесь стильно и современно вместе с нами - будьте в тренде и выбирайте HOBBY LINE!
			</p>
		</div>
		<div class="top-info__slider slider production__slider">
			<div class="top-info__slider-wrap">
				<div class="uk-position-relative uk-visible-toggle uk-light slider__container" tabindex="-1" uk-slider="">
					<ul class="uk-slider-items uk-grid">
						<li class="uk-width-4-4">
						<div class="uk-panel slider__item">
 <span><source srcset="<span id=" title="Код PHP (Скрытый правами доступа)" class="bxhtmled-surrogate"><?=SITE_TEMPLATE_PATH?><span class="bxhtmled-surrogate-inner"><span class="bxhtmled-right-side-item-icon"></span><span class="bxhtmled-comp-lable" unselectable="on" spellcheck="false">Код PHP *</span></span>/img/main/f1.webp" type="image/webp"&gt; <img src="<?=SITE_TEMPLATE_PATH?>/img/main/f1.webp"></span>
						</div>
 </li>
						<li class="uk-width-4-4 slider__item">
						<div class="uk-panel">
 <span><source srcset="<span id=" title="Код PHP (Скрытый правами доступа)" class="bxhtmled-surrogate"><?=SITE_TEMPLATE_PATH?><span class="bxhtmled-surrogate-inner"><span class="bxhtmled-right-side-item-icon"></span><span class="bxhtmled-comp-lable" unselectable="on" spellcheck="false">Код PHP *</span></span>/img/main/f1.webp" type="image/webp"&gt; <img src="<?=SITE_TEMPLATE_PATH?>/img/main/f1.webp"></span>
						</div>
 </li>
						<li class="uk-width-4-4 slider__item">
						<div class="uk-panel">
 <span><source srcset="<span id=" title="Код PHP (Скрытый правами доступа)" class="bxhtmled-surrogate"><?=SITE_TEMPLATE_PATH?><span class="bxhtmled-surrogate-inner"><span class="bxhtmled-right-side-item-icon"></span><span class="bxhtmled-comp-lable" unselectable="on" spellcheck="false">Код PHP *</span></span>/img/main/f1.webp" type="image/webp"&gt; <img src="<?=SITE_TEMPLATE_PATH?>/img/main/f1.webp"> </span>
						</div>
 </li>
						<li class="uk-width-4-4 slider__item">
						<div class="uk-panel">
 <span><source srcset="<span id=" title="Код PHP (Скрытый правами доступа)" class="bxhtmled-surrogate"><?=SITE_TEMPLATE_PATH?><span class="bxhtmled-surrogate-inner"><span class="bxhtmled-right-side-item-icon"></span><span class="bxhtmled-comp-lable" unselectable="on" spellcheck="false">Код PHP *</span></span>/img/main/f1.webp" type="image/webp"&gt; <img src="<?=SITE_TEMPLATE_PATH?>/img/main/f1.webp"> </span>
						</div>
 </li>
						<li class="uk-width-4-4 slider__item">
						<div class="uk-panel">
 <span><source srcset="<span id=" title="Код PHP (Скрытый правами доступа)" class="bxhtmled-surrogate"><?=SITE_TEMPLATE_PATH?><span class="bxhtmled-surrogate-inner"><span class="bxhtmled-right-side-item-icon"></span><span class="bxhtmled-comp-lable" unselectable="on" spellcheck="false">Код PHP *</span></span>/img/main/f1.webp" type="image/webp"&gt; <img src="<?=SITE_TEMPLATE_PATH?>/img/main/f1.webp"> </span>
						</div>
 </li>
					</ul>
					<div class="slider__wrap-arrows">
 <a class="slider__prev" href="#" uk-slider-item="previous"></a> <a class="slider__next" href="#" uk-slider-item="next"></a>
					</div>
				</div>
				<div class="slider__bg top-info__bg production__bg us__bg">
				</div>
			</div>
		</div>
		<h2 class="top-title-mob">О нашей компании</h2>
	</div>
	<div class="top-info__heart-dots heart-dots">
	</div>
</div>
 <section class="main-content history">
<h2 class="title history__title">Наша история</h2>
<div class="history__slider btn-slider">
	<div class="btn-slider__btn-side">
		<div class="btn-slider__btn-item btn-slider__btn-active">
			2008
		</div>
		<div class="btn-slider__btn-item">
			2010
		</div>
		<div class="btn-slider__btn-item">
			2011
		</div>
		<div class="btn-slider__btn-item">
			2013
		</div>
		<div class="btn-slider__btn-item">
			2015
		</div>
		<div class="btn-slider__btn-item">
			2016
		</div>
		<div class="btn-slider__btn-item">
			2019
		</div>
		<div class="btn-slider__btn-item">
			2020
		</div>
	</div>
	<div class="btn-slider__content-side">
		<div class="btn-slider__content-item btn-slider__content-active uk-animation-slide-bottom">
			<div class="btn-slider__info">
				<h5 class="btn-slider__label">2008 • Открытие компании</h5>
				<p class="btn-slider__article">
					 Далеко-далеко за словесными горами в стране гласных и согласных живут рыбные тексты. Вдали от всех живут они в буквенных домах на берегу Семантика большого языкового океана. Маленький ручеек Даль журчит по всей стране и обеспечивает ее всеми необходимыми правилами. Эта парадигматическая страна, в которой жаренные члены предложения залетают прямо в рот. Даже всемогущая пунктуация не имеет власти над рыбными текстами, ведущими безорфографичный образ жизни. Однажды одна маленькая строчка рыбного текста по имени Lorem ipsum решила выйти в большой мир грамматики. Великий Оксмокс предупреждал ее о злых запятых, диких знаках вопроса и коварных точках с запятой, но текст не дал сбить себя с толку. Он собрал семь своих заглавных букв, подпоясал инициал за пояс и пустился в дорогу
				</p>
			</div>
			<div class="btn-slider__img">
 <span><source srcset="<span id=" title="Код PHP (Скрытый правами доступа)" class="bxhtmled-surrogate"><?=SITE_TEMPLATE_PATH?><span class="bxhtmled-surrogate-inner"><span class="bxhtmled-right-side-item-icon"></span><span class="bxhtmled-comp-lable" unselectable="on" spellcheck="false">Код PHP *</span></span>/img/main/history.webp" type="image/webp"&gt; <img src="<?=SITE_TEMPLATE_PATH?>/img/main/history.webp"> </span>
			</div>
		</div>
		<div class="btn-slider__content-item uk-animation-slide-bottom">
			<div class="btn-slider__info">
				<h5 class="btn-slider__label">2010 • Открытие компании</h5>
				<p class="btn-slider__article">
					 Далеко-далеко за словесными горами в стране гласных и согласных живут рыбные тексты. Вдали от всех живут они в буквенных домах на берегу Семантика большого языкового океана. Маленький ручеек Даль журчит по всей стране и обеспечивает ее всеми необходимыми правилами. Эта парадигматическая страна, в которой жаренные члены предложения залетают прямо в рот. Даже всемогущая пунктуация не имеет власти над рыбными текстами, ведущими безорфографичный образ жизни. Однажды одна маленькая строчка рыбного текста по имени Lorem ipsum решила выйти в большой мир грамматики. Великий Оксмокс предупреждал ее о злых запятых, диких знаках вопроса и коварных точках с запятой, но текст не дал сбить себя с толку. Он собрал семь своих заглавных букв, подпоясал инициал за пояс и пустился в дорогу
				</p>
			</div>
			<div class="btn-slider__img">
 <span><source srcset="<span id=" title="Код PHP (Скрытый правами доступа)" class="bxhtmled-surrogate"><?=SITE_TEMPLATE_PATH?><span class="bxhtmled-surrogate-inner"><span class="bxhtmled-right-side-item-icon"></span><span class="bxhtmled-comp-lable" unselectable="on" spellcheck="false">Код PHP *</span></span>/img/main/history.webp" type="image/webp"&gt; <img src="<?=SITE_TEMPLATE_PATH?>/img/main/history.webp"> </span>
			</div>
		</div>
		<div class="btn-slider__content-item uk-animation-slide-bottom">
			<div class="btn-slider__info">
				<h5 class="btn-slider__label">2011 • Открытие компании</h5>
				<p class="btn-slider__article">
					 Далеко-далеко за словесными горами в стране гласных и согласных живут рыбные тексты. Вдали от всех живут они в буквенных домах на берегу Семантика большого языкового океана. Маленький ручеек Даль журчит по всей стране и обеспечивает ее всеми необходимыми правилами. Эта парадигматическая страна, в которой жаренные члены предложения залетают прямо в рот. Даже всемогущая пунктуация не имеет власти над рыбными текстами, ведущими безорфографичный образ жизни. Однажды одна маленькая строчка рыбного текста по имени Lorem ipsum решила выйти в большой мир грамматики. Великий Оксмокс предупреждал ее о злых запятых, диких знаках вопроса и коварных точках с запятой, но текст не дал сбить себя с толку. Он собрал семь своих заглавных букв, подпоясал инициал за пояс и пустился в дорогу
				</p>
			</div>
			<div class="btn-slider__img">
 <span><source srcset="<span id=" title="Код PHP (Скрытый правами доступа)" class="bxhtmled-surrogate"><?=SITE_TEMPLATE_PATH?><span class="bxhtmled-surrogate-inner"><span class="bxhtmled-right-side-item-icon"></span><span class="bxhtmled-comp-lable" unselectable="on" spellcheck="false">Код PHP *</span></span>/img/main/history.webp" type="image/webp"&gt; <img src="<?=SITE_TEMPLATE_PATH?>/img/main/history.webp"></span>
			</div>
		</div>
		<div class="btn-slider__content-item uk-animation-slide-bottom">
			<div class="btn-slider__info">
				<h5 class="btn-slider__label">2013 • Открытие компании</h5>
				<p class="btn-slider__article">
					 Далеко-далеко за словесными горами в стране гласных и согласных живут рыбные тексты. Вдали от всех живут они в буквенных домах на берегу Семантика большого языкового океана. Маленький ручеек Даль журчит по всей стране и обеспечивает ее всеми необходимыми правилами. Эта парадигматическая страна, в которой жаренные члены предложения залетают прямо в рот. Даже всемогущая пунктуация не имеет власти над рыбными текстами, ведущими безорфографичный образ жизни. Однажды одна маленькая строчка рыбного текста по имени Lorem ipsum решила выйти в большой мир грамматики. Великий Оксмокс предупреждал ее о злых запятых, диких знаках вопроса и коварных точках с запятой, но текст не дал сбить себя с толку. Он собрал семь своих заглавных букв, подпоясал инициал за пояс и пустился в дорогу
				</p>
			</div>
			<div class="btn-slider__img">
 <span><source srcset="<span id=" title="Код PHP (Скрытый правами доступа)" class="bxhtmled-surrogate"><?=SITE_TEMPLATE_PATH?><span class="bxhtmled-surrogate-inner"><span class="bxhtmled-right-side-item-icon"></span><span class="bxhtmled-comp-lable" unselectable="on" spellcheck="false">Код PHP *</span></span>/img/main/history.webp" type="image/webp"&gt; <img src="<?=SITE_TEMPLATE_PATH?>/img/main/history.webp"> </span>
			</div>
		</div>
		<div class="btn-slider__content-item uk-animation-slide-bottom">
			<div class="btn-slider__info">
				<h5 class="btn-slider__label">2015 • Открытие компании</h5>
				<p class="btn-slider__article">
					 Далеко-далеко за словесными горами в стране гласных и согласных живут рыбные тексты. Вдали от всех живут они в буквенных домах на берегу Семантика большого языкового океана. Маленький ручеек Даль журчит по всей стране и обеспечивает ее всеми необходимыми правилами. Эта парадигматическая страна, в которой жаренные члены предложения залетают прямо в рот. Даже всемогущая пунктуация не имеет власти над рыбными текстами, ведущими безорфографичный образ жизни. Однажды одна маленькая строчка рыбного текста по имени Lorem ipsum решила выйти в большой мир грамматики. Великий Оксмокс предупреждал ее о злых запятых, диких знаках вопроса и коварных точках с запятой, но текст не дал сбить себя с толку. Он собрал семь своих заглавных букв, подпоясал инициал за пояс и пустился в дорогу
				</p>
			</div>
			<div class="btn-slider__img">
 <span><source srcset="<span id=" title="Код PHP (Скрытый правами доступа)" class="bxhtmled-surrogate"><?=SITE_TEMPLATE_PATH?><span class="bxhtmled-surrogate-inner"><span class="bxhtmled-right-side-item-icon"></span><span class="bxhtmled-comp-lable" unselectable="on" spellcheck="false">Код PHP *</span></span>/img/main/history.webp" type="image/webp"&gt; <img src="<?=SITE_TEMPLATE_PATH?>/img/main/history.webp"> </span>
			</div>
		</div>
		<div class="btn-slider__content-item uk-animation-slide-bottom">
			<div class="btn-slider__info">
				<h5 class="btn-slider__label">2016 • Открытие компании</h5>
				<p class="btn-slider__article">
					 Далеко-далеко за словесными горами в стране гласных и согласных живут рыбные тексты. Вдали от всех живут они в буквенных домах на берегу Семантика большого языкового океана. Маленький ручеек Даль журчит по всей стране и обеспечивает ее всеми необходимыми правилами. Эта парадигматическая страна, в которой жаренные члены предложения залетают прямо в рот. Даже всемогущая пунктуация не имеет власти над рыбными текстами, ведущими безорфографичный образ жизни. Однажды одна маленькая строчка рыбного текста по имени Lorem ipsum решила выйти в большой мир грамматики. Великий Оксмокс предупреждал ее о злых запятых, диких знаках вопроса и коварных точках с запятой, но текст не дал сбить себя с толку. Он собрал семь своих заглавных букв, подпоясал инициал за пояс и пустился в дорогу
				</p>
			</div>
			<div class="btn-slider__img">
 <span><source srcset="<span id=" title="Код PHP (Скрытый правами доступа)" class="bxhtmled-surrogate"><?=SITE_TEMPLATE_PATH?><span class="bxhtmled-surrogate-inner"><span class="bxhtmled-right-side-item-icon"></span><span class="bxhtmled-comp-lable" unselectable="on" spellcheck="false">Код PHP *</span></span>/img/main/history.webp" type="image/webp"&gt; <img src="<?=SITE_TEMPLATE_PATH?>/img/main/history.webp"> </span>
			</div>
		</div>
		<div class="btn-slider__content-item uk-animation-slide-bottom">
			<div class="btn-slider__info">
				<h5 class="btn-slider__label">2019 • Открытие компании</h5>
				<p class="btn-slider__article">
					 Далеко-далеко за словесными горами в стране гласных и согласных живут рыбные тексты. Вдали от всех живут они в буквенных домах на берегу Семантика большого языкового океана. Маленький ручеек Даль журчит по всей стране и обеспечивает ее всеми необходимыми правилами. Эта парадигматическая страна, в которой жаренные члены предложения залетают прямо в рот. Даже всемогущая пунктуация не имеет власти над рыбными текстами, ведущими безорфографичный образ жизни. Однажды одна маленькая строчка рыбного текста по имени Lorem ipsum решила выйти в большой мир грамматики. Великий Оксмокс предупреждал ее о злых запятых, диких знаках вопроса и коварных точках с запятой, но текст не дал сбить себя с толку. Он собрал семь своих заглавных букв, подпоясал инициал за пояс и пустился в дорогу
				</p>
			</div>
			<div class="btn-slider__img">
 <span><source srcset="<span id=" title="Код PHP (Скрытый правами доступа)" class="bxhtmled-surrogate"><?=SITE_TEMPLATE_PATH?><span class="bxhtmled-surrogate-inner"><span class="bxhtmled-right-side-item-icon"></span><span class="bxhtmled-comp-lable" unselectable="on" spellcheck="false">Код PHP *</span></span>/img/main/history.webp" type="image/webp"&gt; <img src="<?=SITE_TEMPLATE_PATH?>/img/main/history.webp"> </span>
			</div>
		</div>
		<div class="btn-slider__content-item uk-animation-slide-bottom">
			<div class="btn-slider__info">
				<h5 class="btn-slider__label">2020 • Открытие компании</h5>
				<p class="btn-slider__article">
					 Далеко-далеко за словесными горами в стране гласных и согласных живут рыбные тексты. Вдали от всех живут они в буквенных домах на берегу Семантика большого языкового океана. Маленький ручеек Даль журчит по всей стране и обеспечивает ее всеми необходимыми правилами. Эта парадигматическая страна, в которой жаренные члены предложения залетают прямо в рот. Даже всемогущая пунктуация не имеет власти над рыбными текстами, ведущими безорфографичный образ жизни. Однажды одна маленькая строчка рыбного текста по имени Lorem ipsum решила выйти в большой мир грамматики. Великий Оксмокс предупреждал ее о злых запятых, диких знаках вопроса и коварных точках с запятой, но текст не дал сбить себя с толку. Он собрал семь своих заглавных букв, подпоясал инициал за пояс и пустился в дорогу
				</p>
			</div>
			<div class="btn-slider__img">
 <span><source srcset="<span id=" title="Код PHP (Скрытый правами доступа)" class="bxhtmled-surrogate"><?=SITE_TEMPLATE_PATH?><span class="bxhtmled-surrogate-inner"><span class="bxhtmled-right-side-item-icon"></span><span class="bxhtmled-comp-lable" unselectable="on" spellcheck="false">Код PHP *</span></span>/img/main/history.webp" type="image/webp"&gt; <img src="<?=SITE_TEMPLATE_PATH?>/img/main/history.webp"> </span>
			</div>
		</div>
		<div class="uk-flex uk-flex-between uk-margin-small-top btn-slider__btn-nav">
 <a href="#b0" class="btn-slider__mobile-btn btn-slider__btn-prev slider__prev" uk-scroll=""></a> <a href="#b0" class="btn-slider__mobile-btn btn-slider__btn-next slider__next" uk-scroll=""></a>
		</div>
	</div>
</div>
 </section>
<div class="top-info minify-top-info production__bottom-info" uk-scrollspy="cls: uk-animation-fade; target: div; delay: 100; repeat: false">
	<div class="top-info__wrap main-content">
		<div class="top-info__content production__content">
			<h2 class="top-title">Концепция работы</h2>
			<p class="top-info__col">
				Представьте себе бренд, который одинаково любим и партнерами и покупателями. И чья продукция может похвастаться ассортиментом от классики до современных трендов! Все это можно сказать о HOBBY LINE - бренде, который вот уже 20 лет успешно производит и продает носки, колготки и чулочные изделия на российском рынке! Концепция торговой марки HOBBY LINE базируется на трех мощных китах: широчайший ассортимент, доступные цены и бесперебойные поставки!&nbsp;
			</p>
			<p class="top-info__col">
				<br>
				 Благодаря продукции HOBBY LINE каждый покупатель, независимо от пола и возраста, найдет среди моделей бренда что-то, подходящее именно ему. Одних порадует комфорт, уют, тепло и гипоаллергенность нашего классического ассортимента.&nbsp;
			</p>
			<p class="top-info__col">
				&nbsp; <br>
				Другим яркие яркие решения торговой марки помогут выделиться из толпы, приобрести статус модника и даже стать инноватором среди своих родных, друзей или коллег! ТМ HOBBY LINE всегда уделяет особое внимание взаимодействию с покупателями. Поэтому нас выбирают и нам доверяют сотни партнеров по всей стране: от Калининграда до Хабаровска!
			</p>
		</div>
		<div class="top-info__slider slider production__slider production__bottom-wrap">
			<div class="top-info__slider-wrap">
				<div class="uk-position-relative uk-visible-toggle uk-light slider__container" tabindex="-1" uk-slider="">
					<ul class="uk-slider-items uk-grid">
						<li class="uk-width-4-4">
						<div class="uk-panel slider__item">
 <span><source srcset="<span id=" title="Код PHP (Скрытый правами доступа)" class="bxhtmled-surrogate"><?=SITE_TEMPLATE_PATH?><span class="bxhtmled-surrogate-inner"><span class="bxhtmled-right-side-item-icon"></span><span class="bxhtmled-comp-lable" unselectable="on" spellcheck="false">Код PHP *</span></span>/img/main/f1.webp" type="image/webp"&gt;<img src="<?=SITE_TEMPLATE_PATH?>/img/main/f1.jpg" alt=""></span>
						</div>
 </li>
						<li class="uk-width-4-4 slider__item">
						<div class="uk-panel">
 <span><source srcset="<span id=" title="Код PHP (Скрытый правами доступа)" class="bxhtmled-surrogate"><?=SITE_TEMPLATE_PATH?><span class="bxhtmled-surrogate-inner"><span class="bxhtmled-right-side-item-icon"></span><span class="bxhtmled-comp-lable" unselectable="on" spellcheck="false">Код PHP *</span></span>/img/main/f1.webp" type="image/webp"&gt;<img src="<?=SITE_TEMPLATE_PATH?>/img/main/f1.jpg" alt=""></span>
						</div>
 </li>
						<li class="uk-width-4-4 slider__item">
						<div class="uk-panel">
 <span><source srcset="<span id=" title="Код PHP (Скрытый правами доступа)" class="bxhtmled-surrogate"><?=SITE_TEMPLATE_PATH?><span class="bxhtmled-surrogate-inner"><span class="bxhtmled-right-side-item-icon"></span><span class="bxhtmled-comp-lable" unselectable="on" spellcheck="false">Код PHP *</span></span>/img/main/f1.webp" type="image/webp"&gt;<img src="<?=SITE_TEMPLATE_PATH?>/img/main/f1.jpg" alt=""></span>
						</div>
 </li>
						<li class="uk-width-4-4 slider__item">
						<div class="uk-panel">
 <span><source srcset="<span id=" title="Код PHP (Скрытый правами доступа)" class="bxhtmled-surrogate"><?=SITE_TEMPLATE_PATH?><span class="bxhtmled-surrogate-inner"><span class="bxhtmled-right-side-item-icon"></span><span class="bxhtmled-comp-lable" unselectable="on" spellcheck="false">Код PHP *</span></span>/img/main/f1.webp" type="image/webp"&gt;<img src="<?=SITE_TEMPLATE_PATH?>/img/main/f1.jpg" alt=""></span>
						</div>
 </li>
						<li class="uk-width-4-4 slider__item">
						<div class="uk-panel">
 <span><source srcset="<span id=" title="Код PHP (Скрытый правами доступа)" class="bxhtmled-surrogate"><?=SITE_TEMPLATE_PATH?><span class="bxhtmled-surrogate-inner"><span class="bxhtmled-right-side-item-icon"></span><span class="bxhtmled-comp-lable" unselectable="on" spellcheck="false">Код PHP *</span></span>/img/main/f1.webp" type="image/webp"&gt;<img src="<?=SITE_TEMPLATE_PATH?>/img/main/f1.jpg" alt=""></span>
						</div>
 </li>
					</ul>
					<div class="slider__wrap-arrows">
 <a class="slider__prev" href="#" uk-slider-item="previous"></a> <a class="slider__next" href="#" uk-slider-item="next"></a>
					</div>
				</div>
				<div class="slider__bg top-info__bg production__bg us__bg-second">
				</div>
			</div>
		</div>
		<h2 class="top-title-mob concept-work__title">Концепция работы</h2>
	</div>
	<div class="top-info__heart-dots heart-dots production__bg-bottom">
	</div>
</div>
 <section class="video main-content" uk-scrollspy="cls: uk-animation-fade; target: div; delay: 100; repeat: false" offset-top="-200">
<h2 class="title video__label">Ролик о нас</h2>
<div class="video__wrap">
 <video src="https://yootheme.com/site/images/media/yootheme-pro.mp4" class="video__video" controls="" playsinline="" uk-video="automute: true"></video>
	<div class="video__bg">
	</div>
</div>
<div class="video__heart-dots heart-dots">
</div>
 </section> <section class="gallery main-content">
<h2 class="title gallery__title">Будни нашего коллектива</h2>
<div class="slider-vertical">
	<div class="slider-vertical-wrap">
		<div class="slider-vertical__slide">
			<div class="slider-vertical__item">
				<span><source srcset="<span id=" title="Код PHP (Скрытый правами доступа)" class="bxhtmled-surrogate"><?=SITE_TEMPLATE_PATH?><span class="bxhtmled-surrogate-inner"><span class="bxhtmled-right-side-item-icon"></span><span class="bxhtmled-comp-lable" unselectable="on" spellcheck="false">Код PHP *</span></span>/img/main/gallery.webp" type="image/webp"&gt; <img src="<?=SITE_TEMPLATE_PATH?>/img/main/gallery.webp"> </span>
			</div>
		</div>
		<div class="slider-vertical__item">
			<span><source srcset="<span id=" title="Код PHP (Скрытый правами доступа)" class="bxhtmled-surrogate"><?=SITE_TEMPLATE_PATH?><span class="bxhtmled-surrogate-inner"><span class="bxhtmled-right-side-item-icon"></span><span class="bxhtmled-comp-lable" unselectable="on" spellcheck="false">Код PHP *</span></span>/img/main/gallery.webp" type="image/webp"&gt; <img src="<?=SITE_TEMPLATE_PATH?>/img/main/gallery.webp"> </span>
		</div>
		<div class="slider-vertical__item">
			<span><source srcset="<span id=" title="Код PHP (Скрытый правами доступа)" class="bxhtmled-surrogate"><?=SITE_TEMPLATE_PATH?><span class="bxhtmled-surrogate-inner"><span class="bxhtmled-right-side-item-icon"></span><span class="bxhtmled-comp-lable" unselectable="on" spellcheck="false">Код PHP *</span></span>/img/main/gallery.webp" type="image/webp"&gt; <img src="<?=SITE_TEMPLATE_PATH?>/img/main/gallery.webp"> </span>
		</div>
		<div class="slider-vertical__item">
			<span><source srcset="<span id=" title="Код PHP (Скрытый правами доступа)" class="bxhtmled-surrogate"><?=SITE_TEMPLATE_PATH?><span class="bxhtmled-surrogate-inner"><span class="bxhtmled-right-side-item-icon"></span><span class="bxhtmled-comp-lable" unselectable="on" spellcheck="false">Код PHP *</span></span>/img/main/gallery.webp" type="image/webp"&gt; <img src="<?=SITE_TEMPLATE_PATH?>/img/main/gallery.webp"> </span>
		</div>
		<div class="slider-vertical__item">
			<span><source srcset="<span id=" title="Код PHP (Скрытый правами доступа)" class="bxhtmled-surrogate"><?=SITE_TEMPLATE_PATH?><span class="bxhtmled-surrogate-inner"><span class="bxhtmled-right-side-item-icon"></span><span class="bxhtmled-comp-lable" unselectable="on" spellcheck="false">Код PHP *</span></span>/img/main/gallery.webp" type="image/webp"&gt; <img src="<?=SITE_TEMPLATE_PATH?>/img/main/gallery.webp"> </span>
		</div>
	</div>
	<div class="nav-slick">
		<div class="slider-vertical__prev">
		</div>
		<div class="slider-vertical-nav">
			<div>
				<span><source srcset="<span id=" title="Код PHP (Скрытый правами доступа)" class="bxhtmled-surrogate"><?=SITE_TEMPLATE_PATH?><span class="bxhtmled-surrogate-inner"><span class="bxhtmled-right-side-item-icon"></span><span class="bxhtmled-comp-lable" unselectable="on" spellcheck="false">Код PHP *</span></span>/img/main/gallery.webp" type="image/webp"&gt; <img src="<?=SITE_TEMPLATE_PATH?>/img/main/gallery.webp"> </span>
			</div>
			<div>
				<span><source srcset="<span id=" title="Код PHP (Скрытый правами доступа)" class="bxhtmled-surrogate"><?=SITE_TEMPLATE_PATH?><span class="bxhtmled-surrogate-inner"><span class="bxhtmled-right-side-item-icon"></span><span class="bxhtmled-comp-lable" unselectable="on" spellcheck="false">Код PHP *</span></span>/img/main/gallery.webp" type="image/webp"&gt; <img src="<?=SITE_TEMPLATE_PATH?>/img/main/gallery.webp"> </span>
			</div>
			<div>
				<span><source srcset="<span id=" title="Код PHP (Скрытый правами доступа)" class="bxhtmled-surrogate"><?=SITE_TEMPLATE_PATH?><span class="bxhtmled-surrogate-inner"><span class="bxhtmled-right-side-item-icon"></span><span class="bxhtmled-comp-lable" unselectable="on" spellcheck="false">Код PHP *</span></span>/img/main/gallery.webp" type="image/webp"&gt; <img src="<?=SITE_TEMPLATE_PATH?>/img/main/gallery.webp"> </span>
			</div>
			<div>
				<span><source srcset="<span id=" title="Код PHP (Скрытый правами доступа)" class="bxhtmled-surrogate"><?=SITE_TEMPLATE_PATH?><span class="bxhtmled-surrogate-inner"><span class="bxhtmled-right-side-item-icon"></span><span class="bxhtmled-comp-lable" unselectable="on" spellcheck="false">Код PHP *</span></span>/img/main/gallery.webp" type="image/webp"&gt; <img src="<?=SITE_TEMPLATE_PATH?>/img/main/gallery.webp"> </span>
			</div>
			<div>
				<span><source srcset="<span id=" title="Код PHP (Скрытый правами доступа)" class="bxhtmled-surrogate"><?=SITE_TEMPLATE_PATH?><span class="bxhtmled-surrogate-inner"><span class="bxhtmled-right-side-item-icon"></span><span class="bxhtmled-comp-lable" unselectable="on" spellcheck="false">Код PHP *</span></span>/img/main/gallery.webp" type="image/webp"&gt; <img src="<?=SITE_TEMPLATE_PATH?>/img/main/gallery.webp"> </span>
			</div>
		</div>
		<div class="slider-vertical__next">
		</div>
	</div>
</div>
<div class="uk-position-relative uk-visible-toggle uk-light gallery__mobile-slider" tabindex="-1" uk-slider="" autoplay="true">
	<ul class="uk-slider-items uk-child-width-1-1 uk-child-width-1-2@s uk-child-width-1-3@m uk-grid">
		<li>
		<div class="uk-panel">
 <span><source srcset="<span id=" title="Код PHP (Скрытый правами доступа)" class="bxhtmled-surrogate"><?=SITE_TEMPLATE_PATH?><span class="bxhtmled-surrogate-inner"><span class="bxhtmled-right-side-item-icon"></span><span class="bxhtmled-comp-lable" unselectable="on" spellcheck="false">Код PHP *</span></span>/img/main/gallery.webp" type="image/webp"&gt; <img src="<?=SITE_TEMPLATE_PATH?>/img/main/gallery.webp"> </span>
		</div>
 </li>
		<li>
		<div class="uk-panel">
 <span><source srcset="<span id=" title="Код PHP (Скрытый правами доступа)" class="bxhtmled-surrogate"><?=SITE_TEMPLATE_PATH?><span class="bxhtmled-surrogate-inner"><span class="bxhtmled-right-side-item-icon"></span><span class="bxhtmled-comp-lable" unselectable="on" spellcheck="false">Код PHP *</span></span>/img/main/gallery.webp" type="image/webp"&gt; <img src="<?=SITE_TEMPLATE_PATH?>/img/main/gallery.webp"> </span>
		</div>
 </li>
		<li>
		<div class="uk-panel">
 <span><source srcset="<span id=" title="Код PHP (Скрытый правами доступа)" class="bxhtmled-surrogate"><?=SITE_TEMPLATE_PATH?><span class="bxhtmled-surrogate-inner"><span class="bxhtmled-right-side-item-icon"></span><span class="bxhtmled-comp-lable" unselectable="on" spellcheck="false">Код PHP *</span></span>/img/main/gallery.webp" type="image/webp"&gt; <img src="<?=SITE_TEMPLATE_PATH?>/img/main/gallery.webp"> </span>
		</div>
 </li>
		<li>
		<div class="uk-panel">
 <span><source srcset="<span id=" title="Код PHP (Скрытый правами доступа)" class="bxhtmled-surrogate"><?=SITE_TEMPLATE_PATH?><span class="bxhtmled-surrogate-inner"><span class="bxhtmled-right-side-item-icon"></span><span class="bxhtmled-comp-lable" unselectable="on" spellcheck="false">Код PHP *</span></span>/img/main/gallery.webp" type="image/webp"&gt; <img src="<?=SITE_TEMPLATE_PATH?>/img/main/gallery.webp"> </span>
		</div>
 </li>
		<li>
		<div class="uk-panel">
 <span><source srcset="<span id=" title="Код PHP (Скрытый правами доступа)" class="bxhtmled-surrogate"><?=SITE_TEMPLATE_PATH?><span class="bxhtmled-surrogate-inner"><span class="bxhtmled-right-side-item-icon"></span><span class="bxhtmled-comp-lable" unselectable="on" spellcheck="false">Код PHP *</span></span>/img/main/gallery.webp" type="image/webp"&gt; <img src="<?=SITE_TEMPLATE_PATH?>/img/main/gallery.webp"> </span>
		</div>
 </li>
		<li>
		<div class="uk-panel">
 <span><source srcset="<span id=" title="Код PHP (Скрытый правами доступа)" class="bxhtmled-surrogate"><?=SITE_TEMPLATE_PATH?><span class="bxhtmled-surrogate-inner"><span class="bxhtmled-right-side-item-icon"></span><span class="bxhtmled-comp-lable" unselectable="on" spellcheck="false">Код PHP *</span></span>/img/main/gallery.webp" type="image/webp"&gt; <img src="<?=SITE_TEMPLATE_PATH?>/img/main/gallery.webp"> </span>
		</div>
 </li>
	</ul>
 <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous="" uk-slider-item="previous"></a> <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next="" uk-slider-item="next"></a>
</div>
 </section><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>